# Artifact Packager for ACM DL - SIGIR Artifact Evaluation Committee (AEC)

This repository contains the source code of a web application to prepare artifact packages for their submission to the ACM digital library.

This application is part of the workflow adopted by the _SIGIR Artifact Evaluation Committee (AEC)_ as part of the [ACM SIGIR Artifact Badging](https://sigir.org/general-information/acm-sigir-artifact-badging/) initiative.

Artifacts awarded with badges are uploaded in the ACM digital library in one of the following sections, according to their type:

* Datasets – https://dl.acm.org/artifacts/dataset
* Software – https://dl.acm.org/artifacts/software

[Here](https://dl.acm.org/do/10.1145/3506572) is an example of an artifact as deployed in the ACM digital library.

This software prepares the artifact package according to the format required by the ACM digital library for submission.

Developed by [Nicola Ferro](https://www.dei.unipd.it/~ferro/), [Department of Information Engineering](https://www.dei.unipd.it/en/), [University of Padua](https://www.unipd.it/en/), Italy.

Copyright and license information can be found in the file LICENSE.  Additional information can be found in the file NOTICE.

### Credits

Contributors:

* Darío Garigliotti, Aalborg University, Denmark – Spanish resource bundle.

## Installation and Setup

It requires Java 17 or greater, Servlet 5.0 or greater, JSP 3.0 or greater, and EL 4.0 or greater.

It can be run in any servlet container, e.g. [Apache Tomcat 10.0](https://tomcat.apache.org/) or greater.

To perform a minimal setup of the application, you need to edit a few configuration files in the `src/main/resources` folder:

* `userDatastore.properties`: it contains the list of users and their credentials. You need to have at least one user with role `AEC` and one user with role `ACM`.
   The user(s) with role `AEC` can create new artifacts which will be sent to the user(s) with role `ACM`.

* `mailManager.properties`: it contains the configuration parameters for sending emails, e.g. SMTP host, authentication credentials for that host, if any.

* `jwtManager.properties`: it contains the configuration for creating JSON Web Tokens. You need to set a proper AES 256 key for encrypting JWT; it must be base64-encoded.

* `artifactDatastore.properties`: it contains the configuration for storing and accessing artifacts.
   You need to define a directory where the artifacts will be actually stored, a user with role `ACM` who is the recipient of the emails about artifact creation and download, the DOI prefix to be used for the artifact DOI, and provide the full URL where the Web application is deployed.

* `log4j2.xml`: it contains the configuration for the logging infrastructure. You need to define the directory where the log files will be stored.

* `web.xml`: the application uses [Google reCAPTCHA v3](https://developers.google.com/recaptcha/docs/v3) to avoid abuse of the Web application. You need to create a reCAPTCHA v3 instance for your deployed application according to the Google procedures and configure the reCAPTCHA secret key and reCAPTCHA client key provided by Google in the corresponding context parameters of this file.