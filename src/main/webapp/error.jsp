<%@include file="/assets/include/jsp-setup.jsp" %>

<!DOCTYPE html>
<html>
<head>
	<%@include file="/assets/include/head-setup.jsp" %>

	<title><fmt:message key="labels.error.head.title" bundle="${i18n}"/></title>
	<meta name="description" content="<fmt:message key="labels.error.head.description" bundle="${i18n}"/>" >
</head>


<body>

<!-- ======= Header ======= -->
<header id="header" class="fixed-top">
	<div class="container d-flex align-items-center justify-content-between">

		<div class="logo">
			<h1 class="text-light"><a href="<c:url value="/"/>"><span><fmt:message key="labels.logo" bundle="${i18n}"/></span></a>
			</h1>
		</div>

	</div>
</header><!-- End Header -->

<main id="main">

	<!-- ======= Error Section ======= -->
	<section id="error">
		<div class="container">

			<div class="section-title" data-aos="fade-in" data-aos-delay="100">
				<h2><fmt:message key="labels.error.title" bundle="${i18n}"/></h2>
			</div>

			<div class="row text-center" data-aos="fade-up">

				<div class="col-lg-5 col-md-4"></div>

				<div class="col-lg-2 col-md-4">
						<img src="<c:url value="/assets/img/error.png"/>" class="figure-img img-fluid rounded"
							 alt="<fmt:message key="labels.error.alt" bundle="${i18n}"/>">
				</div>

				<div class="col-lg-5 col-md-4"></div>

			</div>

			<div class="row">

				<div class="col-lg-2 col-md-1"></div>

				<div class="col-lg-8 col-md-10">

					<c:choose>
						<c:when test="${not empty message}">

							<c:choose>
								<c:when test="${message.error}">

									<div class="alert alert-warning alert-dismissible fade show" role="alert">
										<p>
											<strong><fmt:message key="messages.error.error" bundle="${i18n}"/> <c:out
													value="${message.code}"/> - <c:out
													value="${message.type}"/>.</strong>
										</p>

										<button type="button" class="btn-close" data-bs-dismiss="alert"
												aria-label="Close"></button>

										<hr>

										<p><c:out value="${message.description}"/></p>
									</div>

								</c:when>

								<c:otherwise>

									<div class="alert alert-warning alert-dismissible fade show" role="alert">
										<p>
											<strong><fmt:message key="messages.error.error" bundle="${i18n}"/> C1000 -
												INTERNAL_ERROR.</strong>
										</p>

										<button type="button" class="btn-close" data-bs-dismiss="alert"
												aria-label="Close"></button>

										<hr>

										<p><fmt:message key="messages.error.unexpectedError" bundle="${i18n}"/></p>
									</div>
								</c:otherwise>
							</c:choose>

						</c:when>

						<c:otherwise>
							<div class="alert alert-warning alert-dismissible fade show" role="alert">
								<p>
									<strong><fmt:message key="messages.error.error" bundle="${i18n}"/> C1000 -
										INTERNAL_ERROR.</strong>
								</p>

								<button type="button" class="btn-close" data-bs-dismiss="alert"
										aria-label="Close"></button>

								<hr>

								<p><fmt:message key="messages.error.unexpectedError" bundle="${i18n}"/></p>
							</div>
						</c:otherwise>
					</c:choose>

				</div>

			</div>

			<div class="col-lg-2 col-md-1"></div>

		</div>

		<div class="row text-center">
			<div class="col-lg-2 col-md-1"></div>

			<div class="col-lg-8 col-md-10">
				<a href="<c:url value="/"/>" class="btn btn-info btn-lg" role="button"><fmt:message key="labels.error.backhome" bundle="${i18n}"/></a>
			</div>

			<div class="col-lg-2 col-md-1"></div>

		</div>
	</section><!-- End Error Section -->

</main><!-- End #main -->


<%@include file="/assets/include/footer.jsp" %>

<%@include file="/assets/include/foot-setup.jsp" %>

</body>
</html>