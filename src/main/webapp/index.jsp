<%@include file="/assets/include/jsp-setup.jsp" %>

<!DOCTYPE html>
<html>

<head>
	<%@include file="/assets/include/head-setup.jsp" %>

	<title><fmt:message key="labels.index.head.title" bundle="${i18n}"/></title>
	<meta name="description" content="<fmt:message key="labels.index.head.description" bundle="${i18n}"/>" >
</head>

<body>

<!-- ======= Header ======= -->
<header id="header" class="fixed-top header-transparent">
	<div class="container d-flex align-items-center justify-content-between">

		<div class="logo">
			<h1 class="text-light"><a href="<c:url value="/"/>"><span><fmt:message key="labels.logo" bundle="${i18n}"/></span></a>
			</h1>
		</div>

		<nav id="navbar" class="navbar">
			<ul>
				<li><a class="nav-link scrollto" href="#hero"><fmt:message key="labels.menu.home"
																		   bundle="${i18n}"/></a></li>
				<li><a class="nav-link scrollto" href="#about"><fmt:message key="labels.menu.about"
																			bundle="${i18n}"/></a></li>
				<li><a class="nav-link scrollto" href="<c:url value="/login"/>"><fmt:message
						key="labels.menu.signin" bundle="${i18n}"/></a></li>
			</ul>
			<i class="bi bi-list mobile-nav-toggle"></i>
		</nav><!-- .navbar -->

	</div>
</header><!-- End Header -->

<!-- ======= Hero Section ======= -->
<section id="hero">
	<div class="hero-container" data-aos="fade-up">
		<h1><fmt:message key="labels.index.welcome" bundle="${i18n}"/></h1>
		<h2><fmt:message key="labels.index.vision" bundle="${i18n}"/></h2>

		<!-- START Message -->
		<%@include file="/assets/include/display-message.jsp" %>
		<!-- END Message -->

		<a href="#about" class="btn-get-started scrollto"><i class="bx bx-chevrons-down"></i></a>
	</div>
</section><!-- End Hero -->

<main id="main">

	<!-- ======= About Section ======= -->
	<section id="about" class="about">
		<div class="container">

			<div class="row no-gutters">
				<div class="content col-xl-5 d-flex align-items-stretch" data-aos="fade-up">
					<div class="content">
						<h3><fmt:message key="labels.index.about.overall.title" bundle="${i18n}"/></h3>
						<fmt:message key="labels.index.about.overall.content" bundle="${i18n}"/>
					</div>
				</div>
				<div class="col-xl-7 d-flex align-items-stretch">
					<div class="icon-boxes d-flex flex-column justify-content-center">
						<div class="row">
							<div class="col-md-6 icon-box" data-aos="fade-up" data-aos-delay="100">
								<i class="bx bx-target-lock"></i>
								<h4><fmt:message key="labels.index.about.box1.title" bundle="${i18n}"/></h4>
								<fmt:message key="labels.index.about.box1.content" bundle="${i18n}"/>
							</div>
							<div class="col-md-6 icon-box" data-aos="fade-up" data-aos-delay="200">
								<i class="bx bx-badge-check"></i>
								<h4><fmt:message key="labels.index.about.box2.title" bundle="${i18n}"/></h4>
								<fmt:message key="labels.index.about.box2.content" bundle="${i18n}"/>
							</div>
							<div class="col-md-6 icon-box" data-aos="fade-up" data-aos-delay="300">
								<i class="bx bx-award"></i>
								<h4><fmt:message key="labels.index.about.box3.title" bundle="${i18n}"/></h4>
								<fmt:message key="labels.index.about.box3.content" bundle="${i18n}"/>
							</div>
							<div class="col-md-6 icon-box" data-aos="fade-up" data-aos-delay="400">
								<i class="bx bx-info"></i>
								<h4><fmt:message key="labels.index.about.box4.title" bundle="${i18n}"/></h4>
								<fmt:message key="labels.index.about.box4.content" bundle="${i18n}"/>
							</div>
						</div>
					</div><!-- End .content-->
				</div>
			</div>

		</div>
	</section><!-- End About Section -->

</main><!-- End #main -->

<%@include file="/assets/include/footer.jsp" %>

<%@include file="/assets/include/foot-setup.jsp" %>

</body>

</html>