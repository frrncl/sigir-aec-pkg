<%@include file="/assets/include/jsp-setup.jsp" %>

<!DOCTYPE html>
<html>
<head>
	<%@include file="/assets/include/head-setup.jsp" %>

	<title><fmt:message key="labels.aec.createArtifact.head.title" bundle="${i18n}"/></title>
	<meta name="description" content="<fmt:message key="labels.aec.createArtifact.head.description" bundle="${i18n}"/>" >
</head>

<body>

<!-- ======= Header ======= -->
<header id="header" class="fixed-top">
	<div class="container d-flex align-items-center justify-content-between">

		<div class="logo">
			<h1 class="text-light"><a href="<c:url value="/"/>"><span><fmt:message key="labels.logo"
																				   bundle="${i18n}"/></span></a>
			</h1>
		</div>

		<nav id="navbar" class="navbar">
			<ul>
				<li><a class="nav-link" href="<c:url value="/#hero"/>"><fmt:message key="labels.menu.home"
																					bundle="${i18n}"/></a></li>
				<li><a class="nav-link" href="<c:url value="/#about"/>"><fmt:message key="labels.menu.about"
																					 bundle="${i18n}"/></a></li>
				<li class="dropdown"><a href="#"><span><fmt:message key="labels.menu.aec.home" bundle="${i18n}"/></span>
					<i class="bi bi-chevron-down"></i></a>
					<ul>
						<li><a href="#"><fmt:message key="labels.menu.aec.create" bundle="${i18n}"/></a></li>
					</ul>
				</li>
				<li><a class="nav-link" href="<c:url value="/logout"/>"><fmt:message key="labels.menu.signout"
																					 bundle="${i18n}"/></a></li>
			</ul>
			<i class="bi bi-list mobile-nav-toggle"></i>
		</nav><!-- .navbar -->

	</div>
</header><!-- End Header -->

<main id="main">

	<!-- ======= AEC Packager Section ======= -->
	<section id="aecpkg">
		<div class="container" data-aos="fade-up">

			<div class="section-title">
				<h2><fmt:message key="labels.aec.createArtifact.title" bundle="${i18n}"/></h2>
			</div>

			<!-- START Message -->
			<%@include file="/assets/include/display-message.jsp" %>
			<!-- END Message -->

			<form id="form-create-artifact" action="<c:url value="/protected/aec/create-artifact"/>"
				  method="post" enctype="multipart/form-data" role="form" novalidate>

				<input type="hidden" id="g-recaptcha-response" name="g-recaptcha-response">

				<!-- START Base coordinates -->
				<div class="card">
					<div class="card-header">
						<fmt:message key="labels.aec.createArtifact.base.title" bundle="${i18n}"/>
					</div>
					<div class="card-body">

						<div class="row">

							<div class="col-lg-4">
								<div class="form-group">
									<label for="orid"><fmt:message
											key="labels.aec.createArtifact.base.orid.label"
											bundle="${i18n}"/></label>
									<div class="controls">
										<input id="orid" type="text" name="orid"
											   placeholder="<fmt:message key="labels.aec.createArtifact.base.orid.placeholder" bundle="${i18n}" />"
											   data-rule-required="true"
											   data-msg-required="<fmt:message key="labels.aec.createArtifact.base.orid.required" bundle="${i18n}" />"
											   data-rule-digits="true"
											   data-msg-digits="<fmt:message key="labels.aec.createArtifact.base.orid.valid" bundle="${i18n}" />"
											   data-rule-min="1"
											   data-msg-min="<fmt:message key="labels.aec.createArtifact.base.orid.valid" bundle="${i18n}" />"
											   class="form-control">
									</div>
								</div>
							</div>

							<div class="col-lg-4">
								<div class="form-group">
									<label for="acmid"><fmt:message
											key="labels.aec.createArtifact.base.acmid.label"
											bundle="${i18n}"/></label>
									<div class="controls">
										<input id="acmid" type="text" name="acmid"
											   placeholder="<fmt:message key="labels.aec.createArtifact.base.acmid.placeholder" bundle="${i18n}" />"
											   data-rule-required="true"
											   data-msg-required="<fmt:message key="labels.aec.createArtifact.base.acmid.required" bundle="${i18n}" />"
											   data-rule-digits="true"
											   data-msg-digits="<fmt:message key="labels.aec.createArtifact.base.acmid.valid" bundle="${i18n}" />"
											   data-rule-min="1"
											   data-msg-min="<fmt:message key="labels.aec.createArtifact.base.acmid.valid" bundle="${i18n}" />"
											   class="form-control">
									</div>
								</div>
							</div>

							<div class="col-lg-4">
								<div class="form-group">
									<label for="relatedPaperDoi"><fmt:message
											key="labels.aec.createArtifact.base.relatedPaperDoi.label"
											bundle="${i18n}"/></label>
									<div class="controls">
										<input id="relatedPaperDoi" type="text" name="relatedPaperDoi"
											   placeholder="<fmt:message key="labels.aec.createArtifact.base.relatedPaperDoi.placeholder" bundle="${i18n}" />"
											   data-rule-required="true"
											   data-msg-required="<fmt:message key="labels.aec.createArtifact.base.relatedPaperDoi.required" bundle="${i18n}" />"
											   class="form-control">
									</div>
								</div>
							</div>

						</div>

						<div class="row mt-3">

							<div class="col-lg-12">
								<div class="form-group">
									<label for="title"><fmt:message
											key="labels.aec.createArtifact.base.title.label"
											bundle="${i18n}"/></label>
									<div class="controls">
										<input id="title" type="text" name="title"
											   placeholder="<fmt:message key="labels.aec.createArtifact.base.title.placeholder" bundle="${i18n}" />"
											   data-rule-required="true"
											   data-msg-required="<fmt:message key="labels.aec.createArtifact.base.title.required" bundle="${i18n}" />"
											   class="form-control">
									</div>
								</div>
							</div>

						</div>

					</div>
				</div>
				<!-- END Base coordinates -->

				<!-- START authors -->
				<div class="card mt-5">
					<div class="card-header">
						<fmt:message key="labels.aec.createArtifact.author.title" bundle="${i18n}"/>
					</div>
					<div class="card-body">

						<div id="authorBody">
							<div id="authorBlock">

								<div class="row">

									<div class="col-lg-4">
										<div class="form-group">
											<label for="firstName"><fmt:message
													key="labels.aec.createArtifact.author.firstName.label"
													bundle="${i18n}"/></label>
											<div class="controls">
												<input id="firstName" type="text" name="firstName"
													   placeholder="<fmt:message key="labels.aec.createArtifact.author.firstName.placeholder" bundle="${i18n}" />"
													   data-rule-required="true"
													   data-msg-required="<fmt:message key="labels.aec.createArtifact.author.firstName.required" bundle="${i18n}" />"
													   class="form-control">
											</div>
										</div>
									</div>

									<div class="col-lg-4">
										<div class="form-group">
											<label for="lastName"><fmt:message
													key="labels.aec.createArtifact.author.lastName.label"
													bundle="${i18n}"/></label>
											<div class="controls">
												<input id="lastName" type="text" name="lastName"
													   placeholder="<fmt:message key="labels.aec.createArtifact.author.lastName.placeholder" bundle="${i18n}" />"
													   data-rule-required="true"
													   data-msg-required="<fmt:message key="labels.aec.createArtifact.author.lastName.required" bundle="${i18n}" />"
													   class="form-control">
											</div>
										</div>
									</div>

									<div class="col-lg-4">
										<div class="form-group">
											<label for="displayName"><fmt:message
													key="labels.aec.createArtifact.author.displayName.label"
													bundle="${i18n}"/></label>
											<div class="controls">
												<input id="displayName" type="text"
													   name="displayName"
													   placeholder="<fmt:message key="labels.aec.createArtifact.author.displayName.placeholder" bundle="${i18n}" />"
													   data-rule-required="true"
													   data-msg-required="<fmt:message key="labels.aec.createArtifact.author.displayName.required" bundle="${i18n}" />"
													   class="form-control">
											</div>
										</div>
									</div>

								</div>

								<div class="row mt-3 mb-5">

									<div class="col-lg-12">
										<div class="form-group">
											<label for="affiliation"><fmt:message
													key="labels.aec.createArtifact.author.affiliation.label"
													bundle="${i18n}"/></label>
											<div class="controls">
												<input id="affiliation" type="text"
													   name="affiliation"
													   placeholder="<fmt:message key="labels.aec.createArtifact.author.affiliation.placeholder" bundle="${i18n}" />"
													   data-rule-required="true"
													   data-msg-required="<fmt:message key="labels.aec.createArtifact.author.affiliation.required" bundle="${i18n}" />"
													   class="form-control">
											</div>
										</div>
									</div>

								</div>

							</div>
						</div>
						<div class="row">
							<div class="col-lg-2">
								<a id="add-author" class="btn btn-outline-info" role="button">
									<fmt:message key="labels.aec.createArtifact.author.addAuthor" bundle="${i18n}"/></a>
							</div>
						</div>
					</div>
				</div>
				<!-- END authors -->

				<!-- START Badges -->
				<div class="card mt-5">
					<div class="card-header">
						<fmt:message key="labels.aec.createArtifact.badges.title" bundle="${i18n}"/>
					</div>
					<div class="card-body">

						<div class="row">

							<div class="col-lg-6" id="type-group">
								<div class="form-group">
									<label for="type"><fmt:message key="labels.aec.createArtifact.badges.type.label"
																   bundle="${i18n}"/></label>
									<div class="input-group">
										<div class="input-group-text">
											<input class="form-check-input mt-0" id="type" name="type" type="radio"
												   value="dataset"
												   data-rule-required="true"
												   data-msg-required="<fmt:message key="labels.aec.createArtifact.badges.type.required" bundle="${i18n}" />">
										</div>
										<div class="form-control">
											<fmt:message key="labels.aec.createArtifact.badges.type.dataset"
														 bundle="${i18n}"/>
										</div>
									</div>

									<div class="input-group">
										<div class="input-group-text">
											<input class="form-check-input mt-0" name="type" type="radio"
												   value="software">
										</div>
										<div class="form-control">
											<fmt:message key="labels.aec.createArtifact.badges.type.software"
														 bundle="${i18n}"/>
										</div>
									</div>
								</div>
							</div>

							<div class="col-lg-6" id="awarded-group">
								<label for="awarded"><fmt:message key="labels.aec.createArtifact.badges.awarded.label"
																  bundle="${i18n}"/></label>
								<div class="input-group">
									<div class="input-group-text">
										<input class="form-check-input mt-0" id="awarded" name="awarded" type="checkbox"
											   value="functional"
											   data-rule-required="true"
											   data-msg-required="<fmt:message key="labels.aec.createArtifact.badges.awarded.required" bundle="${i18n}" />">
									</div>
									<div class="form-control">
										<fmt:message key="labels.aec.createArtifact.badges.awarded.functional"
													 bundle="${i18n}"/>
									</div>
								</div>

								<div class="input-group">
									<div class="input-group-text">
										<input class="form-check-input mt-0" name="awarded" type="checkbox"
											   value="reusable">
									</div>
									<div class="form-control">
										<fmt:message key="labels.aec.createArtifact.badges.awarded.reusable"
													 bundle="${i18n}"/>
									</div>
								</div>

								<div class="input-group">
									<div class="input-group-text">
										<input class="form-check-input mt-0" name="awarded" type="checkbox"
											   value="available">
									</div>
									<div class="form-control">
										<fmt:message key="labels.aec.createArtifact.badges.awarded.available"
													 bundle="${i18n}"/>
									</div>
								</div>

								<div class="input-group">
									<div class="input-group-text">
										<input class="form-check-input mt-0" name="awarded" type="checkbox"
											   value="reproduced">
									</div>
									<div class="form-control">
										<fmt:message key="labels.aec.createArtifact.badges.awarded.reproduced"
													 bundle="${i18n}"/>
									</div>
								</div>

								<div class="input-group">
									<div class="input-group-text">
										<input class="form-check-input mt-0" name="awarded" type="checkbox"
											   value="replicated">
									</div>
									<div class="form-control">
										<fmt:message key="labels.aec.createArtifact.badges.awarded.replicated"
													 bundle="${i18n}"/>
									</div>
								</div>
							</div>

						</div>

					</div>
				</div>
				<!-- END Badges -->

				<!-- START Description -->
				<div class="card mt-5">
					<div class="card-header">
						<fmt:message key="labels.aec.createArtifact.description.title" bundle="${i18n}"/>
					</div>
					<div class="card-body">

						<div class="row">

							<div class="col-lg-12">
								<div class="form-group">
									<label for="abstract"><fmt:message
											key="labels.aec.createArtifact.description.abstract.label"
											bundle="${i18n}"/></label>
									<div class="controls">
										<textarea id="abstract" name="abstract"
												  placeholder="<fmt:message key="labels.aec.createArtifact.description.abstract.placeholder" bundle="${i18n}" />"
												  class="form-control"
												  data-rule-required="true"
												  data-msg-required="<fmt:message key="labels.aec.createArtifact.description.abstract.required" bundle="${i18n}" />"></textarea>
									</div>
								</div>
							</div>

						</div>

						<div class="row mt-3">

							<div class="col-lg-5">
								<div class="form-group">
									<label for="orurl"><fmt:message
											key="labels.aec.createArtifact.description.orurl.label"
											bundle="${i18n}"/></label>
									<div class="controls">
										<input id="orurl" type="url" name="orurl"
											   placeholder="<fmt:message key="labels.aec.createArtifact.description.orurl.placeholder" bundle="${i18n}" />"
											   data-rule-required="true"
											   data-msg-required="<fmt:message key="labels.aec.createArtifact.description.orurl.required" bundle="${i18n}" />"
											   data-rule-url="true"
											   data-msg-url="<fmt:message key="labels.aec.createArtifact.description.orurl.valid" bundle="${i18n}" />"
											   class="form-control">
									</div>
								</div>
							</div>

							<div class="col-lg-7">
								<div class="form-group">
									<label for="repository"><fmt:message
											key="labels.aec.createArtifact.description.repository.label"
											bundle="${i18n}"/></label>
									<div class="controls">
										<textarea id="repository" name="repository"
												  placeholder="<fmt:message key="labels.aec.createArtifact.description.repository.placeholder" bundle="${i18n}" />"
												  class="form-control"></textarea>
									</div>
								</div>
							</div>

						</div>

						<div id="keywordBody">
							<div id="keywordBlock">

								<div class="row mt-3">

									<div class="col-lg-4">
										<div class="form-group">
											<label for="keyword"><fmt:message
													key="labels.aec.createArtifact.description.keyword.label"
													bundle="${i18n}"/></label>
											<div class="controls">
												<input id="keyword" type="text" name="keyword"
													   placeholder="<fmt:message key="labels.aec.createArtifact.description.keyword.placeholder" bundle="${i18n}" />"
													   data-rule-required="true"
													   data-msg-required="<fmt:message key="labels.aec.createArtifact.description.keyword.required" bundle="${i18n}" />"
													   class="form-control">
											</div>
										</div>
									</div>

								</div>
							</div>
						</div>

						<div class="row mt-5">
							<div class="col-lg-2">
								<a id="add-keyword" class="btn btn-outline-info" role="button">
									<fmt:message key="labels.aec.createArtifact.description.addKeyword"
												 bundle="${i18n}"/></a>
							</div>
						</div>

					</div>
				</div>


				<!-- END Description -->

				<!-- START Artifact -->
				<div class="card mt-5">
					<div class="card-header">
						<fmt:message key="labels.aec.createArtifact.artifact.title" bundle="${i18n}"/>
					</div>
					<div class="card-body">

						<div class="row">

							<div class="col-lg-12">
								<div class="form-group">
									<label for="file"><fmt:message
											key="labels.aec.createArtifact.artifact.file.label"
											bundle="${i18n}"/></label>
									<div class="controls">
										<input id="file" name="artifactfile" type="file"
											   accept="application/zip,.zip"
											   placeholder="<fmt:message key="labels.aec.createArtifact.artifact.file.placeholder" bundle="${i18n}" />"
											   data-rule-required="true"
											   data-msg-required="<fmt:message key="labels.aec.createArtifact.artifact.file.required" bundle="${i18n}" />"
											   class="form-control">
									</div>
								</div>
							</div>

						</div>

					</div>
				</div>
				<!-- END Artifact -->

				<!-- START submit button -->
				<div class="row text-center mt-5">

					<div class="col-lg-2"></div>

					<div class="col-lg-8">
						<button class="btn btn-info btn-lg" type="submit"><fmt:message
								key="labels.aec.createArtifact.create.label" bundle="${i18n}"/></button>
					</div>

					<div class="col-lg-2"></div>
				</div>
				<!-- END submit button -->

			</form>

		</div>
	</section><!-- End AEC Packager Section -->

</main><!-- End #main -->

<%@include file="/assets/include/footer.jsp" %>

<%@include file="/assets/include/foot-setup.jsp" %>

<%@include file="/assets/include/recaptcha-setup.jsp" %>

<script src="<c:url value="/protected/assets/js/aec-create-artifact.js"/>"></script>

</body>
</html>