/*
 * Copyright (c) 2021-2022 University of Padua, Italy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *
 * @author Nicola Ferro
 * @version 1.00
 * @since 1.00
 */


// setup validation when document is ready
$(document).ready(function () {

    $('#form-create-artifact').validate(
        {
            errorElement: "em",
            errorClass: "invalid-feedback",
            validClass: "valid-feedback",
            errorPlacement: function ( error, element ) {

                if ( element.prop( "type" ) === "checkbox" ) {
                    error.insertAfter( $('#awarded-group') );
                } else  if ( element.prop( "type" ) === "radio" ) {
                    error.insertAfter( $('#type-group') );
                } else {
                    error.insertAfter( element );
                }
            },
            highlight: function ( element, errorClass, validClass ) {
                $( element ).parents( ".controls" ).addClass( "is-invalid" ).removeClass( "is-valid" );
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".controls" ).addClass( "is-valid" ).removeClass( "is-invalid" );
            }
        }
    );

    // store a template for the author and keyword blocks to be cloned, if needed
    const authorTemplate = $('#authorBlock').clone();
    const keywordTemplate = $('#keywordBlock').clone();

    // where to append the cloned author or keyword blocks
    const authorBody = $('#authorBody');
    const keywordBody = $('#keywordBody');

    // Add an event to the "Add Author" button
    $('#add-author').click(function() {

        authorTemplate.clone().appendTo(authorBody);

    });

    // Add an event to the "Add Keyword" button
    $('#add-keyword').click(function() {

        keywordTemplate.clone().appendTo(keywordBody);

    });

});


// setup recaptcha for the login form
grecaptcha.ready(function () {
    // do request for recaptcha token
    grecaptcha.execute(aecpkg.recaptcha, {action: 'CREATE_ARTIFACT'})
        .then(function (token) {
            // add token value to form
            document.getElementById('g-recaptcha-response').value = token;
        });
});
