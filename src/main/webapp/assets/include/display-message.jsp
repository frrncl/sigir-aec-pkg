<c:if test="${not empty message}">

	<div class="row">

		<div class="col-lg-2 col-md-1"></div>

		<div class="col-lg-8 col-md-10">
			<c:choose>
				<c:when test="${message.error}">

					<div class="alert alert-warning alert-dismissible fade show" role="alert">
						<p>
							<strong><fmt:message key="messages.error.error" bundle="${i18n}"/>
								<c:out value="${message.code}"/> - <c:out value="${message.type}"/>.</strong>
						</p>

						<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>

						<hr>

						<p><c:out value="${message.description}"/></p>
					</div>

				</c:when>

				<c:otherwise>

					<div class="alert alert-success alert-dismissible fade show" role="alert">

						<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>

						<c:out value="${message.description}"/>
					</div>
				</c:otherwise>
			</c:choose>
		</div>

		<div class="col-lg-2 col-md-1"></div>

	</div>

</c:if>


