

	<!-- ======= Footer ======= -->
	<footer id="footer">

		<div class="container">
			<div class="copyright">
				<fmt:message key="labels.footer.copyright" bundle="${i18n}"/>
			</div>
			<div class="credits">
				<!-- All the links in the footer should remain intact. -->
				<!-- You can delete the links only if you purchased the pro version. -->
				<!-- Licensing information: https://bootstrapmade.com/license/ -->
				<!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/squadfree-free-bootstrap-template-creative/ -->

				<fmt:message key="labels.footer.designedBy" bundle="${i18n}"/>
			</div>
		</div>
	</footer><!-- End Footer -->

	<a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

