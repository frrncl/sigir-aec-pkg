
	<meta charset="UTF-8" />

	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<meta http-equiv="content-type" content="text/html;charset=UTF-8" />

	<meta name="author" content="Nicola Ferro" />

	<!-- Favicons -->
	<link rel="icon" type="image/png" href="<c:url value="/assets/img/favicon.png"/>">
	<link rel="apple-touch-icon" href="<c:url value="/assets/img/apple-touch-icon.png"/>">

	<!-- Google Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

	<!-- Vendor CSS Files -->
	<link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">

	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
		  rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
		  crossorigin="anonymous">

	<link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.7.2/font/bootstrap-icons.css" rel="stylesheet" >

	<link href='https://unpkg.com/boxicons@2.1.1/css/boxicons.min.css' rel='stylesheet'>

	<link href="https://cdn.jsdelivr.net/npm/glightbox/dist/css/glightbox.min.css" rel="stylesheet"  />

	<link href="https://unpkg.com/swiper@7/swiper-bundle.min.css"  rel="stylesheet" />

	<!-- Template Main CSS File -->
	<link href="<c:url value="/assets/css/style.css"/>" rel="stylesheet" type="text/css">


