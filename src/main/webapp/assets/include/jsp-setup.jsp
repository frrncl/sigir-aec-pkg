<!--
~ Copyright (c) 2021-2022 University of Padua, Italy
~
~ Licensed under the Apache License, Version 2.0 (the "License");
~ you may not use this file except in compliance with the License.
~ You may obtain a copy of the License at
~
~ https://www.apache.org/licenses/LICENSE-2.0
~
~ Unless required by applicable law or agreed to in writing, software
~ distributed under the License is distributed on an "AS IS" BASIS,
~ WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
~ See the License for the specific language governing permissions and
~ limitations under the License.
~
~ Author: Nicola Ferro (ferro@dei.unipd.it)
~ Version: 1.0
~ Since: 1.0
-->

<%@page contentType="text/html; charset=utf-8" pageEncoding="UTF-8" %>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:choose>
	<c:when test="${not empty user}">
		<fmt:setLocale value="${user.locale.language}" scope="page"/>
	</c:when>
	<c:otherwise>
		<fmt:setLocale value="${pageContext.request.locale.language}" scope="page"/>
	</c:otherwise>
</c:choose>

<fmt:setBundle basename="${initParam['it.unipd.dei.aecpkg.web.baseResourceBundleName']}" var="i18n" scope="page"/>