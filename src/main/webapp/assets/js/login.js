/*
 * Copyright (c) 2021-2022 University of Padua, Italy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *
 * @author Nicola Ferro
 * @version 1.00
 * @since 1.00
 */


// setup validation when document is ready
$(document).ready(function () {

    $('#form-login').validate(
        {
            errorElement: "em",
            errorClass: "invalid-feedback",
            validClass: "valid-feedback",
            highlight: function ( element, errorClass, validClass ) {
                $( element ).parents( ".controls" ).addClass( "is-invalid" ).removeClass( "is-valid" );
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".controls" ).addClass( "is-valid" ).removeClass( "is-invalid" );
            }
        }
    );

});

// setup recaptcha for the login form
grecaptcha.ready(function () {
    // do request for recaptcha token
    grecaptcha.execute(aecpkg.recaptcha, {action: 'AUTHENTICATE_USER'})
        .then(function (token) {
            // add token value to form
            document.getElementById('g-recaptcha-response').value = token;
        });
});
