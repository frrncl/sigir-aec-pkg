<?xml version="1.0" encoding="UTF-8"?>

<!--
  ~  Copyright (c) 2021-2022 University of Padua, Italy
  ~
  ~  Licensed under the Apache License, Version 2.0 (the "License");
  ~  you may not use this file except in compliance with the License.
  ~  You may obtain a copy of the License at
  ~
  ~       https://www.apache.org/licenses/LICENSE-2.0
  ~
  ~  Unless required by applicable law or agreed to in writing, software
  ~  distributed under the License is distributed on an "AS IS" BASIS,
  ~  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  ~  See the License for the specific language governing permissions and
  ~  limitations under the License.
  ~
  ~   Author: Nicola Ferro (ferro@dei.unipd.it)
  ~   Version: 1.0
  ~   Since: 1.0
  -->

<web-app id="sigir-aec-pkg" version="5.0" xmlns="https://jakarta.ee/xml/ns/jakartaee"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="https://jakarta.ee/xml/ns/jakartaee https://jakarta.ee/xml/ns/jakartaee/web-app_5_0.xsd">
    
	<display-name>SIGIR AEC Artifact Packager</display-name>
	<description>Prepares artifact packages for submission to the ACM digital library.</description>

	<context-param>
		<param-name>it.unipd.dei.aecpkg.web.application.configuration</param-name>
		<param-value>application.properties</param-value>
	</context-param>

	<context-param>
		<param-name>it.unipd.dei.aecpkg.web.recaptchaSecret</param-name>
		<param-value>6LeAY80dAAAAAHeEu5FLKORDy1Y7Vifna-ffd2yI</param-value>
	</context-param>

	<context-param>
		<param-name>it.unipd.dei.aecpkg.web.recaptchaClient</param-name>
		<param-value>6LeAY80dAAAAAGx2WleXu4SXeMXxgTU9lDJA399g</param-value>
	</context-param>

	<context-param>
		<param-name>it.unipd.dei.aecpkg.web.baseResourceBundleName</param-name>
		<param-value>i18n</param-value>
	</context-param>

	<!--
     Listener for starting and stopping the Application in the Web container
   -->
	<listener>
		<description>Starts up and shuts down the system</description>
		<listener-class>it.unipd.dei.aecpkg.web.ApplicationServletManager</listener-class>
	</listener>

	<!-- Setting the HTTP Server Header -->
	<filter>
		<filter-name>HttpServerHeaderFilter</filter-name>
		<filter-class>it.unipd.dei.aecpkg.web.HttpServerHeaderFilter</filter-class>
		<init-param>
			<param-name>aecpkg.server.header</param-name>
			<param-value>SIGIR AEC Packager/1.0</param-value>
		</init-param>
	</filter>
	<filter-mapping>
		<filter-name>HttpServerHeaderFilter</filter-name>
		<url-pattern>/*</url-pattern>
	</filter-mapping>

	<!-- Protecting resources -->
	<filter>
		<filter-name>ProtectedResourceFilter</filter-name>
		<filter-class>it.unipd.dei.aecpkg.web.ProtectedResourceFilter</filter-class>
		<init-param>
			<param-name>aecpkg.path.aec</param-name>
			<param-value>/protected/aec</param-value>
		</init-param>
		<init-param>
			<param-name>aecpkg.path.acm</param-name>
			<param-value>/protected/acm</param-value>
		</init-param>
		<init-param>
			<param-name>aecpkg.path.assets</param-name>
			<param-value>/protected/assets</param-value>
		</init-param>
		<init-param>
			<param-name>aecpkg.path.artifacts</param-name>
			<param-value>/protected/artifact</param-value>
		</init-param>
	</filter>
	<filter-mapping>
		<filter-name>ProtectedResourceFilter</filter-name>
		<url-pattern>/protected/*</url-pattern>
	</filter-mapping>

	<!-- Welcome file -->
	<welcome-file-list>
		<welcome-file>index.jsp</welcome-file>
	</welcome-file-list>

	<!-- Default error page -->
	<servlet>
		<servlet-name>Error</servlet-name>
		<jsp-file>/error.jsp</jsp-file>
	</servlet>

	<servlet-mapping>
		<servlet-name>Error</servlet-name>
		<url-pattern>/error</url-pattern>
	</servlet-mapping>

	<error-page>
		<location>/error</location>
	</error-page>

	<servlet>
		<servlet-name>Login</servlet-name>
		<jsp-file>/login.jsp</jsp-file>
	</servlet>

	<servlet-mapping>
		<servlet-name>Login</servlet-name>
		<url-pattern>/login</url-pattern>
	</servlet-mapping>

	<servlet>
		<servlet-name>Logout</servlet-name>
		<servlet-class>it.unipd.dei.aecpkg.web.LogoutUserServlet</servlet-class>
	</servlet>

	<servlet-mapping>
		<servlet-name>Logout</servlet-name>
		<url-pattern>/logout</url-pattern>
	</servlet-mapping>

	<servlet>
		<servlet-name>AuthenticateUser</servlet-name>
		<servlet-class>it.unipd.dei.aecpkg.web.AuthenticateUserServlet</servlet-class>
	</servlet>

	<servlet-mapping>
		<servlet-name>AuthenticateUser</servlet-name>
		<url-pattern>/home</url-pattern>
	</servlet-mapping>

	<servlet>
		<servlet-name>CreateArtifact</servlet-name>
		<servlet-class>it.unipd.dei.aecpkg.web.CreateArtifactServlet</servlet-class>
		<multipart-config>
			<max-file-size>52428800</max-file-size> <!-- 50 Mbyte -->
			<max-request-size>57671680</max-request-size> <!-- 55 Mbyte -->
			<file-size-threshold>5242880</file-size-threshold> <!-- 5 Mbyte -->
		</multipart-config>
	</servlet>

	<servlet-mapping>
		<servlet-name>CreateArtifact</servlet-name>
		<url-pattern>/protected/aec/create-artifact</url-pattern>
	</servlet-mapping>

	<servlet>
		<servlet-name>Download</servlet-name>
		<jsp-file>/download.jsp</jsp-file>
	</servlet>

	<servlet-mapping>
		<servlet-name>Download</servlet-name>
		<url-pattern>/download</url-pattern>
	</servlet-mapping>

	<servlet>
		<servlet-name>DownloadArtifactPackage</servlet-name>
		<servlet-class>it.unipd.dei.aecpkg.web.DownloadArtifactPackageServlet</servlet-class>
	</servlet>

	<servlet-mapping>
		<servlet-name>DownloadArtifactPackage</servlet-name>
		<url-pattern>/protected/artifact/download</url-pattern>
	</servlet-mapping>


</web-app>
