<%@include file="/assets/include/jsp-setup.jsp" %>

<!DOCTYPE html>
<html>
<head>
	<%@include file="/assets/include/head-setup.jsp" %>

	<title><fmt:message key="labels.downloadArtifactPackage.head.title" bundle="${i18n}"/></title>
	<meta name="description" content="<fmt:message key="labels.downloadArtifactPackage.head.description" bundle="${i18n}"/>" >
</head>

<body>

<!-- ======= Header ======= -->
<header id="header" class="fixed-top">
	<div class="container d-flex align-items-center justify-content-between">

		<div class="logo">
			<h1 class="text-light"><a href="<c:url value="/"/>"><span><fmt:message key="labels.logo" bundle="${i18n}"/></span></a>
			</h1>
		</div>

		<nav id="navbar" class="navbar">
			<ul>
				<li><a class="nav-link" href="<c:url value="/#hero"/>"><fmt:message key="labels.menu.home"
																					bundle="${i18n}"/></a></li>
				<li><a class="nav-link" href="<c:url value="/#about"/>"><fmt:message key="labels.menu.about"
																					 bundle="${i18n}"/></a></li>
			</ul>
			<i class="bi bi-list mobile-nav-toggle"></i>
		</nav><!-- .navbar -->

	</div>
</header><!-- End Header -->

<main id="main">

	<!-- ======= Download Section ======= -->
	<section id="login">
		<div class="container" data-aos="fade-up">

			<div class="section-title">
				<h2><fmt:message key="labels.downloadArtifactPackage.title" bundle="${i18n}"/></h2>
			</div>

			<!-- START Message -->
			<%@include file="/assets/include/display-message.jsp" %>
			<!-- END Message -->

			<div class="row">
				<div class="col-lg-6 ">
					<img src="<c:url value="/assets/img/package.jpg"/>" class="figure-img img-fluid rounded"
						 alt="<fmt:message key="labels.downloadArtifactPackage.alt" bundle="${i18n}"/>">
				</div>

				<div class="col-lg-6">
					<form id="form-download" action="<c:url value="/protected/artifact/download"/>" method="post" role="form" novalidate>

						<input type="hidden" id="g-recaptcha-response" name="g-recaptcha-response">

						<input type="hidden" id="jwt" name="jwt" value="${param.jwt}">

						<div class="form-group mt-3">
							<label for="jwt-disabled"><fmt:message key="labels.downloadArtifactPackage.jwt.label"
																		  bundle="${i18n}"/></label>
							<div class="controls">
								<textarea id="jwt-disabled"  rows="10"
										  class="form-control" disabled>${param.jwt}</textarea>
							</div>
						</div>

						<div class="text-center">
							<button class="btn btn-info btn-lg mt-3" type="submit"><fmt:message
									key="labels.downloadArtifactPackage.download.label" bundle="${i18n}"/></button>
						</div>
					</form>
				</div>

			</div>


		</div>
	</section><!-- End Download Section -->

</main><!-- End #main -->

<%@include file="/assets/include/footer.jsp" %>

<%@include file="/assets/include/foot-setup.jsp" %>

<%@include file="/assets/include/recaptcha-setup.jsp" %>

<script src="<c:url value="/assets/js/download.js"/>"></script>

</body>
</html>