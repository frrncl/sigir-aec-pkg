/*
 * Copyright (c) 2021-2022 University of Padua, Italy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.unipd.dei.aecpkg.component;

import it.unipd.dei.aecpkg.resource.Resource;
import it.unipd.dei.aecpkg.resource.User;
import org.apache.logging.log4j.ThreadContext;

import java.util.HashMap;
import java.util.Map;


/**
 * Provides the overall context of the application on a per-thread basis.
 *
 * @author Nicola Ferro
 * @version 1.00
 * @since 1.00
 */
public final class ApplicationContext {

	/**
	 * The per-thread application context
	 */
	private static final ThreadLocal<Map<String, Object>> context = ThreadLocal.withInitial(HashMap::new);

	/**
	 * The user who is performing an action
	 */
	private static final String USER = "USER";

	/**
	 * The IP address of the user who is performing an action
	 */
	private static final String IP = "IP";

	/**
	 * The action performed by the user
	 */
	private static final String ACTION = "ACTION";

	/**
	 * The resource currently processed
	 */
	private static final String RESOURCE = "RESOURCE";

	/**
	 * Sets the {@code user} currently performing actions.
	 * <p>
	 * If {@code null}, it simply returns.
	 *
	 * @param user the {@code user} currently performing actions.
	 */
	public static void setUser(final User user) {
		if (user != null) {
			context.get().put(USER, user);
			ThreadContext.put(USER, user.getIdentifier());
		}
	}

	/**
	 * Returns the {@code User} currently performing actions.
	 *
	 * @return the {@code User} currently performing actions, if any; {@code null} otherwise.
	 */
	public static User getUser() {
		return (User) context.get().get(USER);
	}

	/**
	 * Checks whether there is a {@code User} in the current context.
	 *
	 * @return {@code true} if there is a {@code User} in the current context; {@code false} otherwise.
	 */
	public static boolean hasUser() {
		return context.get().containsKey(USER);
	}

	/**
	 * Removes the {@code User} currently performing actions.
	 */
	public static void removeUser() {
		context.get().remove(USER);
		ThreadContext.remove(USER);
	}

	/**
	 * Sets the {@code IP} addressed of the user currently performing actions.
	 * <p>
	 * If {@code null} or empty, it simply returns.
	 *
	 * @param ip the {@code IP} addressed of the user currently performing actions.
	 */
	public static void setIPAddress(final String ip) {
		if (ip != null && !ip.isEmpty()) {
			context.get().put(IP, ip);
			ThreadContext.put(IP, ip);
		}
	}

	/**
	 * Returns the {@code IP} addressed of the user currently performing actions.
	 *
	 * @return he {@code IP} addressed of the user currently performing actions, if any; {@code null} otherwise.
	 */
	public static String getIPAddress() {
		return (String) context.get().get(IP);
	}

	/**
	 * Checks whether there is a {@code IP} address in the current context.
	 *
	 * @return {@code true} if there is a {@code IP} address in the current context; {@code false} otherwise.
	 */
	public static boolean hasIPAddress() {
		return context.get().containsKey(IP);
	}

	/**
	 * Removes the {@code IP} addressed of the user currently performing actions.
	 */
	public static void removeIPAddress() {
		context.get().remove(IP);
		ThreadContext.remove(IP);
	}


	/**
	 * Sets the {@code action} currently performed.
	 * <p>
	 * If {@code null}, it simply returns.
	 *
	 * @param action the action currently performed.
	 */
	public static void setAction(final String action) {
		if (action != null) {
			context.get().put(ACTION, action);
			ThreadContext.put(ACTION, action);
		}
	}

	/**
	 * Returns the action currently performed.
	 *
	 * @return the action currently performed, if any; {@code null} otherwise.
	 */
	public static String getAction() {
		return (String) context.get().get(ACTION);
	}

	/**
	 * Checks whether there is an action in the current context.
	 *
	 * @return {@code true} if there is an action in the current context; {@code false} otherwise.
	 */
	public static boolean hasAction() {
		return context.get().containsKey(ACTION);
	}

	/**
	 * Removes the action currently performed.
	 */
	public static void removeAction() {
		context.get().remove(ACTION);
		ThreadContext.remove(ACTION);
	}

	/**
	 * Sets the {@code resource} currently processed.
	 * <p>
	 * If {@code null}, it simply returns.
	 *
	 * @param resource the resource currently processed.
	 */
	public static void setResource(final Resource<?> resource) {
		if (resource != null) {
			context.get().put(RESOURCE, resource);
			ThreadContext.put(RESOURCE, resource.getIdentifier());
		}
	}

	/**
	 * Returns the resource currently processed.
	 *
	 * @return the resource currently processed, if any; {@code null} otherwise.
	 */
	public static Resource<?> getResource() {
		return (Resource<?>) context.get().get(RESOURCE);
	}

	/**
	 * Checks whether there is a resource in the current context.
	 *
	 * @return {@code true} if there is a resource in the current context; {@code false} otherwise.
	 */
	public static boolean hasResource() {
		return context.get().containsKey(RESOURCE);
	}

	/**
	 * Removes the resource currently processed.
	 */
	public static void removeResource() {
		context.get().remove(RESOURCE);
		ThreadContext.remove(RESOURCE);
	}

	/**
	 * This class can be neither instantiated nor sub-classed.
	 */
	private ApplicationContext() {
		throw new AssertionError(String.format("No instances of %s allowed.", ApplicationContext.class.getName()));
	}
}
