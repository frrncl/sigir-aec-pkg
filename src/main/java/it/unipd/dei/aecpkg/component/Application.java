/*
 * Copyright (c) 2021-2022 University of Padua, Italy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.unipd.dei.aecpkg.component;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.message.Message;
import org.apache.logging.log4j.message.StringFormattedMessage;

import java.util.Collections;
import java.util.List;

/**
 * Represents the overall application.
 *
 * It provides methods for starting up and shutting down the system.
 *
 * @author Nicola Ferro
 * @version 1.00
 * @since 1.00
 */
public final class Application extends AbstractComponent implements Component {

	/**
	 * The possible actions for {@code Application}.
	 *
	 * @author Nicola Ferro
	 * @version 1.00
	 * @since 1.00
	 */
	public static final class ACTIONS {

		/**
		 * The start-up of the whole application.
		 */
		public static final String START_UP_APPLICATION = "START_UP_APPLICATION";

		/**
		 * The shut-down of the whole application
		 */
		public static final String SHUT_DOWN_APPLICATION = "SHUT_DOWN_APPLICATION";

	}

	/**
	 * Name of the default configuration file
	 */
	private static final String DEFAULT_CONFIG_FILE = "application.properties";

	/**
	 * Name of the property defining the labels of the components to be run.
	 */
	private static final String PROP_COMPONENT_LABELS = Application.class.getName() + ".component.labels";

	/**
	 * Name of the prefix of the property defining the class and configuration of a component.
	 */
	private static final String PROP_COMPONENT_PREFIX = Application.class.getName() + ".component.";

	/**
	 * Name of the postfix of the property defining the class of a component.
	 */
	private static final String PROP_COMPONENT_CLASS_POSTFIX = ".class";

	/**
	 * Name of the postfix of the property defining the configuration of a component.
	 */
	private static final String PROP_COMPONENT_CONFIGURATION_POSTFIX = ".configuration";


	/**
	 * Creates the application.
	 *
	 * @param configFileName the name of the file containing the configuration for this component.
	 *
	 * @throws ComponentException if something goes wrong while creating the component.
	 */
	public Application(final String configFileName) throws ComponentException {
		super(configFileName);
	}

	/**
	 * Creates the application.
	 *
	 * @throws ComponentException if something goes wrong while creating the component.
	 */
	public Application() throws ComponentException {
		this(DEFAULT_CONFIG_FILE);
	}


	/*
	 * -------------------------------------------------------------------------
	 * Methods defined in the superclass AbstractComponent follow.
	 * -------------------------------------------------------------------------
	 */

	/**
	 * Starts up the {@code Application}.
	 *
	 * @throws ComponentException if something goes wrong during the start-up.
	 */
	@Override
	protected void doStart() throws ComponentException {

		ApplicationContext.setAction(ACTIONS.START_UP_APPLICATION);

		try {
			// parse the declared components
			if (config.containsKey(PROP_COMPONENT_LABELS)) {

				// process component in order of declaration to preserve
				// possible dependencies
				for (final String label : parseList(config.get(PROP_COMPONENT_LABELS))) {

					LOGGER.debug("Processing configuration to start component %s.", label);

					final String propComponentClass = PROP_COMPONENT_PREFIX + label + PROP_COMPONENT_CLASS_POSTFIX;
					final String propComponentConfiguration = PROP_COMPONENT_PREFIX + label + PROP_COMPONENT_CONFIGURATION_POSTFIX;

					final String componentClass = config.get(propComponentClass);
					final String componentConfiguration = config.get(propComponentConfiguration);

					if (componentClass == null) {
						final Message msg = new StringFormattedMessage(
								"Invalid configuration for component %s: no class defined.", label);
						LOGGER.error(msg);
						throw new ComponentException(msg.getFormattedMessage());
					}

					// instantiate the component
					try {
						if (componentConfiguration != null) {
							ComponentManager.createComponent(Class.forName(componentClass).asSubclass(Component.class),
									componentConfiguration);
						} else {
							ComponentManager.createComponent(Class.forName(componentClass).asSubclass(Component.class));
						}
					} catch (ClassNotFoundException e) {
						final Message msg = new StringFormattedMessage("Unable to instantiate component %s: %s.", label,
								e.getMessage());
						LOGGER.error(msg, e);
						throw new ComponentException(msg.getFormattedMessage(), e);
					}
				}

				LOGGER.debug("Components successfully created and started up.");

			} else {
				final Message msg = new StringFormattedMessage(
						"Invalid application configuration: no components to start.");
				LOGGER.error(msg);
				throw new ComponentException(msg.getFormattedMessage());
			}

		} finally {
			ApplicationContext.removeAction();
		}

	}

	/**
	 * Shuts down the {@code Application}.
	 *
	 * @throws ComponentException if something goes wrong while shutting down the component.
	 */
	@Override
	protected void doStop() throws ComponentException {

		ApplicationContext.setAction(ACTIONS.SHUT_DOWN_APPLICATION);

		try {

			// parse the declared components
			if (config.containsKey(PROP_COMPONENT_LABELS)) {

				// process the components in reverse order of declaration to
				// preserve dependencies
				final List<String> components = parseList(config.get(PROP_COMPONENT_LABELS));
				Collections.reverse(components);

				for (final String label : components) {

					final String propComponentClass = PROP_COMPONENT_PREFIX + label + PROP_COMPONENT_CLASS_POSTFIX;
					final String componentClass = config.get(propComponentClass);


					if (componentClass == null) {
						final Message msg = new StringFormattedMessage(
								"Invalid configuration for component %s: no class defined.", label);
						LOGGER.error(msg);
						throw new ComponentException(msg.getFormattedMessage());
					}

					// remove the component
					try {
						ComponentManager.removeComponent(Class.forName(componentClass).asSubclass(Component.class));
					} catch (ClassNotFoundException e) {
						final Message msg = new StringFormattedMessage("Unable to shut down component %s: %s.", label,
								e.getMessage());
						LOGGER.error(msg, e);
						throw new ComponentException(msg.getFormattedMessage(), e);
					}
				}

				LOGGER.debug("Components successfully shut down.");

			} else {
				final Message msg = new StringFormattedMessage(
						"Invalid application configuration: no components to stop.");
				LOGGER.error(msg);
				throw new ComponentException(msg.getFormattedMessage());
			}

			LogManager.shutdown();

		} finally {
			ApplicationContext.removeAction();
		}

	}

}
