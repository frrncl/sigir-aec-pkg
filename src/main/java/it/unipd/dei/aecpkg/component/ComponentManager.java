/*
 * Copyright (c) 2021-2022 University of Padua, Italy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.unipd.dei.aecpkg.component;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.message.Message;
import org.apache.logging.log4j.message.StringFormattedMessage;
import org.apache.logging.log4j.message.StringFormatterMessageFactory;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * Controls and manages any {@link Component} of the system.
 * <p>
 * In particular, it provides methods for creating, obtaining, and destroying {@code Component} objects.
 * <p>
 * Note that we assume to have just one {@code Component} object for each class.
 *
 * @author Nicola Ferro
 * @version 1.00
 * @since 1.00
 */
public final class ComponentManager {

	/**
	 * The logger.
	 */
	private static final Logger LOGGER = LogManager.getLogger(ComponentManager.class,
			StringFormatterMessageFactory.INSTANCE);

	/**
	 * The instantiated components
	 */
	private static final List<Component> COMPONENTS = new ArrayList<>();

	/**
	 * This class can be neither sub-classed nor instantiated.
	 */
	private ComponentManager() {
		final Message msg = new StringFormattedMessage("No instances of %s allowed", ComponentManager.class.getName());
		LOGGER.error(msg);
		throw new AssertionError(msg.getFormattedMessage());
	}

	/**
	 * Creates a new {@code Component} according to the given class object, starts it, and adds it to this manager.
	 * <p>
	 * This method assumes that the {@code Component} has a default constructor and a default configuration file.
	 *
	 * @param c   the class of the component to create.
	 * @param <C> the type of the {@code Component} to be created.
	 *
	 * @return the just created component.
	 *
	 * @throws ComponentException if something goes wrong while creating the component.
	 */
	public static <C extends Component> C createComponent(final Class<C> c) throws ComponentException {
		return createComponent(c, null);
	}

	/**
	 * Creates a new {@code Component} according to the given class object, configures it, starts it, and adds it to
	 * this manager.
	 * <p>
	 * Note that only concrete classes can be used for {@code cls}, i.e. interfaces, enumerations, and abstract classes
	 * are not allowed and will cause a {@code ComponentException} to be thrown.
	 * <p>
	 * This method assumes that the actual {@code Component} implementation has a constructor which accepts the name of
	 * the configuration file and able to process it.
	 *
	 * @param cls            the class of the component to create.
	 * @param configFileName the file containing the configuration for the component.
	 * @param <C>            the type of the {@code Component} to be created.
	 *
	 * @return the just created component.
	 *
	 * @throws ComponentException   if something goes wrong while creating the component or {@code cls} is not a
	 *                              concrete class.
	 * @throws NullPointerException if {@code cls} is {@code null}.
	 */
	@SuppressWarnings("unchecked")
	public static synchronized <C extends Component> C createComponent(final Class<C> cls, final String configFileName) throws
			ComponentException {

		if (cls == null) {
			final Message msg = new StringFormattedMessage("The class of the component cannot be null.");
			LOGGER.error(msg);
			throw new NullPointerException(msg.getFormattedMessage());
		}

		if (cls.isInterface()) {
			final Message msg = new StringFormattedMessage(
					"%s is an interface. Components can be created only from concrete classes.", cls.getName());
			LOGGER.error(msg);
			throw new ComponentException(msg.getFormattedMessage());
		}
		if (cls.isEnum()) {
			final Message msg = new StringFormattedMessage(
					"%sStringFormattedMessage is an enumeration. Components can be created only from concrete classes.",
					cls.getName());
			LOGGER.error(msg);
			throw new ComponentException(msg.getFormattedMessage());
		}
		if (Modifier.isAbstract(cls.getModifiers())) {
			final Message msg = new StringFormattedMessage(
					"%s is an abstract class. Components can be created only from concrete classes.", cls.getName());
			LOGGER.error(msg);
			throw new ComponentException(msg.getFormattedMessage());
		}

		try {

			// determine if we already have a Component object instantiated for that class (note that we assume just
			// one Component object per class), otherwise create a new one
			Component c = COMPONENTS.stream().filter(cmp -> cmp.getClass().equals(cls)).findFirst().orElseGet(() -> {

				Component cc = null;

				try {
					cc = configFileName == null || configFileName.isEmpty() ? cls.getDeclaredConstructor()
							.newInstance() : cls.getConstructor(String.class).newInstance(configFileName);

					// start the component
					cc.start();
				} catch (final InstantiationException | IllegalAccessException | NoSuchMethodException | SecurityException | IllegalArgumentException | InvocationTargetException e) {

					final Message msg = new StringFormattedMessage("Unable to instantiate the component %s.",
							cls.getName());
					LOGGER.error(msg, e);
					throw new UncheckedComponentException(new ComponentException(msg.getFormattedMessage(), e));
				} catch (ComponentException ce) {
					throw new UncheckedComponentException(ce);
				}

				return cc;


			});

			COMPONENTS.add(c);

			return (C) c;

		} catch (UncheckedComponentException uce) {
			throw uce.getCause();
		}
	}

	/**
	 * Checks whether this manager contains a {@code Component} object for the given class/interface.
	 *
	 * @param cls the class/interface of the component to check.
	 *
	 * @return {@code true} if this manager contains the requested {@code Component}, {@code false} otherwise.
	 */
	public static boolean hasComponent(final Class<? extends Component> cls) {
		return cls != null && COMPONENTS.stream().anyMatch(c -> cls.isAssignableFrom(c.getClass()));
	}


	/**
	 * Returns the (first/only) {@code Component} object corresponding to the given class/interface, if any.
	 *
	 * @param cls the class/interface of the {@code Component} to get.
	 * @param <C> the type of the {@code Component} to get.
	 *
	 * @return an {@link Optional} containing the requested {@code Component} object if any.
	 */
	public static synchronized <C extends Component> Optional<C> getComponent(final Class<C> cls) {
		try {
			return COMPONENTS.stream().filter(c -> cls.isAssignableFrom(c.getClass())).map(cls::cast).findFirst();
		} catch (final ClassCastException cce) {
			final Message msg = new StringFormattedMessage("Unable to cast to class %s.", cls.getName());
			LOGGER.warn(msg, cce);
			return Optional.empty();
		}
	}


	/**
	 * Returns a list of all the {@code Component} objects contained in this manager.
	 *
	 * @return a list of {@code Component} objects or {@link Collections#emptyList()} if there is no {@code Component}
	 * 		in this manager.
	 */
	public static List<Component> getComponents() {
		return COMPONENTS.isEmpty() ? Collections.emptyList() : Collections.unmodifiableList(COMPONENTS);
	}

	/**
	 * Removes the {@code Component} of the given class from this manager and stops them.
	 * <p>
	 * Note that only concrete classes can be used for {@code cls}, i.e. interfaces, enumerations, and abstract classes
	 * are not allowed and will cause a {@code ComponentException} to be thrown.
	 *
	 * @param cls the class of the components to remove.
	 *
	 * @throws ComponentException if something goes wrong while removing the component or if {@code cls} is not a
	 *                            concrete class.
	 */
	public static synchronized void removeComponent(final Class<? extends Component> cls) throws ComponentException {

		if (cls == null) {
			final Message msg = new StringFormattedMessage("The class of the component cannot be null.");
			LOGGER.error(msg);
			throw new NullPointerException(msg.getFormattedMessage());
		}

		if (cls.isInterface()) {
			final Message msg = new StringFormattedMessage(
					"%s is an interface. Components can be removed only for concrete classes.", cls.getName());
			LOGGER.error(msg);
			throw new ComponentException(msg.getFormattedMessage());
		}
		if (cls.isEnum()) {
			final Message msg = new StringFormattedMessage(
					"%s is an enumeration. Components can be removed only for concrete classes.", cls.getName());
			LOGGER.error(msg);
			throw new ComponentException(msg.getFormattedMessage());
		}
		if (Modifier.isAbstract(cls.getModifiers())) {
			final Message msg = new StringFormattedMessage(
					"%s is an abstract class. Components can be removed only for concrete classes.", cls.getName());
			LOGGER.error(msg);
			throw new ComponentException(msg.getFormattedMessage());
		}

		// move in reverse order to simplify removal and account for any dependencies among components
		for (int i = COMPONENTS.size() - 1; i >= 0; i--) {
			Component c = COMPONENTS.get(i);

			if (cls.isAssignableFrom(c.getClass())) {
				COMPONENTS.remove(i);

				c.stop();

				return;
			}
		}


	}

}
