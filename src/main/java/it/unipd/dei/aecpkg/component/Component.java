/*
 * Copyright (c) 2021-2022 University of Padua, Italy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.unipd.dei.aecpkg.component;

/**
 * Represents a generic component of the system.
 * 
 * A {@code Component} provides two methods for managing its life cycle:
 * <ul>
 * <li> {@link Component#start()} starts up the component;</li>
 * <li> {@link Component#stop()} shuts down the component.</li>
 * </ul>
 *
 * <p>
 * In addition, each {@code Component} can provide information about its current {@link Status} by calling {@link
 * Component#getStatus()}.
 * </p>
 *
 * @author Nicola Ferro
 * @version 1.00
 * @since 1.00
 */
public interface Component {

	/**
	 * Represents the status of a {@link Component}.
	 *
	 * @author Nicola Ferro
	 * @version 1.00
	 * @since 1.00
	 */
	enum Status {

		/**
		 * Indicates that the {@code Component} is not running, because either it has not been started yet or it has
		 * been shut down.
		 */
		NOT_RUNNING,

		/**
		 * Indicates that the {@code Component} is starting up.
		 */
		STARTING_UP,

		/**
		 * Indicates that the {@code Component} is properly running.
		 */
		RUNNING,

		/**
		 * Indicates that the {@code Component} is shutting down.
		 */
		SHUTTING_DOWN,

		/**
		 * Indicates some error condition in the {@code Component}.
		 */
		ERROR
	}

	/**
	 * Returns the current status of the {@code Component}.
	 *
	 * @return the current status of the {@code Component}.
	 */
	Status getStatus();

	/**
	 * Starts up the {@code Component}.
	 *
	 * @throws ComponentException if something goes wrong while starting the component.
	 */
	void start() throws ComponentException;

	/**
	 * Stops the {@code Component}.
	 *
	 * @throws ComponentException if something goes wrong while stopping the component.
	 */
	void stop() throws ComponentException;

}
