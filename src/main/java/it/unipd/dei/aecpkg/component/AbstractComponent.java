/*
 * Copyright (c) 2021-2022 University of Padua, Italy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.unipd.dei.aecpkg.component;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.message.Message;
import org.apache.logging.log4j.message.StringFormattedMessage;
import org.apache.logging.log4j.message.StringFormatterMessageFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.stream.Collectors;

/**
 * Represents the common superclass for implementing a generic {@link Component} of the system.
 *
 * @author Nicola Ferro
 * @version 1.00
 * @since 1.00
 */
public abstract class AbstractComponent implements Component {

	/**
	 * A LOGGER available for all the subclasses.
	 */
	protected static final Logger LOGGER = LogManager.getLogger(AbstractComponent.class,
			StringFormatterMessageFactory.INSTANCE);

	/**
	 * The configuration for the component.
	 */
	protected final Map<String, String> config;

	/**
	 * The lock for the {@code AbstractComponent}
	 */
	private final Object lock;

	/**
	 * The name of the requested configuration file.
	 */
	private final String configFileName;

	/**
	 * The current status of the component.
	 */
	private Status status = Status.NOT_RUNNING;

	/**
	 * Creates a new component, reading its configuration from the given file.
	 *
	 * @param configFileName the name of the file containing the configuration for this component.
	 *
	 * @throws ComponentException if something goes wrong while creating the component.
	 */
	protected AbstractComponent(final String configFileName) throws ComponentException {

		lock = new Object();

		if (configFileName == null) {
			final Message msg = new StringFormattedMessage("Component %s: configuration file name cannot be null.",
					this.getClass().getName());
			LOGGER.error(msg);
			throw new ComponentException(msg.getFormattedMessage());
		}

		if (configFileName.isBlank()) {
			final Message msg = new StringFormattedMessage("Component %s: configuration file name cannot be empty.",
					this.getClass().getName());
			LOGGER.error(msg);
			throw new ComponentException(msg.getFormattedMessage());
		}

		this.configFileName = configFileName;

		// Get the class loader
		ClassLoader cl = AbstractComponent.class.getClassLoader();
		if (cl == null) {
			cl = ClassLoader.getSystemClassLoader();
			LOGGER.debug("Component %s: using system class loader.", this.getClass().getName());
		}

		// The properties holding the configuration of the component
		final Properties cfg = new Properties();

		try(InputStream is = cl.getResourceAsStream(configFileName)) {

			if (is == null) {
				final Message msg = new StringFormattedMessage("Component %s: the configuration file %s cannot be opened.",
						this.getClass().getName(), configFileName);
				LOGGER.error(msg);
				throw new ComponentException(msg.getFormattedMessage());
			}

			cfg.load(is);

		} catch (IOException ioe) {
			final Message msg = new StringFormattedMessage("Component %s: the configuration file %s cannot be loaded.",
					this.getClass().getName(), configFileName);
			LOGGER.error(msg, ioe);
			throw new ComponentException(msg.getFormattedMessage(), ioe);
		}

		this.config = cfg.entrySet().stream()
				.collect(Collectors.toMap(e -> e.getKey().toString(), e -> e.getValue().toString()));

	}

	public final Status getStatus() {
		synchronized (lock) {
			return status;
		}
	}

	public final void start() throws ComponentException {

		LOGGER.info("Starting component %s [%s].", this.getClass().getName(), configFileName);

		synchronized (lock) {
			if (!(status == Status.NOT_RUNNING || status == Status.ERROR)) {
				final Message msg = new StringFormattedMessage("Status is %s. Component %s [%s] cannot be started.",
						status, this.getClass().getName(), configFileName);
				LOGGER.error(msg);
				throw new ComponentException(msg.getFormattedMessage());
			}

			status = Status.STARTING_UP;
		}

		try {
			doStart();
		} catch (final Exception e) {
			synchronized (lock) {
				status = Status.ERROR;

				final Message msg = new StringFormattedMessage("Unable to start component %s [%s].",
						this.getClass().getName(), configFileName);
				LOGGER.error(msg, e);
				throw new ComponentException(msg.getFormattedMessage(), e);
			}
		}

		synchronized (lock) {
			status = Status.RUNNING;
		}

		LOGGER.info("Component %s [%s] successfully started.", this.getClass().getName(), configFileName);

	}

	public final void stop() throws ComponentException {

		LOGGER.info("Stopping component %s [%s].", this.getClass().getName(), configFileName);

		synchronized (lock) {
			if (!(status == Status.RUNNING || status == Status.ERROR)) {
				final Message msg = new StringFormattedMessage("Status is %s. Component %s [%s] cannot be stopped.",
						status, this.getClass().getName(), configFileName);
				LOGGER.error(msg);
				throw new ComponentException(msg.getFormattedMessage());
			}

			status = Status.SHUTTING_DOWN;
		}

		try {
			doStop();
		} catch (final Exception e) {
			synchronized (lock) {
				status = Status.ERROR;

				final Message msg = new StringFormattedMessage("Unable to stop component %s [%s].",
						this.getClass().getName(), configFileName);
				LOGGER.error(msg, e);
				throw new ComponentException(msg.getFormattedMessage(), e);
			}
		}

		synchronized (lock) {
			status = Status.NOT_RUNNING;
		}

		LOGGER.info("Component %s [%s] successfully stopped.", this.getClass().getName(), configFileName);
	}

	/**
	 * Provides a textual representation of the status of a {@code Component}.
	 */
	public String toString() {
		final ToStringBuilder tsb = new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE);

		final StringBuffer sb = tsb.getStringBuffer();


		tsb.append("class", this.getClass().getName()).append("config file", configFileName).append("status", status);

		// print each configuration parameter
		tsb.append("config");
		config.forEach((k, v) -> {
			sb.append(" ");
			tsb.append(k, v);
		});

		return tsb.toString();
	}

	/**
	 * Two {@code Component} objects are equal if they are the same object.
	 */
	public boolean equals(final Object o) {
		return this == o;
	}


	public int hashCode() {
		return System.identityHashCode(this);
	}

	/**
	 * Performs the actual start of the component. Concrete subclasses of {@code AbstractComponent} have to implement
	 * this method in order to provide their actual start up sequence.
	 *
	 * @throws Exception if something goes wrong while starting the component.
	 */
	protected abstract void doStart() throws Exception;

	/**
	 * Performs the actual stop of the component. Concrete subclasses of {@code AbstractComponent} have to implement
	 * this method in order to provide their actual shut down sequence.
	 *
	 * @throws Exception if something goes wrong while stopping the component.
	 */
	protected abstract void doStop() throws Exception;

	/**
	 * Cloning is not allowed.
	 *
	 * @throws CloneNotSupportedException the method always throws this exception, since cloning is not allowed.
	 */
	protected final Object clone() throws CloneNotSupportedException {
		final Message msg = new StringFormattedMessage("Attempt to clone the component %s [%s]. Clone is not allowed.",
				this.getClass().getName(), configFileName);
		LOGGER.error(msg);
		throw new CloneNotSupportedException(msg.getFormattedMessage());
	}

	/**
	 * Parses a property containing a list of items into a list of strings.
	 *
	 * @param list the value of the property to parse.
	 *
	 * @return a list of strings.
	 */
	protected static List<String> parseList(final String list) {
		return Arrays.stream(list.split(",")).map(String::trim).collect(Collectors.toList());
	}

}


