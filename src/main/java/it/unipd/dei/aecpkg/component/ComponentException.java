/*
 * Copyright (c) 2021-2022 University of Padua, Italy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.unipd.dei.aecpkg.component;

/**
 * Reports an error occurred within a {@link Component} of the system.
 *
 * @author Nicola Ferro
 * @version 1.00
 * @since 1.00
 */
public class ComponentException extends Exception {

	/**
	 * Constructs a new empty exception.
	 */
	public ComponentException() {
		super();
	}

	/**
	 * Constructs a new exception with the specified detail {@code message}.
	 *
	 * @param message the detail message.
	 */
	public ComponentException(final String message) {
		super(message);
	}

	/**
	 * Constructs a new exception with the specified detail {@code message} and {@code cause}.
	 * <p>
	 * Note that the detail message associated with {@code cause} is not automatically incorporated in this exception's
	 * detail message.
	 * </p>
	 *
	 * @param message the detail message.
	 * @param cause   the cause of this exception.
	 */
	public ComponentException(final String message, final Throwable cause) {
		super(message, cause);
	}

	/**
	 * Constructs a new exception with the specified {@code cause} and a detail message of {@code (cause==null ? null :
	 * cause.toString())} (which typically contains the class and detail message of {@code cause}).
	 *
	 * @param cause the cause of this exception.
	 */
	public ComponentException(final Throwable cause) {
		super(cause);
	}

}
