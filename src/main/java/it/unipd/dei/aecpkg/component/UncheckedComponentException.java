/*
 * Copyright (c) 2021-2022 University of Padua, Italy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.unipd.dei.aecpkg.component;

import java.util.Objects;

/**
 * Wraps an {@link ComponentException} with an unchecked exception.
 *
 * @author Nicola Ferro
 * @version 1.00
 * @since 1.00
 */
public class UncheckedComponentException extends RuntimeException {


    /**
     * Constructs an instance of this class.
     *
     * @param message the detail message, can be {@code null}.
     * @param cause   the {@code ComponentException}.
     *
     * @throws NullPointerException if the cause is {@code null}.
     */
    public UncheckedComponentException(final String message, final ComponentException cause) {
        super(message, Objects.requireNonNull(cause));
    }

    /**
     * Constructs an instance of this class.
     *
     * @param cause the {@code ComponentException}.
     *
     * @throws NullPointerException if the cause is {@code null}.
     */
    public UncheckedComponentException(final ComponentException cause) {
        super(Objects.requireNonNull(cause));
    }

    /**
     * Returns the cause of this exception.
     *
     * @return the {@code ComponentException} which is the cause of this exception.
     */
    @Override
    public ComponentException getCause() {
        return (ComponentException) super.getCause();
    }

}
