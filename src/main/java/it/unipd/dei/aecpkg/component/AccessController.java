/*
 * Copyright (c) 2021-2022 University of Padua, Italy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.unipd.dei.aecpkg.component;


import it.unipd.dei.aecpkg.resource.Role;
import it.unipd.dei.aecpkg.resource.User;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.apache.logging.log4j.message.Message;
import org.apache.logging.log4j.message.StringFormattedMessage;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;

/**
 * Performs authentications and authorization checks for the different {@code Action}s that can be performed by the
 * application.
 *
 * @author Nicola Ferro
 * @version 1.00
 * @since 1.00
 */
public final class AccessController extends AbstractComponent implements Component {

	/**
	 * Name of the default configuration file
	 */
	private static final String DEFAULT_CONFIG_FILE = "accessController.properties";

	/**
	 * Name of the prefix of the property defining the authentication, authorization, and access control list for an
	 * action.
	 */
	private static final String PROP_ACTION_PREFIX = AccessController.class.getName() + ".action.";

	/**
	 * Name of the postfix of the property defining the authentication of an action.
	 */
	private static final String PROP_ACTION_AUTHENTICATION_POSTFIX = ".authentication";

	/**
	 * Name of the postfix of the property defining the authorization of an action.
	 */
	private static final String PROP_ACTION_AUTHORIZATION_POSTFIX = ".authorization";

	/**
	 * Name of the postfix of the property defining the access control list of an action.
	 */
	private static final String PROP_ACTION_AUTHORIZED_ROLES_POSTFIX = ".authorizedRoles";

	/**
	 * Indicates the actions for which authentication is requested.
	 */
	private final HashSet<String> authentication;

	/**
	 * Indicates the actions for which authorization is requested and the authorized roles
	 */
	private final HashMap<String, EnumSet<Role>> authorization;

	/**
	 * Creates the access controller.
	 *
	 * @param configFileName the name of the file containing the configuration for this component.
	 *
	 * @throws ComponentException if something goes wrong while creating the component.
	 */
	public AccessController(final String configFileName) throws ComponentException {
		super(configFileName);

		authentication = new HashSet<>();
		authorization = new HashMap<>();
	}

	/**
	 * Creates the access controller.
	 *
	 * @throws ComponentException if something goes wrong while creating the component.
	 */
	public AccessController() throws ComponentException {
		this(DEFAULT_CONFIG_FILE);
	}

	/**
	 * Checks whether the given {@code action} requires authentication.
	 *
	 * @param action the action to check.
	 *
	 * @return {@code true} if the {@code action} requires authentication, {@code false} otherwise.
	 */
	public boolean requiresAuthentication(final String action) {
		return authentication.contains(action);
	}

	/**
	 * Checks whether the {@code user} is authorized to perform the {@code action} The check is performed on the basis
	 * of the role to which the user belongs.
	 *
	 * @param user   the user to check.
	 * @param action the action to check.
	 *
	 * @return {@code true} if the {@code user} is authorized to perform the {@code action} or if no authorization is
	 * 		required for the given action; {@code false} otherwise.
	 */
	public boolean isAuthorized(final User user, final String action) {

		// if there is no action or no user, then the user is not authorized
		if (action == null || user == null) {
			return false;
		}

		// if no authorization is required, then the user is authorized
		if (!authorization.containsKey(action)) {
			return true;
		}

		// if the user has no role, then the user is not authorized
		if (user.getRole() == null) {
			return false;
		}

		final Role role = user.getRole();
		return role.isRoot() || authorization.get(action).contains(role);

	}


	/**
	 * Performs access control on the basis of the {@code action} and {@code user} currently contained in the {@link
	 * ApplicationContext}.
	 *
	 * @throws AuthenticationRequiredException if authentication is required and no user is contained in the {@code
	 *                                         ApplicationContext}.
	 * @throws AuthorizationRequiredException  if the user currently contained in the {@code ApplicationContext} has
	 *                                         insufficient rights to perform the requested {@code Action}.
	 * @throws OperationException              if there is no {@code Action} currently contained in the {@code
	 *                                         ApplicationContext}.
	 * @throws ComponentException              if the {@code AccessController} is not running.
	 */
	public void controlAccess() throws ComponentException {

		if (!ApplicationContext.hasAction()) {
			final Message msg = new StringFormattedMessage(
					"There is not action set in the application context. Cannot perform access control.");
			LOGGER.error(msg);
			throw new OperationException(msg.getFormattedMessage());
		}

		final String action = ApplicationContext.getAction();

		// if no authentication is required, access control is successful and can return
		if (!authentication.contains(action)) {
			return;
		}

		if (!ApplicationContext.hasUser()) {
			final Message msg = new StringFormattedMessage("Authentication is required to perform %s.", action);
			LOGGER.warn(msg);
			throw new AuthenticationRequiredException(msg.getFormattedMessage());
		}

		// if no authorization is required, access control is successful and can return
		if (!authorization.containsKey(action)) {
			return;
		}

		final User user = ApplicationContext.getUser();

		final Role role = user.getRole();

		if (role == null) {
			final Message msg = new StringFormattedMessage(
					"There is no role specified for user %s. Cannot perform access control for action %s.",
					user.getIdentifier(), action);
			LOGGER.error(msg);
			throw new OperationException(msg.getFormattedMessage());
		}


		if (!role.isRoot() && !authorization.get(action).contains(role)) {
			final Message msg = new StringFormattedMessage("User %s has insufficient rights to perform %s.",
					user.getIdentifier(), action);
			LOGGER.warn(msg);
			throw new AuthorizationRequiredException(msg.getFormattedMessage());
		}
	}


	/**
	 * Provides a textual representation of the state of a {@code Component}.
	 */
	public String toString() {
		final ToStringBuilder tsb = new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE).appendSuper(
				super.toString());

		final StringBuffer sb = tsb.getStringBuffer();

		// print each action requiring authentication
		tsb.append("authentication");
		authentication.forEach((a) -> {
			sb.append("  ");
			tsb.append(a);
		});

		// print each action requiring authorization
		tsb.append("authorization");
		authorization.forEach((k, v) -> {
			sb.append("  ");
			v.forEach((r) -> tsb.append(k, r));
		});


		return tsb.toString();
	}


	/*
	 * -------------------------------------------------------------------------
	 * Methods defined in the superclass AbstractComponent follow.
	 * -------------------------------------------------------------------------
	 */

	/**
	 * Starts up the {@code AccessController}.
	 *
	 * @throws ComponentException if something goes wrong during the start-up.
	 */
	@Override
	protected final void doStart() throws ComponentException {


		// the set of action actually defined in the configuration
		final HashSet<String> actions = new HashSet<>(config.size());

		// iterate over properties to extract action names, avoiding duplication due to multiple parameters for each
		// action name
		for (final String propName : config.keySet()) {
			actions.add(parseActionName(propName));
		}

		for (final String action : actions) {

			final String propAuthentication = PROP_ACTION_PREFIX + action + PROP_ACTION_AUTHENTICATION_POSTFIX;
			final String propAuthorization = PROP_ACTION_PREFIX + action + PROP_ACTION_AUTHORIZATION_POSTFIX;
			final String propAuthorizedRoles = PROP_ACTION_PREFIX + action + PROP_ACTION_AUTHORIZED_ROLES_POSTFIX;

			final boolean authentication = Boolean.parseBoolean(config.get(propAuthentication));
			final boolean authorization = Boolean.parseBoolean(config.get(propAuthorization));
			final String authorizedRoles = config.get(propAuthorizedRoles);


			// cannot perform authorization if authentication is not required
			if (authorization && !authentication) {
				final Message msg = new StringFormattedMessage(
						"Invalid configuration for action %s: authorization requires authentication to be set.",
						action);
				LOGGER.error(msg);
				throw new ComponentException(msg.getFormattedMessage());
			}

			// cannot perform authorization if there are no authorized roles
			if (authorization && authorizedRoles == null) {
				final Message msg = new StringFormattedMessage(
						"Invalid configuration for action %s: authorization requires a list of authorized roles.",
						action);
				LOGGER.error(msg);
				throw new ComponentException(msg.getFormattedMessage());
			}

			// update the set of actions which require authentication
			if (authentication) {
				this.authentication.add(action);
			}

			if (authorization) {

				// create the set of the authorized roles for the action
				final EnumSet<Role> authRoles = EnumSet.noneOf(Role.class);
				this.authorization.put(action, authRoles);

				for (final String label : parseList(authorizedRoles)) {

					Role r = Role.parse(label);

					if (r == null) {
						final Message msg = new StringFormattedMessage(
								"Invalid configuration for action %s: no role matches with label %s.", action, label);
						LOGGER.error(msg);
						throw new ComponentException(msg.getFormattedMessage());
					}

					authRoles.add(r);

				}
			}

		}
	}


	/**
	 * Shuts down the {@code AccessController}.
	 */
	@Override
	protected final void doStop() {
		authentication.clear();
		authorization.clear();
	}

	/**
	 * Extracts the name of an action from the given property name.
	 *
	 * @param propName the name of the property.
	 *
	 * @return the name of the action.
	 *
	 * @throws ComponentException if it is not possible to extract an action name from the given property name.
	 */
	private static String parseActionName(final String propName) throws ComponentException {
		try {
			return propName.substring(propName.lastIndexOf(".", propName.lastIndexOf(".") - 1) + 1,
					propName.lastIndexOf("."));
		} catch (final IndexOutOfBoundsException e) {
			final Message msg = new StringFormattedMessage("Invalid property name %s.", propName);
			LOGGER.error(msg, e);
			throw new ComponentException(msg.getFormattedMessage(), e);
		}
	}

}
