/*
 * Copyright (c) 2021-2022 University of Padua, Italy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.unipd.dei.aecpkg.resource;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Provides the ISO codes for the representation of names of languages.
 * <p>
 * It follows the ISO standards:
 *
 * <ul>
 *
 * <li><a href="http://www.iso.org/iso/en/CatalogueDetailPage.CatalogueDetail?CSNUMBER=22109"
 * target="_blank">ISO 639-1:2002</a>: Codes for the representation of names of languages -- Part 1: Alpha-2 code;</li>
 *
 * <li><a href="http://www.iso.org/iso/en/CatalogueDetailPage.CatalogueDetail?CSNUMBER=4767" target="_blank">ISO
 * 639-2:1998</a>: Codes for the representation of names of languages -- Part 2: Alpha-3 code.</li>
 *
 * </ul>
 * <p>
 * Please see <a href="http://www.loc.gov/standards/iso639-2/php/code_list.php" target="_blank">Codes for the
 * representation of names of languages maintained by the Library of Congress</a> for a complete and updated list of
 * language codes.
 *
 * @author Nicola Ferro
 * @version 1.00
 * @since 1.00
 */
public enum Language {

	/**
	 * The Afar language.
	 */
	AAR("aar", "aa", "Afar"),

	/**
	 * The Abkhazian language.
	 */
	ABK("abk", "ab", "Abkhazian"),

	/**
	 * The Achinese language.
	 */
	ACE("ace", null, "Achinese"),

	/**
	 * The Acoli language.
	 */
	ACH("ach", null, "Acoli"),

	/**
	 * The Adangme language.
	 */
	ADA("ada", null, "Adangme"),

	/**
	 * The Adyghe; Adygei language.
	 */
	ADY("ady", null, "Adyghe; Adygei"),

	/**
	 * The Afro-Asiatic languages language.
	 */
	AFA("afa", null, "Afro-Asiatic languages"),

	/**
	 * The Afrihili language.
	 */
	AFH("afh", null, "Afrihili"),

	/**
	 * The Afrikaans language.
	 */
	AFR("afr", "af", "Afrikaans"),

	/**
	 * The Ainu language.
	 */
	AIN("ain", null, "Ainu"),

	/**
	 * The Akan language.
	 */
	AKA("aka", "ak", "Akan"),

	/**
	 * The Akkadian language.
	 */
	AKK("akk", null, "Akkadian"),

	/**
	 * The Aleut language.
	 */
	ALE("ale", null, "Aleut"),

	/**
	 * The Algonquian languages language.
	 */
	ALG("alg", null, "Algonquian languages"),

	/**
	 * The Southern Altai language.
	 */
	ALT("alt", null, "Southern Altai"),

	/**
	 * The Amharic language.
	 */
	AMH("amh", "am", "Amharic"),

	/**
	 * The English, Old (ca.450-1100) language.
	 */
	ANG("ang", null, "English, Old (ca.450-1100)"),

	/**
	 * The Angika language.
	 */
	ANP("anp", null, "Angika"),

	/**
	 * The Apache languages language.
	 */
	APA("apa", null, "Apache languages"),

	/**
	 * The Arabic language.
	 */
	ARA("ara", "ar", "Arabic"),

	/**
	 * The Official Aramaic (700-300 BCE); Imperial Aramaic (700-300 BCE) language.
	 */
	ARC("arc", null, "Official Aramaic (700-300 BCE); Imperial Aramaic (700-300 BCE)"),

	/**
	 * The Aragonese language.
	 */
	ARG("arg", "an", "Aragonese"),

	/**
	 * /** The Mapudungun; Mapuche language.
	 */
	ARN("arn", null, "Mapudungun; Mapuche"),

	/**
	 * The Arapaho language.
	 */
	ARP("arp", null, "Arapaho"),

	/**
	 * The Artificial languages language.
	 */
	ART("art", null, "Artificial languages"),

	/**
	 * The Arawak language.
	 */
	ARW("arw", null, "Arawak"),

	/**
	 * The Assamese language.
	 */
	ASM("asm", "as", "Assamese"),

	/**
	 * The Asturian; Bable; Leonese; Asturleonese language.
	 */
	AST("ast", null, "Asturian; Bable; Leonese; Asturleonese"),

	/**
	 * The Athapascan languages language.
	 */
	ATH("ath", null, "Athapascan languages"),

	/**
	 * The Australian languages language.
	 */
	AUS("aus", null, "Australian languages"),

	/**
	 * The Avaric language.
	 */
	AVA("ava", "av", "Avaric"),

	/**
	 * The Avestan language.
	 */
	AVE("ave", "ae", "Avestan"),

	/**
	 * The Awadhi language.
	 */
	AWA("awa", null, "Awadhi"),

	/**
	 * The Aymara language.
	 */
	AYM("aym", "ay", "Aymara"),

	/**
	 * The Azerbaijani language.
	 */
	AZE("aze", "az", "Azerbaijani"),

	/**
	 * The Banda languages language.
	 */
	BAD("bad", null, "Banda languages"),

	/**
	 * The Bamileke languages language.
	 */
	BAI("bai", null, "Bamileke languages"),

	/**
	 * The Bashkir language.
	 */
	BAK("bak", "ba", "Bashkir"),

	/**
	 * The Baluchi language.
	 */
	BAL("bal", null, "Baluchi"),

	/**
	 * The Bambara language.
	 */
	BAM("bam", "bm", "Bambara"),

	/**
	 * The Balinese language.
	 */
	BAN("ban", null, "Balinese"),

	/**
	 * The Basa language.
	 */
	BAS("bas", null, "Basa"),

	/**
	 * The Baltic languages language.
	 */
	BAT("bat", null, "Baltic languages"),

	/**
	 * The Beja; Bedawiyet language.
	 */
	BEJ("bej", null, "Beja; Bedawiyet"),

	/**
	 * The Belarusian language.
	 */
	BEL("bel", "be", "Belarusian"),

	/**
	 * The Bemba language.
	 */
	BEM("bem", null, "Bemba"),

	/**
	 * The Bengali language.
	 */
	BEN("ben", "bn", "Bengali"),

	/**
	 * The Berber languages language.
	 */
	BER("ber", null, "Berber languages"),

	/**
	 * The Bhojpuri language.
	 */
	BHO("bho", null, "Bhojpuri"),

	/**
	 * The Bihari languages language.
	 */
	BIH("bih", "bh", "Bihari languages"),

	/**
	 * The Bikol language.
	 */
	BIK("bik", null, "Bikol"),

	/**
	 * The Bini; Edo language.
	 */
	BIN("bin", null, "Bini; Edo"),

	/**
	 * The Bislama language.
	 */
	BIS("bis", "bi", "Bislama"),

	/**
	 * The Siksika language.
	 */
	BLA("bla", null, "Siksika"),

	/**
	 * The Bantu (Other) language.
	 */
	BNT("bnt", null, "Bantu (Other)"),

	/**
	 * The Tibetan language.
	 */
	BOD("bod", "bo", "Tibetan"),

	/**
	 * The Bosnian language.
	 */
	BOS("bos", "bs", "Bosnian"),

	/**
	 * The Braj language.
	 */
	BRA("bra", null, "Braj"),

	/**
	 * The Breton language.
	 */
	BRE("bre", "br", "Breton"),

	/**
	 * The Batak languages language.
	 */
	BTK("btk", null, "Batak languages"),

	/**
	 * The Buriat language.
	 */
	BUA("bua", null, "Buriat"),

	/**
	 * The Buginese language.
	 */
	BUG("bug", null, "Buginese"),

	/**
	 * The Bulgarian language.
	 */
	BUL("bul", "bg", "Bulgarian"),

	/**
	 * The Blin; Bilin language.
	 */
	BYN("byn", null, "Blin; Bilin"),

	/**
	 * The Caddo language.
	 */
	CAD("cad", null, "Caddo"),

	/**
	 * The Central American Indian languages language.
	 */
	CAI("cai", null, "Central American Indian languages"),

	/**
	 * The Galibi Carib language.
	 */
	CAR("car", null, "Galibi Carib"),

	/**
	 * The Catalan; Valencian language.
	 */
	CAT("cat", "ca", "Catalan; Valencian"),

	/**
	 * The Caucasian languages language.
	 */
	CAU("cau", null, "Caucasian languages"),

	/**
	 * The Cebuano language.
	 */
	CEB("ceb", null, "Cebuano"),

	/**
	 * The Celtic languages language.
	 */
	CEL("cel", null, "Celtic languages"),

	/**
	 * The Czech language.
	 */
	CES("ces", "cs", "Czech"),

	/**
	 * The Chamorro language.
	 */
	CHA("cha", "ch", "Chamorro"),

	/**
	 * The Chibcha language.
	 */
	CHB("chb", null, "Chibcha"),

	/**
	 * The Chechen language.
	 */
	CHE("che", "ce", "Chechen"),

	/**
	 * The Chagatai language.
	 */
	CHG("chg", null, "Chagatai"),

	/**
	 * The Chuukese language.
	 */
	CHK("chk", null, "Chuukese"),

	/**
	 * The Mari language.
	 */
	CHM("chm", null, "Mari"),

	/**
	 * The Chinook jargon language.
	 */
	CHN("chn", null, "Chinook jargon"),

	/**
	 * The Choctaw language.
	 */
	CHO("cho", null, "Choctaw"),

	/**
	 * The Chipewyan; Dene Suline language.
	 */
	CHP("chp", null, "Chipewyan; Dene Suline"),

	/**
	 * The Cherokee language.
	 */
	CHR("chr", null, "Cherokee"),

	/**
	 * The Church Slavic; Old Slavonic; Church Slavonic; Old Bulgarian; Old Church Slavonic language.
	 */
	CHU("chu", "cu", "Church Slavic; Old Slavonic; Church Slavonic; Old Bulgarian; Old Church Slavonic"),

	/**
	 * The Chuvash language.
	 */
	CHV("chv", "cv", "Chuvash"),

	/**
	 * The Cheyenne language.
	 */
	CHY("chy", null, "Cheyenne"),

	/**
	 * The Chamic languages language.
	 */
	CMC("cmc", null, "Chamic languages"),

	/**
	 * The Coptic language.
	 */
	COP("cop", null, "Coptic"),

	/**
	 * The Cornish language.
	 */
	COR("cor", "kw", "Cornish"),

	/**
	 * The Corsican language.
	 */
	COS("cos", "co", "Corsican"),

	/**
	 * The Creoles and pidgins, English based language.
	 */
	CPE("cpe", null, "Creoles and pidgins, English based"),

	/**
	 * The Creoles and pidgins, French-based language.
	 */
	CPF("cpf", null, "Creoles and pidgins, French-based "),

	/**
	 * The Creoles and pidgins, Portuguese-based language.
	 */
	CPP("cpp", null, "Creoles and pidgins, Portuguese-based "),

	/**
	 * The Cree language.
	 */
	CRE("cre", "cr", "Cree"),

	/**
	 * The Crimean Tatar; Crimean Turkish language.
	 */
	CRH("crh", null, "Crimean Tatar; Crimean Turkish"),

	/**
	 * The Creoles and pidgins language.
	 */
	CRP("crp", null, "Creoles and pidgins "),

	/**
	 * The Kashubian language.
	 */
	CSB("csb", null, "Kashubian"),

	/**
	 * The Cushitic languages language.
	 */
	CUS("cus", null, "Cushitic languages"),

	/**
	 * The Welsh language.
	 */
	CYM("cym", "cy", "Welsh"),

	/**
	 * The Dakota language.
	 */
	DAK("dak", null, "Dakota"),

	/**
	 * The Danish language.
	 */
	DAN("dan", "da", "Danish"),

	/**
	 * The Dargwa language.
	 */
	DAR("dar", null, "Dargwa"),

	/**
	 * The Land Dayak languages language.
	 */
	DAY("day", null, "Land Dayak languages"),

	/**
	 * The Delaware language.
	 */
	DEL("del", null, "Delaware"),

	/**
	 * The Slave (Athapascan) language.
	 */
	DEN("den", null, "Slave (Athapascan)"),

	/**
	 * The Dogrib language.
	 */
	DGR("dgr", null, "Dogrib"),

	/**
	 * The Dinka language.
	 */
	DIN("din", null, "Dinka"),

	/**
	 * The Divehi; Dhivehi; Maldivian language.
	 */
	DIV("div", "dv", "Divehi; Dhivehi; Maldivian"),

	/**
	 * The Dogri language.
	 */
	DOI("doi", null, "Dogri"),

	/**
	 * The Dravidian languages language.
	 */
	DRA("dra", null, "Dravidian languages"),

	/**
	 * The Lower Sorbian language.
	 */
	DSB("dsb", null, "Lower Sorbian"),

	/**
	 * The Duala language.
	 */
	DUA("dua", null, "Duala"),

	/**
	 * The Dutch, Middle (ca.1050-1350) language.
	 */
	DUM("dum", null, "Dutch, Middle (ca.1050-1350)"),

	/**
	 * The Dyula language.
	 */
	DYU("dyu", null, "Dyula"),

	/**
	 * The Dzongkha language.
	 */
	DZO("dzo", "dz", "Dzongkha"),

	/**
	 * The Efik language.
	 */
	EFI("efi", null, "Efik"),

	/**
	 * The Egyptian (Ancient) language.
	 */
	EGY("egy", null, "Egyptian (Ancient)"),

	/**
	 * The Ekajuk language.
	 */
	EKA("eka", null, "Ekajuk"),

	/**
	 * The Greek, Modern (1453-) language.
	 */
	ELL("ell", "el", "Greek, Modern (1453-)"),

	/**
	 * The Elamite language.
	 */
	ELX("elx", null, "Elamite"),

	/**
	 * The English language.
	 */
	ENG("eng", "en", "English"),

	/**
	 * The English, Middle (1100-1500) language.
	 */
	ENM("enm", null, "English, Middle (1100-1500)"),

	/**
	 * The Esperanto language.
	 */
	EPO("epo", "eo", "Esperanto"),

	/**
	 * The Estonian language.
	 */
	EST("est", "et", "Estonian"),

	/**
	 * The Basque language.
	 */
	EUS("eus", "eu", "Basque"),

	/**
	 * The Ewe language.
	 */
	EWE("ewe", "ee", "Ewe"),

	/**
	 * The Ewondo language.
	 */
	EWO("ewo", null, "Ewondo"),

	/**
	 * The Fang language.
	 */
	FAN("fan", null, "Fang"),

	/**
	 * The Faroese language.
	 */
	FAO("fao", "fo", "Faroese"),

	/**
	 * The Persian language.
	 */
	FAS("fas", "fa", "Persian"),

	/**
	 * The Fanti language.
	 */
	FAT("fat", null, "Fanti"),

	/**
	 * The Fijian language.
	 */
	FIJ("fij", "fj", "Fijian"),

	/**
	 * The Filipino; Pilipino language.
	 */
	FIL("fil", null, "Filipino; Pilipino"),

	/**
	 * The Finnish language.
	 */
	FIN("fin", "fi", "Finnish"),

	/**
	 * The Finno-Ugrian languages language.
	 */
	FIU("fiu", null, "Finno-Ugrian languages"),

	/**
	 * The Fon language.
	 */
	FON("fon", null, "Fon"),

	/**
	 * The French language.
	 */
	FRE("fre", "fr", "French"),

	/**
	 * The French, Middle (ca.1400-1600) language.
	 */
	FRM("frm", null, "French, Middle (ca.1400-1600)"),

	/**
	 * The French, Old (842-ca.1400) language.
	 */
	FRO("fro", null, "French, Old (842-ca.1400)"),

	/**
	 * The Northern Frisian language.
	 */
	FRR("frr", null, "Northern Frisian"),

	/**
	 * The Eastern Frisian language.
	 */
	FRS("frs", null, "Eastern Frisian"),

	/**
	 * The Western Frisian language.
	 */
	FRY("fry", "fy", "Western Frisian"),

	/**
	 * The Fulah language.
	 */
	FUL("ful", "ff", "Fulah"),

	/**
	 * The Friulian language.
	 */
	FUR("fur", null, "Friulian"),

	/**
	 * The Ga language.
	 */
	GAA("gaa", null, "Ga"),

	/**
	 * The Gayo language.
	 */
	GAY("gay", null, "Gayo"),

	/**
	 * The Gbaya language.
	 */
	GBA("gba", null, "Gbaya"),

	/**
	 * The Germanic languages language.
	 */
	GEM("gem", null, "Germanic languages"),

	/**
	 * The German language.
	 */
	GER("ger", "de", "German"),

	/**
	 * The Geez language.
	 */
	GEZ("gez", null, "Geez"),

	/**
	 * The Gilbertese language.
	 */
	GIL("gil", null, "Gilbertese"),

	/**
	 * The Gaelic; Scottish Gaelic language.
	 */
	GLA("gla", "gd", "Gaelic; Scottish Gaelic"),

	/**
	 * The Irish language.
	 */
	GLE("gle", "ga", "Irish"),

	/**
	 * The Galician language.
	 */
	GLG("glg", "gl", "Galician"),

	/**
	 * The Manx language.
	 */
	GLV("glv", "gv", "Manx"),

	/**
	 * The German, Middle High (ca.1050-1500) language.
	 */
	GMH("gmh", null, "German, Middle High (ca.1050-1500)"),

	/**
	 * The German, Old High (ca.750-1050) language.
	 */
	GOH("goh", null, "German, Old High (ca.750-1050)"),

	/**
	 * The Gondi language.
	 */
	GON("gon", null, "Gondi"),

	/**
	 * The Gorontalo language.
	 */
	GOR("gor", null, "Gorontalo"),

	/**
	 * The Gothic language.
	 */
	GOT("got", null, "Gothic"),

	/**
	 * The Grebo language.
	 */
	GRB("grb", null, "Grebo"),

	/**
	 * The Greek, Ancient (to 1453) language.
	 */
	GRC("grc", null, "Greek, Ancient (to 1453)"),

	/**
	 * The Guarani language.
	 */
	GRN("grn", "gn", "Guarani"),

	/**
	 * The Swiss German; Alemannic; Alsatian language.
	 */
	GSW("gsw", null, "Swiss German; Alemannic; Alsatian"),

	/**
	 * The Gujarati language.
	 */
	GUJ("guj", "gu", "Gujarati"),

	/**
	 * The Gwich'in language.
	 */
	GWI("gwi", null, "Gwich'in"),

	/**
	 * The Haida language.
	 */
	HAI("hai", null, "Haida"),

	/**
	 * The Haitian; Haitian Creole language.
	 */
	HAT("hat", "ht", "Haitian; Haitian Creole"),

	/**
	 * The Hausa language.
	 */
	HAU("hau", "ha", "Hausa"),

	/**
	 * The Hawaiian language.
	 */
	HAW("haw", null, "Hawaiian"),

	/**
	 * The Hebrew language.
	 */
	HEB("heb", "he", "Hebrew"),

	/**
	 * The Herero language.
	 */
	HER("her", "hz", "Herero"),

	/**
	 * The Hiligaynon language.
	 */
	HIL("hil", null, "Hiligaynon"),

	/**
	 * The Himachali languages; Western Pahari languages language.
	 */
	HIM("him", null, "Himachali languages; Western Pahari languages"),

	/**
	 * The Hindi language.
	 */
	HIN("hin", "hi", "Hindi"),

	/**
	 * The Hittite language.
	 */
	HIT("hit", null, "Hittite"),

	/**
	 * The Hmong; Mong language.
	 */
	HMN("hmn", null, "Hmong; Mong"),

	/**
	 * The Hiri Motu language.
	 */
	HMO("hmo", "ho", "Hiri Motu"),

	/**
	 * The Croatian language.
	 */
	HRV("hrv", "hr", "Croatian"),

	/**
	 * The Upper Sorbian language.
	 */
	HSB("hsb", null, "Upper Sorbian"),

	/**
	 * The Hungarian language.
	 */
	HUN("hun", "hu", "Hungarian"),

	/**
	 * The Hupa language.
	 */
	HUP("hup", null, "Hupa"),

	/**
	 * The Armenian language.
	 */
	HYE("hye", "hy", "Armenian"),

	/**
	 * The Iban language.
	 */
	IBA("iba", null, "Iban"),

	/**
	 * The Igbo language.
	 */
	IBO("ibo", "ig", "Igbo"),

	/**
	 * The Ido language.
	 */
	IDO("ido", "io", "Ido"),

	/**
	 * The Sichuan Yi; Nuosu language.
	 */
	III("iii", "ii", "Sichuan Yi; Nuosu"),

	/**
	 * The Ijo languages language.
	 */
	IJO("ijo", null, "Ijo languages"),

	/**
	 * The Inuktitut language.
	 */
	IKU("iku", "iu", "Inuktitut"),

	/**
	 * The Interlingue; Occidental language.
	 */
	ILE("ile", "ie", "Interlingue; Occidental"),

	/**
	 * The Iloko language.
	 */
	ILO("ilo", null, "Iloko"),

	/**
	 * The Interlingua (International Auxiliary Language Association) language.
	 */
	INA("ina", "ia", "Interlingua (International Auxiliary Language Association)"),

	/**
	 * The Indic languages language.
	 */
	INC("inc", null, "Indic languages"),

	/**
	 * The Indonesian language.
	 */
	IND("ind", "id", "Indonesian"),

	/**
	 * The Indo-European languages language.
	 */
	INE("ine", null, "Indo-European languages"),

	/**
	 * The Ingush language.
	 */
	INH("inh", null, "Ingush"),

	/**
	 * The Inupiaq language.
	 */
	IPK("ipk", "ik", "Inupiaq"),

	/**
	 * The Iranian languages language.
	 */
	IRA("ira", null, "Iranian languages"),

	/**
	 * The Iroquoian languages language.
	 */
	IRO("iro", null, "Iroquoian languages"),

	/**
	 * The Icelandic language.
	 */
	ISL("isl", "is", "Icelandic"),

	/**
	 * The Italian language.
	 */
	ITA("ita", "it", "Italian"),

	/**
	 * The Javanese language.
	 */
	JAV("jav", "jv", "Javanese"),

	/**
	 * The Lojban language.
	 */
	JBO("jbo", null, "Lojban"),

	/**
	 * The Japanese language.
	 */
	JPN("jpn", "ja", "Japanese"),

	/**
	 * The Judeo-Persian language.
	 */
	JPR("jpr", null, "Judeo-Persian"),

	/**
	 * The Judeo-Arabic language.
	 */
	JRB("jrb", null, "Judeo-Arabic"),

	/**
	 * The Kara-Kalpak language.
	 */
	KAA("kaa", null, "Kara-Kalpak"),

	/**
	 * The Kabyle language.
	 */
	KAB("kab", null, "Kabyle"),

	/**
	 * The Kachin; Jingpho language.
	 */
	KAC("kac", null, "Kachin; Jingpho"),

	/**
	 * The Kalaallisut; Greenlandic language.
	 */
	KAL("kal", "kl", "Kalaallisut; Greenlandic"),

	/**
	 * The Kamba language.
	 */
	KAM("kam", null, "Kamba"),

	/**
	 * The Kannada language.
	 */
	KAN("kan", "kn", "Kannada"),

	/**
	 * The Karen languages language.
	 */
	KAR("kar", null, "Karen languages"),

	/**
	 * The Kashmiri language.
	 */
	KAS("kas", "ks", "Kashmiri"),

	/**
	 * The Georgian language.
	 */
	KAT("kat", "ka", "Georgian"),

	/**
	 * The Kanuri language.
	 */
	KAU("kau", "kr", "Kanuri"),

	/**
	 * The Kawi language.
	 */
	KAW("kaw", null, "Kawi"),

	/**
	 * The Kazakh language.
	 */
	KAZ("kaz", "kk", "Kazakh"),

	/**
	 * The Kabardian language.
	 */
	KBD("kbd", null, "Kabardian"),

	/**
	 * The Khasi language.
	 */
	KHA("kha", null, "Khasi"),

	/**
	 * The Khoisan languages language.
	 */
	KHI("khi", null, "Khoisan languages"),

	/**
	 * The Central Khmer language.
	 */
	KHM("khm", "km", "Central Khmer"),

	/**
	 * The Khotanese; Sakan language.
	 */
	KHO("kho", null, "Khotanese; Sakan"),

	/**
	 * The Kikuyu; Gikuyu language.
	 */
	KIK("kik", "ki", "Kikuyu; Gikuyu"),

	/**
	 * The Kinyarwanda language.
	 */
	KIN("kin", "rw", "Kinyarwanda"),

	/**
	 * The Kirghiz; Kyrgyz language.
	 */
	KIR("kir", "ky", "Kirghiz; Kyrgyz"),

	/**
	 * The Kimbundu language.
	 */
	KMB("kmb", null, "Kimbundu"),

	/**
	 * The Konkani language.
	 */
	KOK("kok", null, "Konkani"),

	/**
	 * The Komi language.
	 */
	KOM("kom", "kv", "Komi"),

	/**
	 * The Kongo language.
	 */
	KON("kon", "kg", "Kongo"),

	/**
	 * The Korean language.
	 */
	KOR("kor", "ko", "Korean"),

	/**
	 * The Kosraean language.
	 */
	KOS("kos", null, "Kosraean"),

	/**
	 * The Kpelle language.
	 */
	KPE("kpe", null, "Kpelle"),

	/**
	 * The Karachay-Balkar language.
	 */
	KRC("krc", null, "Karachay-Balkar"),

	/**
	 * The Karelian language.
	 */
	KRL("krl", null, "Karelian"),

	/**
	 * The Kru languages language.
	 */
	KRO("kro", null, "Kru languages"),

	/**
	 * The Kurukh language.
	 */
	KRU("kru", null, "Kurukh"),

	/**
	 * The Kuanyama; Kwanyama language.
	 */
	KUA("kua", "kj", "Kuanyama; Kwanyama"),

	/**
	 * The Kumyk language.
	 */
	KUM("kum", null, "Kumyk"),

	/**
	 * The Kurdish language.
	 */
	KUR("kur", "ku", "Kurdish"),

	/**
	 * The Kutenai language.
	 */
	KUT("kut", null, "Kutenai"),

	/**
	 * The Ladino language.
	 */
	LAD("lad", null, "Ladino"),

	/**
	 * The Lahnda language.
	 */
	LAH("lah", null, "Lahnda"),

	/**
	 * The Lamba language.
	 */
	LAM("lam", null, "Lamba"),

	/**
	 * The Lao language.
	 */
	LAO("lao", "lo", "Lao"),

	/**
	 * The Latin language.
	 */
	LAT("lat", "la", "Latin"),

	/**
	 * The Latvian language.
	 */
	LAV("lav", "lv", "Latvian"),

	/**
	 * The Lezghian language.
	 */
	LEZ("lez", null, "Lezghian"),

	/**
	 * The Limburgan; Limburger; Limburgish language.
	 */
	LIM("lim", "li", "Limburgan; Limburger; Limburgish"),

	/**
	 * The Lingala language.
	 */
	LIN("lin", "ln", "Lingala"),

	/**
	 * The Lithuanian language.
	 */
	LIT("lit", "lt", "Lithuanian"),

	/**
	 * The Mongo language.
	 */
	LOL("lol", null, "Mongo"),

	/**
	 * The Lozi language.
	 */
	LOZ("loz", null, "Lozi"),

	/**
	 * The Luxembourgish; Letzeburgesch language.
	 */
	LTZ("ltz", "lb", "Luxembourgish; Letzeburgesch"),

	/**
	 * The Luba-Lulua language.
	 */
	LUA("lua", null, "Luba-Lulua"),

	/**
	 * The Luba-Katanga language.
	 */
	LUB("lub", "lu", "Luba-Katanga"),

	/**
	 * The Ganda language.
	 */
	LUG("lug", "lg", "Ganda"),

	/**
	 * The Luiseno language.
	 */
	LUI("lui", null, "Luiseno"),

	/**
	 * The Lunda language.
	 */
	LUN("lun", null, "Lunda"),

	/**
	 * The Luo (Kenya and Tanzania) language.
	 */
	LUO("luo", null, "Luo (Kenya and Tanzania)"),

	/**
	 * The Lushai language.
	 */
	LUS("lus", null, "Lushai"),

	/**
	 * The Madurese language.
	 */
	MAD("mad", null, "Madurese"),

	/**
	 * The Magahi language.
	 */
	MAG("mag", null, "Magahi"),

	/**
	 * The Marshallese language.
	 */
	MAH("mah", "mh", "Marshallese"),

	/**
	 * The Maithili language.
	 */
	MAI("mai", null, "Maithili"),

	/**
	 * The Makasar language.
	 */
	MAK("mak", null, "Makasar"),

	/**
	 * The Malayalam language.
	 */
	MAL("mal", "ml", "Malayalam"),

	/**
	 * The Mandingo language.
	 */
	MAN("man", null, "Mandingo"),

	/**
	 * The Austronesian languages language.
	 */
	MAP("map", null, "Austronesian languages"),

	/**
	 * The Marathi language.
	 */
	MAR("mar", "mr", "Marathi"),

	/**
	 * The Masai language.
	 */
	MAS("mas", null, "Masai"),

	/**
	 * The Moksha language.
	 */
	MDF("mdf", null, "Moksha"),

	/**
	 * The Mandar language.
	 */
	MDR("mdr", null, "Mandar"),

	/**
	 * The Mende language.
	 */
	MEN("men", null, "Mende"),

	/**
	 * The Irish, Middle (900-1200) language.
	 */
	MGA("mga", null, "Irish, Middle (900-1200)"),

	/**
	 * The Mi'kmaq; Micmac language.
	 */
	MIC("mic", null, "Mi'kmaq; Micmac"),

	/**
	 * The Minangkabau language.
	 */
	MIN("min", null, "Minangkabau"),

	/**
	 * The Uncoded languages language.
	 */
	MIS("mis", null, "Uncoded languages"),

	/**
	 * The Macedonian language.
	 */
	MKD("mkd", "mk", "Macedonian"),

	/**
	 * The Mon-Khmer languages language.
	 */
	MKH("mkh", null, "Mon-Khmer languages"),

	/**
	 * The Malagasy language.
	 */
	MLG("mlg", "mg", "Malagasy"),

	/**
	 * The Maltese language.
	 */
	MLT("mlt", "mt", "Maltese"),

	/**
	 * The Manchu language.
	 */
	MNC("mnc", null, "Manchu"),

	/**
	 * The Manipuri language.
	 */
	MNI("mni", null, "Manipuri"),

	/**
	 * The Manobo languages language.
	 */
	MNO("mno", null, "Manobo languages"),

	/**
	 * The Mohawk language.
	 */
	MOH("moh", null, "Mohawk"),

	/**
	 * The Mongolian language.
	 */
	MON("mon", "mn", "Mongolian"),

	/**
	 * The Mossi language.
	 */
	MOS("mos", null, "Mossi"),

	/**
	 * The Maori language.
	 */
	MRI("mri", "mi", "Maori"),

	/**
	 * The Malay language.
	 */
	MSA("msa", "ms", "Malay"),

	/**
	 * The Multiple languages language.
	 */
	MUL("mul", null, "Multiple languages"),

	/**
	 * The Munda languages language.
	 */
	MUN("mun", null, "Munda languages"),

	/**
	 * The Creek language.
	 */
	MUS("mus", null, "Creek"),

	/**
	 * The Mirandese language.
	 */
	MWL("mwl", null, "Mirandese"),

	/**
	 * The Marwari language.
	 */
	MWR("mwr", null, "Marwari"),

	/**
	 * The Burmese language.
	 */
	MYA("mya", "my", "Burmese"),

	/**
	 * The Mayan languages language.
	 */
	MYN("myn", null, "Mayan languages"),

	/**
	 * The Erzya language.
	 */
	MYV("myv", null, "Erzya"),

	/**
	 * The Nahuatl languages language.
	 */
	NAH("nah", null, "Nahuatl languages"),

	/**
	 * The North American Indian languages language.
	 */
	NAI("nai", null, "North American Indian languages"),

	/**
	 * The Neapolitan language.
	 */
	NAP("nap", null, "Neapolitan"),

	/**
	 * The Nauru language.
	 */
	NAU("nau", "na", "Nauru"),

	/**
	 * The Navajo; Navaho language.
	 */
	NAV("nav", "nv", "Navajo; Navaho"),

	/**
	 * The Ndebele, South; South Ndebele language.
	 */
	NBL("nbl", "nr", "Ndebele, South; South Ndebele"),

	/**
	 * The Ndebele, North; North Ndebele language.
	 */
	NDE("nde", "nd", "Ndebele, North; North Ndebele"),

	/**
	 * The Ndonga language.
	 */
	NDO("ndo", "ng", "Ndonga"),

	/**
	 * The Low German; Low Saxon; German, Low; Saxon, Low language.
	 */
	NDS("nds", null, "Low German; Low Saxon; German, Low; Saxon, Low"),

	/**
	 * The Nepali language.
	 */
	NEP("nep", "ne", "Nepali"),

	/**
	 * The Nepal Bhasa; Newari language.
	 */
	NEW("new", null, "Nepal Bhasa; Newari"),

	/**
	 * The Nias language.
	 */
	NIA("nia", null, "Nias"),

	/**
	 * The Niger-Kordofanian languages language.
	 */
	NIC("nic", null, "Niger-Kordofanian languages"),

	/**
	 * The Niuean language.
	 */
	NIU("niu", null, "Niuean"),

	/**
	 * The Dutch; Flemish language.
	 */
	NLD("nld", "nl", "Dutch; Flemish"),

	/**
	 * The Norwegian Nynorsk; Nynorsk, Norwegian language.
	 */
	NNO("nno", "nn", "Norwegian Nynorsk; Nynorsk, Norwegian"),

	/**
	 * The Bokmål, Norwegian; Norwegian Bokmål language.
	 */
	NOB("nob", "nb", "Bokmål, Norwegian; Norwegian Bokmål"),

	/**
	 * The Nogai language.
	 */
	NOG("nog", null, "Nogai"),

	/**
	 * The Norse, Old language.
	 */
	NON("non", null, "Norse, Old"),

	/**
	 * The Norwegian language.
	 */
	NOR("nor", "no", "Norwegian"),

	/**
	 * The N'Ko language.
	 */
	NQO("nqo", null, "N'Ko"),

	/**
	 * The Pedi; Sepedi; Northern Sotho language.
	 */
	NSO("nso", null, "Pedi; Sepedi; Northern Sotho"),

	/**
	 * The Nubian languages language.
	 */
	NUB("nub", null, "Nubian languages"),

	/**
	 * The Classical Newari; Old Newari; Classical Nepal Bhasa language.
	 */
	NWC("nwc", null, "Classical Newari; Old Newari; Classical Nepal Bhasa"),

	/**
	 * The Chichewa; Chewa; Nyanja language.
	 */
	NYA("nya", "ny", "Chichewa; Chewa; Nyanja"),

	/**
	 * The Nyamwezi language.
	 */
	NYM("nym", null, "Nyamwezi"),

	/**
	 * The Nyankole language.
	 */
	NYN("nyn", null, "Nyankole"),

	/**
	 * The Nyoro language.
	 */
	NYO("nyo", null, "Nyoro"),

	/**
	 * The Nzima language.
	 */
	NZI("nzi", null, "Nzima"),

	/**
	 * The Occitan (post 1500); Provençal language.
	 */
	OCI("oci", "oc", "Occitan (post 1500); Provençal"),

	/**
	 * The Ojibwa language.
	 */
	OJI("oji", "oj", "Ojibwa"),

	/**
	 * The Oriya language.
	 */
	ORI("ori", "or", "Oriya"),

	/**
	 * The Oromo language.
	 */
	ORM("orm", "om", "Oromo"),

	/**
	 * The Osage language.
	 */
	OSA("osa", null, "Osage"),

	/**
	 * The Ossetian; Ossetic language.
	 */
	OSS("oss", "os", "Ossetian; Ossetic"),

	/**
	 * The Turkish, Ottoman (1500-1928) language.
	 */
	OTA("ota", null, "Turkish, Ottoman (1500-1928)"),

	/**
	 * The Otomian languages language.
	 */
	OTO("oto", null, "Otomian languages"),

	/**
	 * The Papuan languages language.
	 */
	PAA("paa", null, "Papuan languages"),

	/**
	 * The Pangasinan language.
	 */
	PAG("pag", null, "Pangasinan"),

	/**
	 * The Pahlavi language.
	 */
	PAL("pal", null, "Pahlavi"),

	/**
	 * The Pampanga; Kapampangan language.
	 */
	PAM("pam", null, "Pampanga; Kapampangan"),

	/**
	 * The Panjabi; Punjabi language.
	 */
	PAN("pan", "pa", "Panjabi; Punjabi"),

	/**
	 * The Papiamento language.
	 */
	PAP("pap", null, "Papiamento"),

	/**
	 * The Palauan language.
	 */
	PAU("pau", null, "Palauan"),

	/**
	 * The Persian, Old (ca.600-400 B.C.) language.
	 */
	PEO("peo", null, "Persian, Old (ca.600-400 B.C.)"),

	/**
	 * The Philippine languages language.
	 */
	PHI("phi", null, "Philippine languages"),

	/**
	 * The Phoenician language.
	 */
	PHN("phn", null, "Phoenician"),

	/**
	 * The Pali language.
	 */
	PLI("pli", "pi", "Pali"),

	/**
	 * The Polish language.
	 */
	POL("pol", "pl", "Polish"),

	/**
	 * The Pohnpeian language.
	 */
	PON("pon", null, "Pohnpeian"),

	/**
	 * The Portuguese language.
	 */
	POR("por", "pt", "Portuguese"),

	/**
	 * The Prakrit languages language.
	 */
	PRA("pra", null, "Prakrit languages"),

	/**
	 * The Provençal, Old (to 1500) language.
	 */
	PRO("pro", null, "Provençal, Old (to 1500)"),

	/**
	 * The Pushto; Pashto language.
	 */
	PUS("pus", "ps", "Pushto; Pashto"),

	/**
	 * The Reserved for local use language.
	 */
	QAA_QTZ("qaa-qtz", null, "Reserved for local use"),

	/**
	 * The Quechua language.
	 */
	QUE("que", "qu", "Quechua"),

	/**
	 * The Rajasthani language.
	 */
	RAJ("raj", null, "Rajasthani"),

	/**
	 * The Rapanui language.
	 */
	RAP("rap", null, "Rapanui"),

	/**
	 * The Rarotongan; Cook Islands Maori language.
	 */
	RAR("rar", null, "Rarotongan; Cook Islands Maori"),

	/**
	 * The Romance languages language.
	 */
	ROA("roa", null, "Romance languages"),

	/**
	 * The Romansh language.
	 */
	ROH("roh", "RM", "Romansh"),

	/**
	 * The Romany language.
	 */
	ROM("rom", null, "Romany"),

	/**
	 * The Romanian; Moldavian; Moldovan language.
	 */
	RON("ron", "ro", "Romanian; Moldavian; Moldovan"),

	/**
	 * The Rundi language.
	 */
	RUN("run", "rn", "Rundi"),

	/**
	 * The Aromanian; Arumanian; Macedo-Romanian language.
	 */
	RUP("rup", null, "Aromanian; Arumanian; Macedo-Romanian"),

	/**
	 * The Russian language.
	 */
	RUS("rus", "ru", "Russian"),

	/**
	 * The Sandawe language.
	 */
	SAD("sad", null, "Sandawe"),

	/**
	 * The Sango language.
	 */
	SAG("sag", "sg", "Sango"),

	/**
	 * The Yakut language.
	 */
	SAH("sah", null, "Yakut"),

	/**
	 * The South American Indian (Other) language.
	 */
	SAI("sai", null, "South American Indian (Other)"),

	/**
	 * The Salishan languages language.
	 */
	SAL("sal", null, "Salishan languages"),

	/**
	 * The Samaritan Aramaic language.
	 */
	SAM("sam", null, "Samaritan Aramaic"),

	/**
	 * The Sanskrit language.
	 */
	SAN("san", "sa", "Sanskrit"),

	/**
	 * The Sasak language.
	 */
	SAS("sas", null, "Sasak"),

	/**
	 * The Santali language.
	 */
	SAT("sat", null, "Santali"),

	/**
	 * The Sicilian language.
	 */
	SCN("scn", null, "Sicilian"),

	/**
	 * The Scots language.
	 */
	SCO("sco", null, "Scots"),

	/**
	 * The Selkup language.
	 */
	SEL("sel", null, "Selkup"),

	/**
	 * The Semitic languages language.
	 */
	SEM("sem", null, "Semitic languages"),

	/**
	 * The Irish, Old (to 900) language.
	 */
	SGA("sga", null, "Irish, Old (to 900)"),

	/**
	 * The Sign Languages language.
	 */
	SGN("sgn", null, "Sign Languages"),

	/**
	 * The Shan language.
	 */
	SHN("shn", null, "Shan"),

	/**
	 * The Sidamo language.
	 */
	SID("sid", null, "Sidamo"),

	/**
	 * The Sinhala; Sinhalese language.
	 */
	SIN("sin", "si", "Sinhala; Sinhalese"),

	/**
	 * The Siouan languages language.
	 */
	SIO("sio", null, "Siouan languages"),

	/**
	 * The Sino-Tibetan languages language.
	 */
	SIT("sit", null, "Sino-Tibetan languages"),

	/**
	 * The Slavic languages language.
	 */
	SLA("sla", null, "Slavic languages"),

	/**
	 * The Slovak language.
	 */
	SLK("slk", "sk", "Slovak"),

	/**
	 * The Slovenian language.
	 */
	SLV("slv", "sl", "Slovenian"),

	/**
	 * The Southern Sami language.
	 */
	SMA("sma", null, "Southern Sami"),

	/**
	 * The Northern Sami language.
	 */
	SME("sme", "se", "Northern Sami"),

	/**
	 * The Sami languages language.
	 */
	SMI("smi", null, "Sami languages"),

	/**
	 * The Lule Sami language.
	 */
	SMJ("smj", null, "Lule Sami"),

	/**
	 * The Inari Sami language.
	 */
	SMN("smn", null, "Inari Sami"),

	/**
	 * The Samoan language.
	 */
	SMO("smo", "sm", "Samoan"),

	/**
	 * The Skolt Sami language.
	 */
	SMS("sms", null, "Skolt Sami"),

	/**
	 * The Shona language.
	 */
	SNA("sna", "sn", "Shona"),

	/**
	 * The Sindhi language.
	 */
	SND("snd", "sd", "Sindhi"),

	/**
	 * The Soninke language.
	 */
	SNK("snk", null, "Soninke"),

	/**
	 * The Sogdian language.
	 */
	SOG("sog", null, "Sogdian"),

	/**
	 * The Somali language.
	 */
	SOM("som", "so", "Somali"),

	/**
	 * The Songhai languages language.
	 */
	SON("son", null, "Songhai languages"),

	/**
	 * The Sotho, Southern language.
	 */
	SOT("sot", "st", "Sotho, Southern"),

	/**
	 * The Spanish; Castilian language.
	 */
	SPA("spa", "es", "Spanish; Castilian"),

	/**
	 * The Albanian language.
	 */
	SQI("sqi", "sq", "Albanian"),

	/**
	 * The Sardinian language.
	 */
	SRD("srd", "sc", "Sardinian"),

	/**
	 * The Sranan Tongo language.
	 */
	SRN("srn", null, "Sranan Tongo"),

	/**
	 * The Serbian language.
	 */
	SRP("srp", "sr", "Serbian"),

	/**
	 * The Serer language.
	 */
	SRR("srr", null, "Serer"),

	/**
	 * The Nilo-Saharan languages language.
	 */
	SSA("ssa", null, "Nilo-Saharan languages"),

	/**
	 * The Swati language.
	 */
	SSW("ssw", "ss", "Swati"),

	/**
	 * The Sukuma language.
	 */
	SUK("suk", null, "Sukuma"),

	/**
	 * The Sundanese language.
	 */
	SUN("sun", "su", "Sundanese"),

	/**
	 * The Susu language.
	 */
	SUS("sus", null, "Susu"),

	/**
	 * The Sumerian language.
	 */
	SUX("sux", null, "Sumerian"),

	/**
	 * The Swahili language.
	 */
	SWA("swa", "sw", "Swahili"),

	/**
	 * The Swedish language.
	 */
	SWE("swe", "sv", "Swedish"),

	/**
	 * The Classical Syriac language.
	 */
	SYC("syc", null, "Classical Syriac"),

	/**
	 * The Syriac language.
	 */
	SYR("syr", null, "Syriac"),

	/**
	 * The Tahitian language.
	 */
	TAH("tah", "ty", "Tahitian"),

	/**
	 * The Tai languages language.
	 */
	TAI("tai", null, "Tai languages"),

	/**
	 * The Tamil language.
	 */
	TAM("tam", "ta", "Tamil"),

	/**
	 * The Tatar language.
	 */
	TAT("tat", "tt", "Tatar"),

	/**
	 * The Telugu language.
	 */
	TEL("tel", "te", "Telugu"),

	/**
	 * The Timne language.
	 */
	TEM("tem", null, "Timne"),

	/**
	 * The Tereno language.
	 */
	TER("ter", null, "Tereno"),

	/**
	 * The Tetum language.
	 */
	TET("tet", null, "Tetum"),

	/**
	 * The Tajik language.
	 */
	TGK("tgk", "tg", "Tajik"),

	/**
	 * The Tagalog language.
	 */
	TGL("tgl", "tl", "Tagalog"),

	/**
	 * The Thai language.
	 */
	THA("tha", "th", "Thai"),

	/**
	 * The Tigre language.
	 */
	TIG("tig", null, "Tigre"),

	/**
	 * The Tigrinya language.
	 */
	TIR("tir", "ti", "Tigrinya"),

	/**
	 * The Tiv language.
	 */
	TIV("tiv", null, "Tiv"),

	/**
	 * The Tokelau language.
	 */
	TKL("tkl", null, "Tokelau"),

	/**
	 * The Klingon; tlhIngan-Hol language.
	 */
	TLH("tlh", null, "Klingon; tlhIngan-Hol"),

	/**
	 * The Tlingit language.
	 */
	TLI("tli", null, "Tlingit"),

	/**
	 * The Tamashek language.
	 */
	TMH("tmh", null, "Tamashek"),

	/**
	 * The Tonga (Nyasa) language.
	 */
	TOG("tog", null, "Tonga (Nyasa)"),

	/**
	 * The Tonga (Tonga Islands) language.
	 */
	TON("ton", "to", "Tonga (Tonga Islands)"),

	/**
	 * The Tok Pisin language.
	 */
	TPI("tpi", null, "Tok Pisin"),

	/**
	 * The Tsimshian language.
	 */
	TSI("tsi", null, "Tsimshian"),

	/**
	 * The Tswana language.
	 */
	TSN("tsn", "tn", "Tswana"),

	/**
	 * The Tsonga language.
	 */
	TSO("tso", "ts", "Tsonga"),

	/**
	 * The Turkmen language.
	 */
	TUK("tuk", "tk", "Turkmen"),

	/**
	 * The Tumbuka language.
	 */
	TUM("tum", null, "Tumbuka"),

	/**
	 * The Tupi languages language.
	 */
	TUP("tup", null, "Tupi languages"),

	/**
	 * The Turkish language.
	 */
	TUR("tur", "tr", "Turkish"),

	/**
	 * The Altaic languages language.
	 */
	TUT("tut", null, "Altaic languages"),

	/**
	 * The Tuvalu language.
	 */
	TVL("tvl", null, "Tuvalu"),

	/**
	 * The Twi language.
	 */
	TWI("twi", "tw", "Twi"),

	/**
	 * The Tuvinian language.
	 */
	TYV("tyv", null, "Tuvinian"),

	/**
	 * The Udmurt language.
	 */
	UDM("udm", null, "Udmurt"),

	/**
	 * The Ugaritic language.
	 */
	UGA("uga", null, "Ugaritic"),

	/**
	 * The Uighur; Uyghur language.
	 */
	UIG("uig", "ug", "Uighur; Uyghur"),

	/**
	 * The Ukrainian language.
	 */
	UKR("ukr", "uk", "Ukrainian"),

	/**
	 * The Umbundu language.
	 */
	UMB("umb", null, "Umbundu"),

	/**
	 * The Undetermined language.
	 */
	UND("und", null, "Undetermined"),

	/**
	 * The Urdu language.
	 */
	URD("urd", "ur", "Urdu"),

	/**
	 * The Uzbek language.
	 */
	UZB("uzb", "uz", "Uzbek"),

	/**
	 * The Vai language.
	 */
	VAI("vai", null, "Vai"),

	/**
	 * The Venda language.
	 */
	VEN("ven", "ve", "Venda"),

	/**
	 * The Vietnamese language.
	 */
	VIE("vie", "vi", "Vietnamese"),

	/**
	 * The Volapük language.
	 */
	VOL("vol", "vo", "Volapük"),

	/**
	 * The Votic language.
	 */
	VOT("vot", null, "Votic"),

	/**
	 * The Wakashan languages language.
	 */
	WAK("wak", null, "Wakashan languages"),

	/**
	 * The Walamo language.
	 */
	WAL("wal", null, "Walamo"),

	/**
	 * The Waray language.
	 */
	WAR("war", null, "Waray"),

	/**
	 * The Washo language.
	 */
	WAS("was", null, "Washo"),

	/**
	 * The Sorbian languages language.
	 */
	WEN("wen", null, "Sorbian languages"),

	/**
	 * The Walloon language.
	 */
	WLN("wln", "wa", "Walloon"),

	/**
	 * The Wolof language.
	 */
	WOL("wol", "wo", "Wolof"),

	/**
	 * The Kalmyk; Oirat language.
	 */
	XAL("xal", null, "Kalmyk; Oirat"),

	/**
	 * The Xhosa language.
	 */
	XHO("xho", "xh", "Xhosa"),

	/**
	 * The Yao language.
	 */
	YAO("yao", null, "Yao"),

	/**
	 * The Yapese language.
	 */
	YAP("yap", null, "Yapese"),

	/**
	 * The Yiddish language.
	 */
	YID("yid", "yi", "Yiddish"),

	/**
	 * The Yoruba language.
	 */
	YOR("yor", "yo", "Yoruba"),

	/**
	 * The Yupik languages language.
	 */
	YPK("ypk", null, "Yupik languages"),

	/**
	 * The Zapotec language.
	 */
	ZAP("zap", null, "Zapotec"),

	/**
	 * The Blissymbols; Blissymbolics; Bliss language.
	 */
	ZBL("zbl", null, "Blissymbols; Blissymbolics; Bliss"),

	/**
	 * The Zenaga language.
	 */
	ZEN("zen", null, "Zenaga"),

	/**
	 * The Zhuang; Chuang language.
	 */
	ZHA("zha", "za", "Zhuang; Chuang"),

	/**
	 * The Chinese language.
	 */
	ZHO("zho", "zh", "Chinese"),

	/**
	 * The Zande languages language.
	 */
	ZND("znd", null, "Zande languages"),

	/**
	 * The Zulu language.
	 */
	ZUL("zul", "zu", "Zulu"),

	/**
	 * The Zuni language.
	 */
	ZUN("zun", null, "Zuni"),

	/**
	 * The No linguistic content; Not applicable language.
	 */
	ZXX("zxx", null, "No linguistic content; Not applicable"),

	/**
	 * The Zaza; Dimili; Dimli; Kirdki; Kirmanjki; Zazaki language.
	 */
	ZZA("zza", null, "Zaza; Dimili; Dimli; Kirdki; Kirmanjki; Zazaki"),
	;

	/**
	 * Contains the mapping between two letters codes and Languages.
	 */
	private static final Map<String, Language> TWO_LETTERS_CODES;

	/**
	 * Contains the mapping between three letters codes and Languages.
	 */
	private static final Map<String, Language> THREE_LETTERS_CODES;

	/**
	 * Contains the mapping between full language names and Languages.
	 */
	private static final Map<String, Language> FULL_NAMES;


	static {


		TWO_LETTERS_CODES = new HashMap<>();
		THREE_LETTERS_CODES = new HashMap<>();
		FULL_NAMES = new HashMap<>();


		Arrays.stream(Language.values()).forEach(l -> {
			THREE_LETTERS_CODES.put(l.threeLettersCode.trim().toLowerCase(), l);
			FULL_NAMES.put(l.name.trim().toLowerCase(), l);

			if (l.twoLettersCode != null) {
				TWO_LETTERS_CODES.put(l.twoLettersCode.trim().toLowerCase(), l);
			}
		});

		// Fix synonyms for Albanian
		THREE_LETTERS_CODES.put("alb", Language.SQI);

		// Fix synonyms for Armenian
		THREE_LETTERS_CODES.put("arm", Language.HYE);

		// Fix synonyms for Basque
		THREE_LETTERS_CODES.put("baq", Language.EUS);

		// Fix synonyms for Tibetan
		THREE_LETTERS_CODES.put("tib", Language.BOD);

		// Fix synonyms for Burmese
		THREE_LETTERS_CODES.put("bur", Language.MYA);

		// Fix synonyms for Czech
		THREE_LETTERS_CODES.put("cze", Language.CES);

		// Fix synonyms for Chinese
		THREE_LETTERS_CODES.put("chi", Language.ZHO);

		// Fix synonyms for Welsh
		THREE_LETTERS_CODES.put("wel", Language.CYM);

		// Fix synonyms for German
		THREE_LETTERS_CODES.put("deu", Language.GER);

		// Fix synonyms for Dutch; Flemish
		THREE_LETTERS_CODES.put("dut", Language.NLD);

		// Fix synonyms for Greek, Modern (1453-)
		THREE_LETTERS_CODES.put("gre", Language.ELL);

		// Fix synonyms for Persian
		THREE_LETTERS_CODES.put("per", Language.FAS);

		// Fix synonyms for French
		THREE_LETTERS_CODES.put("fra", Language.FRE);

		// Fix synonyms for Georgian
		THREE_LETTERS_CODES.put("geo", Language.KAT);

		// Fix synonyms for Icelandic
		THREE_LETTERS_CODES.put("ice", Language.ISL);

		// Fix synonyms for Macedonian
		THREE_LETTERS_CODES.put("mac", Language.MKD);

		// Fix synonyms for Maori
		THREE_LETTERS_CODES.put("mao", Language.MRI);

		// Fix synonyms for Malay
		THREE_LETTERS_CODES.put("may", Language.MSA);

		// Fix synonyms for Romanian; Moldavian; Moldovan
		THREE_LETTERS_CODES.put("rum", Language.RON);

		// Fix synonyms for Slovak
		THREE_LETTERS_CODES.put("slo", Language.SLK);


	}

	/**
	 * Three letters language code.
	 */
	private final String threeLettersCode;

	/**
	 * Two letters language code.
	 */
	private final String twoLettersCode;

	/**
	 * The full name of the language.
	 */
	private final String name;

	/**
	 * Constructs a new language.
	 *
	 * @param threeLettersCode the three letters language code according to ISO 639-2 / ISO 639-3 / ISO 639-5.
	 * @param twoLettersCode   the two letters language code according to ISO 639-1.
	 * @param name             the full name of the language.
	 */
	Language(final String threeLettersCode, final String twoLettersCode, final String name) {
		this.threeLettersCode = threeLettersCode;
		this.twoLettersCode = twoLettersCode;
		this.name = name;
	}

	/**
	 * Returns the three letters code for this language according to ISO 639-2 / ISO 639-3 / ISO 639-5.
	 *
	 * @return the three letters code.
	 */
	public String getThreeLettersCode() {
		return threeLettersCode;
	}

	/**
	 * Returns the two letters code for this language according to ISO 639-1.
	 *
	 * @return the two letters code, if any, or {@code null} otherwise.
	 */
	public String getTwoLettersCode() {
		return twoLettersCode;
	}

	/**
	 * Returns the name of this language.
	 *
	 * @return the name of this language.
	 */
	public String getName() {
		return name;
	}


	/**
	 * Parses the two letters code associated to a language and returns the corresponding object.
	 *
	 * @param code the two letters code for the language.
	 *
	 * @return the {@code Language} object corresponding to the {@code code}, if any, or {@code null} otherwise.
	 */
	public static Language parseTwoLettersCode(final String code) {
		return ((code == null) || code.isBlank()) ? null : TWO_LETTERS_CODES.get(code.trim().toLowerCase());
	}

	/**
	 * Parses the three letters code associated to a language and returns the corresponding object.
	 *
	 * @param code the three letters code for the language.
	 *
	 * @return the {@code Language} object corresponding to the {@code code}, if any, or {@code null} otherwise.
	 */
	public static Language parseThreeLettersCode(final String code) {
		return ((code == null) || code.isBlank()) ? null : THREE_LETTERS_CODES.get(code.trim().toLowerCase());
	}

	/**
	 * Parses the full name associated to a language and returns the corresponding object.
	 *
	 * @param name the full name for the language.
	 *
	 * @return the {@code Language} object corresponding to the {@code name}, if any, or {@code null} otherwise.
	 */
	public static Language parseName(final String name) {

		if (name == null || name.isBlank()) {
			return null;
		}

		final String s = name.trim().toLowerCase();

		// short path: we have an exact match
		if (FULL_NAMES.containsKey(s)) {
			return FULL_NAMES.get(s);
		}

		// long path: look for the first approximate match
		return Arrays.stream(Language.values()).filter(l -> l.getName().contains(s) || s.contains(l.getName()))
				.findFirst().orElse(null);
	}

	/**
	 * Parses the given string representation of a {@code Language}.
	 *
	 * @param language the string representation of a language.
	 *
	 * @return the corresponding {@code Language}, if any, or {@code null} otherwise.
	 */
	public static Language parse(final String language) {

		if (language == null || language.isBlank()) {
			return null;
		}

		// try the two letters code
		Language l = Language.parseTwoLettersCode(language);
		if (l != null) {
			return l;
		}

		// if not recognized, try the three letters code
		l = Language.parseThreeLettersCode(language);
		if (l != null) {
			return l;
		}

		// if still not recognized, try the full name.
		l = Language.parseName(language);
		if (l != null) {
			return l;
		}

		// if still not recognized, try with the first two letters
		if (language.length() > 2) {
			l = Language.parseTwoLettersCode(language.substring(0, 1));
			if (l != null) {
				return l;
			}
		}

		// if still not recognized, try with the first three letters
		if (language.length() > 3) {
			l = Language.parseThreeLettersCode(language.substring(0, 2));
		}

		return l;
	}


}
