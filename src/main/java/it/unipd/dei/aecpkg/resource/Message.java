/*
 * Copyright (c) 2021-2022 University of Padua, Italy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.unipd.dei.aecpkg.resource;

import it.unipd.dei.aecpkg.resource.implementation.MessageResource;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.message.StringFormattedMessage;
import org.apache.logging.log4j.message.StringFormatterMessageFactory;

import java.time.OffsetDateTime;

/**
 * Represents a message in the system. It could be an error message.
 *
 * @author Nicola Ferro
 * @version 1.00
 * @since 1.00
 */
public interface Message extends Resource<Message> {

	/**
	 * Returns the description of the message.
	 *
	 * @return the description of the message.
	 */
	String getDescription();

	/**
	 * Indicates whether the message is about an error or not.
	 *
	 * @return {@code true} if the message is about an error, {@code false} otherwise.
	 */
	boolean isError();

	/**
	 * Returns the unique code of the error, only in the case of error messages.
	 *
	 * @return the unique code of the error, if any, or {@code null} otherwise.
	 */
	String getCode();

	/**
	 * Returns the unique type of the error, only in the case of error messages.
	 *
	 * @return the unique type of the error, if any, or {@code null} otherwise.
	 */
	String getType();

	/**
	 * Creates a new {@code Message} with a type 4 random UUID identifier and {@link OffsetDateTime#now()} as creation
	 * and timestamp.
	 *
	 * @param description the description of the  message.
	 *
	 * @return the new {@code Message}.
	 *
	 * @throws NullPointerException     if {@code description} is {@code null}.
	 * @throws IllegalArgumentException if {@code description} is empty.
	 */
	static Message create(final String description) {
		return new MessageResource(description);
	}

	/**
	 * Creates a new error {@code Message} with a type 4 random UUID identifier and {@link OffsetDateTime#now()} as
	 * creation and timestamp.
	 *
	 * @param description the description of the message.
	 * @param code        the unique code of the error.
	 * @param type        the unique type of the error, only in the case of error messages.
	 *
	 * @return the new {@code Message}.
	 *
	 * @throws NullPointerException     if {@code description} or {@code code} or {@code type} are {@code null}.
	 * @throws IllegalArgumentException if {@code description} or {@code code} or {@code type} are empty.
	 */
	static Message create(final String description, final String code, final String type) {
		return new MessageResource(description, code, type);
	}

	/**
	 * Creates a new error {@code Message} with a type 4 random UUID identifier and {@link OffsetDateTime#now()} as
	 * creation and timestamp.
	 *
	 * @param description the description of the message.
	 * @param cet         the information about one of the standard {@code CommonErrorTypes}.
	 *
	 * @return the new {@code Message}.
	 *
	 * @throws NullPointerException     if {@code description} or {@code cet} are {@code null}.
	 * @throws IllegalArgumentException if {@code description} is empty.
	 */
	static Message create(final String description, final CommonErrorTypes cet) {

		if (cet == null) {
			final Logger logger = LogManager.getLogger(Message.class, StringFormatterMessageFactory.INSTANCE);

			final org.apache.logging.log4j.message.Message msg = new StringFormattedMessage(
					"CommonErrorType cannot be null when creating an error message; optional description %s.",
					description);

			logger.error(msg);

			throw new NullPointerException(msg.getFormattedMessage());
		}

		return new MessageResource(description, cet.getCode(), cet.getType());
	}

	/**
	 * Creates a new error {@code Message} with a type 4 random UUID identifier and {@link OffsetDateTime#now()} as
	 * creation and timestamp.
	 *
	 * @param cet the information about one of the standard {@code CommonErrorTypes}.
	 *
	 * @return the new {@code Message}.
	 *
	 * @throws NullPointerException if {@code cet} is {@code null}.
	 */
	static Message create(final CommonErrorTypes cet) {

		if (cet == null) {
			final Logger logger = LogManager.getLogger(Message.class, StringFormatterMessageFactory.INSTANCE);

			final org.apache.logging.log4j.message.Message msg = new StringFormattedMessage(
					"CommonErrorType cannot be null when creating an error message.");

			logger.error(msg);

			throw new NullPointerException(msg.getFormattedMessage());
		}

		return new MessageResource(cet.getType(), cet.getCode(), cet.getType());
	}


}
