/*
 * Copyright (c) 2021-2022 University of Padua, Italy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.unipd.dei.aecpkg.resource;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;


/**
 * Provides the ISO codes for the representation of names of countries.
 *
 * It follows the ISO standard:
 *
 * <ul>
 *
 * <li><a href="http://www.iso.org/iso/home/store/catalogue_ics/catalogue_detail_ics.htm?csnumber=63545"
 * target="_blank">ISO 3166-1:2013</a>: Codes for the representation of names of countries and their subdivisions --
 * Part 1: Country codes.</li>
 *
 * </ul>
 *
 * Please see <a href="http://www.iso.org/iso/publication_item.html?pid=PUB500001:en" target="_blank"> ISO 3166
 * Maintenance agency</a> for a complete and updated list of country codes.
 *
 * @author Nicola Ferro
 * @version 1.00
 * @since 1.00
 */
public enum Country {

	/**
	 * The Afghanistan country.
	 */
	AFG("AFG", "AF", "Afghanistan"),

	/**
	 * The Angola country.
	 */
	AGO("AGO", "AO", "Angola"),

	/**
	 * The Albania country.
	 */
	ALB("ALB", "AL", "Albania"),

	/**
	 * The Andorra country.
	 */
	AND("AND", "AD", "Andorra"),

	/**
	 * The United Arab Emirates country.
	 */
	ARE("ARE", "AE", "United Arab Emirates"),

	/**
	 * The Argentina country.
	 */
	ARG("ARG", "AR", "Argentina"),

	/**
	 * The Armenia country.
	 */
	ARM("ARM", "AM", "Armenia"),

	/**
	 * The French Southern Territories country.
	 */
	ATF("ATF", "TF", "French Southern Territories"),

	/**
	 * The Antigua and Barbuda country.
	 */
	ATG("ATG", "AG", "Antigua and Barbuda"),

	/**
	 * The Australia country.
	 */
	AUS("AUS", "AU", "Australia"),

	/**
	 * The Austria country.
	 */
	AUT("AUT", "AT", "Austria"),

	/**
	 * The Azerbaijan country.
	 */
	AZE("AZE", "AZ", "Azerbaijan"),

	/**
	 * The Burundi country.
	 */
	BDI("BDI", "BI", "Burundi"),

	/**
	 * The Belgium country.
	 */
	BEL("BEL", "BE", "Belgium"),

	/**
	 * The Benin country.
	 */
	BEN("BEN", "BJ", "Benin"),

	/**
	 * The Burkina Faso country.
	 */
	BFA("BFA", "BF", "Burkina Faso"),

	/**
	 * The Bangladesh country.
	 */
	BGD("BGD", "BD", "Bangladesh"),

	/**
	 * The Bulgaria country.
	 */
	BGR("BGR", "BG", "Bulgaria"),

	/**
	 * The Bahrain country.
	 */
	BHR("BHR", "BH", "Bahrain"),

	/**
	 * The Bahamas country.
	 */
	BHS("BHS", "BS", "Bahamas"),

	/**
	 * The Bosnia and Herzegovina country.
	 */
	BIH("BIH", "BA", "Bosnia and Herzegovina"),

	/**
	 * The Belarus country.
	 */
	BLR("BLR", "BY", "Belarus"),

	/**
	 * The Belize country.
	 */
	BLZ("BLZ", "BZ", "Belize"),

	/**
	 * The Bolivia, Plurinational State Of country.
	 */
	BOL("BOL", "BO", "Bolivia, Plurinational State Of"),

	/**
	 * The Brazil country.
	 */
	BRA("BRA", "BR", "Brazil"),

	/**
	 * The Barbados country.
	 */
	BRB("BRB", "BB", "Barbados"),

	/**
	 * The Brunei Darussalam country.
	 */
	BRN("BRN", "BN", "Brunei Darussalam"),

	/**
	 * The Bhutan country.
	 */
	BTN("BTN", "BT", "Bhutan"),

	/**
	 * The Botswana country.
	 */
	BWA("BWA", "BW", "Botswana"),

	/**
	 * The Central African Republic country.
	 */
	CAF("CAF", "CF", "Central African Republic"),

	/**
	 * The Canada country.
	 */
	CAN("CAN", "CA", "Canada"),

	/**
	 * The Switzerland country.
	 */
	CHE("CHE", "CH", "Switzerland"),

	/**
	 * The Chile country.
	 */
	CHL("CHL", "CL", "Chile"),

	/**
	 * The China country.
	 */
	CHN("CHN", "CN", "China"),

	/**
	 * The Cote D'Ivoire country.
	 */
	CIV("CIV", "CI", "Cote D'Ivoire"),

	/**
	 * The Cameroon country.
	 */
	CMR("CMR", "CM", "Cameroon"),

	/**
	 * The Congo, The Democratic Republic Of The country.
	 */
	COD("COD", "CD", "Congo, The Democratic Republic Of The"),

	/**
	 * The Congo country.
	 */
	COG("COG", "CG", "Congo"),

	/**
	 * The Colombia country.
	 */
	COL("COL", "CO", "Colombia"),

	/**
	 * The Comoros country.
	 */
	COM("COM", "KM", "Comoros"),

	/**
	 * The Cape Verde country.
	 */
	CPV("CPV", "CV", "Cape Verde"),

	/**
	 * The Costa Rica country.
	 */
	CRI("CRI", "CR", "Costa Rica"),

	/**
	 * The Cuba country.
	 */
	CUB("CUB", "CU", "Cuba"),

	/**
	 * The Cayman Islands country.
	 */
	CYM("CYM", "KY", "Cayman Islands"),

	/**
	 * The Cyprus country.
	 */
	CYP("CYP", "CY", "Cyprus"),

	/**
	 * The Czech Republic country.
	 */
	CZE("CZE", "CZ", "Czech Republic"),

	/**
	 * The Germany country.
	 */
	DEU("DEU", "DE", "Germany"),

	/**
	 * The Djibouti country.
	 */
	DJI("DJI", "DJ", "Djibouti"),

	/**
	 * The Dominica country.
	 */
	DMA("DMA", "DM", "Dominica"),

	/**
	 * The Denmark country.
	 */
	DNK("DNK", "DK", "Denmark"),

	/**
	 * The Dominican Republic country.
	 */
	DOM("DOM", "DO", "Dominican Republic"),

	/**
	 * The Algeria country.
	 */
	DZA("DZA", "DZ", "Algeria"),

	/**
	 * The Ecuador country.
	 */
	ECU("ECU", "EC", "Ecuador"),

	/**
	 * The Egypt country.
	 */
	EGY("EGY", "EG", "Egypt"),

	/**
	 * The Eritrea country.
	 */
	ERI("ERI", "ER", "Eritrea"),

	/**
	 * The Western Sahara country.
	 */
	ESH("ESH", "EH", "Western Sahara"),

	/**
	 * The Spain country.
	 */
	ESP("ESP", "ES", "Spain"),

	/**
	 * The Estonia country.
	 */
	EST("EST", "EE", "Estonia"),

	/**
	 * The Ethiopia country.
	 */
	ETH("ETH", "ET", "Ethiopia"),

	/**
	 * The Finland country.
	 */
	FIN("FIN", "FI", "Finland"),

	/**
	 * The Fiji country.
	 */
	FJI("FJI", "FJ", "Fiji"),

	/**
	 * The France country.
	 */
	FRA("FRA", "FR", "France"),

	/**
	 * The Micronesia, Federated States Of country.
	 */
	FSM("FSM", "FM", "Micronesia, Federated States Of"),

	/**
	 * The Gabon country.
	 */
	GAB("GAB", "GA", "Gabon"),

	/**
	 * The United Kingdom country.
	 */
	GBR("GBR", "GB", "United Kingdom"),

	/**
	 * The Georgia country.
	 */
	GEO("GEO", "GE", "Georgia"),

	/**
	 * The Ghana country.
	 */
	GHA("GHA", "GH", "Ghana"),

	/**
	 * The Guinea country.
	 */
	GIN("GIN", "GN", "Guinea"),

	/**
	 * The Gambia country.
	 */
	GMB("GMB", "GM", "Gambia"),

	/**
	 * The Guinea-Bissau country.
	 */
	GNB("GNB", "GW", "Guinea-Bissau"),

	/**
	 * The Equatorial Guinea country.
	 */
	GNQ("GNQ", "GQ", "Equatorial Guinea"),

	/**
	 * The Greece country.
	 */
	GRC("GRC", "GR", "Greece"),

	/**
	 * The Grenada country.
	 */
	GRD("GRD", "GD", "Grenada"),

	/**
	 * The Greenland country.
	 */
	GRL("GRL", "GL", "Greenland"),

	/**
	 * The Guatemala country.
	 */
	GTM("GTM", "GT", "Guatemala"),

	/**
	 * The Guyana country.
	 */
	GUY("GUY", "GY", "Guyana"),

	/**
	 * The Honduras country.
	 */
	HND("HND", "HN", "Honduras"),

	/**
	 * The Croatia country.
	 */
	HRV("HRV", "HR", "Croatia"),

	/**
	 * The Haiti country.
	 */
	HTI("HTI", "HT", "Haiti"),

	/**
	 * The Hungary country.
	 */
	HUN("HUN", "HU", "Hungary"),

	/**
	 * The Indonesia country.
	 */
	IDN("IDN", "ID", "Indonesia"),

	/**
	 * The India country.
	 */
	IND("IND", "IN", "India"),

	/**
	 * The Ireland country.
	 */
	IRL("IRL", "IE", "Ireland"),

	/**
	 * The Iran, Islamic Republic Of country.
	 */
	IRN("IRN", "IR", "Iran, Islamic Republic Of"),

	/**
	 * The Iraq country.
	 */
	IRQ("IRQ", "IQ", "Iraq"),

	/**
	 * The Iceland country.
	 */
	ISL("ISL", "IS", "Iceland"),

	/**
	 * The Israel country.
	 */
	ISR("ISR", "IL", "Israel"),

	/**
	 * The Italy country.
	 */
	ITA("ITA", "IT", "Italy"),

	/**
	 * The Jamaica country.
	 */
	JAM("JAM", "JM", "Jamaica"),

	/**
	 * The Jordan country.
	 */
	JOR("JOR", "JO", "Jordan"),

	/**
	 * The Japan country.
	 */
	JPN("JPN", "JP", "Japan"),

	/**
	 * The Kazakhstan country.
	 */
	KAZ("KAZ", "KZ", "Kazakhstan"),

	/**
	 * The Kenya country.
	 */
	KEN("KEN", "KE", "Kenya"),

	/**
	 * The Kyrgyzstan country.
	 */
	KGZ("KGZ", "KG", "Kyrgyzstan"),

	/**
	 * The Cambodia country.
	 */
	KHM("KHM", "KH", "Cambodia"),

	/**
	 * The Kiribati country.
	 */
	KIR("KIR", "KI", "Kiribati"),

	/**
	 * The Saint Kitts And Nevis country.
	 */
	KNA("KNA", "KN", "Saint Kitts And Nevis"),

	/**
	 * The Korea, Republic of country.
	 */
	KOR("KOR", "KR", "Korea, Republic of"),

	/**
	 * The Kuwait country.
	 */
	KWT("KWT", "KW", "Kuwait"),

	/**
	 * The Lao People's Democratic Republic country.
	 */
	LAO("LAO", "LA", "Lao People's Democratic Republic"),

	/**
	 * The Lebanon country.
	 */
	LBN("LBN", "LB", "Lebanon"),

	/**
	 * The Liberia country.
	 */
	LBR("LBR", "LR", "Liberia"),

	/**
	 * The Libyan Arab Jamahiriya country.
	 */
	LBY("LBY", "LY", "Libyan Arab Jamahiriya"),

	/**
	 * The Liechtenstein country.
	 */
	LIE("LIE", "LI", "Liechtenstein"),

	/**
	 * The Sri Lanka country.
	 */
	LKA("LKA", "LK", "Sri Lanka"),

	/**
	 * The Lesotho country.
	 */
	LSO("LSO", "LS", "Lesotho"),

	/**
	 * The Lithuania country.
	 */
	LTU("LTU", "LT", "Lithuania"),

	/**
	 * The Luxembourg country.
	 */
	LUX("LUX", "LU", "Luxembourg"),

	/**
	 * The Latvia country.
	 */
	LVA("LVA", "LV", "Latvia"),

	/**
	 * The Morocco country.
	 */
	MAR("MAR", "MA", "Morocco"),

	/**
	 * The Moldova, Republic of country.
	 */
	MDA("MDA", "MD", "Moldova, Republic of"),

	/**
	 * The Madagascar country.
	 */
	MDG("MDG", "MG", "Madagascar"),

	/**
	 * The Maldives country.
	 */
	MDV("MDV", "MV", "Maldives"),

	/**
	 * The Mexico country.
	 */
	MEX("MEX", "MX", "Mexico"),

	/**
	 * The Marshall Islands country.
	 */
	MHL("MHL", "MH", "Marshall Islands"),

	/**
	 * The Macedonia, the Former Yugoslav Republic Of country.
	 */
	MKD("MKD", "MK", "Macedonia, the Former Yugoslav Republic Of"),

	/**
	 * The Mali country.
	 */
	MLI("MLI", "ML", "Mali"),

	/**
	 * The Myanmar country.
	 */
	MMR("MMR", "MM", "Myanmar"),

	/**
	 * The Montenegro country.
	 */
	MNE("MNE", "ME", "Montenegro"),

	/**
	 * The Mongolia country.
	 */
	MNG("MNG", "MN", "Mongolia"),

	/**
	 * The Mozambique country.
	 */
	MOZ("MOZ", "MZ", "Mozambique"),

	/**
	 * The Mauritania country.
	 */
	MRT("MRT", "MR", "Mauritania"),

	/**
	 * The Mauritius country.
	 */
	MUS("MUS", "MU", "Mauritius"),

	/**
	 * The Malawi country.
	 */
	MWI("MWI", "MW", "Malawi"),

	/**
	 * The Malaysia country.
	 */
	MYS("MYS", "MY", "Malaysia"),

	/**
	 * The Namibia country.
	 */
	NAM("NAM", "NA", "Namibia"),

	/**
	 * The Niger country.
	 */
	NER("NER", "NE", "Niger"),

	/**
	 * The Nigeria country.
	 */
	NGA("NGA", "NG", "Nigeria"),

	/**
	 * The Nicaragua country.
	 */
	NIC("NIC", "NI", "Nicaragua"),

	/**
	 * The Netherlands country.
	 */
	NLD("NLD", "NL", "Netherlands"),

	/**
	 * The Norway country.
	 */
	NOR("NOR", "NO", "Norway"),

	/**
	 * The Nepal country.
	 */
	NPL("NPL", "NP", "Nepal"),

	/**
	 * The Nauru country.
	 */
	NRU("NRU", "NR", "Nauru"),

	/**
	 * The New Zealand country.
	 */
	NZL("NZL", "NZ", "New Zealand"),

	/**
	 * The Oman country.
	 */
	OMN("OMN", "OM", "Oman"),

	/**
	 * The Pakistan country.
	 */
	PAK("PAK", "PK", "Pakistan"),

	/**
	 * The Panama country.
	 */
	PAN("PAN", "PA", "Panama"),

	/**
	 * The Peru country.
	 */
	PER("PER", "PE", "Peru"),

	/**
	 * The Philippines country.
	 */
	PHL("PHL", "PH", "Philippines"),

	/**
	 * The Palau country.
	 */
	PLW("PLW", "PW", "Palau"),

	/**
	 * The Papua New Guinea country.
	 */
	PNG("PNG", "PG", "Papua New Guinea"),

	/**
	 * The Poland country.
	 */
	POL("POL", "PL", "Poland"),

	/**
	 * The Korea, Democratic People's Republic Of country.
	 */
	PRK("PRK", "KP", "Korea, Democratic People's Republic Of"),

	/**
	 * The Portugal country.
	 */
	PRT("PRT", "PT", "Portugal"),

	/**
	 * The Paraguay country.
	 */
	PRY("PRY", "PY", "Paraguay"),

	/**
	 * The Qatar country.
	 */
	QAT("QAT", "QA", "Qatar"),

	/**
	 * The Romania country.
	 */
	ROU("ROU", "RO", "Romania"),

	/**
	 * The Russian Federation country.
	 */
	RUS("RUS", "RU", "Russian Federation"),

	/**
	 * The Rwanda country.
	 */
	RWA("RWA", "RW", "Rwanda"),

	/**
	 * The Saudi Arabia country.
	 */
	SAU("SAU", "SA", "Saudi Arabia"),

	/**
	 * The Sudan country.
	 */
	SDN("SDN", "SD", "Sudan"),

	/**
	 * The Senegal country.
	 */
	SEN("SEN", "SN", "Senegal"),

	/**
	 * The Singapore country.
	 */
	SGP("SGP", "SG", "Singapore"),

	/**
	 * The Saint Helena, Ascension and Tristan Da Cunha country.
	 */
	SHN("SHN", "SH", "Saint Helena, Ascension and Tristan Da Cunha"),

	/**
	 * The Solomon Islands country.
	 */
	SLB("SLB", "SB", "Solomon Islands"),

	/**
	 * The Sierra Leone country.
	 */
	SLE("SLE", "SL", "Sierra Leone"),

	/**
	 * The El Salvador country.
	 */
	SLV("SLV", "SV", "El Salvador"),

	/**
	 * The San Marino country.
	 */
	SMR("SMR", "SM", "San Marino"),

	/**
	 * The Somalia country.
	 */
	SOM("SOM", "SO", "Somalia"),

	/**
	 * The Serbia country.
	 */
	SRB("SRB", "RS", "Serbia"),

	/**
	 * The Sao Tome and Principe country.
	 */
	STP("STP", "ST", "Sao Tome and Principe"),

	/**
	 * The Suriname country.
	 */
	SUR("SUR", "SR", "Suriname"),

	/**
	 * The Slovakia country.
	 */
	SVK("SVK", "SK", "Slovakia"),

	/**
	 * The Slovenia country.
	 */
	SVN("SVN", "SI", "Slovenia"),

	/**
	 * The Sweden country.
	 */
	SWE("SWE", "SE", "Sweden"),

	/**
	 * The Swaziland country.
	 */
	SWZ("SWZ", "SZ", "Swaziland"),

	/**
	 * The Seychelles country.
	 */
	SYC("SYC", "SC", "Seychelles"),

	/**
	 * The Syrian Arab Republic country.
	 */
	SYR("SYR", "SY", "Syrian Arab Republic"),

	/**
	 * The Chad country.
	 */
	TCD("TCD", "TD", "Chad"),

	/**
	 * The Togo country.
	 */
	TGO("TGO", "TG", "Togo"),

	/**
	 * The Thailand country.
	 */
	THA("THA", "TH", "Thailand"),

	/**
	 * The Tajikistan country.
	 */
	TJK("TJK", "TJ", "Tajikistan"),

	/**
	 * The Turkmenistan country.
	 */
	TKM("TKM", "TM", "Turkmenistan"),

	/**
	 * The Timor-Leste country.
	 */
	TLS("TLS", "TL", "Timor-Leste"),

	/**
	 * The Tonga country.
	 */
	TON("TON", "TO", "Tonga"),

	/**
	 * The Trinidad and Tobago country.
	 */
	TTO("TTO", "TT", "Trinidad and Tobago"),

	/**
	 * The Tunisia country.
	 */
	TUN("TUN", "TN", "Tunisia"),

	/**
	 * The Turkey country.
	 */
	TUR("TUR", "TR", "Turkey"),

	/**
	 * The Tuvalu country.
	 */
	TUV("TUV", "TV", "Tuvalu"),

	/**
	 * The Taiwan, Province Of China country.
	 */
	TWN("TWN", "TW", "Taiwan, Province Of China"),

	/**
	 * The Tanzania, United Republic of country.
	 */
	TZA("TZA", "TZ", "Tanzania, United Republic of"),

	/**
	 * The Uganda country.
	 */
	UGA("UGA", "UG", "Uganda"),

	/**
	 * The Ukraine country.
	 */
	UKR("UKR", "UA", "Ukraine"),

	/**
	 * The United States Minor Outlying Islands country.
	 */
	UMI("UMI", "UM", "United States Minor Outlying Islands"),

	/**
	 * The Uruguay country.
	 */
	URY("URY", "UY", "Uruguay"),

	/**
	 * The United States country.
	 */
	USA("USA", "US", "United States"),

	/**
	 * The Uzbekistan country.
	 */
	UZB("UZB", "UZ", "Uzbekistan"),

	/**
	 * The Saint Vincent And The Grenedines country.
	 */
	VCT("VCT", "VC", "Saint Vincent And The Grenedines"),

	/**
	 * The Venezuela, Bolivarian Republic of country.
	 */
	VEN("VEN", "VE", "Venezuela, Bolivarian Republic of"),

	/**
	 * The Viet Nam country.
	 */
	VNM("VNM", "VN", "Viet Nam"),

	/**
	 * The Vanuatu country.
	 */
	VUT("VUT", "VU", "Vanuatu"),

	/**
	 * The Samoa country.
	 */
	WSM("WSM", "WS", "Samoa"),

	/**
	 * The Yemen country.
	 */
	YEM("YEM", "YE", "Yemen"),

	/**
	 * The South Africa country.
	 */
	ZAF("ZAF", "ZA", "South Africa"),

	/**
	 * The Zambia country.
	 */
	ZMB("ZMB", "ZM", "Zambia"),

	/**
	 * The Zimbabwe country.
	 */
	ZWE("ZWE", "ZW", "Zimbabwe"),
	;

	/**
	 * Contains the mapping between two letters codes and Countries.
	 */
	private static final Map<String, Country> TWO_LETTERS_CODES;

	/**
	 * Contains the mapping between three letters codes and Countries.
	 */
	private static final Map<String, Country> THREE_LETTERS_CODES;

	/**
	 * Contains the mapping between full country names and Countries.
	 */
	private static final Map<String, Country> FULL_NAMES;

	static {
		TWO_LETTERS_CODES = new HashMap<>();
		THREE_LETTERS_CODES = new HashMap<>();
		FULL_NAMES = new HashMap<>();

		Arrays.stream(Country.values()).forEach(c -> {
			TWO_LETTERS_CODES.put(c.twoLettersCode.trim().toLowerCase(), c);
			THREE_LETTERS_CODES.put(c.threeLettersCode.trim().toLowerCase(), c);
			FULL_NAMES.put(c.name.trim().toLowerCase(), c);
		});

	}

	/**
	 * Three letters country code.
	 */
	private final String threeLettersCode;

	/**
	 * Two letters country code.
	 */
	private final String twoLettersCode;

	/**
	 * The full name of the country.
	 */
	private final String name;

	/**
	 * Constructs a new country.
	 *
	 * @param threeLettersCode the three letters country code according to ISO 3166-1.
	 * @param twoLettersCode   the two letters country code according to ISO 3166-1.
	 * @param name             the full name of the country.
	 */
	Country(final String threeLettersCode, final String twoLettersCode, final String name) {
		this.threeLettersCode = threeLettersCode;
		this.twoLettersCode = twoLettersCode;
		this.name = name;
	}

	/**
	 * Returns the three letters code for this country according to ISO 3166-1.
	 *
	 * @return the three letters code.
	 */
	public String getThreeLettersCode() {
		return threeLettersCode;
	}

	/**
	 * Returns the two letters code for this country according to ISO 3166-1.
	 *
	 * @return the two letters code.
	 */
	public String getTwoLettersCode() {
		return twoLettersCode;
	}

	/**
	 * Returns the full name of this country.
	 *
	 * @return the full name of this country.
	 */
	public String getName() {
		return name;
	}

	/**
	 * Parses the two letters code associated to a country and returns the corresponding object.
	 *
	 * @param code the two letters code for the country.
	 *
	 * @return the {@code Country} object corresponding to the {@code code}, if any, or {@code null} otherwise.
	 */
	public static Country parseTwoLettersCode(final String code) {
		return ((code == null) || code.isBlank()) ? null : TWO_LETTERS_CODES.get(code.trim().toLowerCase());
	}

	/**
	 * Parses the three letters code associated to acCountry and returns the corresponding object.
	 *
	 * @param code the three letters code for the country.
	 *
	 * @return the {@code Country} object corresponding to the {@code code}, if any, or {@code null} otherwise.
	 */
	public static Country parseThreeLettersCode(final String code) {
		return ((code == null) || code.isBlank()) ? null : THREE_LETTERS_CODES.get(code.trim().toLowerCase());
	}

	/**
	 * Parses the full name associated to a country and returns the corresponding object.
	 *
	 * @param name the full name for the country.
	 *
	 * @return the {@code Country} object corresponding to the  {@code name}, if any, or {@code null} otherwise.
	 */
	public static Country parseName(final String name) {
		if (name == null || name.isBlank()) {
			return null;
		}

		final String s = name.trim().toLowerCase();

		// short path: we have an exact match
		if (FULL_NAMES.containsKey(s)) {
			return FULL_NAMES.get(s);
		}

		// long path: look for the first approximate match
		return Arrays.stream(Country.values()).filter(c -> c.getName().contains(s) || s.contains(c.getName()))
				.findFirst().orElse(null);
	}

	/**
	 * Parses the given string representation of a {@code Country}.
	 *
	 * @param country the string representation of a country.
	 *
	 * @return the corresponding {@code Country}, if any, or {@code null} otherwise.
	 */
	public static Country parse(final String country) {

		if (country == null || country.isBlank()) {
			return null;
		}

		// try the two letters code
		Country c = Country.parseTwoLettersCode(country);
		if (c != null) {
			return c;
		}

		// if not recognized, try the three letters code
		c = Country.parseThreeLettersCode(country);
		if (c != null) {
			return c;
		}

		// if still not recognized, try the full name.
		c = Country.parseName(country);
		if (c != null) {
			return c;
		}

		// if still not recognized, try with the first two letters
		if (country.length() > 2) {
			c = Country.parseTwoLettersCode(country.substring(0, 1));
			if (c != null) {
				return c;
			}
		}

		// if still not recognized, try with the first three letters
		if (country.length() > 3) {
			c = Country.parseThreeLettersCode(country.substring(0, 2));
		}

		return c;

	}
}