/*
 * Copyright (c) 2021-2022 University of Padua, Italy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.unipd.dei.aecpkg.resource.implementation;


import it.unipd.dei.aecpkg.resource.*;

/**
 * Lists the names of the fields to be used for representing {@link Resource}s.
 *
 * @author Nicola Ferro
 * @version 1.00
 * @since 1.00
 */
public interface FieldNames {

    /**
     * The name of the fields common to all {@link Resource}s.
     *
     * @author Nicola Ferro
     * @version 1.00
     * @since 1.00
     */
    public static final class COMMON {

        /**
         * See {@link Resource#getIdentifier()}.
         */
        public static final String IDENTIFIER = "identifier";

        /**
         * See {@link Resource#getCreated()}.
         */
        public static final String CREATED = "created";

        /**
         * See {@link Resource#getLastModified()}.
         */
        public static final String LAST_MODIFIED = "last-modified";

        /**
         * The media-type of a {@code Resource}.
         */
        public static final String MEDIA_TYPE = "media-type";

        /**
         * The language of a {@code Resource}.
         */
        public static final String LANGUAGE = "language";

        /**
         * The country of a {@code Resource}.
         */
        public static final String COUNTRY = "country";

        /**
         * The description of a {@code Resource}.
         */
        public static final String DESCRIPTION = "description";

        /**
         * The type of a {@code Resource}.
         */
        public static final String TYPE = "type";

        /**
         * The name of a {@code Resource}.
         */
        public static final String NAME = "name";

    }

    /**
     * The name of the fields for {@link Message}s.
     *
     * @author Nicola Ferro
     * @version 1.00
     * @since 1.00
     */
    public static final class MESSAGE {

        /**
         * The root field of a {@code Message}.
         */
        public static final String MESSAGE = "message";

        /**
         * See {@link Message#getCode()}.
         */
        public static final String CODE = "code";

        /**
         * See {@link Message#isError()}
         */
        public static final String IS_ERROR = "isError";

    }

    /**
     * The name of the fields for {@link User}s.
     *
     * @author Nicola Ferro
     * @version 1.00
     * @since 1.00
     */
    public static final class USER {

        /**
         * The root field of a user.
         */
        public static final String USER = "user";

        /**
         * See {@link User#getPassword()}.
         */
        public static final String PASSWORD = "password";

        /**
         * See {@link User#getRole()}.
         */
        public static final String ROLE = "role";

        /**
         * See {@link User#getFirstName()}.
         */
        public static final String FIRST_NAME = "first-name";

        /**
         * See {@link User#getMiddleName()}.
         */
        public static final String MIDDLE_NAME = "middle-name";

        /**
         * See {@link User#getLastName()}.
         */
        public static final String LAST_NAME = "last-name";

        /**
         * See {@link User#getAffiliation()}.
         */
        public static final String AFFILIATION = "affiliation";

    }

    /**
     * The name of the fields for {@link BinaryObject}s.
     *
     * @author Nicola Ferro
     * @version 1.00
     * @since 1.00
     */
    public static final class BINARY_OBJECT {

        /**
         * The root field of a {@code BinaryObject}.
         */
        public static final String BINARY_OBJECT = "binary-object";

        /**
         * The content of a binary object.
         */
        public static final String CONTENT = "content";

        /**
         * The content-transfer-encoding of a binary object.
         */
        public static final String CONTENT_TRANSFER_ENCODING = "content-transfer-encoding";

        /**
         * The value base64 for the content-transfer-encoding of a binary object.
         */
        public static final String BASE64 = "base64";
    }

    /**
     * The name of the fields for {@link Artifact}s.
     *
     * @author Nicola Ferro
     * @version 1.00
     * @since 1.00
     */
    public static final class ARTIFACT {

        /**
         * The root field of an {@code Artifact}.
         */
        public static final String ARTIFACT = "artifact";

        /**
         * See {@link Artifact#getOpenReviewID()}.
         */
        public static final String OPEN_REVIEW_ID = "openReview-id";

        /**
         * See {@link Artifact#getRelatedPaperDOI()}.
         */
        public static final String RELATED_PAPER_DOI = "related-paper-doi";

        /**
         * See {@link Artifact#getTitle()}.
         */
        public static final String TITLE = "title";

        /**
         * See {@link Artifact#getAbstract()}.
         */
        public static final String ABSTRACT = "abstract";

        /**
         * See {@link Artifact#getAuthors()}.
         */
        public static final String AUTHORS = "authors";
        /**
         * See {@link Artifact.Author}.
         */
        public static final String AUTHOR = "author";

        /**
         * See {@link Artifact.Author#displayName()}.
         */
        public static final String DISPLAY_NAME = "displayName";

        /**
         * See {@link Artifact#getBadges()}.
         */
        public static final String BADGES = "badges";

        /**
         * See {@link Artifact#getBadges()}.
         */
        public static final String BADGE = "badge";

        /**
         * See {@link Artifact#getOpenReviewURL()}.
         */
        public static final String OPEN_REVIEW_URL = "openReview-url";

        /**
         * See {@link Artifact#getExternalRepository()}.
         */
        public static final String REPOSITORY = "repository";

        /**
         * See {@link Artifact#getKeywords()}.
         */
        public static final String KEYWORDS = "keywords";

        /**
         * See {@link Artifact#getKeywords()}.
         */
        public static final String KEYWORD = "keyword";

    }



}
