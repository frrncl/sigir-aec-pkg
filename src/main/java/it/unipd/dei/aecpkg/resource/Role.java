/*
 * Copyright (c) 2021-2022 University of Padua, Italy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.unipd.dei.aecpkg.resource;

/**
 * Represents the role of a {@link User}.
 *
 * @author Nicola Ferro
 * @version 1.00
 * @since 1.00
 */
public enum Role {

	/**
	 * An administrator of the system.
	 */
	ROOT(true),

	/**
	 * A user who belongs the SIGIR Artifact Evaluation Committee.
	 */
	AEC(false),

	/**
	 * A user who belongs to the ACM digital library staff.
	 */
	ACM(false),

	/**
	 * A user who is an author of an artifact
	 */
	AUTHOR(false);

	/**
	 * Indicates whether this is a root role.
	 */
	private final boolean root;

	/**
	 * Creates a new {@code Role}.
	 *
	 * @param root indicates whether the {@code Role} is a root, if {@code true}, or not, if {@code false}.
	 */
	Role(final boolean root) {
		this.root = root;
	}

	/**
	 * Indicates whether this is a root {@code Role} or not.
	 *
	 * @return {@code true} if it is a root {@code Role}, {@code false} otherwise.
	 */
	public final boolean isRoot() {
		return root;
	}

	/**
	 * Parses the given string representation of a {@code Role}.
	 *
	 * @param role the string representation of a {@code Role}.
	 *
	 * @return the corresponding {@code Role}, if any, or {@code null} otherwise;
	 */
	public static Role parse(final String role) {

		if ((role == null) || role.isBlank()) {
			return null;
		}

		return switch (role.trim().toLowerCase()) {
			case "root", "admin", "adm", "administrator" -> ROOT;
			case "aec", "sigir" -> AEC;
			case "acm", "dl" -> ACM;
			case "author", "au" -> AUTHOR;
			default -> null;
		};
	}

}
