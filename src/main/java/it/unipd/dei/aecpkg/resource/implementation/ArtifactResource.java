/*
 * Copyright (c) 2021-2022 University of Padua, Italy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package it.unipd.dei.aecpkg.resource.implementation;

import it.unipd.dei.aecpkg.resource.*;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.apache.logging.log4j.message.Message;
import org.apache.logging.log4j.message.StringFormattedMessage;

import java.net.URL;
import java.time.OffsetDateTime;
import java.util.Collections;
import java.util.EnumSet;
import java.util.List;

/**
 * Represents an artifact.
 *
 * @author Nicola Ferro
 * @version 1.00
 * @since 1.00
 */
public class ArtifactResource extends AbstractResource<Artifact> implements Artifact {

	/**
	 * The identifier of the artifact within the OpenReview system.
	 */
	private final int openReviewID;

	/**
	 * The DOI of the paper related to the artifact.
	 */
	private final String relatedDOI;

	/**
	 * The title of the artifact.
	 */
	private final String title;

	/**
	 * The list of the authors of the artifact.
	 */
	private final List<Author> authors;

	/**
	 * The type of the artifact.
	 */
	private final Type type;

	/**
	 * The list of badges awarded to the artifact.
	 */
	private final EnumSet<Badge> badges;

	/**
	 * The abstract of the artifact.
	 */
	private final String aAbstract;

	/**
	 * The URL of the review about the artifact within the OpenReview system.
	 */
	private final URL openReviewURL;

	/**
	 * Additional information about an external repository holding the artifact.
	 */
	private final String repository;

	/**
	 * The list of the keywords describing the artifact.
	 */
	private final List<String> keywords;

	/**
	 * The file containing the actual artifact.
	 */
	private final BinaryObject artifact;


	/**
	 * Creates a new {@code Artifact}.
	 *
	 * @param identifier    the identifier of the artifact.
	 * @param created       the creation timestamp of the artifact, if any.
	 * @param lastModified  the last modification timestamp of the user, if any.
	 * @param openReviewID  the identifier of the artifact within the OpenReview system. It should be a positive
	 *                      integer; any other value is considered as zero.
	 * @param relatedDOI    the DOI of the paper related to the artifact, if any. An empty string is considered as a
	 *                      {@code null}.
	 * @param title         the title of the artifact, if any. An empty string is considered as a * {@code null}.
	 * @param authors       the list of the authors of the artifact, if any. A {@code null} is considered as an empty
	 *                      list.
	 * @param type          the type of the artifact, if any.
	 * @param badges        the list of badges awarded to the artifact, if any. A {@code null} is considered as an empty
	 *                      list.
	 * @param aAbstract     the abstract of the artifact, if any. An empty string is considered as a {@code null}.
	 * @param openReviewURL the URL of the review about the artifact within the OpenReview system, if any.
	 * @param repository    additional information about an external repository holding the artifact, if any. An empty
	 *                      string is considered as a {@code null}.
	 * @param keywords      the list of the keywords describing the artifact, if any. A {@code null} is considered as an
	 *                      empty list.
	 * @param artifact      the file containing the actual artifact, if any.
	 *
	 * @throws IllegalArgumentException if {@code identifier} is not a valid positive integer.
	 */
	public ArtifactResource(final String identifier, final OffsetDateTime created, final OffsetDateTime lastModified, final int openReviewID, final String relatedDOI, final String title, final List<Author> authors, final Type type, final List<Badge> badges, final String aAbstract, final URL openReviewURL, final String repository, final List<String> keywords, final BinaryObject artifact) {
		super(identifier, created, lastModified);

		try {
			int i = Integer.parseInt(identifier);

			if (i <= 0) {
				final Message msg = new StringFormattedMessage("Identifier %s should be greater than 0.", identifier);
				LOGGER.error(msg);
				throw new IllegalArgumentException(msg.getFormattedMessage());
			}
		} catch (NumberFormatException e) {
			final Message msg = new StringFormattedMessage("Identifier %s is not a valid integer: %s.", identifier,
					e.getMessage());
			LOGGER.error(msg, e);
			throw new IllegalArgumentException(msg.getFormattedMessage(), e);
		}

		this.openReviewID = Math.max(openReviewID, 0);
		this.relatedDOI = ((relatedDOI == null) || relatedDOI.isBlank()) ? null : relatedDOI;
		this.title = ((title == null) || title.isBlank()) ? null : title;
		this.authors = authors == null ? Collections.emptyList() : authors;
		this.type = type;
		this.badges = badges == null ? EnumSet.noneOf(Badge.class) : EnumSet.copyOf(badges);
		this.aAbstract = ((aAbstract == null) || aAbstract.isBlank()) ? null : aAbstract;
		this.openReviewURL = openReviewURL;
		this.repository = ((repository == null) || repository.isBlank()) ? null : repository;
		this.keywords = keywords == null ? Collections.emptyList() : keywords;
		this.artifact = artifact;
	}

	/**
	 * Creates a new {@code Artifact}.
	 *
	 * @param identifier   the identifier of the artifact.
	 * @param created      the creation timestamp of the artifact, if any.
	 * @param openReviewID the identifier of the artifact within the OpenReview system. It should be a positive integer;
	 *                     any other value is considered as zero.
	 *
	 * @throws IllegalArgumentException if {@code identifier} is not a valid positive integer.
	 */
	public ArtifactResource(final String identifier, final OffsetDateTime created, final int openReviewID) {
		this(identifier, created, null, openReviewID, null, null, null, null, null, null, null, null, null, null);
	}

	@Override
	public int getOpenReviewID() {
		return openReviewID;
	}

	@Override
	public String getRelatedPaperDOI() {
		return relatedDOI;
	}

	@Override
	public String getTitle() {
		return title;
	}

	@Override
	public Type getType() {
		return type;
	}

	@Override
	public List<Badge> getBadges() {
		return badges.stream().toList();
	}

	@Override
	public List<Author> getAuthors() {
		return authors;
	}

	@Override
	public String getAbstract() {
		return aAbstract;
	}

	@Override
	public URL getOpenReviewURL() {
		return openReviewURL;
	}

	@Override
	public String getExternalRepository() {
		return repository;
	}

	@Override
	public List<String> getKeywords() {
		return keywords;
	}

	@Override
	public BinaryObject getArtifact() {
		return artifact;
	}

	/**
	 * Two {@code Artifact} objects are equal if they have the same identifier.
	 */
	@Override
	public boolean equals(final Object o) {
		return (this == o) || ((o instanceof User) && super.equals(o));
	}

	@Override
	public int hashCode() {
		return 61 * super.hashCode();
	}

	@Override
	public final String toString() {
		ToStringBuilder tsb = new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE).appendSuper(super.toString());

		tsb.append(FieldNames.ARTIFACT.OPEN_REVIEW_ID, openReviewID);

		if (relatedDOI != null) {
			tsb.append(FieldNames.ARTIFACT.RELATED_PAPER_DOI, relatedDOI);
		}

		if (title != null) {
			tsb.append(FieldNames.ARTIFACT.TITLE, title);
		}

		for (Author a : authors) {
			tsb.append(FieldNames.ARTIFACT.AUTHOR,
					new StringBuilder(a.firstName()).append(" ").append(a.lastName()).append(", ")
							.append(a.affiliation()));
		}

		if (type != null) {
			tsb.append(FieldNames.COMMON.TYPE, type);
		}

		for (Badge b : badges) {
			tsb.append(FieldNames.ARTIFACT.BADGE, b.getOfficialName());
		}

		if (aAbstract != null) {
			tsb.append(FieldNames.ARTIFACT.ABSTRACT, aAbstract);
		}

		if (openReviewURL != null) {
			tsb.append(FieldNames.ARTIFACT.OPEN_REVIEW_URL, openReviewURL);
		}

		if (repository != null) {
			tsb.append(FieldNames.ARTIFACT.REPOSITORY, repository);
		}

		if (artifact != null) {
			tsb.appendToString(artifact.toString());
		}

		return tsb.toString();
	}


}
