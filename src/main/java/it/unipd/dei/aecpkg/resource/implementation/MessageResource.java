/*
 * Copyright (c) 2021-2022 University of Padua, Italy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.unipd.dei.aecpkg.resource.implementation;

import it.unipd.dei.aecpkg.resource.Message;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.apache.logging.log4j.message.StringFormattedMessage;

import java.time.OffsetDateTime;
import java.util.UUID;

/**
 * Represents an error.
 *
 * @author Nicola Ferro
 * @version 1.00
 * @since 1.00
 */
public class MessageResource extends AbstractResource<Message> implements Message {

	/**
	 * The description of message.
	 */
	private final String description;


	/**
	 * Indicates whether it is an error message ({@code true}) or a generic one ({@code false}).
	 */
	private final boolean isError;

	/**
	 * The unique code of the error, only in the case of error messages.
	 */
	private final String code;

	/**
	 * The unique type of the error, only in the case of error messages.
	 */
	private final String type;

	/**
	 * Creates a new {@code Message} with a type 4 random UUID identifier and {@link OffsetDateTime#now()} as creation
	 * and timestamp.
	 *
	 * @param description the description of the message.
	 * @param isError     indicates it is an error message ({@code true}) or a generic one ({@code false}).
	 * @param code        the unique code of the error, only in the case of error messages.
	 * @param type        the unique type of the error, only in the case of error messages.
	 *
	 * @throws NullPointerException     if {@code description} is {@code null} or if {@code code} and/or {@code type}
	 *                                  are {@code null} in the case of error messages.
	 * @throws IllegalArgumentException if {@code description} is empty or if {@code code} and/or {@code type} are empty
	 *                                  in the case of error messages.
	 */
	private MessageResource(final String description, final boolean isError, final String code, final String type) {

		super(UUID.randomUUID().toString(), OffsetDateTime.now(), null);

		if (description == null) {
			org.apache.logging.log4j.message.Message msg = new StringFormattedMessage(
					"Message description cannot be null.");
			LOGGER.error(msg);
			throw new NullPointerException(msg.getFormattedMessage());
		}

		if (description.isBlank()) {
			org.apache.logging.log4j.message.Message msg = new StringFormattedMessage(
					"Message description cannot be empty.");
			LOGGER.error(msg);
			throw new IllegalArgumentException(msg.getFormattedMessage());
		}
		this.description = description;

		this.isError = isError;

		if (isError && code == null) {
			org.apache.logging.log4j.message.Message msg = new StringFormattedMessage("Error code cannot be null.");
			LOGGER.error(msg);
			throw new NullPointerException(msg.getFormattedMessage());
		}

		if (isError && code.isBlank()) {
			org.apache.logging.log4j.message.Message msg = new StringFormattedMessage("Error code cannot be empty.");
			LOGGER.error(msg);
			throw new IllegalArgumentException(msg.getFormattedMessage());
		}
		this.code = code;

		if (isError && type == null) {
			org.apache.logging.log4j.message.Message msg = new StringFormattedMessage("Error type cannot be null.");
			LOGGER.error(msg);
			throw new NullPointerException(msg.getFormattedMessage());
		}

		if (isError && type.isBlank()) {
			org.apache.logging.log4j.message.Message msg = new StringFormattedMessage("Error type cannot be empty.");
			LOGGER.error(msg);
			throw new IllegalArgumentException(msg.getFormattedMessage());
		}
		this.type = type;
	}

	/**
	 * Creates a new {@code Message} with a type 4 random UUID identifier and {@link OffsetDateTime#now()} as creation
	 * and timestamp.
	 *
	 * @param description the description of the  message.
	 *
	 * @throws NullPointerException     if {@code description} is {@code null}.
	 * @throws IllegalArgumentException if {@code description} is empty.
	 */
	public MessageResource(final String description) {
		this(description, false, null, null);
	}

	/**
	 * Creates a new error {@code Message} with a type 4 random UUID identifier and {@link OffsetDateTime#now()} as
	 * creation and timestamp.
	 *
	 * @param description the description of the message.
	 * @param code        the unique code of the error.
	 * @param type        the unique type of the error, only in the case of error messages.
	 *
	 * @throws NullPointerException     if {@code description} or {@code code} or {@code type} are {@code null}.
	 * @throws IllegalArgumentException if {@code description} or {@code code} or {@code type} are empty.
	 */
	public MessageResource(final String description, final String code, final String type) {
		this(description, true, code, type);
	}


	@Override
	public final String getDescription() {
		return description;
	}

	@Override
	public final boolean isError() {
		return isError;
	}

	@Override
	public final String getCode() {
		return code;
	}

	@Override
	public final String getType() {
		return type;
	}


	@Override
	public final String toString() {
		ToStringBuilder tsb = new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE).appendSuper(super.toString())
				.append(FieldNames.COMMON.DESCRIPTION, description).append(FieldNames.MESSAGE.IS_ERROR, isError);

		if (code != null) {
			tsb.append(FieldNames.MESSAGE.CODE, code);
		}

		if (type != null) {
			tsb.append(FieldNames.COMMON.TYPE, type);
		}

		return tsb.toString();
	}

	@Override
	public final boolean equals(Object o) {
		return (this == o) || ((o instanceof Message) && super.equals(o));
	}

	@Override
	public final int hashCode() {
		return 37 * super.hashCode();
	}

}
