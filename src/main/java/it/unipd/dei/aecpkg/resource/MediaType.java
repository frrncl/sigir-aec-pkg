/*
 * Copyright (c) 2021-2022 University of Padua, Italy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.unipd.dei.aecpkg.resource;

import java.util.*;


/**
 * Provides the media type of an object according to MIME standard.
 * 
 * For information about the MIME (Multipurpose Internet Mail Extensions) standard, please see:
 *
 * <ul>
 *
 * <li><a href="http://www.ietf.org/rfc/rfc2045.txt" target="_new">RFC 2045</a>: MIME Part One: Format of Internet
 * Message Bodies;</li>
 *
 * <li><a href="http://www.ietf.org/rfc/rfc2046.txt" target="_new">RFC 2046</a>: MIME Part Two: Media Types;</li>
 *
 * <li><a href="http://www.ietf.org/rfc/rfc2047.txt" target="_new">RFC 2047</a>: MIME Part Three: Message Header
 * Extensions for Non-ASCII Text;</li>
 *
 * <li><a href="http://www.ietf.org/rfc/rfc4289.txt" target="_new">RFC 4289</a>: MIME Part Four: Registration
 * Procedures;</li>
 *
 * <li><a href="http://www.ietf.org/rfc/rfc2049.txt" target="_new">RFC 2049</a>: MIME Part Five: Conformance Criteria
 * and Examples;</li>
 *
 * <li><a href="http://www.ietf.org/rfc/rfc2231.txt" target="_new">RFC 2231</a>: MIME Parameter Value and Encoded Word
 * Extensions: Character Sets, Languages, and Continuations;</li>
 *
 * <li><a href="http://www.ietf.org/rfc/rfc2387.txt" target="_new">RFC 2387</a>: The MIME Multipart/Related
 * Content-type.</li>
 *
 * </ul>
 * 
 * For a complete and updated list of registered media types, please see <a href="http://www.iana.org/assignments/media-types/"
 * target="_blank">MIME Media Types</a>.
 * 
 * For a complete and updated list of official charset names, please see <a href="http://www.iana.org/assignments/character-sets"
 * target="_blank">CHARACTER SETS</a>.
 *
 * @author Nicola Ferro
 * @version 1.00
 * @since 1.00
 */
public enum MediaType {

	/**
	 * The text/plain MIME media type with the default encoding of the system.
	 * 
	 * See <a href="http://www.rfc-editor.org/rfc/rfc3676.txt" target="_new">RFC 3676</a>.
	 */
	TEXT_PLAIN("text", "plain", true, false, "txt", "Plain text."),

	/**
	 * The text/plain MIME media type encoded in US-ASCII.
	 * 
	 * See <a href="http://www.rfc-editor.org/rfc/rfc3676.txt" target="_new">RFC 3676</a>.
	 */
	TEXT_PLAIN_US_ASCII("text", "plain", true, false, "txt", "Plain text encoded in US-ASCII.", "charset", "US-ASCII"),

	/**
	 * The text/plain MIME media type encoded in ISO_8859-1:1987.
	 * 
	 * See <a href="http://www.rfc-editor.org/rfc/rfc3676.txt" target="_new">RFC 3676</a>.
	 */
	TEXT_PLAIN_ISO8859_1("text", "plain", true, false, "txt", "Plain text encoded in ISO-8859-1.", "charset",
			"ISO-8859-1"),

	/**
	 * The text/plain MIME media type encoded in UTF-8.
	 * 
	 * See <a href="http://www.rfc-editor.org/rfc/rfc3676.txt" target="_new">RFC 3676</a>.
	 */
	TEXT_PLAIN_UTF8("text", "plain", true, false, "txt", "Plain text encoded in UTF-8.", "charset", "UTF-8"),

	/**
	 * The text/xml MIME media type with the default encoding of the system.
	 * 
	 * See <a href="http://www.rfc-editor.org/rfc/rfc3023.txt" target="_new">RFC 3023</a>.
	 */
	TEXT_XML("text", "xml", true, true, "xml", "XML text."),

	/**
	 * The text/xml MIME media type encoded in US-ASCII.
	 * 
	 * See <a href="http://www.rfc-editor.org/rfc/rfc3023.txt" target="_new">RFC 3023</a>.
	 */
	TEXT_XML_US_ASCII("text", "xml", true, true, "xml", "XML text encoded in US-ASCII.", "charset", "US-ASCII"),

	/**
	 * The text/xml MIME media type encoded in ISO_8859-1:1987.
	 * 
	 * See <a href="http://www.rfc-editor.org/rfc/rfc3023.txt" target="_new">RFC 3023</a>.
	 */
	TEXT_XML_ISO8859_1("text", "xml", true, true, "xml", "XML text encoded in ISO-8859-1.", "charset", "ISO-8859-1"),

	/**
	 * The text/xml MIME media type encoded in UTF-8.
	 * 
	 * See <a href="http://www.rfc-editor.org/rfc/rfc3023.txt" target="_new">RFC 3023</a>.
	 */
	TEXT_XML_UTF8("text", "xml", true, true, "xml", "XML text encoded in UTF-8.", "charset", "UTF-8"),

	/**
	 * The text/sgml MIME media type with the default encoding of the system.
	 * 
	 * See <a href="http://www.rfc-editor.org/rfc/rfc1874.txt" target="_new">RFC 1874</a>.
	 */
	TEXT_SGML("text", "sgml", true, false, "sgml", "SGML text."),

	/**
	 * The text/sgml MIME media type encoded in US-ASCII.
	 * 
	 * See <a href="http://www.rfc-editor.org/rfc/rfc1874.txt" target="_new">RFC 1874</a>.
	 */
	TEXT_SGML_US_ASCII("text", "sgml", true, false, "sgml", "SGML text encoded in US-ASCII.", "charset", "US-ASCII"),

	/**
	 * The text/sgml MIME media type encoded in ISO_8859-1:1987.
	 * 
	 * See <a href="http://www.rfc-editor.org/rfc/rfc1874.txt" target="_new">RFC 1874</a>.
	 */
	TEXT_SGML_ISO8859_1("text", "sgml", true, false, "sgml", "SGML text encoded in ISO-8859-1.", "charset",
			"ISO-8859-1"),

	/**
	 * The text/html MIME media type with the default encoding of the system.
	 * 
	 * See <a href="http://www.rfc-editor.org/rfc/rfc2854.txt" target="_new">RFC 2854</a>.
	 */
	TEXT_HTML("text", "html", true, true, "html", "HTML document."),

	/**
	 * The text/html MIME media type encoded in US-ASCII.
	 * 
	 * See <a href="http://www.rfc-editor.org/rfc/rfc2854.txt" target="_new">RFC 2854</a>.
	 */
	TEXT_HTML_US_ASCII("text", "html", true, true, "html", "HTML document encoded in US-ASCII.", "charset", "US-ASCII"),

	/**
	 * The text/html MIME media type encoded in ISO_8859-1:1987.
	 * 
	 * See <a href="http://www.rfc-editor.org/rfc/rfc2854.txt" target="_new">RFC 2854</a>.
	 */
	TEXT_HTML_ISO8859_1("text", "html", true, true, "html", "HTML document encoded in ISO-8859-1.", "charset",
			"ISO-8859-1"),

	/**
	 * The text/html MIME media type encoded in UTF-8.
	 * 
	 * See <a href="http://www.rfc-editor.org/rfc/rfc2854.txt" target="_new">RFC 2854</a>.
	 */
	TEXT_HTML_UTF8("text", "html", true, true, "html", "HTML document encoded in UTF-8.", "charset", "UTF-8"),

	/**
	 * The text/css MIME media type with the default encoding of the system.
	 * 
	 * 
	 * See <a href="http://www.rfc-editor.org/rfc/rfc2318.txt" target="_new">RFC 2318</a>.
	 */
	TEXT_CSS("text", "css", true, false, "css", "Cascading Style Sheets (CSS) style."),

	/**
	 * The text/css MIME media type encoded in US-ASCII.
	 * 
	 * See <a href="http://www.rfc-editor.org/rfc/rfc2318.txt" target="_new">RFC 2318</a>.
	 */
	TEXT_CSS_US_ASCII("text", "css", true, false, "css", "Cascading Style Sheets (CSS) style encoded in US-ASCII.",
			"charset", "US-ASCII"),

	/**
	 * The text/css MIME media type encoded in ISO_8859-1:1987.
	 * 
	 * See <a href="http://www.rfc-editor.org/rfc/rfc2318.txt" target="_new">RFC 2318</a>.
	 */
	TEXT_CSS_ISO8859_1("text", "css", true, false, "css", "Cascading Style Sheets (CSS) style encoded in ISO-8859-1.",
			"charset", "ISO-8859-1"),

	/**
	 * The text/css MIME media type encoded in UTF-8.
	 * 
	 * See <a href="http://www.rfc-editor.org/rfc/rfc2318.txt" target="_new">RFC 2318</a>.
	 */
	TEXT_CSS_UTF8("text", "css", true, false, "css", "Cascading Style Sheets (CSS) style encoded in UTF-8.", "charset",
			"UTF-8"),

	/**
	 * The text/rtf MIME media type. <br>
	 */
	TEXT_RTF("text", "rtf", true, false, "rtf", "Rich Text Format (RTF) document."),

	/**
	 * The text/csv MIME media type with the default encoding of the system.
	 * 
	 * See <a href="http://www.rfc-editor.org/rfc/rfc4180.txt" target="_new">RFC 4180</a>.
	 */
	TEXT_CSV("text", "csv", true, false, "csv", "Comma Separated Values (CSV) file."),

	/**
	 * The text/csv MIME media type encoded in US-ASCII.
	 * 
	 * See <a href="http://www.rfc-editor.org/rfc/rfc4180.txt" target="_new">RFC 4180</a>.
	 */
	TEXT_CSV_US_ASCII("text", "csv", true, false, "csv", "Comma Separated Values (CSV) file encoded in US-ASCII.",
			"charset", "US-ASCII"),

	/**
	 * The text/csv MIME media type encoded in ISO_8859-1:1987.
	 * 
	 * See <a href="http://www.rfc-editor.org/rfc/rfc4180.txt" target="_new">RFC 4180</a>.
	 */
	TEXT_CSV_ISO8859_1("text", "csv", true, false, "csv", "Comma Separated Values (CSV) file encoded in ISO-8859-1.",
			"charset", "ISO-8859-1"),

	/**
	 * The text/csv MIME media type encoded in UTF-8.
	 * 
	 * See <a href="http://www.rfc-editor.org/rfc/rfc4180.txt" target="_new">RFC 4180</a>.
	 */
	TEXT_CSV_UTF8("text", "csv", true, false, "csv", "Comma Separated Values (CSV) file encoded in UTF-8.", "charset",
			"UTF-8"),

	/**
	 * The text/n3 MIME media type with the default encoding of the system.
	 * 
	 * See <a href="http://www.w3.org/TeamSubmission/n3/" target="_new">Notation3 (N3): A readable RDF syntax</a>.
	 */
	TEXT_N3("text", "n3", true, false, "n3", "Notation3  file."),

	/**
	 * The text/n3 MIME media type encoded in UTF-8.
	 * 
	 * See <a href="http://www.w3.org/TeamSubmission/n3/" target="_new">Notation3 (N3): A readable RDF syntax</a>.
	 */
	TEXT_N3_UTF8("text", "n3", true, false, "n3", "Notation3 encoded in UTF-8.", "charset", "UTF-8"),

	/**
	 * The text/turtle MIME media type with the default encoding of the system.
	 * 
	 * See <a href="http://www.w3.org/TeamSubmission/turtle/" target="_new">Turtle - Terse RDF Triple Language</a>.
	 */
	TEXT_TURTLE("text", "turtle", true, false, "turtle", "Turtle  file."),

	/**
	 * The text/turtle MIME media type encoded in UTF-8.
	 * 
	 * See <a href="http://www.w3.org/TeamSubmission/turtle/" target="_new">Turtle - Terse RDF Triple Language</a>.
	 */
	TEXT_TURTLE_UTF8("text", "turtle", true, false, "turtle", "Turtle encoded in UTF-8.", "charset", "UTF-8"),

	/**
	 * The application/xml MIME media type with the default encoding of the system.
	 * 
	 * See <a href="http://www.rfc-editor.org/rfc/rfc3023.txt" target="_new">RFC 3023</a>.
	 */
	APPLICATION_XML("application", "xml", true, true, "xml", "XML text."),

	/**
	 * The application/xml MIME media type encoded in US-ASCII.
	 * 
	 * See <a href="http://www.rfc-editor.org/rfc/rfc3023.txt" target="_new">RFC 3023</a>.
	 */
	APPLICATION_XML_US_ASCII("application", "xml", true, true, "xml", "XML text encoded in US-ASCII.", "charset",
			"US-ASCII"),

	/**
	 * The application/xml MIME media type encoded in ISO_8859-1:1987.
	 * 
	 * See
	 * <a href="http://www.rfc-editor.org/rfc/rfc3023.txt" target="_new">RFC 3023</a>.
	 */
	APPLICATION_XML_ISO8859_1("application", "xml", true, true, "xml", "XML text encoded in ISO-8859-1.", "charset",
			"ISO-8859-1"),

	/**
	 * The application/xml MIME media type encoded in UTF-8.
	 * 
	 * See <a href="http://www.rfc-editor.org/rfc/rfc3023.txt" target="_new">RFC 3023</a>.
	 */
	APPLICATION_XML_UTF8("application", "xml", true, true, "xml", "XML text encoded in UTF-8.", "charset", "UTF-8"),

	/**
	 * The application/xhtml+xml MIME media type with the default encoding of the system.
	 * 
	 * See <a href="http://www.rfc-editor.org/rfc/rfc3236.txt" target="_new">RFC 3236</a>.
	 */
	APPLICATION_XHTML_XML("application", "xhtml+xml", true, true, "xhtml", "XHTML document."),

	/**
	 * The application/xhtml+xml MIME media type encoded in US-ASCII.
	 * 
	 * See
	 * <a href="http://www.rfc-editor.org/rfc/rfc3236.txt" target="_new">RFC 3236</a>.
	 */
	APPLICATION_XHTML_XML_US_ASCII("application", "xhtml+xml", true, true, "xhtml",
			"XHTML document encoded in US-ASCII.", "charset", "US-ASCII"),

	/**
	 * The application/xhtml+xml MIME media type encoded in ISO_8859-1:1987.
	 * 
	 * 
	 * See <a href="http://www.rfc-editor.org/rfc/rfc3236.txt" target="_new">RFC 3236</a>.
	 */
	APPLICATION_XHTML_XML_ISO8859_1("application", "xhtml+xml", true, true, "xhtml",
			"XHTML document encoded in ISO-8859-1.", "charset", "ISO-8859-1"),

	/**
	 * The application/xhtml+xml MIME media type encoded in UTF-8.
	 * 
	 * See <a href="http://www.rfc-editor.org/rfc/rfc3236.txt" target="_new">RFC 3236</a>.
	 */
	APPLICATION_XHTML_XML_UTF8("application", "xhtml+xml", true, true, "xhtml", "XHTML document encoded in UTF-8.",
			"charset", "UTF-8"),

	/**
	 * The application/pdf MIME media type.
	 * 
	 * See <a href="http://www.rfc-editor.org/rfc/rfc3778.txt" target="_new">RFC 3778</a>.
	 */
	APPLICATION_PDF("application", "pdf", false, false, "pdf", "Portable Document Format (PDF) document."),

	/**
	 * The application/zip MIME media type.
	 */
	APPLICATION_ZIP("application", "zip", false, false, "zip", "Zip compressed archive."),


	/**
	 * The application/x-www-form-urlencoded MIME media type.
	 */
	APPLICATION_X_WWW_FORM_URLENCODED("application", "x-www-form-urlencoded", false, false, "www",
			"HTML form encoding format."),


	/**
	 * The application/x-compress MIME media type.
	 */
	APPLICATION_X_COMPRESS("application", "x-compress", false, false, "z", "UNIX compressed archive."),

	/**
	 * The application/x-compressed MIME media type.
	 */
	APPLICATION_X_COMPRESSED("application", "x-compressed", false, false, "tgz", "Compressed archive."),

	/**
	 * The application/x-gzip MIME media type.
	 */
	APPLICATION_X_GZIP("application", "x-gzip", false, false, "gz", "GNU zip compressed archive."),

	/**
	 * The application/x-gtar MIME media type.
	 */
	APPLICATION_X_GTAR("application", "x-gtar", false, false, "gtar", "GNU TAR (Tape ARchive) archive."),

	/**
	 * The application/x-tar MIME media type.
	 */
	APPLICATION_X_TAR("application", "x-tar", false, false, "tar", "TAR (Tape ARchive) compressed archive."),

	/**
	 * The application/octet-stream MIME media type.
	 * 
	 * See <a href="http://www.ietf.org/rfc/rfc2045.txt" target="_new">RFC 2045</a> and <a
	 * href="http://www.ietf.org/rfc/rfc2046.txt" target="_new">RFC 2046</a>.
	 */
	APPLICATION_OCTET_STREAM("application", "octet-stream", false, false, "bin", "Raw octet stream."),

	/**
	 * The application/x-msmetafile MIME media type.
	 */
	APPLICATION_X_MSMETAFILE("application", "x-msmetafile", false, false, "wmf", "Windows metafile image."),

	/**
	 * The application/postscript MIME media type.
	 * 
	 * See <a href="http://www.ietf.org/rfc/rfc2045.txt" target="_new">RFC 2045</a> and <a
	 * href="http://www.ietf.org/rfc/rfc2046.txt" target="_new">RFC 2046</a>.
	 */
	APPLICATION_POSTSCRIPT("application", "postscript", false, false, "ps", "Postscript document."),

	/**
	 * The application/json MIME media type.
	 * 
	 * See <a href="http://www.ietf.org/rfc/rfc4627.txt" target="_new">RFC 4627</a>.
	 */
	APPLICATION_JSON("application", "json", true, false, "json", "JavaScript Object Notation (JSON) document."),

	/**
	 * The application/json+schema MIME media type.
	 * 
	 * See <a href="http://tools.ietf.org/html/draft-zyp-json-schema-02" target="_new">JSON Schema</a>.
	 */
	APPLICATION_JSON_SCHEMA("application", "schema+json", true, false, "json",
			"JavaScript Object Notation (JSON) schema."),

	/**
	 * The application/ecmascript MIME media type.
	 * 
	 * See <a href="http://www.ietf.org/rfc/rfc4329.txt" target="_new">RFC 4329</a>.
	 */
	APPLICATION_ECMASCRIPT("application", "ecmascript", true, false, "js", "Ecmascript document."),

	/**
	 * The application/javascript MIME media type.
	 * 
	 * See <a href="http://www.ietf.org/rfc/rfc4329.txt" target="_new">RFC 4329</a>.
	 */
	APPLICATION_JAVASCRIPT("application", "javascript", true, false, "js", "Javascript document."),

	/**
	 * The application/vnd.ms-excel MIME media type.
	 */
	APPLICATION_VND_MS_EXCEL("application", "vnd.ms-excel", false, false, "xls", "Microsoft Excel document."),

	/**
	 * The application/vnd.ms-powerpoint MIME media type.
	 */
	APPLICATION_VND_MS_POWERPOINT("application", "vnd.ms-powerpoint", false, false, "ppt",
			"Microsoft Powerpoint document."),

	/**
	 * The application/vnd.ms-project MIME media type.
	 */
	APPLICATION_VND_MS_PROJECT("application", "vnd.ms-project", false, false, "mpp", "Microsoft Project document."),

	/**
	 * The application/vnd.ms-cab-compressed MIME media type.
	 */
	APPLICATION_VND_MS_CAB_COMPRESSED("application", "vnd.ms-cab-compressed", false, false, "cab",
			"Microsoft Cabinet compressed file."),

	/**
	 * The application/msword MIME media type.
	 */
	APPLICATION_MSWORD("application", "msword", false, false, "doc", "Microsoft Word document."),

	/**
	 * The application/x-msaccess MIME media type.
	 */
	APPLICATION_X_MSACCESS("application", "x-msaccess", false, false, "mdb", "Microsoft Access database."),

	/**
	 * The application/x-mspublisher MIME media type.
	 */
	APPLICATION_X_MSPUBLISHER("application", "x-mspublisher", false, false, "pub", "Microsoft Publisher document."),

	/**
	 * The application/x-latex MIME media type.
	 */
	APPLICATION_X_LATEX("application", "x-latex", true, false, "latex", "LaTeX document."),

	/**
	 * The application/x-tex MIME media type.
	 */
	APPLICATION_X_TEX("application", "x-tex", true, false, "tex", "TeX document."),

	/**
	 * The application/atom+xml MIME media type.
	 * 
	 * See <a href="http://www.rfc-editor.org/rfc/rfc4287.txt" target="_new">RFC 4827</a> and <a
	 * href="http://www.rfc-editor.org/rfc/rfc5023.txt" target="_new">RFC 5023</a>.
	 */
	APPLICATION_ATOM_XML("application", "atom+xml", true, true, "atom", "Atom Syndication Format document."),

	/**
	 * The application/atom+xml MIME media type encoded in US-ASCII.
	 * 
	 * See
	 * <a href="http://www.rfc-editor.org/rfc/rfc4287.txt" target="_new">RFC 4827</a> and <a
	 * href="http://www.rfc-editor.org/rfc/rfc5023.txt" target="_new">RFC 5023</a>.
	 */
	APPLICATION_ATOM_XML_US_ASCII("application", "atom+xml", true, true, "atom",
			"Atom Syndication Format document encoded in US-ASCII.", "charset", "US-ASCII"),

	/**
	 * The application/atom+xml MIME media type encoded in ISO_8859-1:1987.
	 * 
	 * See <a href="http://www.rfc-editor.org/rfc/rfc4287.txt" target="_new">RFC 4827</a> and <a
	 * href="http://www.rfc-editor.org/rfc/rfc5023.txt" target="_new">RFC 5023</a>.
	 */
	APPLICATION_ATOM_XML_ISO_8859_1("application", "atom+xml", true, true, "atom",
			"Atom Syndication Format document encoded in ISO-8859-1.", "charset", "ISO-8859-1"),

	/**
	 * The application/atom+xml MIME media type encoded in UFT-8.
	 * 
	 * See <a href="http://www.rfc-editor.org/rfc/rfc4287.txt" target="_new">RFC 4827</a> and <a
	 * href="http://www.rfc-editor.org/rfc/rfc5023.txt" target="_new">RFC 5023</a>.
	 */
	APPLICATION_ATOM_XML_UTF_8("application", "atom+xml", true, true, "atom",
			"Atom Syndication Format document encoded in UFT-8.", "charset", "UTF-8"),

	/**
	 * The application/rss+xml MIME media type.
	 * 
	 * See <a href="http://www.rssboard.org/rss-mime-type-application.txt" target="_new">RSS MIME Type Application</a>.
	 */
	APPLICATION_RSS_XML("application", "rss+xml", true, true, "rss", "Really Simple Syndication (RSS) document."),

	/**
	 * The application/rss+xml MIME media type encoded in US-ASCII.
	 * 
	 * See <a href="http://www.rssboard.org/rss-mime-type-application.txt" target="_new">RSS MIME Type Application</a>.
	 */
	APPLICATION_RSS_XML_US_ASCII("application", "rss+xml", true, true, "rss",
			"Really Simple Syndication (RSS) document encoded in US-ASCII.", "charset", "US-ASCII"),

	/**
	 * The application/rss+xml MIME media type encoded in ISO_8859-1:1987.
	 * 
	 * See <a href="http://www.rssboard.org/rss-mime-type-application.txt" target="_new">RSS MIME media type
	 * Application</a>.
	 */
	APPLICATION_RSS_XML_ISO_8859_1("application", "rss+xml", true, true, "rss",
			"Really Simple Syndication (RSS) document encoded in ISO-8859-1.", "charset", "ISO-8859-1"),

	/**
	 * The application/rss+xml MIME media type encoded in UTF-8.
	 * 
	 * See <a href="http://www.rssboard.org/rss-mime-type-application.txt" target="_new">RSS MIME Type Application</a>.
	 */
	APPLICATION_RSS_XML_UTF_8("application", "rss+xml", true, true, "rss",
			"Really Simple Syndication (RSS) document encoded in UTF-8.", "charset", "UTF-8"),

	/**
	 * The application/voicexml+xml MIME media type.
	 * 
	 * See <a href="http://www.rfc-editor.org/rfc/rfc4267.txt" target="_new">RFC 4267</a>.
	 */
	APPLICATION_VOICEXML_XML("application", "voicexml+xml", true, true, "vxml",
			"Voice Extensible Markup Language (VoiceXML) document."),

	/**
	 * The application/voicexml+xml MIME media type encoded in US-ASCII. <br> See <a
	 * href="http://www.rfc-editor.org/rfc/rfc4267.txt" target="_new">RFC 4267</a>.
	 */
	APPLICATION_VOICEXML_XML_US_ASCII("application", "voicexml+xml", true, true, "vxml",
			"Voice Extensible Markup Language (VoiceXML) document encoded in US-ASCII.", "charset", "US-ASCII"),

	/**
	 * The application/voicexml+xml MIME media type encoded in ISO_8859-1:1987.
	 * 
	 * 
	 * See <a href="http://www.rfc-editor.org/rfc/rfc4267.txt" target="_new">RFC 4267</a>.
	 */
	APPLICATION_VOICEXML_XML_ISO_8859_1("application", "voicexml+xml", true, true, "vxml",
			"Voice Extensible Markup Language (VoiceXML) document encoded in ISO-8859-1.", "charset", "ISO-8859-1"),

	/**
	 * The application/voicexml+xml MIME media type encoded in UTF-8.
	 * 
	 * See
	 * <a href="http://www.rfc-editor.org/rfc/rfc4267.txt" target="_new">RFC 4267</a>.
	 */
	APPLICATION_VOICEXML_XML_UTF_8("application", "voicexml+xml", true, true, "vxml",
			"Voice Extensible Markup Language (VoiceXML) document encoded in UTF-8.", "charset", "UTF-8"),

	/**
	 * The application/rdf+xml MIME media type.
	 * 
	 * See <a href="http://www.ietf.org/rfc/rfc3870.txt" target="_new">RFC 3780</a>.
	 */
	APPLICATION_RDF_XML("application", "rdf+xml", true, true, "rdf",
			"Extensible Markup Language (XML) serialization of a Resource Description Framework (RDF) document."),

	/**
	 * The application/rdf+xml MIME media type encoded in US-ASCII.
	 * 
	 * See <a href="http://www.ietf.org/rfc/rfc3870.txt" target="_new">RFC 3780</a>.
	 */
	APPLICATION_RDF_XML_US_ASCII("application", "rdf+xml", true, true, "rdf",
			"Extensible Markup Language (XML) serialization of a Resource Description Framework (RDF) document encoded in US-ASCII.",
			"charset", "US-ASCII"),

	/**
	 * The application/rdf+xml MIME media type encoded in ISO_8859-1:1987.
	 * 
	 * See <a href="http://www.ietf.org/rfc/rfc3870.txt" target="_new">RFC 3780</a>.
	 */
	APPLICATION_RDF_XML_ISO_8859_1("application", "rdf+xml", true, true, "rdf",
			"Extensible Markup Language (XML) serialization of a Resource Description Framework (RDF) document encoded in ISO-8859-1.",
			"charset", "ISO-8859-1"),

	/**
	 * The application/rdf+xml MIME media type encoded in UTF-8..
	 * 
	 * See <a href="http://www.ietf.org/rfc/rfc3870.txt" target="_new">RFC 3780</a>.
	 */
	APPLICATION_RDF_XML_UTF_8("application", "rdf+xml", true, true, "rdf",
			"Extensible Markup Language (XML) serialization of a Resource Description Framework (RDF) document encoded in UTF-8.",
			"charset", "UTF-8"),

	/**
	 * The application/x-shockwave-flash MIME media type.
	 */
	APPLICATION_X_SHOCKWAVE_FLASH("application", "x-shockwave-flash", false, false, "swf",
			"Macromedia ShockWave Flash (SWF) object."),

	/**
	 * The image/bmp MIME media type.
	 */
	IMAGE_BMP("image", "bmp", false, false, "bmp", "Bitmap image."),

	/**
	 * The image/gif MIME media type.
	 * 
	 * See <a href="http://www.ietf.org/rfc/rfc2045.txt" target="_new">RFC 2045</a> and <a
	 * href="http://www.ietf.org/rfc/rfc2046.txt" target="_new">RFC 2046</a>.
	 */
	IMAGE_GIF("image", "gif", false, false, "gif", "Graphics Interchange Format (GIF) image."),

	/**
	 * The image/jpeg MIME media type.
	 * 
	 * See <a href="http://www.ietf.org/rfc/rfc2045.txt" target="_new">RFC 2045</a> and <a
	 * href="http://www.ietf.org/rfc/rfc2046.txt" target="_new">RFC 2046</a>.
	 */
	IMAGE_JPEG("image", "jpeg", false, false, "jpg", "Joint Photographic Experts Group (JPEG) image."),

	/**
	 * The image/png MIME media type.
	 */
	IMAGE_PNG("image", "png", false, false, "png", "Portable Network Graphics (PNG) image."),

	/**
	 * The image/tiff MIME media type.
	 * 
	 * See <a href="http://www.ietf.org/rfc/rfc3302.txt" target="_new">RFC 3302</a>.
	 */
	IMAGE_TIFF("image", "tiff", false, false, "tif", "Tag Image File Format (TIFF) image."),

	/**
	 * The image/svg+xml MIME media type.
	 * 
	 * See <a href="http://www.w3.org/TR/2005/WD-SVGMobile12-20050413/mimereg.html" target="_new">L Media Type
	 * registration for image/svg+xml </a>.
	 */
	IMAGE_SVG_XML("image", "svg+xml", true, true, "svg", "Scalable Vector Graphics (SVG) image."),

	/**
	 * The image/svg+xml MIME media type encoded in US-ASCII.
	 * 
	 * See <a href="http://www.w3.org/TR/2005/WD-SVGMobile12-20050413/mimereg.html" target="_new">L Media Type
	 * registration for image/svg+xml </a>.
	 */
	IMAGE_SVG_XML_US_ASCII("image", "svg+xml", true, true, "svg",
			"Scalable Vector Graphics (SVG) image encoded in US-ASCII.", "charset", "US-ASCII"),

	/**
	 * The image/svg+xml MIME media type encoded in ISO_8859-1:1987.
	 * 
	 * See <a href="http://www.w3.org/TR/2005/WD-SVGMobile12-20050413/mimereg.html" target="_new">L Media Type
	 * registration for image/svg+xml </a>.
	 */
	IMAGE_SVG_XML_ISO_8859_1("image", "svg+xml", true, true, "svg",
			"Scalable Vector Graphics (SVG) image encoded in ISO 8859-1.", "charset", "ISO-8859-1"),

	/**
	 * The image/svg+xml MIME media type encode in UTF-8.
	 * 
	 * See <a href="http://www.w3.org/TR/2005/WD-SVGMobile12-20050413/mimereg.html" target="_new">L Media Type
	 * registration for image/svg+xml </a>.
	 */
	IMAGE_SVG_XML_UTF_8("image", "svg+xml", true, true, "svg", "Scalable Vector Graphics (SVG) image encoded in UTF-8.",
			"charset", "UTF-8"),

	/**
	 * The audio/basic MIME media type.
	 */
	AUDIO_BASIC("audio", "basic", false, false, "au", "AU audio file."),

	/**
	 * The audio/mid MIME media type.
	 */
	AUDIO_MID("audio", "mid", true, false, "mid", "Musical Instrument Digital Interface (MIDI) audio file."),

	/**
	 * The audio/mpeg MIME media type.
	 */
	AUDIO_MPEG("audio", "mpeg", false, false, "mp3",
			"Moving Picture Experts Group (MPEG-1) Audio Layer 3 (MP3) audio file."),

	/**
	 * The audio/x-wav MIME media type.
	 */
	AUDIO_X_WAV("audio", "x-wav", false, false, "wav", "Waveform audio file."),

	/**
	 * The audio/x-pn-realaudio MIME media type.
	 */
	AUDIO_X_PN_REALAUDIO("audio", "x-pn-realaudio", false, false, "ra", "RealAudio (RA) file."),

	/**
	 * The video/mpeg MIME media type.
	 * 
	 * See <a href="http://www.ietf.org/rfc/rfc2045.txt" target="_new">RFC 2045</a> and <a
	 * href="http://www.ietf.org/rfc/rfc2046.txt" target="_new">RFC 2046</a>.
	 */
	VIDEO_MPEG("video", "mpeg", false, false, "mpg", "Moving Picture Experts Group (MPEG) video file."),

	/**
	 * The video/mp4 MIME media type. See <a href="http://www.rfc-editor.org/rfc/rfc4337.txt" target="_new">RFC
	 * 4337</a>.
	 */
	VIDEO_MP4("video", "mp4", false, false, "mp4", "Moving Picture Experts Group (MPEG) 4 video file."),

	/**
	 * The video/mpeg MIME media type.
	 */
	VIDEO_X_MSVIDEO("video", "x-msvideo", false, false, "avi", "Audio Video Interleave (AVI) video file."),

	/**
	 * The multipart/alternative MIME media type.
	 * 
	 * See <a href="http://www.ietf.org/rfc/rfc2045.txt" target="_new">RFC 2045</a> and <a
	 * href="http://www.ietf.org/rfc/rfc2046.txt" target="_new">RFC 2046</a>.
	 */
	MULTIPART_ALTERNATIVE("multipart", "alternative", false, false, "alternative", "Multipart alternative document."),

	/**
	 * The multipart/form-data MIME media type.
	 * 
	 * See <a href="http://www.rfc-editor.org/rfc/rfc2388.txt" target="_new">RFC 2388</a>.
	 */
	MULTIPART_FORMDATA("multipart", "form-data", false, false, "form-data", "Multipart form-data."),

	/**
	 * The multipart/mixed MIME media type.
	 * 
	 * See <a href="http://www.ietf.org/rfc/rfc2045.txt" target="_new">RFC 2045</a> and <a
	 * href="http://www.ietf.org/rfc/rfc2046.txt" target="_new">RFC 2046</a>.
	 */
	MULTIPART_MIXED("multipart", "mixed", false, false, "mixed", "Multipart mixed document."),

	/**
	 * The multipart/parallel MIME media type.
	 * 
	 * See <a href="http://www.ietf.org/rfc/rfc2045.txt" target="_new">RFC 2045</a> and <a
	 * href="http://www.ietf.org/rfc/rfc2046.txt" target="_new">RFC 2046</a>.
	 */
	MULTIPART_PARALLEL("multipart", "parallel", false, false, "parallel", "Multipart parallel document."),

	/**
	 * The multipart/related MIME media type.
	 * 
	 * See <a href="http://www.rfc-editor.org/rfc/rfc2387.txt" target=" _new">RFC 2387</a>.
	 */
	MULTIPART_RELATED("multipart", "related", false, false, "related", "Multipart related document.");

	/**
	 * Represents the possibile kinds of match between two MIME media types.
	 *
	 * @author Nicola Ferro
	 * @version 1.00
	 * @since 1.00
	 */
	public enum Match {

		/**
		 * Indicates that two {@code MediaType} objects does not match at all.
		 */
		NO_MATCH,

		/**
		 * Indicates that two {@code MediaType} objects match only for the type part.
		 */
		TYPE_MATCH,

		/**
		 * Indicates that two {@code MediaType} objects match only for both the type and subtype part.
		 */
		TYPE_SUBTYPE_MATCH,

		/**
		 * Indicates that two {@code MediaType} objects match exactly, i.e. the type, the subtype, and any parameter are
		 * equal.
		 */
		EXACT_MATCH
	}

	/**
	 * Maps file FILE_EXTENSIONS to MIME media types
	 */
	private static final Map<String, MediaType> FILE_EXTENSIONS;

	/**
	 * Maps encoded names to MIME media types
	 */
	private static final Map<String, MediaType> ENCODED_NAMES;

	/**
	 * String representation of type
	 */
	private final String type;

	/**
	 * String representation of subtype
	 */
	private final String subtype;

	/**
	 * A description of the MIME media type.
	 */
	private final String description;

	/**
	 * The default file extension to be used when saving this MIME media type
	 */
	private final String fileExtension;

	/**
	 * The list of all the file FILE_EXTENSIONS for this MIME media type.
	 */
	private final Set<String> fileExtensions;

	/**
	 * Additional parameter for the type
	 */
	private final Map<String, String> parameters;

	/**
	 * The external form of this MIME media type
	 */
	private final String external;

	/**
	 * Indicates whether this is a textual MIME media type.
	 */
	private final boolean isText;

	/**
	 * Indicates whether this is an XML-based MIME media type.
	 */
	private final boolean isXml;

	static {
		ENCODED_NAMES = new HashMap<>();
		FILE_EXTENSIONS = new HashMap<>();

		// copy the encode name and file extension to the overall maps
		// Note that we normalize everything to lower cases to ease the comparison with externally provided strings
		Arrays.stream(MediaType.values()).forEach(m -> {
			ENCODED_NAMES.put(m.external.toLowerCase(), m);
			FILE_EXTENSIONS.put(m.fileExtension.toLowerCase(), m);
		});


		// Fix some issues due to textual MIME media types and their parameters
		// As above, we normalize everything to lower cases to ease the comparison with externally provided strings
		FILE_EXTENSIONS.put(TEXT_PLAIN.fileExtension.toLowerCase(), TEXT_PLAIN);
		FILE_EXTENSIONS.put(TEXT_HTML.fileExtension.toLowerCase(), TEXT_HTML);
		FILE_EXTENSIONS.put(TEXT_XML.fileExtension.toLowerCase(), TEXT_XML);
		FILE_EXTENSIONS.put(TEXT_SGML.fileExtension.toLowerCase(), TEXT_SGML);
		FILE_EXTENSIONS.put(TEXT_CSS.fileExtension.toLowerCase(), TEXT_CSS);
		FILE_EXTENSIONS.put(TEXT_CSV.fileExtension.toLowerCase(), TEXT_CSV);
		FILE_EXTENSIONS.put(APPLICATION_XML.fileExtension.toLowerCase(), APPLICATION_XML);
		FILE_EXTENSIONS.put(APPLICATION_XHTML_XML.fileExtension.toLowerCase(), APPLICATION_XHTML_XML);
		FILE_EXTENSIONS.put("tex", APPLICATION_X_LATEX);
		FILE_EXTENSIONS.put(APPLICATION_ATOM_XML.fileExtension.toLowerCase(), APPLICATION_ATOM_XML);
		FILE_EXTENSIONS.put(APPLICATION_RSS_XML.fileExtension.toLowerCase(), APPLICATION_RSS_XML);
		FILE_EXTENSIONS.put(APPLICATION_VOICEXML_XML.fileExtension.toLowerCase(), APPLICATION_VOICEXML_XML);
		FILE_EXTENSIONS.put(APPLICATION_RDF_XML.fileExtension.toLowerCase(), APPLICATION_RDF_XML);
		FILE_EXTENSIONS.put(IMAGE_SVG_XML.fileExtension.toLowerCase(), IMAGE_SVG_XML);

		// Add alternative FILE_EXTENSIONS for the same MIME media type
		TEXT_PLAIN.addFileExtension("text");
		TEXT_HTML.addFileExtension("htm");

		TEXT_SGML.addFileExtension("sgm");

		IMAGE_TIFF.addFileExtension("tiff");

		IMAGE_JPEG.addFileExtension("jpe");
		IMAGE_JPEG.addFileExtension("jpeg");

		APPLICATION_POSTSCRIPT.addFileExtension("ai");
		APPLICATION_POSTSCRIPT.addFileExtension("eps");

		APPLICATION_X_GZIP.addFileExtension("gzip");

		APPLICATION_OCTET_STREAM.addFileExtension("class");
		APPLICATION_OCTET_STREAM.addFileExtension("exe");
		APPLICATION_OCTET_STREAM.addFileExtension("lha");
		APPLICATION_OCTET_STREAM.addFileExtension("lhz");

		APPLICATION_VND_MS_EXCEL.addFileExtension("xla");
		APPLICATION_VND_MS_EXCEL.addFileExtension("xlc");
		APPLICATION_VND_MS_EXCEL.addFileExtension("xlm");
		APPLICATION_VND_MS_EXCEL.addFileExtension("xlt");
		APPLICATION_VND_MS_EXCEL.addFileExtension("xlw");
		APPLICATION_VND_MS_EXCEL.addFileExtension("xlsx");

		APPLICATION_VND_MS_POWERPOINT.addFileExtension("pot");
		APPLICATION_VND_MS_POWERPOINT.addFileExtension("pps");
		APPLICATION_VND_MS_POWERPOINT.addFileExtension("pptx");

		APPLICATION_MSWORD.addFileExtension("dot");
		APPLICATION_MSWORD.addFileExtension("docx");

		APPLICATION_X_MSACCESS.addFileExtension("accdb");

		APPLICATION_X_LATEX.addFileExtension("tex");

		AUDIO_BASIC.addFileExtension("snd");
		AUDIO_MID.addFileExtension("rmi");
		AUDIO_X_PN_REALAUDIO.addFileExtension("ram");

		VIDEO_MPEG.addFileExtension("mp2");
		VIDEO_MPEG.addFileExtension("mpa");
		VIDEO_MPEG.addFileExtension("mpe");
		VIDEO_MPEG.addFileExtension("mpeg");
		VIDEO_MPEG.addFileExtension("mpv2");
	}

	/**
	 * Constructs a new MIME media type with the type and subtype.
	 *
	 * @param type          the type of this MIME media type;
	 * @param subtype       the subtype of this MIME media type.
	 * @param isText        indicates whether this is a textual MIME media type.
	 * @param isXml         indicates whether this is a XML-based MIME media type.
	 * @param fileExtension the default file extension for this MIME media type
	 * @param description   the description of this MIME media type.
	 * @param parameters    any additional parameter for the MIME media type. They have to come in (parameter name,
	 *                      parameter value) pairs.
	 */
	MediaType(String type, String subtype, boolean isText, boolean isXml, String fileExtension, String description, String... parameters) {
		this.type = type.trim();
		this.subtype = subtype.trim();
		this.isText = isText;
		this.isXml = isXml;
		this.fileExtension = fileExtension.trim();
		this.description = description;
		this.fileExtensions = new HashSet<>();
		this.fileExtensions.add(this.fileExtension);
		this.parameters = new HashMap<>();

		// Sets additional parameters, which come in (key, value) pairs
		if (parameters != null) {
			for (int i = 0; i < parameters.length; i += 2) {
				this.parameters.put(parameters[i].trim(), parameters[i + 1].trim());
			}
		}

		// Constructs the external form of this MIME media type
		StringBuilder sb = new StringBuilder(type);
		sb.append('/');
		sb.append(subtype);

		this.parameters.keySet().forEach(k -> {
			sb.append(';');
			sb.append(k);
			sb.append('=');
			sb.append(this.parameters.get(k));
		});

		external = sb.toString();
	}

	/**
	 * Compares this MIME media type with the{@code other}, in order to determine how good the{@code other} MIME media
	 * type matches this MIME media type.
	 * 
	 * The method returns a matching level among:
	 * <ul>
	 * <li>{@link Match#NO_MATCH}: the two {@code MediaType} objects does not match at all;</li>
	 * <li>{@link Match#TYPE_MATCH}: the two {@code MediaType} objects match only for the type part.</li>
	 * <li>{@link Match#TYPE_SUBTYPE_MATCH}: the two {@code MediaType} objects match only for both the type
	 * and subtype part.</li>
	 * <li>{@link Match#EXACT_MATCH}: the two {@code MediaType} objects match exactly, i.e. the type, the
	 * subtype, and the parameters, if any, are equal.</li>
	 * </ul>
	 * 
	 * The matches are ranked from worst match to best match.
	 *
	 * @param other the other MIME media type to match against ourself.
	 *
	 * @return the degree of match between the two MIME media types.
	 */
	public Match match(MediaType other) {

		Match match = Match.NO_MATCH;

		if (other == null) {
			return match;
		}

		// start by matching types:
		if (type.equals(other.type)) {
			match = Match.TYPE_MATCH;

			// if types match, check subtypes
			if (subtype.equals(other.subtype)) {
				match = Match.TYPE_SUBTYPE_MATCH;
			}

			// if there are no parameters or all the parameters match, then we have an exact matct
			match = parameters.keySet().stream().allMatch(
					k -> other.parameters.containsKey(k) && other.parameters.get(k)
							.equals(parameters.get(k))) ? Match.EXACT_MATCH : match;
		}

		return match;
	}

	/**
	 * Returns the type of this MIME media type.
	 *
	 * @return the type of this MIME media type.
	 */
	public String getType() {
		return type;
	}

	/**
	 * Returns the subtype of this MIME media type.
	 *
	 * @return the subtype of this MIME media type.
	 */
	public String getSubType() {
		return subtype;
	}

	/**
	 * Returns the description of this MIME media type.
	 *
	 * @return the description of this MIME media type.
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Returns the default file extension used for this MIME media type.
	 *
	 * @return the default file extension used for this MIME media type.
	 */
	public String getFileExtension() {
		return fileExtension;
	}

	/**
	 * Returns all the file extensions registered for this MIME media type.
	 *
	 * @return a list containing all the file extensions registered for this MIME media type. The list has always size
	 * 		at least 1.
	 */
	public List<String> getFileExtensions() {
		return new ArrayList<>(fileExtensions);
	}

	/**
	 * Checks whether the given file {@code extension} is registered for this MIME media type.
	 *
	 * @param extension the file extension to check.
	 *
	 * @return {@code true} if the file extension is registered for this MIME media type, {@code false} otherwise.
	 */
	public boolean hasFileExtension(final String extension) {
		return !((extension == null) || extension.isEmpty()) && fileExtensions.contains(extension.trim().toLowerCase());
	}

	/**
	 * Indicates whether this MIME media type corresponds to a textual content.
	 * 
	 * MIME media types other than the ones belonging to the {@code text} type may hold textual content such as, for
	 * example, {@code application/xml}. This method provides an easy way to check for this.
	 *
	 * @return {@code true} if this MIME media type corresponds to a textual content, {@code false} otherwise.
	 */
	public boolean isTextual() {
		return isText;
	}

	/**
	 * Indicates whether this MIME media type corresponds to a XML-based content.
	 *
	 * @return {@code true} if this MIME media type corresponds to a XML-based content, {@code false} otherwise.
	 */
	public boolean isXml() {
		return isXml;
	}

	/**
	 * Returns a list of the parameters defined in this MIME media type, if any. Each object in the list is of type
	 * {@code String}.
	 *
	 * @return a list containing all of the parameters of this MIME media type, if any; otherwise it returns {@link
	 *        Collections#emptyList()}.
	 */
	public List<String> getParameters() {
		return (parameters.isEmpty() ? Collections.emptyList() : new ArrayList<>(parameters.keySet()));
	}

	/**
	 * Returns {@code true} if this MIME media type has some value for the given parameter.
	 *
	 * @param name the parameter to check.
	 *
	 * @return {@code true} if this parameter has a value, {@code false} otherwise.
	 */
	public boolean hasParameter(String name) {
		return !((name == null) || name.isEmpty()) && parameters.containsKey(name.trim().toLowerCase());
	}

	/**
	 * Returns the value of the requested paramater, if any.
	 *
	 * @param name the parameter whose value is to be returned.
	 *
	 * @return the value of the parameter, if any, or {@code null} otherwise.
	 */
	public String getParameterValue(String name) {
		return ((name == null) || name.isEmpty()) ? null : parameters.get(name.trim().toLowerCase());
	}

	/**
	 * Returns a string representation of the MIME media type.
	 * 
	 * For example, for the {@link #TEXT_PLAIN} it is {@code text/plain}.
	 *
	 * @return a string representation of the MIME media type.
	 */
	public String getEncodedName() {
		return external;
	}

	/**
	 * Parses the string representation of a MIME media type and returns the corresponding object.
	 *
	 * @param encodedName the string representation of a MIME media type.
	 *
	 * @return the MIME object corresponding to {@code encodedName}, if any, or {@code null} otherwise.
	 */
	public static MediaType parseEncodedName(String encodedName) {

		if ((encodedName == null) || encodedName.isBlank()) {
			return null;
		}

		encodedName = encodedName.trim().toLowerCase();

		MediaType mime = ENCODED_NAMES.get(encodedName);

		// may be we don't know some parameters: use the type/subtype only
		if ((mime == null) && (encodedName.indexOf(';') != -1)) {
			mime = ENCODED_NAMES.get(encodedName.substring(0, encodedName.indexOf(';')));
		}

		return mime;

	}

	/**
	 * Parses the file extension associated to a MIME media type and returns the corresponding object.
	 *
	 * @param extension the file extension.
	 *
	 * @return the MIME object corresponding to {@code extension}, if any, or {@code null} otherwise.
	 */
	public static MediaType parseFileExtension(String extension) {
		return ((extension == null) || extension.isBlank()) ? null : FILE_EXTENSIONS.get(
				extension.trim().toLowerCase());
	}

	@Override
	public String toString() {
		return external;
	}

	/**
	 * Adds a new file extension for this MIME media type
	 *
	 * @param extension the file extension to add.
	 */
	private void addFileExtension(String extension) {
		FILE_EXTENSIONS.computeIfAbsent(extension.trim().toLowerCase(), k -> {
			fileExtensions.add(k);
			return this;
		});


	}

}
