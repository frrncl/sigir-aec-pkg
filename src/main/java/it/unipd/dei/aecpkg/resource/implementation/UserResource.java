/*
 * Copyright (c) 2021-2022 University of Padua, Italy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package it.unipd.dei.aecpkg.resource.implementation;

import it.unipd.dei.aecpkg.component.AccessController;
import it.unipd.dei.aecpkg.resource.Country;
import it.unipd.dei.aecpkg.resource.Language;
import it.unipd.dei.aecpkg.resource.Role;
import it.unipd.dei.aecpkg.resource.User;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.apache.commons.validator.routines.EmailValidator;
import org.apache.logging.log4j.message.Message;
import org.apache.logging.log4j.message.StringFormattedMessage;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.OffsetDateTime;
import java.util.Locale;

/**
 * Represents a user of the system.
 *
 * @author Nicola Ferro
 * @version 1.00
 * @since 1.00
 */
public class UserResource extends AbstractResource<User> implements User {

	/**
	 * The email validator.
	 */
	private static final EmailValidator EV = EmailValidator.getInstance(true, true);

	/**
	 * The hashing function
	 */
	private static final MessageDigest MD;

	/**
	 * The password
	 */
	private final String password;

	/**
	 * The role of the user
	 */
	private final Role role;

	/**
	 * The first name of the user
	 */
	private final String firstName;

	/**
	 * The middle name of the user
	 */
	private final String middleName;

	/**
	 * The last name of the user
	 */
	private final String lastName;

	/**
	 * The affiliation of the user
	 */
	private final String affiliation;

	/**
	 * The country of the user
	 */
	private final Country country;

	/**
	 * The language of the user
	 */
	private final Language language;

	static {
		try {
			MD = MessageDigest.getInstance("SHA-256");
		} catch (NoSuchAlgorithmException e) {
			final Message msg = new StringFormattedMessage("Unable to instantiate SHA-256 message digest.");
			LOGGER.warn(msg, e);
			throw new IllegalArgumentException(msg.getFormattedMessage(), e);
		}
	}


	/**
	 * Creates a new {@code User}.
	 *
	 * @param identifier   the identifier of the user. It must be a valid email address.
	 * @param created      the creation timestamp of the user, if any.
	 * @param lastModified the last modification timestamp of the user, if any.
	 * @param password     the password of the user, if any. An empty string for {@code password} is considered as a
	 *                     {@code null}.
	 * @param role         the role of the user, if any.
	 * @param firstName    the first name of the user, if any. An empty string for {@code firstName} is considered as a
	 *                     {@code null}.
	 * @param middleName   the middle name of the user, if any. An empty string for {@code middleName} is considered as
	 *                     a {@code null}.
	 * @param lastName     the last name of the user, if any. An empty string for {@code lastName} is considered as a
	 *                     {@code null}.
	 * @param affiliation  the affiliation of the user, if any.
	 * @param country      the country of the user, if any.
	 * @param language     the language of the user, if any.
	 *
	 * @throws NullPointerException     if {@code identifier} is {@code null}.
	 * @throws IllegalArgumentException if {@code identifier} is empty or is not a valid email address.
	 */
	public UserResource(final String identifier, final OffsetDateTime created, final OffsetDateTime lastModified, final String password, final Role role, final String firstName, final String middleName, final String lastName, final String affiliation, final Country country, final Language language) {
		super(identifier, created, lastModified);

		if (!EV.isValid(identifier)) {
			final Message msg = new StringFormattedMessage("Identifier %s is not a valid email address.", identifier);
			LOGGER.error(msg);
			throw new IllegalArgumentException(msg.getFormattedMessage());
		}

		// Ensure passwords are never stored as clear text
		this.password = ((password == null) || password.isEmpty()) ? null : new String(
				Hex.encodeHex(MD.digest(password.getBytes()), true));
		//this.password = ((password == null) || password.isEmpty()) ? null : password;

		this.role = role;
		this.firstName = ((firstName == null) || firstName.isBlank()) ? null : firstName;
		this.middleName = ((middleName == null) || middleName.isBlank()) ? null : middleName;
		this.lastName = ((lastName == null) || lastName.isBlank()) ? null : lastName;
		this.affiliation = ((affiliation == null) || affiliation.isBlank()) ? null : affiliation;
		this.country = country;
		this.language = language;
	}

	/**
	 * Creates a new {@code User} with the given {@code username} and {@code password}.
	 * <p>
	 * The main purpose for this constructor is to create {@code User} objects for authentication purposes.
	 *
	 * @param identifier the identifier of the user.
	 * @param password   the password of the user.
	 *
	 * @throws NullPointerException     if {@code identifier} or {@code password} are {@code null}.
	 * @throws IllegalArgumentException if {@code identifier} or {@code password} are empty or {@code identifier} is not
	 *                                  a valid email address.
	 */
	public UserResource(final String identifier, final String password) {
		this(identifier, null, null, password, null, null, null, null, null, null, null);

		if (password == null) {
			final Message msg = new StringFormattedMessage("Password cannot be null.");
			LOGGER.error(msg);
			throw new NullPointerException(msg.getFormattedMessage());
		}

		if (password.isEmpty()) {
			final Message msg = new StringFormattedMessage("Password cannot be empty.");
			LOGGER.error(msg);
			throw new IllegalArgumentException(msg.getFormattedMessage());
		}
	}

	/**
	 * Creates a new {@code User} with the given {@code username} and {@code password}.
	 * <p>
	 * The main purpose for this constructor is to allow for password reset.
	 *
	 * @param identifier   the identifier of the user.
	 * @param password     the password of the user.
	 * @param lastModified the last modification timestamp of the user, if any.
	 *
	 * @throws NullPointerException     if {@code identifier} or {@code password} or {@code lastModified} are {@code
	 *                                  null}.
	 * @throws IllegalArgumentException if {@code identifier} or {@code password} are empty or {@code identifier} is not
	 *                                  a valid email address.
	 */
	public UserResource(final String identifier, final String password, final OffsetDateTime lastModified) {
		this(identifier, null, lastModified, password, null, null, null, null, null, null, null);

		if (password == null) {
			final Message msg = new StringFormattedMessage("Password cannot be null.");
			LOGGER.error(msg);
			throw new NullPointerException(msg.getFormattedMessage());
		}

		if (password.isEmpty()) {
			final Message msg = new StringFormattedMessage("Password cannot be empty.");
			LOGGER.error(msg);
			throw new IllegalArgumentException(msg.getFormattedMessage());
		}

		if (lastModified == null) {
			final Message msg = new StringFormattedMessage("Last modification timestamp cannot be null.");
			LOGGER.error(msg);
			throw new NullPointerException(msg.getFormattedMessage());
		}
	}

	/**
	 * Creates a new {@code User} with the given {@code identifier}.
	 * <p>
	 * The main purpose for this constructor is to create {@code User} objects to be referenced in other {@code
	 * Resource}s.
	 *
	 * @param identifier the identifier of the user.
	 *
	 * @throws NullPointerException     if {@code identifier} is {@code null}.
	 * @throws IllegalArgumentException if {@code identifier} is empty or is not a valid email address.
	 */
	public UserResource(final String identifier) {
		this(identifier, null, null, null, null, null, null, null, null, null, null);
	}

	/**
	 * Creates a new {@code User} with the given {@code identifier} and {@code role}.
	 * <p>
	 * The main purpose for this constructor is to create {@code User} objects to be used for authentication and
	 * authorization purposes in {@link AccessController}.
	 *
	 * @param identifier the identifier of the user.
	 * @param role       the role of the user.
	 *
	 * @throws NullPointerException     if {@code identifier} or {@code role} are {@code null}.
	 * @throws IllegalArgumentException if {@code identifier} is empty or is not a valid email address.
	 */
	public UserResource(final String identifier, final Role role) {
		this(identifier, null, null, null, role, null, null, null, null, null, null);

		if (role == null) {
			final Message msg = new StringFormattedMessage("Role cannot be null for user %s.", identifier);
			LOGGER.error(msg);
			throw new NullPointerException(msg.getFormattedMessage());
		}

	}


	@Override
	public String getPassword() {
		return password;
	}

	@Override
	public Role getRole() {
		return role;
	}


	@Override
	public String getFirstName() {
		return firstName;
	}

	@Override
	public String getMiddleName() {
		return middleName;
	}

	@Override
	public String getLastName() {
		return lastName;
	}

	@Override
	public String getFullName() {

		if (firstName == null && middleName == null && lastName != null) {
			return lastName;
		} else if (firstName == null && middleName != null && lastName == null) {
			return middleName;
		} else if (firstName == null && middleName != null && lastName != null) {
			return middleName + " " + lastName;
		} else if (firstName != null && middleName == null && lastName == null) {
			return firstName;
		} else if (firstName != null && middleName == null && lastName != null) {
			return firstName + " " + lastName;
		} else if (firstName != null && middleName != null && lastName == null) {
			return firstName + " " + middleName;
		} else if (firstName != null && middleName != null && lastName != null) {
			return firstName + " " + middleName + " " + lastName;
		} else {
			return null;
		}

	}

	@Override
	public String getAffiliation() {
		return affiliation;
	}

	@Override
	public Country getCountry() {
		return country;
	}

	@Override
	public Language getLanguage() {
		return language;
	}

	@Override
	public Locale getLocale() {

		if (language != null) {
			if (country != null) {
				return new Locale(language.getTwoLettersCode(), country.getTwoLettersCode());
			} else {
				return new Locale(language.getTwoLettersCode());
			}
		}

		return Locale.ENGLISH;
	}

	/**
	 * Two {@code User} objects are equal if they have the same identifier.
	 */
	@Override
	public boolean equals(final Object o) {
		return (this == o) || ((o instanceof User) && super.equals(o));
	}

	@Override
	public int hashCode() {
		return 41 * super.hashCode();
	}

	@Override
	public final String toString() {
		ToStringBuilder tsb = new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE).appendSuper(super.toString());

		if (role != null) {
			tsb.append(FieldNames.USER.ROLE, role);
		}

		if (password != null) {
			tsb.append(FieldNames.USER.PASSWORD, password);
		}

		if (firstName != null) {
			tsb.append(FieldNames.USER.FIRST_NAME, firstName);
		}

		if (middleName != null) {
			tsb.append(FieldNames.USER.MIDDLE_NAME, middleName);
		}

		if (lastName != null) {
			tsb.append(FieldNames.USER.LAST_NAME, lastName);
		}

		if (affiliation != null) {
			tsb.append(FieldNames.USER.AFFILIATION, affiliation);
		}

		if (language != null) {
			tsb.append(FieldNames.COMMON.LANGUAGE, language.getThreeLettersCode());
		}

		if (country != null) {
			tsb.append(FieldNames.COMMON.COUNTRY, country.getThreeLettersCode());
		}

		return tsb.toString();
	}

}
