/*
 * Copyright (c) 2021-2022 University of Padua, Italy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.unipd.dei.aecpkg.resource;

import it.unipd.dei.aecpkg.component.AccessController;
import it.unipd.dei.aecpkg.resource.implementation.UserResource;

import java.time.OffsetDateTime;
import java.util.Locale;

/**
 * Represents a user of the system.
 *
 * @author Nicola Ferro
 * @version 1.00
 * @since 1.00
 */
public interface User extends Resource<User> {

	/**
	 * Returns the role of the user.
	 *
	 * @return the role, if any, or {@code null} otherwise.
	 */
	Role getRole();

	/**
	 * Returns the password.
	 *
	 * @return the password, if any, or {@code null} otherwise.
	 */
	String getPassword();

	/**
	 * Returns the first name of the user.
	 *
	 * @return the first name of the user, if any, or {@code null} otherwise.
	 */
	String getFirstName();

	/**
	 * Returns the middle name of the user.
	 *
	 * @return the middle name of the user, if any, or {@code null} otherwise.
	 */
	String getMiddleName();

	/**
	 * Returns the last name of the user.
	 *
	 * @return the last name of the user, if any, or {@code null} otherwise.
	 */
	String getLastName();

	/**
	 * Returns the full name of the user.
	 *
	 * It is basically the concatenation of {@link #getFirstName()}, {@link #getMiddleName()}, and {@link
	 * #getLastName()} properly managing possible {@code null} values.
	 *
	 * @return the full name of the user, if any, or {@code null} otherwise.
	 */
	String getFullName();

	/**
	 * Returns the affiliation of the user.
	 *
	 * @return the affiliation of the user, if any, or {@code null} otherwise.
	 */
	String getAffiliation();

	/**
	 * Returns the country of the user.
	 *
	 * @return the country of the user, if any, or {@code null} otherwise.
	 */
	Country getCountry();

	/**
	 * Returns the language of the user.
	 *
	 * @return the language of the user, if any, or {@code null} otherwise.
	 */
	Language getLanguage();

	/**
	 * Returns the full {@code Locale} of the user, according to user country and language.
	 *
	 * @return the locale of the user. If the language is not set, then it returns the default locale {@link
	 * Locale#ENGLISH}.
	 */
	Locale getLocale();


	/**
	 * Creates a new {@code User}.
	 *
	 * @param identifier   the identifier of the user. It must be a valid email address.
	 * @param created      the creation timestamp of the user, if any.
	 * @param lastModified the last modification timestamp of the user, if any.
	 * @param password     the password of the user, if any. An empty string for {@code password} is considered as a
	 *                     {@code null}.
	 * @param role         the role of the user, if any.
	 * @param firstName    the first name of the user, if any. An empty string for {@code firstName} is considered as a
	 *                     {@code null}.
	 * @param middleName   the middle name of the user, if any. An empty string for {@code middleName} is considered as
	 *                     a {@code null}.
	 * @param lastName     the last name of the user, if any. An empty string for {@code lastName} is considered as a
	 *                     {@code null}.
	 * @param affiliation  the affiliation of the user, if any.
	 * @param country      the country of the user, if any.
	 * @param language     the language of the user, if any.
	 *
	 * @return the new  {@code User}.
	 *
	 * @throws NullPointerException     if {@code identifier} is {@code null}.
	 * @throws IllegalArgumentException if {@code identifier} is empty or is not a valid email address.
	 */
	static User create(final String identifier, final OffsetDateTime created, final OffsetDateTime lastModified,
					   final String password, final Role role, final String firstName, final String middleName,
					   final String lastName, final String affiliation, final Country country, final Language language) {
		return new UserResource(identifier, created, lastModified, password, role, firstName, middleName, lastName,
				affiliation, country, language);
	}

	/**
	 * Creates a new {@code User} with the given {@code username} and {@code password}.
	 * <p>
	 * The main purpose for this constructor is to allow for password reset.
	 *
	 * @param identifier   the identifier of the user.
	 * @param password     the password of the user.
	 * @param lastModified the last modification timestamp of the user, if any.
	 *
	 *                     @return the new  {@code User}.
	 *
	 * @throws NullPointerException     if {@code identifier} or {@code password} or {@code lastModified} are {@code
	 *                                  null}.
	 * @throws IllegalArgumentException if {@code identifier} or {@code password} are empty or {@code identifier} is not
	 *                                  a valid email address.
	 */
	static User create(final String identifier, final String password, final OffsetDateTime lastModified) {
		return new UserResource(identifier, password, lastModified);
	}

	/**
	 * Creates a new {@code User} with the given {@code identifier}, {@code password}, {@code l}.
	 * <p>
	 * The main purpose for this constructor is to create {@code User} objects for authentication purposes.
	 *
	 * @param identifier the identifier of the user.
	 * @param password   the password of the user.
	 *
	 *                   @return the new  {@code User}.
	 *
	 * @throws NullPointerException     if {@code identifier} or {@code password} are {@code null}.
	 * @throws IllegalArgumentException if {@code identifier} or {@code password} are empty or {@code} identifier is not
	 *                                  a valid email address.
	 */
	static User create(final String identifier, final String password) {
		return new UserResource(identifier, password);
	}


	/**
	 * Creates a new {@code User} with the given {@code identifier}.
	 * <p>
	 * The main purpose for this constructor is to create {@code User} objects to be referenced in other {@code
	 * Resource}s.
	 *
	 * @param identifier the identifier of the user.
	 *
	 *                   @return the new  {@code User}.
	 *
	 * @throws NullPointerException     if {@code identifier} is {@code null} or is not a valid email address.
	 * @throws IllegalArgumentException if {@code identifier} is empty.
	 */
	static User create(final String identifier) {
		return new UserResource(identifier);
	}

	/**
	 * Creates a new {@code User} with the given {@code identifier} and {@code role}.
	 * <p>
	 * The main purpose for this constructor is to create {@code User} objects to be used for authentication and
	 * authorization purposes in {@link AccessController}.
	 *
	 * @param identifier the identifier of the user.
	 * @param role       the role of the user, if any.
	 *
	 *                   @return the new  {@code User}.
	 *
	 * @throws NullPointerException     if {@code identifier} or {@code role} are {@code null}.
	 * @throws IllegalArgumentException if {@code identifier} is empty or is not a valid email address.
	 */
	static User create(final String identifier, final Role role) {
		return new UserResource(identifier, role);
	}


}
