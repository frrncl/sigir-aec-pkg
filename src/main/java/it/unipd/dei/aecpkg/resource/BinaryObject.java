/*
 * Copyright (c) 2021-2022 University of Padua, Italy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.unipd.dei.aecpkg.resource;


import it.unipd.dei.aecpkg.resource.implementation.BinaryObjectResource;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.Reader;
import java.io.Writer;
import java.time.OffsetDateTime;

/**
 * Represents a generic binary object with methods for setting/getting its content.
 *
 * @author Nicola Ferro
 * @version 1.00
 * @since 1.00
 */
public interface BinaryObject extends Resource<BinaryObject> {

	/**
	 * Returns the MIME media type of the binary object.
	 *
	 * @return the MIME media type of the binary object.
	 */
	MediaType getMediaType();

	/**
	 * Returns the language of the binary object.
	 *
	 * @return the language of the binary object, if any, or {@code null} otherwise.
	 */
	Language getLanguage();

	/**
	 * Returns the number of bytes in the binary object.
	 *
	 * @return the length of the binary object in bytes.
	 */
	long length();

	/**
	 * Returns the content of the binary object as a byte array. The byte array contains up to {@code length}
	 * consecutive bytes starting at position {@code pos}.
	 *
	 * @param pos    the ordinal position in the binary object value of the first byte to be extracted; the first byte
	 *               is at position 0.
	 * @param length the number of consecutive bytes to be copied.
	 *
	 * @return a byte array with up to {@code length} consecutive bytes from the binary object, starting with the byte
	 * 		at position {@code pos}.
	 *
	 * @throws IllegalArgumentException if {@code pos} &lt; 0  or {@code pos} &gt; {@code length()} of if {@code length}
	 *                                  &lt; 1.
	 */
	byte[] getContentAsBytes(long pos, long length);

	/**
	 * Returns the whole content of the binary object as a {@code byte} array.
	 *
	 * @return the whole content of the as a {@code byte} array.
	 */
	byte[] getContentAsBytes();

	/**
	 * Returns the content of the binary object as a stream.
	 *
	 * @return an {@code InputStream} object with the content of binary object.
	 */
	InputStream getContentAsBinaryStream();

	/**
	 * Returns the content of the digital resource as a reader.
	 *
	 * @return an {@code Reader} object with the content of binary object.
	 */
	Reader getContentAsCharacterStream();

	/**
	 * Returns a string representation of the content of the binary object.
	 *
	 * @return a string representation of the content of the binary object.
	 */
	String getContentAsString();

	/**
	 * Writes all or part of the given byte array to the binary object and returns the number of bytes written.
	 *
	 * Writing starts at position {@code pos} in the binary object; {@code len} bytes from the given byte array are
	 * written.
	 *
	 * @param pos    the position in the binary object at which to start writing.
	 * @param bytes  the array of bytes to be written to this binary object.
	 * @param offset the offset into the byte array at which to start reading the bytes to be set.
	 * @param len    the number of bytes to be written to the binary object from the byte array . It is given by {@code
	 *               max(bytes.length - offset, len)}.
	 *
	 * @return the number of bytes written.
	 *
	 * @throws IllegalArgumentException if {@code pos} &lt;0 or {@code pos} &gt; {@code length()} or if {@code offset}
	 *                                  &lt;0 or {@code offset} &gt; {@code bytes.length}.
	 * @throws IllegalStateException    if there is any issue while writing the content of the binary object.
	 * @throws NullPointerException     if {@code bytes} is {@code null}.
	 */
	int setBytes(long pos, byte[] bytes, int offset, int len);

	/**
	 * Writes the given byte array to the binary object, starting at position {@code pos}, and returns the number of
	 * bytes written.
	 *
	 * @param pos   the position in the binary object at which to start writing.
	 * @param bytes the byte array to be written to this binary object.
	 *
	 * @return the number of written bytes.
	 *
	 * @throws IllegalArgumentException if {@code pos} &lt; 0 or {@code pos} &gt; {@code length()}.
	 * @throws IllegalStateException    if there is any issue while writing the content of the binary object.
	 * @throws NullPointerException     if {@code bytes} is {@code null}.
	 */
	int setBytes(long pos, byte[] bytes);

	/**
	 * Writes the given array of bytes as content of the binary object.
	 *
	 * @param bytes the byte array to be written to this binary object.
	 *
	 * @return the number of written bytes.
	 *
	 * @throws NullPointerException if {@code bytes} is  {@code null}.
	 */
	int setBytes(byte[] bytes);

	/**
	 * Retrieves a stream that can be used to write to the binary object. The stream begins at position {@code pos}.
	 *
	 * @param pos the ordinal position in the binary object value of the first byte to write; the first byte is at
	 *            position 0.
	 *
	 * @return OutputStream an {@code OutputStream} object to which data can be written.
	 *
	 * @throws IllegalArgumentException if {@code pos} &lt; 0 or {@code pos} &gt; {@code length()}.
	 */
	OutputStream setContentAsBinaryStream(long pos);

	/**
	 * Retrieves a stream that can be used to write to the binary object.
	 *
	 * @return OutputStream an {@code OutputStream} object to which data can be written.
	 */
	OutputStream setContentAsBinaryStream();

	/**
	 * Retrieves a writer that can be used to write to the binary object.
	 *
	 * @return Writer an {@code Writer} object to which data can be written.
	 */
	Writer setContentAsCharacterStream();

	/**
	 * Writes the given string to the binary object.
	 *
	 * @param content the string to write.
	 *
	 * @return the number of written bytes.
	 *
	 * @throws NullPointerException if {@code content} is {@code null}.
	 */
	int setContentAsString(String content);

	/**
	 * Writes bytes read from the given {@code InputStream} to the binary object.
	 *
	 * It does not close the {@code InputStream}.
	 *
	 * @param is the {@code InputStream} from which bytes are read.
	 *
	 * @return the number of written bytes.
	 *
	 * @throws NullPointerException if {@code is} is  {@code null}.
	 */
	long setContentFromBinaryStream(InputStream is);

	/**
	 * Creates a new {@code BinaryObject} with the given identifier.
	 *
	 * @param identifier   the identifier of the binary object, if any.
	 * @param created      the creation timestamp of the binary object, if any.
	 * @param lastModified the last modification timestamp of the binary object, if any.
	 * @param mediaType    the MIME media type of the binary object.
	 * @param language     the language of the binary object, if any.
	 * @param content      the content of the binary object, if any.
	 *
	 * @return the new {@code BinaryObject}
	 *
	 * @throws NullPointerException if {@code mediaType} is {@code null}.
	 */
	static BinaryObject create(final String identifier, final OffsetDateTime created, final OffsetDateTime lastModified, final MediaType mediaType, final Language language, final byte[] content) {
		return new BinaryObjectResource(identifier, created, lastModified, mediaType, language, content);
	}

	/**
	 * Creates a new {@code BinaryObject}.
	 *
	 * @param identifier the identifier of the binary object, if any.
	 * @param mediaType  the MIME media type of the binary object.
	 *
	 * @return the new {@code BinaryObject}
	 *
	 * @throws NullPointerException if {@code mediaType} is {@code null}.
	 */
	static BinaryObject create(final String identifier, final MediaType mediaType) {
		return new BinaryObjectResource(identifier, mediaType);
	}

	/**
	 * Creates a new {@code BinaryObject}.
	 *
	 * @param mediaType the MIME media type of the binary object.
	 *
	 * @return the new {@code BinaryObject}
	 *
	 * @throws NullPointerException if {@code mediaType} is {@code null}.
	 */
	static BinaryObject create(final MediaType mediaType) {
		return new BinaryObjectResource(mediaType);
	}


}
