/*
 * Copyright (c) 2021-2022 University of Padua, Italy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.unipd.dei.aecpkg.resource;

import java.io.OutputStream;
import java.io.Writer;
import java.time.OffsetDateTime;

/**
 * Any entity which has identity.
 *
 * A resource:
 * <ul>
 * <li>is uniquely identified;</li>
 * <li>can be compared to other resources of the same type;</li>
 * <li>can be represented as plain text.</li>
 * </ul>
 *
 * @param <T> the actual type of the {@code Resource}, i.e. its subclass.
 *
 * @author Nicola Ferro
 * @version 1.00
 * @since 1.00
 */
public interface Resource<T extends Resource<T>> extends Comparable<T> {

	/**
	 * Returns the unique identifier of the {@code Resource}.
	 *
	 * @return the unique identifier of the {@code Resource}.
	 */
	String getIdentifier();

	/**
	 * Returns the creation timestamp of the {@code Resource}.
	 *
	 * @return the creation timestamp of the {@code Resource}, if any, or {@code null} otherwise.
	 */
	OffsetDateTime getCreated();


	/**
	 * Returns the last modification timestamp of the {@code Resource}.
	 *
	 * @return the last modification  timestamp of the {@code Resource}, if any, or {@code null} otherwise.
	 */
	OffsetDateTime getLastModified();


	/**
	 * Returns a text representation of the {@code Resource}.
	 *
	 * @return a string containing the text representation of the {@code Resource}.
	 */
	String toString();

	/**
	 * Returns a text representation of the {@code Resource} into the given {@code OutputStream}.
	 *
	 * @param out the stream to which the text representation of the {@code Resource} has to be written.
	 */
	void toString(OutputStream out);

	/**
	 * Returns a text representation of the {@code Resource} into the given {@code Writer}.
	 *
	 * @param out the character stream to which the text representation of the {@code Resource} has to be written.
	 */
	void toString(Writer out);
}
