/*
 * Copyright (c) 2021-2022 University of Padua, Italy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.unipd.dei.aecpkg.resource;

/**
 * Represents commonly-used error types.
 *
 * @author Nicola Ferro
 * @version 1.00
 * @since 1.00
 */
public enum CommonErrorTypes {

	/**
	 * An error internal to the system has occurred.
	 */
	INTERNAL_ERROR("C1000"),

	/**
	 * An unsupported operation has been requested.
	 */
	UNSUPPORTED_OPERATION("C1001"),

	/**
	 * An unsupported output format has been requested.
	 */
	UNSUPPORTED_OUTPUT_FORMAT("C2000"),

	/**
	 * An unsupported input format has been provided.
	 */
	UNSUPPORTED_INPUT_FORMAT("C2001"),

	/**
	 * An invalid parameter (null, empty, missing, ...) has been provided.
	 */
	INVALID_PARAMETER("C2002"),

	/**
	 * An error occurred while representing the resource.
	 */
	REPRESENTATION_ERROR("C2003"),

	/**
	 * A malformed representation of a resource (not well-formed, not valid, ...) has been provided.
	 */
	MALFORMED_REPRESENTATION("C2004"),

	/**
	 * An invalid request (HTTP, Recaptcha failed, ...) has been performed.
	 */
	INVALID_REQUEST("C2005"),

	/**
	 * An attempt to access a resource without the required authentication has been performed.
	 */
	AUTHENTICATION_REQUIRED("C3000"),

	/**
	 * An attempt to access a resource with insufficient access rights has been performed.
	 */
	INSUFFICIENT_ACCESS_RIGHTS("C3001"),

	/**
	 * An unknown resource has been requested.
	 */
	UNKNOWN_RESOURCE("C4000"),

	/**
	 * An attempt to access an invalid resource has been performed.
	 */
	INVALID_RESOURCE("4001"),

	/**
	 * An attempt to create an already existing resource has been performed.
	 */
	DUPLICATED_RESOURCE("C4002"),

	/**
	 * An attempt to refer to an non-existent resource has been performed.
	 */
	NOT_FOUND_RESOURCE("C4003"),

	/**
	 * An attempt to update or delete a resource that cannot be modified has been performed.
	 */
	NOT_MODIFIABLE_RESOURCE("C4004"),

	/**
	 * An attempt to update a resource that has been concurrently updated has been performed.
	 */
	CONCURRENT_RESOURCE_MODIFICATION("C4005"),
	;

	/**
	 * The error code corresponding to this error type.
	 */
	private final String code;

	/**
	 * Creates a new error type.
	 *
	 * @param code the error code corresponding to this error type.
	 */
	CommonErrorTypes(String code) {
		this.code = code;
	}


	/**
	 * Returns the unique code of the error.
	 *
	 * @return the unique code of the error.
	 */
	public String getCode() {
		return code;
	}


	/**
	 * Returns the type of the error.
	 *
	 * @return the type of the error.
	 */
	public String getType() {
		return name();
	}

}