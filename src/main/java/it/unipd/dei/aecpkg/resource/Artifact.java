/*
 * Copyright (c) 2021-2022 University of Padua, Italy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.unipd.dei.aecpkg.resource;

import it.unipd.dei.aecpkg.resource.implementation.ArtifactResource;

import java.net.URL;
import java.time.OffsetDateTime;
import java.util.Collections;
import java.util.List;

/**
 * Represents an artifact.
 *
 * @author Nicola Ferro
 * @version 1.00
 * @since 1.00
 */
public interface Artifact extends Resource<Artifact> {

	/**
	 * Represents an author of an {@code Artifact}.
	 *
	 * @param firstName   the first name of the author.
	 * @param lastName    the last name of the author.
	 * @param displayName the display name of the author, typically &quot;first name&quot; followed by &quot;last
	 *                    name&quot;
	 * @param affiliation the affiliation of the author.
	 *
	 * @author Nicola Ferro
	 * @version 1.00
	 * @since 1.00
	 */
	record Author(String firstName, String lastName, String displayName, String affiliation) {
	}

	/**
	 * Represents the type of an {@link Artifact}.
	 *
	 * @author Nicola Ferro
	 * @version 1.00
	 * @since 1.00
	 */
	enum Type {

		/**
		 * A dataset {@code Artifact}
		 */
		DATASET("dataset", "artfc-dataset"),

		/**
		 * A software {@code Artifact}.
		 */
		SOFTWARE("software", "artfc-software");

		/**
		 * The official ACM name of the type
		 */
		private final String officialName;

		/**
		 * The identifier of the type in the ACM digital library
		 */
		private final String acmDL;

		/**
		 * Creates a new {@code Type}.
		 *
		 * @param officialName the official ACM name of the badge.
		 * @param acmDL        identifier of the badge in the ACM digital library.
		 */
		Type(final String officialName, final String acmDL) {
			this.officialName = officialName;
			this.acmDL = acmDL;
		}

		/**
		 * Returns the official ACM name of the type.
		 *
		 * @return the official ACM name of the type.
		 */
		public String getOfficialName() {
			return officialName;
		}

		/**
		 * Returns the identifier of the type in the ACM digital library.
		 *
		 * @return the identifier of the type in the ACM digital library.
		 */
		public String getAcmDL() {
			return acmDL;
		}

		/**
		 * Parses the given string representation of an {@code Type}.
		 *
		 * @param artifactType the string representation of an {@code Type}.
		 *
		 * @return the corresponding {@code Type}, if any, or {@code null} otherwise;
		 */
		public static Type parse(final String artifactType) {

			if ((artifactType == null) || artifactType.isBlank()) {
				return null;
			}

			return switch (artifactType.trim().toLowerCase()) {
				case "dataset", "d", "artfc-dataset" -> DATASET;
				case "software", "s", "sw", "artfc-software" -> SOFTWARE;
				default -> null;
			};
		}

	}

	/**
	 * Returns the identifier of the {@code Artifact} within the OpenReview system.
	 *
	 * @return the identifier of the {@code Artifact} within the OpenReview system, if any; {@code 0} otherwise
	 */
	int getOpenReviewID();

	/**
	 * Returns the DOI of the paper related to the {@code Artifact}.
	 *
	 * @return the DOI of the paper related to the {@code Artifact}, if any; {@code null} otherwise.
	 */
	String getRelatedPaperDOI();

	/**
	 * Returns the title of the {@code Artifact}.
	 *
	 * @return the title of the {@code Artifact}, if any; {@code null} otherwise.
	 */
	String getTitle();

	/**
	 * Returns the type of the {@code Artifact}.
	 *
	 * @return the type of the {@code Artifact}, if any; {@code null} otherwise.
	 */
	Type getType();

	/**
	 * Returns the list of badges awarded to the {@code Artifact}.
	 *
	 * @return the list of badges awarded to the {@code Artifact}, if any; {@link Collections#emptyList()} otherwise.
	 */
	List<Badge> getBadges();

	/**
	 * Returns the list of the authors of the {@code Artifact}.
	 *
	 * @return the list of the authors of the {@code Artifact}, if any; {@link Collections#emptyList()} otherwise.
	 */
	List<Author> getAuthors();

	/**
	 * Returns the abstract of the {@code Artifact}.
	 *
	 * @return the abstract of the {@code Artifact}, if any; {@code null} otherwise.
	 */
	String getAbstract();

	/**
	 * Returns the URL of the review about the {@code Artifact} within the OpenReview system.
	 *
	 * @return the URL of the review about the {@code Artifact} within the OpenReview system, if any; {@code null}
	 * 		otherwise.
	 */
	URL getOpenReviewURL();

	/**
	 * Returns additional information about an external repository holding the {@code Artifact}.
	 *
	 * @return additional information about an external repository holding the {@code Artifact}, if any; {@code null}
	 * 		otherwise.
	 */
	String getExternalRepository();

	/**
	 * Returns the list of the keywords describing the {@code Artifact}.
	 *
	 * @return the list of the keywords describing the {@code Artifact}, if any; {@link Collections#emptyList()}
	 * 		otherwise.
	 */
	List<String> getKeywords();

	/**
	 * Returns the file containing the actual artifact.
	 *
	 * @return the file containing the actual artifact, if any; {@code null} otherwise.
	 */
	BinaryObject getArtifact();

	/**
	 * Creates a new {@code Artifact}.
	 *
	 * @param identifier    the identifier of the artifact.
	 * @param created       the creation timestamp of the artifact, if any.
	 * @param lastModified  the last modification timestamp of the user, if any.
	 * @param openReviewID  the identifier of the artifact within the OpenReview system. It should be a positive
	 *                      integer; any other value is considered as zero.
	 * @param relatedDOI    the DOI of the paper related to the artifact, if any. An empty string is considered as a
	 *                      {@code null}.
	 * @param title         the title of the artifact, if any. An empty string is considered as a * {@code null}.
	 * @param authors       the list of the authors of the artifact, if any. A {@code null} is considered as an empty
	 *                      list.
	 * @param type          the type of the artifact, if any.
	 * @param badges        the list of badges awarded to the artifact, if any. A {@code null} is considered as an empty
	 *                      list.
	 * @param aAbstract     the abstract of the artifact, if any. An empty string is considered as a {@code null}.
	 * @param openReviewURL the URL of the review about the artifact within the OpenReview system, if any.
	 * @param repository    additional information about an external repository holding the artifact, if any. An empty
	 *                      string is considered as a {@code null}.
	 * @param keywords      the list of the keywords describing the artifact, if any. A {@code null} is considered as an
	 *                      empty list.
	 * @param artifact      the file containing the actual artifact, if any.
	 *
	 * @return the new  {@code Artifact}.
	 *
	 * @throws IllegalArgumentException if {@code identifier} is not a valid positive integer.
	 */
	static Artifact create(final String identifier, final OffsetDateTime created, final OffsetDateTime lastModified, final int openReviewID, final String relatedDOI, final String title, final List<Author> authors, final Type type, final List<Badge> badges, final String aAbstract, final URL openReviewURL, final String repository, final List<String> keywords, final BinaryObject artifact) {
		return new ArtifactResource(identifier, created, lastModified, openReviewID, relatedDOI, title, authors, type,
				badges, aAbstract, openReviewURL, repository, keywords, artifact);
	}

	/**
	 * Creates a new {@code Artifact}.
	 *
	 * @param identifier    the identifier of the artifact.
	 * @param created       the creation timestamp of the artifact, if any.
	 * @param openReviewID  the identifier of the artifact within the OpenReview system. It should be a positive
	 *                      integer; any other value is considered as zero.
	 *
	 * @return the new  {@code Artifact}.
	 *
	 * @throws IllegalArgumentException if {@code identifier} is not a valid positive integer.
	 */
	static Artifact create(final String identifier, final OffsetDateTime created, final int openReviewID) {
		return new ArtifactResource(identifier, created, openReviewID);
	}

}
