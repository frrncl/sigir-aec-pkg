/*
 * Copyright (c) 2021-2022 University of Padua, Italy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.unipd.dei.aecpkg.resource.implementation;


import it.unipd.dei.aecpkg.resource.BinaryObject;
import it.unipd.dei.aecpkg.resource.Language;
import it.unipd.dei.aecpkg.resource.MediaType;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.apache.logging.log4j.message.Message;
import org.apache.logging.log4j.message.StringFormattedMessage;

import java.io.*;
import java.time.OffsetDateTime;
import java.util.UUID;

/**
 * Represent a generic binary object.
 *
 * @author Nicola Ferro
 * @version 1.00
 * @since 1.00
 */
public class BinaryObjectResource extends AbstractResource<BinaryObject> implements BinaryObject {

	/**
	 * The size of internal buffers.
	 */
	private static final int BUFFER_SIZE = 8 * 1024;

	/**
	 * The empty content.
	 */
	private static final byte[] EMPTY_CONTENT = new byte[0];

	/**
	 * The MIME media type of this binary object.
	 */
	private final MediaType mediaType;

	/**
	 * The language of the binary object.
	 */
	private final Language language;

	/**
	 * The content of this binary object.
	 */
	private byte[] content;

	/**
	 * Creates a new {@code BinaryObject} with the given identifier.
	 *
	 * @param identifier   the identifier of the binary object, if any.
	 * @param created      the creation timestamp of the binary object, if any.
	 * @param lastModified the last modification timestamp of the binary object, if any.
	 * @param mediaType    the MIME media type of the binary object.
	 * @param language     the language of the binary object, if any.
	 * @param content      the content of the binary object, if any.
	 *
	 * @throws NullPointerException if {@code mediaType} is {@code null}.
	 */
	public BinaryObjectResource(final String identifier, final OffsetDateTime created, final OffsetDateTime lastModified, final MediaType mediaType, final Language language, final byte[] content) {

		super(identifier == null || identifier.isBlank() ? UUID.randomUUID().toString() : identifier, created,
				lastModified);

		if (mediaType == null) {
			final Message msg = new StringFormattedMessage("MIME media type cannot be null.");
			LOGGER.error(msg);
			throw new NullPointerException(msg.getFormattedMessage());
		}
		this.mediaType = mediaType;

		this.language = language;

		this.content = (content == null ? EMPTY_CONTENT : content);
	}

	/**
	 * Creates a new {@code BinaryObject}.
	 *
	 * @param identifier   the identifier of the binary object, if any.
	 * @param mediaType the MIME media type of the binary object.
	 */
	public BinaryObjectResource(final String identifier, final MediaType mediaType) {
		this(identifier, null, null, mediaType, null, null);
	}

	/**
	 * Creates a new {@code BinaryObject}.
	 *
	 * @param mediaType the MIME media type of the binary object.
	 */
	public BinaryObjectResource(final MediaType mediaType) {
		this(null, null, null, mediaType, null, null);
	}


	@Override
	public MediaType getMediaType() {
		return mediaType;
	}

	@Override
	public Language getLanguage() {
		return language;
	}

	@Override
	public long length() {
		return content.length;
	}

	@Override
	public byte[] getContentAsBytes(final long pos, final long length) {
		if (pos < 0 || pos > content.length) {
			final Message msg = new StringFormattedMessage(
					"Invalid start position %,d for binary object %s. It should be equal to or greater than 0 and equal to or less than the length %,d of the binary object.",
					pos, getIdentifier(), content.length);
			LOGGER.error(msg);
			throw new IllegalArgumentException(msg.getFormattedMessage());
		}

		if (length < 0) {
			final Message msg = new StringFormattedMessage(
					"Invalid length %,d for binary object %s. It should be greater than or equal to 1.", length,
					getIdentifier());
			LOGGER.error(msg);
			throw new IllegalArgumentException(msg.getFormattedMessage());
		}

		final int bytesToCopy = Math.max(content.length, (int) length);

		final byte[] result = new byte[bytesToCopy];

		System.arraycopy(content, (int) pos, result, 0, bytesToCopy);

		return result;
	}

	@Override
	public byte[] getContentAsBytes() {

		final byte[] result = new byte[content.length];

		System.arraycopy(content, 0, result, 0, content.length);

		return result;
	}


	@Override
	public InputStream getContentAsBinaryStream() {
		return new ByteArrayInputStream(content);
	}


	@Override
	public String getContentAsString() {

		final StringBuilder sb = new StringBuilder();

		// if it is a textual binary object convert the raw bytes to a string
		if (mediaType.isTextual()) {

			String c = null;

			// if an encoding is specified
			if (mediaType.hasParameter("charset")) {
				try {
					c = new String(content, mediaType.getParameterValue("charset"));
				} catch (final UnsupportedEncodingException e) {
					final Message msg = new StringFormattedMessage(
							"Unsupported encoding %s for binary object %s. Try to use the system default encoding instead.",
							mediaType.getParameterValue("charset"), getIdentifier());
					LOGGER.warn(msg, e);

					c = new String(content);
				}
			} else {
				c = new String(content);
			}

			sb.append(c);

		} else {
			sb.append(Hex.encodeHex(content));
		}

		return sb.toString();

	}

	@Override
	public Reader getContentAsCharacterStream() {

		Reader r = null;

		// if it is a textual binary object convert the raw bytes to a string
		if (mediaType.isTextual()) {

			if (mediaType.hasParameter("charset")) {
				try {
					r = new InputStreamReader(getContentAsBinaryStream(), mediaType.getParameterValue("charset"));
				} catch (final UnsupportedEncodingException e) {

					final Message msg = new StringFormattedMessage(
							"Unsupported encoding %s for binary object %s. Try to use the system default encoding instead.",
							mediaType.getParameterValue("charset"), getIdentifier());
					LOGGER.warn(msg, e);

					r = new InputStreamReader(getContentAsBinaryStream());
				}
			} else {
				r = new InputStreamReader(getContentAsBinaryStream());
			}
		} else {
			r = new CharArrayReader(Hex.encodeHex(content));
		}

		return r;
	}


	@Override
	public OutputStream setContentAsBinaryStream(final long pos) {

		if (pos < 0 || pos > content.length) {
			final Message msg = new StringFormattedMessage(
					"Invalid start position %,d for binary object %s. It should be equal to or greater than 0 and equal to or less than the length %,d of the binary object.",
					pos, getIdentifier(), content.length);
			LOGGER.error(msg);
			throw new IllegalArgumentException(msg.getFormattedMessage());
		}

		final BinaryObjectOutputStream bytesOut = new BinaryObjectOutputStream();

		// if pos > 0, writes the pos-1 previous bytes from content to the
		// stream, so that we can start writing from pos.
		if (pos > 0) {
			bytesOut.write(content, 0, (int) (pos - 1));
		}

		return bytesOut;

	}

	@Override
	public OutputStream setContentAsBinaryStream() {
		return new BinaryObjectOutputStream();
	}

	@Override
	public Writer setContentAsCharacterStream() {

		Writer w = null;

		// if it is a textual binary object convert the raw bytes to a string
		if (mediaType.hasParameter("charset")) {
			try {
				w = new OutputStreamWriter(setContentAsBinaryStream(), mediaType.getParameterValue("charset"));
			} catch (final UnsupportedEncodingException e) {

				final Message msg = new StringFormattedMessage(
						"Unsupported encoding %s for binary object %s. Try to use the system default encoding instead.",
						mediaType.getParameterValue("charset"), getIdentifier());
				LOGGER.warn(msg, e);

				w = new OutputStreamWriter(setContentAsBinaryStream());
			}
		} else {
			w = new OutputStreamWriter(setContentAsBinaryStream());
		}

		return w;
	}

	@Override
	public int setContentAsString(final String content) {

		// if it is a textual binary object convert the string to raw bytes
		if (mediaType.hasParameter("charset")) {
			try {
				this.content = content.getBytes(mediaType.getParameterValue("charset"));
			} catch (final UnsupportedEncodingException e) {

				final Message msg = new StringFormattedMessage(
						"Unsupported encoding %s for binary object %s. Try to use the system default encoding instead.",
						mediaType.getParameterValue("charset"), getIdentifier());
				LOGGER.warn(msg, e);

				this.content = content.getBytes();
			}
		} else {
			this.content = content.getBytes();
		}

		return this.content.length;
	}


	@Override
	public int setBytes(final long pos, final byte[] bytes, final int offset, final int len) {

		if (bytes == null) {
			final Message msg = new StringFormattedMessage("Bytes cannot be null for binary object %s.",
					getIdentifier());
			LOGGER.error(msg);

			throw new NullPointerException(msg.getFormattedMessage());
		}

		if (pos < 0 || pos > content.length) {
			final Message msg = new StringFormattedMessage(
					"Invalid start position %,d for binary object %s. It should be equal to or greater than 0 and equal to or less than the length %,d of the binary object.",
					pos, getIdentifier(), content.length);
			LOGGER.error(msg);
			throw new IllegalArgumentException(msg.getFormattedMessage());
		}

		if (offset < 0 || offset > bytes.length) {
			final Message msg = new StringFormattedMessage(
					"Invalid offset %,d for binary object %s. It should be equal to or greater than 0 and equal to or less than the bytes.length %,d.",
					offset, getIdentifier(), bytes.length);
			LOGGER.error(msg);

			throw new IllegalArgumentException(msg.getFormattedMessage());
		}

		final OutputStream bytesOut = setContentAsBinaryStream(pos);

		try {
			bytesOut.write(bytes, offset, len);
		} catch (final IOException ioe) {
			final Message msg = new StringFormattedMessage("Error while writing to binary object %s.", getIdentifier());
			LOGGER.error(msg, ioe);
			throw new IllegalStateException(msg.getFormattedMessage(), ioe);
		} finally {
			try {
				bytesOut.close();
			} catch (final IOException ioe) {
				final Message msg = new StringFormattedMessage(
						"Error while closing the output stream for writing to binary object %s.", getIdentifier());
				LOGGER.error(msg, ioe);
				throw new IllegalStateException(msg.getFormattedMessage(), ioe);
			}
		}

		return Math.max(bytes.length - offset, len);

	}

	@Override
	public int setBytes(final long pos, final byte[] bytes) {
		return setBytes(pos, bytes, 0, bytes.length);
	}

	@Override
	public int setBytes(final byte[] bytes) {
		content = (bytes == null ? EMPTY_CONTENT : bytes);

		return content.length;
	}

	public long setContentFromBinaryStream(InputStream is) {

		if (is == null) {
			final Message msg = new StringFormattedMessage("Input stream cannot be null for binary object %s.",
					getIdentifier());
			LOGGER.error(msg);

			throw new NullPointerException(msg.getFormattedMessage());
		}

		final BinaryObjectOutputStream bytesOut = new BinaryObjectOutputStream();
		long written = 0;

		try {
			written = is.transferTo(bytesOut);
		} catch (final IOException ioe) {
			final Message msg = new StringFormattedMessage("Error while writing to binary object %s.", getIdentifier());
			LOGGER.error(msg, ioe);
			throw new IllegalStateException(msg.getFormattedMessage(), ioe);
		} finally {
			try {
				bytesOut.close();
			} catch (final IOException ioe) {
				final Message msg = new StringFormattedMessage(
						"Error while closing the output stream for writing to binary object %s.", getIdentifier());
				LOGGER.error(msg, ioe);
				throw new IllegalStateException(msg.getFormattedMessage(), ioe);
			}
		}

		return written;

	}

	/**
	 * Two {@code BinaryObject} objects are equal if they have the same identifier and namespace.
	 */
	@Override
	public boolean equals(final Object o) {
		return (this == o) || ((o instanceof BinaryObject) && super.equals(o));
	}

	@Override
	public int hashCode() {
		return 59 * super.hashCode();
	}

	@Override
	public final String toString() {
		final ToStringBuilder tsb = new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE).appendSuper(
				super.toString()).append(FieldNames.COMMON.MEDIA_TYPE, mediaType.getEncodedName());

		if (language != null) {
			tsb.append(FieldNames.COMMON.LANGUAGE, language.getThreeLettersCode());
		}

		if (content.length > 0) {
			tsb.append(FieldNames.BINARY_OBJECT.CONTENT, content.length + " byte(s)");
		}

		return tsb.toString();
	}


	/**
	 * Extends {@code ByteArrayOutputStream} so that, when closed, it writes the content of the stream to the {@code
	 * content} of this {@code BinaryObject}
	 *
	 * @author Nicola Ferro
	 * @version 1.00
	 * @since 1.00
	 */
	private final class BinaryObjectOutputStream extends ByteArrayOutputStream {

		/**
		 * Creates a new byte array output stream.
		 * <p>
		 * The buffer capacity is {@link BinaryObjectResource#BUFFER_SIZE}.
		 */
		private BinaryObjectOutputStream() {
			super(BUFFER_SIZE);
		}

		/**
		 * Closes the stream and writes its content to {@link BinaryObjectResource#content}.
		 *
		 * @throws IOException if an I/O error occurs.
		 */
		@Override
		public void close() throws IOException {

			final int streamSize = size();

			// if the stream contains less than content.length bytes, add to it
			// the bytes from content starting from streamSize
			if (streamSize < content.length) {
				this.write(content, streamSize, content.length - streamSize);
			}

			super.close();

			content = toByteArray();

		}

	}


}
