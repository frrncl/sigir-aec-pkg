/*
 * Copyright (c) 2021-2022 University of Padua, Italy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.unipd.dei.aecpkg.resource;

import java.util.Locale;

/**
 * Represents the possible badges for an {@link Artifact}.
 *
 * For more information see
 *
 * <a href="https://www.acm.org/publications/policies/artifact-review-and-badging-current" target="_blank">ACM Artifact
 * Review and Badging</a>.
 *
 * @author Nicola Ferro
 * @version 1.00
 * @since 1.00
 */
public enum Badge {

	/**
	 * The {@code Artifact}s associated with the research are found to be documented, consistent, complete, exercisable,
	 * and include appropriate evidence of verification and validation.
	 */
	FUNCTIONAL("Artifacts Evaluated - Functional v1.1", "artifacts_evaluated_functional_v101"),

	/**
	 * The {@code Artifact}s associated with the paper are of a quality that significantly exceeds minimal
	 * functionality. That is, they have all the qualities of the Artifacts Evaluated – Functional level, but, in
	 * addition, they are very carefully documented and well-structured to the extent that reuse and repurposing is
	 * facilitated. In particular, norms and standards of the research community for artifacts of this type are strictly
	 * adhered to.
	 */
	REUSABLE("Artifacts Evaluated - Reusable v1.1", "artifacts_evaluated_reusable_v101"),

	/**
	 * Author-created {@code Artifact}s relevant to this paper have been placed on a publically accessible archival
	 * repository. A DOI or link to this repository along with a unique identifier for the object is provided.
	 */
	AVAILABLE("Artifacts Available v1.1", "artifacts_available_v101"),

	/**
	 * The main results of the paper have been obtained in a subsequent study by a person or team other than the
	 * authors, using, in part, {@code Artifact}s provided by the author.
	 */
	REPRODUCED("Results Reproduced v1.1", "results_reproduced_v101"),

	/**
	 * The main results of the paper have been independently obtained in a subsequent study by a person or team other
	 * than the authors, without the use of author-supplied {@code Artifact}s.
	 */
	REPLICATED("Results Replicated v1.1", "results_replicated_v101");


	/**
	 * The official ACM name of the badge
	 */
	private final String officialName;

	/**
	 * The identifier of the badge in the ACM digital library
	 */
	private final String acmDL;

	/**
	 * Creates a new {@code Badge}.
	 *
	 * @param officialName the official ACM name of the badge.
	 * @param acmDL        identifier of the badge in the ACM digital library.
	 */
	Badge(final String officialName, final String acmDL) {
		this.officialName = officialName;
		this.acmDL = acmDL;
	}

	/**
	 * Returns the official ACM name of the badge.
	 *
	 * @return the official ACM name of the badge.
	 */
	public String getOfficialName() {
		return officialName;
	}

	/**
	 * Returns the identifier of the badge in the ACM digital library.
	 *
	 * @return the identifier of the badge in the ACM digital library.
	 */
	public String getAcmDL() {
		return acmDL;
	}

	/**
	 * Parses the given string representation of an {@code Badge}.
	 *
	 * @param badge the string representation of an {@code Badge}.
	 *
	 * @return the corresponding {@code Badge}, if any, or {@code null} otherwise;
	 */
	public static Badge parse(final String badge) {

		if ((badge == null) || badge.isBlank()) {
			return null;
		}

		return switch (badge.trim().toLowerCase()) {
			case "functional", "func", "artifacts evaluated - functional v1.1", "artifacts_evaluated_functional_v101" -> FUNCTIONAL;
			case "reusable", "reus", "artifacts evaluated - reusable v1.1", "artifacts_evaluated_reusable_v101" -> REUSABLE;
			case "available", "avail", "artifacts available v1.1", "artifacts_available_v101" -> AVAILABLE;
			case "reproduced", "repro", "results reproduced v1.1", "results_reproduced_v101" -> REPRODUCED;
			case "replicated", "repli", "results replicated v1.1", "results_replicated_v101" -> REPLICATED;
			default -> null;
		};
	}

}
