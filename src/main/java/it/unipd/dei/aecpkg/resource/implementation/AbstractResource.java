/*
 * Copyright (c) 2021-2022 University of Padua, Italy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package it.unipd.dei.aecpkg.resource.implementation;


import it.unipd.dei.aecpkg.resource.MediaType;
import it.unipd.dei.aecpkg.resource.Resource;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.message.StringFormattedMessage;
import org.apache.logging.log4j.message.StringFormatterMessageFactory;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;


/**
 * The common superclass for implementing {@link Resource}s.
 *
 * @param <T> the type of the actual class/interface extending the {@code Resource} interface.
 *
 * @author Nicola Ferro
 * @version 1.00
 * @since 1.00
 */
public abstract class AbstractResource<T extends Resource<T>> implements Resource<T> {

    /**
     * A LOGGER available for all the subclasses.
     */
    protected static final Logger LOGGER = LogManager.getLogger(AbstractResource.class, StringFormatterMessageFactory.INSTANCE);

    /**
     * The identifier of the resource.
     */
    protected final String identifier;

    /**
     * The creation timestamp of the resource.
     */
    protected final OffsetDateTime created;

    /**
     * The last modification timestamp of the resource.
     */
    protected final OffsetDateTime lastModified;

    /**
     * Creates a new {@code Resource} with the given identifier.
     *
     * @param identifier   the identifier of the resource.
     * @param created      the creation timestamp of the resource, if any.
     * @param lastModified the last modification timestamp of the resource, if any.
     *
     * @throws NullPointerException     if {@code identifier} is {@code null}.
     * @throws IllegalArgumentException if {@code identifier} is empty.
     */
    protected AbstractResource(final String identifier, OffsetDateTime created, OffsetDateTime lastModified) {

        if (identifier == null) {
            final org.apache.logging.log4j.message.Message msg = new StringFormattedMessage("Identifier cannot be null.");
            LOGGER.error(msg);
            throw new NullPointerException(msg.getFormattedMessage());
        }

        if (identifier.isBlank()) {
            final org.apache.logging.log4j.message.Message msg = new StringFormattedMessage(
                    "Identifier cannot be empty.");
            LOGGER.error(msg);
            throw new IllegalArgumentException(msg.getFormattedMessage());
        }

        this.identifier = identifier;

        this.created = created;

        this.lastModified = lastModified;
    }

    @Override
    public final String getIdentifier() {
        return identifier;
    }

    @Override
    public final OffsetDateTime getCreated() {
        return created;
    }

    @Override
    public final OffsetDateTime getLastModified() {
        return lastModified;
    }


    /**
     * Two {@code Resource}s are compared by their identifiers.
     *
     * @throws NullPointerException if {@code t} is {@code null}.
     */
    @Override
    public int compareTo(final T t) {

        if (t == null) {
            final org.apache.logging.log4j.message.Message msg = new StringFormattedMessage(
                    "Object to compare to cannot be null.");
            LOGGER.error(msg);
            throw new NullPointerException(msg.getFormattedMessage());
        }

        // compare the identifiers
        return identifier.compareTo(t.getIdentifier());
    }

    /**
     * Two {@code Resource}s are equal if they have the same identifier.
     *
     * {@inheritDoc}
     */
    @Override
    public boolean equals(final Object o) {
        return (o != null) && ((this == o) || (o instanceof Resource<?> && identifier.equals(
                ((Resource<?>) o).getIdentifier())));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        return 31 + identifier.hashCode();
    }

    @Override
    public final void toString(final OutputStream out) {
        toString(new OutputStreamWriter(out, StandardCharsets.UTF_8));
    }


    @Override
    public final void toString(final Writer out) {
        try {
            out.write(toString());
            out.flush();
        } catch (final IOException ioe) {
            final org.apache.logging.log4j.message.Message msg = new StringFormattedMessage(
                    "Unable to write to the output stream for representing the resource %s with type %s to MIME media" +
                    " type %s.", identifier, this.getClass().getName(), MediaType.TEXT_PLAIN.getEncodedName());

            LOGGER.error(msg, ioe);
        }
    }

    @Override
    public String toString() {
        final ToStringBuilder tsb = new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE);

        tsb.append(FieldNames.COMMON.IDENTIFIER, identifier);

        if (created != null) {
            tsb.append(FieldNames.COMMON.CREATED, DateTimeFormatter.ISO_OFFSET_DATE_TIME.format(created));
        }

        if (lastModified != null) {
            tsb.append(FieldNames.COMMON.LAST_MODIFIED, DateTimeFormatter.ISO_OFFSET_DATE_TIME.format(lastModified));
        }

        return tsb.toString();
    }

}
