/*
 * Copyright (c) 2021-2022 University of Padua, Italy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.unipd.dei.aecpkg.datastore.file;


import it.unipd.dei.aecpkg.component.AuthorizationRequiredException;
import it.unipd.dei.aecpkg.component.ComponentException;
import it.unipd.dei.aecpkg.component.DuplicatedResourceException;
import it.unipd.dei.aecpkg.component.ResourceNotFoundException;
import it.unipd.dei.aecpkg.datastore.AbstractArtifactDatastore;
import it.unipd.dei.aecpkg.datastore.ArtifactDatastore;
import it.unipd.dei.aecpkg.resource.Artifact;
import it.unipd.dei.aecpkg.resource.BinaryObject;
import it.unipd.dei.aecpkg.resource.MediaType;
import org.apache.commons.text.StringSubstitutor;
import org.apache.logging.log4j.message.Message;
import org.apache.logging.log4j.message.StringFormattedMessage;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;


/**
 * Implements the {@link it.unipd.dei.aecpkg.datastore.ArtifactDatastore} interface by relying on the file system.
 *
 * @author Nicola Ferro
 * @version 1.00
 * @since 1.00
 */

public final class FileArtifactDatastore extends AbstractArtifactDatastore {

	/**
	 * Name of the default configuration file.
	 */
	private static final String DEFAULT_CONFIG_FILE = "artifactDatastore.properties";

	/**
	 * The format for the name of the directory containing the artifact package.
	 */
	private static final String ARTIFACT_DIRECTORY_FORMAT = "or_%05d";

	/**
	 * The base directory for storing artifacts
	 */
	private final String storeBase;

	/**
	 * Constructs a new component with the given configuration.
	 *
	 * @param configFileName the name of the file containing the configuration for this component.
	 *
	 * @throws ComponentException if something goes wrong while creating the component.
	 */
	public FileArtifactDatastore(final String configFileName) throws ComponentException {

		super(configFileName);

		String tmp = null;

		tmp = config.get(ArtifactDatastore.class.getName() + ".storeBase");
		if (tmp == null || tmp.isBlank()) {
			final Message msg = new StringFormattedMessage("Property %s missing or empty.",
					ArtifactDatastore.class.getName() + ".storeBase");
			LOGGER.error(msg);
			throw new ComponentException(msg.getFormattedMessage());
		}
		storeBase = StringSubstitutor.createInterpolator().replace(tmp);

	}

	/**
	 * Constructs a component with default configuration.
	 *
	 * @throws ComponentException if something goes wrong while creating the component.
	 */
	public FileArtifactDatastore() throws ComponentException {
		this(DEFAULT_CONFIG_FILE);
	}

	/*
	 * -------------------------------------------------------------------------
	 * Methods defined in the interface ArtifactDatastore follow.
	 * -------------------------------------------------------------------------
	 */

	@Override
	public final Artifact create(final Artifact a) throws ComponentException {

		super.create(a);

		final String artifactDirName = String.format(ARTIFACT_DIRECTORY_FORMAT, a.getOpenReviewID());

		final Path artifactDir = Path.of(Paths.get(storeBase).toAbsolutePath().toString(), artifactDirName);

		// if the directory does not already exist, create it
		if (Files.notExists(artifactDir)) {
			try {
				Files.createDirectory(artifactDir);

				LOGGER.info("Artifact directory %s created.", artifactDirName);
			} catch (Exception e) {
				final Message msg = new StringFormattedMessage("Impossible to create the artifact directory %s: %s",
						artifactDirName, e.getMessage());
				LOGGER.error(msg, e);
				throw new ComponentException(msg.getFormattedMessage(), e);
			}
		} else {
			final Message msg = new StringFormattedMessage("An artifact directory %s already exists.", artifactDirName);
			LOGGER.error(msg);
			throw new DuplicatedResourceException(msg.getFormattedMessage());
		}

		if (!Files.isWritable(artifactDir)) {
			final Message msg = new StringFormattedMessage("Artifact directory %s cannot be written.", artifactDirName);
			LOGGER.error(msg);
			throw new ComponentException(msg.getFormattedMessage());
		}

		final BinaryObject artifactPackage = createZipPackage(a);

		final Path artifactPackageFile = Path.of(artifactDir.toAbsolutePath().toString(),
				artifactPackage.getIdentifier());

		LOGGER.info("Artifact package %s successfully created.", artifactPackage.getIdentifier());

		try (InputStream in = artifactPackage.getContentAsBinaryStream(); OutputStream out = new BufferedOutputStream(
				Files.newOutputStream(artifactPackageFile, StandardOpenOption.CREATE_NEW));) {
			in.transferTo(out);
		} catch (Exception e) {
			final Message msg = new StringFormattedMessage(
					"Impossible to write the artifact package %s to disk: %s. Rolling back artifact creation.",
					artifactPackage.getIdentifier(), e.getMessage());
			LOGGER.error(msg, e);

			try {
				Files.delete(artifactDir);
				LOGGER.warn("Artifact directory %s successfully deleted from disk.", artifactDirName);
			} catch (IOException ex) {
				final Message msg2 = new StringFormattedMessage(
						"Unable to roll-back artifact creation for artifact %s: %s.", a.getIdentifier(),
						e.getMessage());
				LOGGER.error(msg2, e);
			}


			throw new ComponentException(msg.getFormattedMessage(), e);
		}

		LOGGER.info("Artifact package %s successfully written to disk.", artifactPackage.getIdentifier());

		try {
			sendArtifactReadyForUploadEmail(a);

			LOGGER.info("Email on artifact %s being ready for upload to ACM successfully sent.", a.getIdentifier());
		} catch (Exception e) {
			final Message msg = new StringFormattedMessage(
					"Unable to send email on artifact %s being ready for upload to ACM: %s. Rolling back artifact creation.",
					a.getIdentifier(), e.getMessage());
			LOGGER.error(msg, e);

			try {
				Files.delete(artifactPackageFile);
				LOGGER.warn("Artifact package %s successfully deleted from disk.", artifactPackage.getIdentifier());

				Files.delete(artifactDir);
				LOGGER.warn("Artifact directory %s successfully deleted from disk.", artifactDirName);
			} catch (IOException ex) {
				final Message msg2 = new StringFormattedMessage(
						"Unable to roll-back artifact creation for artifact %s: %s.", a.getIdentifier(),
						e.getMessage());
				LOGGER.error(msg2, e);
			}

			throw e;
		}

		return a;

	}

	@Override
	public BinaryObject readPackage(Artifact a) throws ComponentException {

		super.readPackage(a);

		final String artifactDirName = String.format(ARTIFACT_DIRECTORY_FORMAT, a.getOpenReviewID());

		final Path artifactDir = Path.of(Paths.get(storeBase).toAbsolutePath().toString(), artifactDirName);

		final String artifactPackageFileName = String.format(PACKAGE_NAME_FORMAT, Integer.valueOf(a.getIdentifier()),
				a.getCreated());

		final Path artifactPackageFile = Path.of(artifactDir.toAbsolutePath().toString(), artifactPackageFileName);

		if (!Files.exists(artifactPackageFile)) {
			final Message msg = new StringFormattedMessage("Artifact package %s does not exist.",
					artifactPackageFileName);
			LOGGER.error(msg);
			throw new ResourceNotFoundException(msg.getFormattedMessage());
		}

		if (!Files.isReadable(artifactPackageFile)) {
			final Message msg = new StringFormattedMessage("Artifact package %s is not readable.",
					artifactPackageFileName);
			LOGGER.error(msg);
			throw new AuthorizationRequiredException(msg.getFormattedMessage());
		}

		final BinaryObject bo = BinaryObject.create(artifactPackageFileName, MediaType.APPLICATION_ZIP);

		try (InputStream in = new BufferedInputStream(Files.newInputStream(artifactPackageFile))) {
			bo.setContentFromBinaryStream(in);
		} catch (IOException e) {
			final Message msg = new StringFormattedMessage("Unable to read artifact package: %s.",
					artifactPackageFileName, e.getMessage());
			LOGGER.error(msg, e);
			throw new ComponentException(msg.getFormattedMessage(), e);
		}

		LOGGER.info("Artifact package %s for artifact %s successfully read from disk.", artifactPackageFileName, a.getIdentifier());

		return bo;
	}

	@Override
	public Artifact read(final Artifact a) throws ComponentException {

		final Message msg = new StringFormattedMessage("Operation not supported.");
		LOGGER.error(msg);
		throw new UnsupportedOperationException(msg.getFormattedMessage());
	}

	@Override
	public Artifact update(final Artifact a) throws ComponentException {

		final Message msg = new StringFormattedMessage("Operation not supported.");
		LOGGER.error(msg);
		throw new UnsupportedOperationException(msg.getFormattedMessage());

	}

	@Override
	public Artifact delete(final Artifact a) throws ComponentException {

		final Message msg = new StringFormattedMessage("Operation not supported.");
		LOGGER.error(msg);
		throw new UnsupportedOperationException(msg.getFormattedMessage());

	}

	@Override
	public Iterable<Artifact> list() throws ComponentException {

		final Message msg = new StringFormattedMessage("Operation not supported.");
		LOGGER.error(msg);
		throw new UnsupportedOperationException(msg.getFormattedMessage());

	}


	/*-------------------------------------------------------------------------
	 * Methods defined in the superclass AbstractComponent follow.
	 *-------------------------------------------------------------------------
	 */

	/**
	 * Starts up the {@code MailManager}.
	 */
	@Override
	protected final void doStart() throws ComponentException {
		ensureStoreBaseDirectoryExists();
	}

	/**
	 * Shuts down the {@code MailManager}.
	 */
	@Override
	protected final void doStop() {
		; // nothing to do
	}


	/**
	 * Checks that the {@code storeBase} directory exists and it is writable; if not, it tries to create it.
	 *
	 * @throws ComponentException if something goes wrong while accessing the file system.
	 */
	private final void ensureStoreBaseDirectoryExists() throws ComponentException {

		final Path storeBaseDir = Paths.get(storeBase);

		// if the directory does not already exist, create it
		if (Files.notExists(storeBaseDir)) {
			try {
				Files.createDirectory(storeBaseDir);

				LOGGER.debug("storeBase directory %s created.", storeBaseDir.toAbsolutePath());
			} catch (Exception e) {
				final Message msg = new StringFormattedMessage("Impossible to create the storeBase directory %s: %s",
						storeBaseDir.toAbsolutePath(), e.getMessage());
				LOGGER.error(msg, e);
				throw new ComponentException(msg.getFormattedMessage(), e);
			}
		}

		if (!Files.isWritable(storeBaseDir)) {
			final Message msg = new StringFormattedMessage("Store base directory %s cannot be written.",
					storeBaseDir.toAbsolutePath());
			LOGGER.error(msg);
			throw new ComponentException(msg.getFormattedMessage());
		}

		if (!Files.isDirectory(storeBaseDir)) {
			final Message msg = new StringFormattedMessage("%s expected to be a store base directory.",
					storeBaseDir.toAbsolutePath());
			LOGGER.error(msg);
			throw new ComponentException(msg.getFormattedMessage());
		}

	}
}
