/*
 * Copyright (c) 2021-2022 University of Padua, Italy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.unipd.dei.aecpkg.datastore;


import it.unipd.dei.aecpkg.component.ComponentException;
import it.unipd.dei.aecpkg.component.ComponentManager;
import it.unipd.dei.aecpkg.component.InvalidResourceException;
import it.unipd.dei.aecpkg.jwt.JwtManager;
import it.unipd.dei.aecpkg.mail.MailManager;
import it.unipd.dei.aecpkg.resource.*;
import org.apache.logging.log4j.message.Message;
import org.apache.logging.log4j.message.StringFormattedMessage;
import org.jose4j.jwt.JwtClaims;

import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * Implements the {@link ArtifactDatastore} interface by providing a starting implementation suitable for all the
 * concrete subclasses.
 *
 * @author Nicola Ferro
 * @version 1.00
 * @since 1.00
 */
public abstract class AbstractArtifactDatastore extends AbstractDatastore<Artifact> implements ArtifactDatastore {

	/**
	 * The format for the name of the zip file containing the artifact package.
	 */
	protected static final String PACKAGE_NAME_FORMAT = "artifacts_%1$d_%2$tY%2$tm%2$td.zip";

	/**
	 * The MailManager
	 */
	private final MailManager mailManager;

	/**
	 * The JwtManager
	 */
	private final JwtManager jwtManager;

	/**
	 * The UserDatastore
	 */
	private final UserDatastore userDatastore;

	/**
	 * The recipient of email in the ACM DL staff
	 */
	private final String acmRecipient;

	/**
	 * The DOI prefix for artifacts in the ACM DL
	 */
	private final String doiPrefix;

	/**
	 * The URL pattern for downloading artifacts
	 */
	private final String downloadUrlPattern;

	/**
	 * The XML output factory.
	 */
	private final XMLOutputFactory xof;

	/**
	 * Creates a new {@code ArtifactDatastore}.
	 *
	 * @param configFileName the name of the file containing the configuration for this component.
	 *
	 * @throws ComponentException if something goes wrong while creating the component.
	 */
	protected AbstractArtifactDatastore(final String configFileName) throws ComponentException {
		super(configFileName);

		jwtManager = ComponentManager.getComponent(JwtManager.class).orElseThrow(() -> {
			final Message msg = new StringFormattedMessage("No JwtManager available.");
			LOGGER.error(msg);
			return new ComponentException(msg.getFormattedMessage());
		});

		mailManager = ComponentManager.getComponent(MailManager.class).orElseThrow(() -> {
			final Message msg = new StringFormattedMessage("No MailManager available.");
			LOGGER.error(msg);
			return new ComponentException(msg.getFormattedMessage());
		});

		userDatastore = ComponentManager.getComponent(UserDatastore.class).orElseThrow(() -> {
			final Message msg = new StringFormattedMessage("No UserDatastore available.");
			LOGGER.error(msg);
			return new ComponentException(msg.getFormattedMessage());
		});

		// the StAX XML output factory
		xof = XMLOutputFactory.newInstance();
		xof.setProperty(XMLOutputFactory.IS_REPAIRING_NAMESPACES, Boolean.TRUE);

		String tmp = config.get(ArtifactDatastore.class.getName() + ".acmRecipient");
		if (tmp == null || tmp.isBlank()) {
			final Message msg = new StringFormattedMessage("Property %s missing or empty.",
					ArtifactDatastore.class.getName() + ".acmRecipient");
			LOGGER.error(msg);
			throw new ComponentException(msg.getFormattedMessage());
		}
		acmRecipient = tmp;

		tmp = config.get(ArtifactDatastore.class.getName() + ".doiPrefix");
		if (tmp == null || tmp.isBlank()) {
			final Message msg = new StringFormattedMessage("Property %s missing or empty.",
					ArtifactDatastore.class.getName() + ".doiPrefix");
			LOGGER.error(msg);
			throw new ComponentException(msg.getFormattedMessage());
		}
		doiPrefix = tmp;

		tmp = config.get(ArtifactDatastore.class.getName() + ".downloadPattern");
		if (tmp == null || tmp.isBlank()) {
			final Message msg = new StringFormattedMessage("Property %s missing or empty.",
					ArtifactDatastore.class.getName() + ".downloadPattern");
			LOGGER.error(msg);
			throw new ComponentException(msg.getFormattedMessage());
		}
		downloadUrlPattern = tmp;
	}


	@Override
	public BinaryObject readPackage(final Artifact a) throws ComponentException {

		// Checks control access rules
		ac.controlAccess();

		// Checks input parameters
		if (a == null) {
			final Message msg = new StringFormattedMessage("Artifact cannot be null.");
			LOGGER.error(msg);
			throw new NullPointerException(msg.getFormattedMessage());
		}

		return null;
	}

	@Override
	public final String getArtifactDownloadLink(final Artifact a, final User u) throws ComponentException {

		// Checks control access rules
		ac.controlAccess();

		// Checks input parameters
		if (a == null) {
			final Message msg = new StringFormattedMessage("Artifact cannot be null.");
			LOGGER.error(msg);
			throw new NullPointerException(msg.getFormattedMessage());
		}

		// Checks input parameters
		if (u == null) {
			final Message msg = new StringFormattedMessage("User cannot be null.");
			LOGGER.error(msg);
			throw new NullPointerException(msg.getFormattedMessage());
		}

		final JwtClaims claims = new JwtClaims();
		claims.setSubject(u.getIdentifier());
		claims.setClaim("role", u.getRole());

		if(u.getFirstName() != null) {
			claims.setClaim("firstName", u.getFirstName());
		}

		if(u.getLastName() != null) {
			claims.setClaim("lastName", u.getLastName());
		}

		if(u.getLanguage() != null) {
			claims.setClaim("language", u.getLanguage().getTwoLettersCode());
		}

		claims.setClaim("created", a.getCreated().toString());
		claims.setClaim("acmID", a.getIdentifier());
		claims.setClaim("orID", String.valueOf(a.getOpenReviewID()));

		return String.format(downloadUrlPattern, jwtManager.encodeJWE(claims));

	}


	/**
	 * Creates a {@code manifest.xml} file for the artifact package.
	 *
	 * @return a {@code manifest.xml} file for the artifact package.
	 *
	 * @throws ComponentException if something goes wrong while creating the file.
	 */
	protected final BinaryObject createManifest() throws ComponentException {

		final BinaryObject bo = BinaryObject.create("manifest.xml", MediaType.TEXT_XML_UTF8);
		final OutputStream out = bo.setContentAsBinaryStream();

		XMLStreamWriter xsw = null;

		try { // note that XMLStreamWriter does not implement AutoCloseable, so we cannot use try-with-resources
			xsw = new PrettyPrintXMLStreamWriter(xof.createXMLStreamWriter(out, "UTF-8"));

			xsw.writeStartDocument("UTF-8", "1.0");

			xsw.writeDTD(
					"<!DOCTYPE submission PUBLIC \"-//Atypon//DTD Literatum Content Submission Manifest DTD v4.2 20140519//EN\" \"atypon/submissionmanifest.4.2.dtd\">");

			xsw.writeStartElement("submission");
			xsw.writeAttribute("group-doi", "10.1145/artifacts-group");
			xsw.writeAttribute("submission-type", "full");

			xsw.writeStartElement("callback");
			xsw.writeStartElement("email");
			xsw.writeCharacters("info@conference-publishing.com");
			xsw.writeEndElement();
			xsw.writeEndElement();

			xsw.writeStartElement("processing-instructions");
			xsw.writeEmptyElement("make-live");
			xsw.writeAttribute("on-condition", "no-fatals");
			xsw.writeEndElement();

			xsw.writeEndElement();

			xsw.writeEndDocument();


		} catch (final XMLStreamException xse) {
			final Message msg = new StringFormattedMessage("Unable use the XML serializer: %s", xse.getMessage());
			LOGGER.error(msg, xse);
			throw new ComponentException(msg.getFormattedMessage(), xse);
		} finally {

			if (xsw != null) {
				try {
					xsw.close();
					out.close();

				} catch (XMLStreamException | IOException e) {
					final Message msg = new StringFormattedMessage("Unable to close the XML serializer: %s",
							e.getMessage());
					LOGGER.error(msg, e);
					throw new ComponentException(msg.getFormattedMessage(), e);
				}
			}


		}

		return bo;

	}


	/**
	 * Creates a {@code acmID.xml} file for the artifact package.
	 *
	 * @param a the artifact for which the file has to be generated.
	 *
	 * @return a {@code acmID.xml} file for the artifact package.
	 *
	 * @throws ComponentException if something goes wrong while creating the file.
	 */
	protected final BinaryObject createMeta(final Artifact a) throws ComponentException {

		final BinaryObject bo = BinaryObject.create(String.format("%s.xml", a.getIdentifier()),
				MediaType.TEXT_XML_UTF8);
		final OutputStream out = bo.setContentAsBinaryStream();

		XMLStreamWriter xsw = null;

		try { // note that XMLStreamWriter does not implement AutoCloseable, so we cannot use try-with-resources
			xsw = new PrettyPrintXMLStreamWriter(xof.createXMLStreamWriter(out, "UTF-8"));

			xsw.writeStartDocument("UTF-8", "1.0");

			xsw.writeStartElement("mets");
			xsw.writeDefaultNamespace("http://www.loc.gov/METS/");
			xsw.writeNamespace("xlink", "http://www.w3.org/1999/xlink");
			xsw.writeNamespace("xsi", "http://www.w3.org/2001/XMLSchema-instance");
			xsw.writeAttribute("http://www.w3.org/2001/XMLSchema-instance", "schemaLocation",
					"http://www.loc.gov/METS/ http://www.loc.gov/standards/mets/mets.xsd");
			xsw.writeAttribute("TYPE", "artifact-doe");

			xsw.writeStartElement("mets", "dmdSec", "http://www.loc.gov/METS/");
			xsw.writeNamespace("mets", "http://www.loc.gov/METS/");
			xsw.writeAttribute("ID", "DMD");

			xsw.writeStartElement("mets", "mdWrap", "http://www.loc.gov/METS/");
			xsw.writeAttribute("MDTYPE", "MODS");

			xsw.writeStartElement("mets", "xmlData", "http://www.loc.gov/METS/");

			xsw.writeStartElement("mods");
			xsw.writeAttribute("xmlns", "http://www.loc.gov/mods/v3");
			xsw.writeAttribute("http://www.w3.org/2001/XMLSchema-instance", "schemaLocation",
					"http://www.loc.gov/mods/v3 http://www.loc.gov/standards/mods/v3/mods.xsd");

			xsw.writeStartElement("mods", "identifier", "http://www.loc.gov/mods/v3");
			xsw.writeAttribute("type", "doi");
			xsw.writeCharacters(String.format("%s%s", doiPrefix, a.getIdentifier()));
			xsw.writeEndElement(); // identifier

			xsw.writeStartElement("mods", "titleInfo", "http://www.loc.gov/mods/v3");
			xsw.writeAttribute("ID", "title");

			xsw.writeStartElement("mods", "title", "http://www.loc.gov/mods/v3");
			xsw.writeCharacters(a.getTitle());
			xsw.writeEndElement(); // title

			xsw.writeEmptyElement("mods", "subTitle", "http://www.loc.gov/mods/v3");

			xsw.writeEndElement(); // titleInfo

			int i = 1;
			for (Artifact.Author au : a.getAuthors()) {

				xsw.writeStartElement("mods", "name", "http://www.loc.gov/mods/v3");
				xsw.writeAttribute("ID", String.format("artseq-%d", i++));

				xsw.writeStartElement("mods", "namePart", "http://www.loc.gov/mods/v3");
				xsw.writeAttribute("type", "given");
				xsw.writeCharacters(au.firstName());
				xsw.writeEndElement(); // namePart

				xsw.writeStartElement("mods", "namePart", "http://www.loc.gov/mods/v3");
				xsw.writeAttribute("type", "family");
				xsw.writeCharacters(au.lastName());
				xsw.writeEndElement(); // namePart

				xsw.writeStartElement("mods", "namePart", "http://www.loc.gov/mods/v3");
				xsw.writeAttribute("type", "termsOfAddress");
				xsw.writeEndElement(); // namePart

				xsw.writeStartElement("mods", "displayForm", "http://www.loc.gov/mods/v3");
				xsw.writeCharacters(au.displayName());
				xsw.writeEndElement(); // displayForm

				xsw.writeStartElement("mods", "nameIdentifier", "http://www.loc.gov/mods/v3");
				xsw.writeAttribute("type", "ORCID");
				xsw.writeEndElement(); // nameIdentifier

				xsw.writeStartElement("mods", "role", "http://www.loc.gov/mods/v3");
				xsw.writeStartElement("mods", "roleTerm", "http://www.loc.gov/mods/v3");
				xsw.writeCharacters("Contributor");
				xsw.writeEndElement(); // roleTerm
				xsw.writeEndElement(); // role

				xsw.writeStartElement("mods", "nameIdentifier", "http://www.loc.gov/mods/v3");
				xsw.writeAttribute("type", "email");
				xsw.writeEndElement(); // nameIdentifier

				xsw.writeStartElement("mods", "affiliation", "http://www.loc.gov/mods/v3");
				xsw.writeCharacters(au.affiliation());
				xsw.writeEndElement(); // affiliation

				xsw.writeEndElement(); // name
			}

			xsw.writeStartElement("mods", "subject", "http://www.loc.gov/mods/v3");
			xsw.writeAttribute("authority", "artifact_type");
			xsw.writeAttribute("ID", "type");
			xsw.writeStartElement("mods", "topic", "http://www.loc.gov/mods/v3");
			xsw.writeAttribute("authority", a.getType().getAcmDL());
			xsw.writeCharacters(a.getType().getOfficialName());
			xsw.writeEndElement(); // topic
			xsw.writeEndElement(); // subject

			xsw.writeStartElement("mods", "subject", "http://www.loc.gov/mods/v3");
			xsw.writeAttribute("authority", "reproducibility-types");
			xsw.writeAttribute("ID", "badges");
			for (Badge b : a.getBadges()) {
				xsw.writeStartElement("mods", "topic", "http://www.loc.gov/mods/v3");
				xsw.writeAttribute("authority", b.getAcmDL());
				xsw.writeCharacters(b.getOfficialName());
				xsw.writeEndElement(); // topic
			}
			xsw.writeEndElement(); // subject

			xsw.writeStartElement("mods", "relatedItem", "http://www.loc.gov/mods/v3");
			xsw.writeAttribute("displayLabel", "Related Article");
			xsw.writeAttribute("xlink", "http://www.w3.org/1999/xlink", "href", a.getRelatedPaperDOI());
			xsw.writeAttribute("ID", "relatedDoi01");
			xsw.writeEndElement(); // relatedItem

			xsw.writeStartElement("mods", "extension", "http://www.loc.gov/mods/v3");

			xsw.writeStartElement("atpn", "do-extensions", "http://www.atypon.com/digital-objects");
			xsw.writeAttribute("http://www.w3.org/2001/XMLSchema-instance", "schemaLocation",
					"http://www.atypon.com/digital-objects http://www.atypon.com/digital-objects/digital-objects.xsd");

			xsw.writeStartElement("atpn", "description", "http://www.atypon.com/digital-objects");
			xsw.writeCData(String.format("<p>%s</p>", a.getAbstract()));
			xsw.writeEndElement(); // description

			xsw.writeStartElement("atpn", "copyright", "http://www.atypon.com/digital-objects");
			xsw.writeCharacters("Author(s)");
			xsw.writeEndElement(); // copyright

			xsw.writeStartElement("atpn", "version", "http://www.atypon.com/digital-objects");
			xsw.writeCharacters("1.0");
			xsw.writeEndElement(); // version

			xsw.writeEmptyElement("atpn", "softwareDependencies", "http://www.atypon.com/digital-objects");
			xsw.writeEmptyElement("atpn", "hardwareDependencies", "http://www.atypon.com/digital-objects");
			xsw.writeEmptyElement("atpn", "installation", "http://www.atypon.com/digital-objects");
			xsw.writeEmptyElement("atpn", "otherInstructions", "http://www.atypon.com/digital-objects");
			xsw.writeEmptyElement("atpn", "eiInstallation", "http://www.atypon.com/digital-objects");
			xsw.writeEmptyElement("atpn", "eiParameterization", "http://www.atypon.com/digital-objects");
			xsw.writeEmptyElement("atpn", "eiEvaluation", "http://www.atypon.com/digital-objects");
			xsw.writeEmptyElement("atpn", "eiWorkflow", "http://www.atypon.com/digital-objects");
			xsw.writeEmptyElement("atpn", "eiOtherInstructions", "http://www.atypon.com/digital-objects");
			xsw.writeEmptyElement("atpn", "dataDocumentation", "http://www.atypon.com/digital-objects");

			xsw.writeStartElement("atpn", "provenance", "http://www.atypon.com/digital-objects");
			if (a.getExternalRepository() == null) {
				xsw.writeCData(String.format(
						"<p>OpenReview for this artifact <a href=\"%1$s\" target=\"_blank\">%1$s</a>.</p>",
						a.getOpenReviewURL()));
			} else {
				xsw.writeCData(String.format(
						"<p>%1$s</p>%n<p>OpenReview for this artifact <a href=\"%2$s\" target=\"_blank\">%2$s</a>.</p>",
						a.getExternalRepository(), a.getOpenReviewURL()));
			}
			xsw.writeEndElement(); // provenance

			xsw.writeEmptyElement("atpn", "textFiles", "http://www.atypon.com/digital-objects");

			xsw.writeStartElement("atpn", "packageFiles", "http://www.atypon.com/digital-objects");
			xsw.writeAttribute("nested-label", "NONE");
			xsw.writeStartElement("atpn", "nestedValue", "http://www.atypon.com/digital-objects");
			xsw.writeAttribute("alt-text", "Artifact");
			xsw.writeCharacters(a.getArtifact().getIdentifier());
			xsw.writeEndElement(); // nestedValue
			xsw.writeEndElement(); // packageFiles

			xsw.writeStartElement("atpn", "accessCondition", "http://www.atypon.com/digital-objects");
			xsw.writeCharacters("free");
			xsw.writeEndElement(); // accessCondition

			xsw.writeEmptyElement("atpn", "licenseUrl", "http://www.atypon.com/digital-objects");

			xsw.writeStartElement("atpn", "keywords", "http://www.atypon.com/digital-objects");
			xsw.writeAttribute("nested-label", "NONE");

			i = 1;
			for (String k : a.getKeywords()) {
				xsw.writeStartElement("atpn", "nestedValue", "http://www.atypon.com/digital-objects");
				xsw.writeAttribute("id", String.format("kw%d", i++));
				xsw.writeCharacters(k);
				xsw.writeEndElement(); // nestedValue

			}
			xsw.writeEndElement(); // keywords

			xsw.writeStartElement("atpn", "baseDoi", "http://www.atypon.com/digital-objects");
			xsw.writeCharacters("10.1145/artifact-doe-class");
			xsw.writeEndElement(); // baseDoi

			xsw.writeEndElement(); // do-extensions

			xsw.writeEndElement(); // extension

			xsw.writeStartElement("mods", "originInfo", "http://www.loc.gov/mods/v3");
			xsw.writeStartElement("mods", "dateIssued", "http://www.loc.gov/mods/v3");
			xsw.writeCharacters(String.format("%tF", a.getCreated()));
			xsw.writeEndElement(); // dateIssued
			xsw.writeEndElement(); // originInfo

			xsw.writeEndElement(); // mods

			xsw.writeEndElement(); // xmlData

			xsw.writeEndElement(); // mdWrap

			xsw.writeEndElement(); // dmdSec

			xsw.writeStartElement("mets", "fileSec", "http://www.loc.gov/METS/");
			xsw.writeStartElement("mets", "fileGrp", "http://www.loc.gov/METS/");
			xsw.writeAttribute("ID", "packageFiles-group");
			xsw.writeStartElement("mets", "file", "http://www.loc.gov/METS/");
			xsw.writeAttribute("ID", String.format("packageFiles-%s", a.getArtifact().getIdentifier()));
			xsw.writeStartElement("mets", "FLocat", "http://www.loc.gov/METS/");
			xsw.writeAttribute("LOCTYPE", "URL");
			xsw.writeAttribute("xlink", "http://www.w3.org/1999/xlink", "href",
					String.format("file://%s", a.getArtifact().getIdentifier()));
			xsw.writeEndElement(); // FLocat
			xsw.writeEndElement(); // file
			xsw.writeEndElement(); // fileGrp
			xsw.writeEndElement(); // fileSec

			xsw.writeStartElement("mets", "structMap", "http://www.loc.gov/METS/");
			xsw.writeStartElement("mets", "div", "http://www.loc.gov/METS/");
			xsw.writeEndElement(); // div
			xsw.writeEndElement(); // structMap

			xsw.writeEndElement(); // mets

			xsw.writeEndDocument();


		} catch (final XMLStreamException xse) {
			final Message msg = new StringFormattedMessage("Unable use the XML serializer: %s", xse.getMessage());
			LOGGER.error(msg, xse);
			throw new ComponentException(msg.getFormattedMessage(), xse);
		} finally {

			if (xsw != null) {
				try {
					xsw.close();
					out.close();
				} catch (XMLStreamException | IOException e) {
					final Message msg = new StringFormattedMessage("Unable to close the XML serializer: %s",
							e.getMessage());
					LOGGER.error(msg, e);
					throw new ComponentException(msg.getFormattedMessage(), e);
				}
			}


		}

		return bo;
	}

	/**
	 * Creates the overall zip file for the artifact package.
	 *
	 * @param a the artifact for which the file has to be generated.
	 *
	 * @return the overall zip file for the artifact package.
	 *
	 * @throws ComponentException if something goes wrong while creating the file.
	 */
	protected final BinaryObject createZipPackage(final Artifact a) throws ComponentException {

		final BinaryObject bo = BinaryObject.create(
				String.format(PACKAGE_NAME_FORMAT, Integer.valueOf(a.getIdentifier()), a.getCreated()),
				MediaType.APPLICATION_ZIP);

		final BinaryObject manifest = createManifest();

		final BinaryObject meta = createMeta(a);

		try (OutputStream out = bo.setContentAsBinaryStream(); ZipOutputStream zout = new ZipOutputStream(out)) {
			ZipEntry ze = new ZipEntry(manifest.getIdentifier());
			zout.putNextEntry(ze);
			manifest.getContentAsBinaryStream().transferTo(zout);
			zout.closeEntry();

			ze = new ZipEntry(String.format("%s/", a.getIdentifier()));
			zout.putNextEntry(ze);

			ze = new ZipEntry(String.format("%s/%s", a.getIdentifier(), a.getArtifact().getIdentifier()));
			zout.putNextEntry(ze);
			a.getArtifact().getContentAsBinaryStream().transferTo(zout);
			zout.closeEntry();

			ze = new ZipEntry(String.format("%s/meta/", a.getIdentifier()));
			zout.putNextEntry(ze);

			ze = new ZipEntry(String.format("%s/meta/%s", a.getIdentifier(), meta.getIdentifier()));
			zout.putNextEntry(ze);
			meta.getContentAsBinaryStream().transferTo(zout);
			zout.closeEntry();

		} catch (IOException e) {
			final Message msg = new StringFormattedMessage("Unable to use the zip serializer: %s", e.getMessage());
			LOGGER.error(msg, e);
			throw new ComponentException(msg.getFormattedMessage(), e);
		}

		return bo;

	}

	/**
	 * Sends the e-mail about an artifact being ready for upload to the ACM DL.
	 *
	 * @param a the artifact.
	 *
	 * @throws ComponentException if there is any problem in sending the e-mail.
	 */
	protected final void sendArtifactReadyForUploadEmail(final Artifact a) throws ComponentException {

		// load the recipient of the email and check that it is a valid user
		final User u = userDatastore.read(User.create(acmRecipient));

		if (Role.ACM != u.getRole()) {
			final Message msg = new StringFormattedMessage("ACM recipient of emails %s has role %s instead of %s.",
					u.getIdentifier(), u.getRole(), Role.ACM);
			LOGGER.error(msg);
			throw new InvalidResourceException(msg.getFormattedMessage());
		}


		mailManager.sendArtifactReadyForUploadEmail(u, a, getArtifactDownloadLink(a, u));

	}


}
