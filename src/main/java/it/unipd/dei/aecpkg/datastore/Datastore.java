/*
 * Copyright (c) 2021-2022 University of Padua, Italy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.unipd.dei.aecpkg.datastore;


import it.unipd.dei.aecpkg.component.*;
import it.unipd.dei.aecpkg.resource.Resource;

/**
 * Defines the common methods for managing the persistence of a {@link Resource}s.
 *
 * @param <R> the type of {@code Resource} whose persistence is managed by this {@code Datastore}.
 *
 * @author Nicola Ferro
 * @version 1.00
 * @since 1.00
 */
public interface Datastore<R extends Resource<?>> extends Component {

    /**
     * Creates a new {@code Resource}.
     *
     * @param r the resource to be created.
     *
     * @return the created resource as contained in the system.
     *
     * @throws DuplicatedResourceException     if the resource already exists.
     * @throws ResourceNotFoundException       if any resources related to the given on do not exist.
     * @throws AuthenticationRequiredException if authentication is required and no user is contained in the {@code
     *                                         ApplicationContext}.
     * @throws AuthorizationRequiredException  if the user currently contained in the {@code ApplicationContext} has
     *                                         insufficient rights to perform the requested {@code Action}.
     * @throws ComponentException              if there is any problem accessing the system.
     * @throws NullPointerException            if the resource {@code r} is {@code null}.
     * @throws UnsupportedOperationException   if the {@code create} operation is not supported by this {@code
     *                                         Datastore}.
     */
    R create(R r) throws ComponentException;

    /**
     * Reads all the information about the resource {@code r}.
     *
     * @param r the resource to be read.
     *
     * @return the resource.
     *
     * @throws ResourceNotFoundException       if the resource does not exist.
     * @throws AuthenticationRequiredException if authentication is required and no user is contained in the {@code
     *                                         ApplicationContext}.
     * @throws AuthorizationRequiredException  if the user currently contained in the {@code ApplicationContext} has
     *                                         insufficient rights to perform the requested {@code Action}.
     * @throws ComponentException              if there is any problem accessing the system.
     * @throws NullPointerException            if the resource {@code r} is {@code null}.
     * @throws UnsupportedOperationException   if the {@code read} operation is not supported by this {@code
     *                                         Datastore}.
     */
    R read(R r) throws ComponentException;

    /**
     * Updates the information about a resource. Any pre-existing information is
     * overwritten. The identifier of the resource cannot be updated.
     *
     * @param r the resource to update.
     *
     * @return the updated resource as contained in the system.
     *
     * @throws ResourceNotFoundException       if the resource does not exist.
     * @throws ConcurrentUpdateException       if the resource has been concurrently updated.
     * @throws AuthenticationRequiredException if authentication is required and no user is contained in the {@code
     *                                         ApplicationContext}.
     * @throws AuthorizationRequiredException  if the user currently contained in the {@code ApplicationContext} has
     *                                         insufficient rights to perform the requested {@code Action}.
     * @throws ComponentException              if there is any problem accessing the system.
     * @throws NullPointerException            if the resource {@code r} is {@code null}.
     * @throws UnsupportedOperationException   if the {@code update} operation is not supported by this {@code
     *                                         Datastore}.
     */
    R update(R r) throws ComponentException;


    /**
     * Deletes all the information about a resource.
     *
     * A {@code Resource} can be deleted only if it is not related to any other resources.
     *
     * @param r the resource to delete.
     *
     * @return the deleted resource as contained in the system.
     *
     * @throws ResourceNotFoundException       if the resource does not exist.
     * @throws ConcurrentUpdateException       if the resource has been concurrently updated.
     * @throws NotModifiableResourceException  if the resource cannot be deleted.
     * @throws AuthenticationRequiredException if authentication is required and no user is contained in the {@code
     *                                         ApplicationContext}.
     * @throws AuthorizationRequiredException  if the user currently contained in the {@code ApplicationContext} has
     *                                         insufficient rights to perform the requested {@code Action}.
     * @throws ComponentException              if there is any problem accessing the system.
     * @throws NullPointerException            if the resource {@code r} is {@code null}.
     * @throws UnsupportedOperationException   if the {@code delete} operation is not supported by this {@code
     *                                         Datastore}.
     */
    R delete(R r) throws ComponentException;


    /**
     * Returns a list of all the existing {@code Resource}s.
     *
     * @return an {@code Iterable} over a list of {@code Resource}s.
     *
     * @throws AuthenticationRequiredException if authentication is required and no user is contained in the {@code
     *                                         ApplicationContext}.
     * @throws AuthorizationRequiredException  if the user currently contained in the {@code ApplicationContext} has
     *                                         insufficient rights to perform the requested {@code Action}.
     * @throws ComponentException              if there is any problem accessing the system.
     * @throws UnsupportedOperationException   if the {@code list} operation is not supported by this {@code
     *                                         Datastore}.
     */
    Iterable<R> list() throws ComponentException;

}
