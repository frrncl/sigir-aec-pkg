/*
 * Copyright (c) 2021-2022 University of Padua, Italy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.unipd.dei.aecpkg.datastore;


import it.unipd.dei.aecpkg.component.*;
import it.unipd.dei.aecpkg.resource.User;

/**
 * Defines the methods for managing the persistence of {@link User} resources.
 *
 * @author Nicola Ferro
 * @version 1.00
 * @since 1.00
 */
public interface UserDatastore extends Datastore<User> {

    /**
     * The possible actions for {@code UserDatastore}.
     *
     * @author Nicola Ferro
     * @version 1.00
     * @since 1.00
     */
    final class ACTIONS {

        /**
         * The authentication of a {@code User}.
         *
         * See {@link UserDatastore#authenticate(User)}.
         */
        public static final String AUTHENTICATE_USER = "AUTHENTICATE_USER";

        /**
         * The log out of a {@code User}.
         */
        public static final String LOGOUT_USER = "LOGOUT_USER";

    }


    /**
     * Authenticates a user, given a {@code User} object where the user name, and the password are specified.
     *
     * <p> Note that this method uses only {@link User#getIdentifier()}  and {@link User#getPassword()} to authenticate
     * the user. </p>
     *
     * @param u the user to authenticate.
     *
     * @return the authenticated user.
     *
     * @throws ResourceNotFoundException       if the user does not exist or if the user cannot be successfully
     *                                         authenticated due to a wrong {@code (username, password)} pair.
     * @throws AuthenticationRequiredException if authentication is required and no user is contained in the {@code
     *                                         ApplicationContext}.
     * @throws AuthorizationRequiredException  if the user currently contained in the {@code ApplicationContext} has
     *                                         insufficient rights to perform the requested {@code Action}.
     * @throws OperationException              if there is no {@code Action} currently contained in the {@code
     *                                         ApplicationContext}.
     * @throws ComponentException              if there is any problem while accessing the system.
     * @throws NullPointerException            if the user is {@code null}.
     */
    User authenticate(User u) throws ComponentException;

}
