/*
 * Copyright (C) 2019. Nutretech s.r.l., Italy - All rights reserved.
 */

package it.unipd.dei.aecpkg.datastore.file;


import it.unipd.dei.aecpkg.component.ApplicationContext;
import it.unipd.dei.aecpkg.component.AuthorizationRequiredException;
import it.unipd.dei.aecpkg.component.ComponentException;
import it.unipd.dei.aecpkg.component.ResourceNotFoundException;
import it.unipd.dei.aecpkg.datastore.AbstractUserDatastore;
import it.unipd.dei.aecpkg.datastore.UserDatastore;
import it.unipd.dei.aecpkg.resource.Country;
import it.unipd.dei.aecpkg.resource.Language;
import it.unipd.dei.aecpkg.resource.Role;
import it.unipd.dei.aecpkg.resource.User;
import org.apache.logging.log4j.message.Message;
import org.apache.logging.log4j.message.StringFormattedMessage;

import java.util.HashMap;

/**
 * Implements the {@link UserDatastore} interface by relying on the file system.
 *
 * @author Nicola Ferro
 * @version 1.00
 * @since 1.00
 */
public final class FileUserDatastore extends AbstractUserDatastore {

	/**
	 * Name of the default configuration file.
	 */
	private static final String DEFAULT_CONFIG_FILE = "userDatastore.properties";

	/**
	 * Name of the property defining the labels of the components to be run.
	 */
	private static final String PROP_USER_LABELS = UserDatastore.class.getName() + ".users";

	/**
	 * Name of the prefix of the property defining the configuration of a user.
	 */
	private static final String PROP_USER_PREFIX = UserDatastore.class.getName() + ".user.";

	/**
	 * Name of the postfix of the property defining the email of a user.
	 */
	private static final String PROP_USER_EMAIL_POSTFIX = ".email";

	/**
	 * Name of the postfix of the property defining the password of a user.
	 */
	private static final String PROP_USER_PASSWORD_POSTFIX = ".password";

	/**
	 * Name of the postfix of the property defining the role of a user.
	 */
	private static final String PROP_USER_ROLE_POSTFIX = ".role";

	/**
	 * Name of the postfix of the property defining the first name of a user.
	 */
	private static final String PROP_USER_FIRSTNAME_POSTFIX = ".firstName";

	/**
	 * Name of the postfix of the property defining the last name of a user.
	 */
	private static final String PROP_USER_LASTNAME_POSTFIX = ".lastName";

	/**
	 * Name of the postfix of the property defining the affiliation of a user.
	 */
	private static final String PROP_USER_AFFILIATION_POSTFIX = ".affiliation";

	/**
	 * Name of the postfix of the property defining the country of a user.
	 */
	private static final String PROP_USER_COUNTRY_POSTFIX = ".country";

	/**
	 * Name of the postfix of the property defining the language of a user.
	 */
	private static final String PROP_USER_LANGUAGE_POSTFIX = ".language";

	/**
	 * The user "database": keys are user identifiers; values are {@code User} objects.
	 */
	private final HashMap<String, User> users;

	/**
	 * Creates a new {@code UserDatastore}.
	 *
	 * @param configFileName the name of the file containing the configuration for this component.
	 *
	 * @throws ComponentException if something goes wrong while creating the component.
	 */
	public FileUserDatastore(final String configFileName) throws ComponentException {
		super(configFileName);

		users = new HashMap<>();
	}

	/**
	 * Constructs a component with default configuration.
	 *
	 * @throws ComponentException if something goes wrong while creating the component.
	 */
	public FileUserDatastore() throws ComponentException {
		this(DEFAULT_CONFIG_FILE);
	}

	/*
	 * -------------------------------------------------------------------------
	 * Methods defined in the interface UserDatastore follow.
	 * -------------------------------------------------------------------------
	 */

	@Override
	public final User create(final User u) throws ComponentException {

		super.create(u);

		final Message msg = new StringFormattedMessage("Operation not supported.");
		LOGGER.error(msg);
		throw new UnsupportedOperationException(msg.getFormattedMessage());

	}

	@Override
	public final User read(final User u) throws ComponentException {

		super.read(u);

		final User uu = users.get(u.getIdentifier());

		if (uu == null) {
			final Message msg = new StringFormattedMessage("User %s not found.", u.getIdentifier());
			LOGGER.error(msg);
			throw new ResourceNotFoundException(msg.getFormattedMessage());
		}

		if (uu.getRole().compareTo(ApplicationContext.getUser().getRole()) <= 0) {
			final Message msg = new StringFormattedMessage(
					"User %s with role %s cannot read user %s with role %s, which is equal to or greater than the other role.",
					ApplicationContext.getUser().getIdentifier(), ApplicationContext.getUser().getRole(),
					uu.getIdentifier(), uu.getRole());
			LOGGER.error(msg);
			throw new AuthorizationRequiredException(msg.getFormattedMessage());
		}

		return uu;
	}

	@Override
	public final User update(final User u) throws ComponentException {

		super.update(u);

		final Message msg = new StringFormattedMessage("Operation not supported.");
		LOGGER.error(msg);
		throw new UnsupportedOperationException(msg.getFormattedMessage());

	}

	@Override
	public final User delete(final User u) throws ComponentException {

		super.delete(u);

		final Message msg = new StringFormattedMessage("Operation not supported.");
		LOGGER.error(msg);
		throw new UnsupportedOperationException(msg.getFormattedMessage());

	}

	@Override
	public final Iterable<User> list() throws ComponentException {

		super.list();

		final Message msg = new StringFormattedMessage("Operation not supported.");
		LOGGER.error(msg);
		throw new UnsupportedOperationException(msg.getFormattedMessage());

	}

	@Override
	public final User authenticate(final User u) throws ComponentException {

		super.authenticate(u);

		final User uu = users.get(u.getIdentifier());

		if (uu == null) {
			final Message msg = new StringFormattedMessage("User %s not found.", u.getIdentifier());
			LOGGER.error(msg);
			throw new ResourceNotFoundException(msg.getFormattedMessage());
		}

		if (!uu.getPassword().equals(u.getPassword())) {
			final Message msg = new StringFormattedMessage("Wrong password for user %s.", u.getIdentifier());
			LOGGER.error(msg);
			throw new ResourceNotFoundException(msg.getFormattedMessage());
		}

		return uu;
	}

	/*-------------------------------------------------------------------------
	 * Methods defined in the superclass AbstractComponent follow.
	 *-------------------------------------------------------------------------
	 */

	@Override
	protected final void doStart() throws Exception {

		super.doStart();

		// parse the declared users
		if (config.containsKey(PROP_USER_LABELS)) {

			// process users
			for (final String label : parseList(config.get(PROP_USER_LABELS))) {

				LOGGER.debug("Start processing configuration of user %s.", label);

				final String email = config.get(PROP_USER_PREFIX + label + PROP_USER_EMAIL_POSTFIX);
				final String password = config.get(PROP_USER_PREFIX + label + PROP_USER_PASSWORD_POSTFIX);
				final Role role = Role.parse(config.get(PROP_USER_PREFIX + label + PROP_USER_ROLE_POSTFIX));
				final String firstName = config.get(PROP_USER_PREFIX + label + PROP_USER_FIRSTNAME_POSTFIX);
				final String lastName = config.get(PROP_USER_PREFIX + label + PROP_USER_LASTNAME_POSTFIX);
				final String affiliation = config.get(PROP_USER_PREFIX + label + PROP_USER_AFFILIATION_POSTFIX);
				final Country country = Country.parse(config.get(PROP_USER_PREFIX + label + PROP_USER_COUNTRY_POSTFIX));
				final Language language = Language.parse(
						config.get(PROP_USER_PREFIX + label + PROP_USER_LANGUAGE_POSTFIX));

				if (email == null) {
					final Message msg = new StringFormattedMessage(
							"Invalid configuration for user %s: no email defined.", label);
					LOGGER.error(msg);
					throw new ComponentException(msg.getFormattedMessage());
				}

				if (password == null) {
					final Message msg = new StringFormattedMessage(
							"Invalid configuration for user %s: no password defined.", label);
					LOGGER.error(msg);
					throw new ComponentException(msg.getFormattedMessage());
				}

				if (role == null) {
					final Message msg = new StringFormattedMessage(
							"Invalid configuration for user %s: no role defined or unknown.", label);
					LOGGER.error(msg);
					throw new ComponentException(msg.getFormattedMessage());
				}

				if (country == null) {
					final Message msg = new StringFormattedMessage(
							"Invalid configuration for user %s: no country defined or unknown.", label);
					LOGGER.error(msg);
					throw new ComponentException(msg.getFormattedMessage());
				}

				if (language == null) {
					final Message msg = new StringFormattedMessage(
							"Invalid configuration for user %s: no language defined or unknown.", label);
					LOGGER.error(msg);
					throw new ComponentException(msg.getFormattedMessage());
				}

				users.put(email,
						User.create(email, null, null, password, role, firstName, null, lastName, affiliation, country,
								language));

			}

			LOGGER.debug("User database successfully created.");

		} else {
			final Message msg = new StringFormattedMessage("Invalid UserDatastore configuration: no users.");
			LOGGER.error(msg);
			throw new ComponentException(msg.getFormattedMessage());
		}


	}

	@Override
	protected final void doStop() throws Exception {
		super.doStop();

		users.clear();
	}

}
