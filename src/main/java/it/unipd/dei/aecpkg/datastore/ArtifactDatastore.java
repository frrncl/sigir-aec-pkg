/*
 * Copyright (c) 2021-2022 University of Padua, Italy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.unipd.dei.aecpkg.datastore;


import it.unipd.dei.aecpkg.component.AuthenticationRequiredException;
import it.unipd.dei.aecpkg.component.AuthorizationRequiredException;
import it.unipd.dei.aecpkg.component.ComponentException;
import it.unipd.dei.aecpkg.component.ResourceNotFoundException;
import it.unipd.dei.aecpkg.resource.Artifact;
import it.unipd.dei.aecpkg.resource.BinaryObject;
import it.unipd.dei.aecpkg.resource.Resource;
import it.unipd.dei.aecpkg.resource.User;

/**
 * Defines the methods for managing the persistence of {@link it.unipd.dei.aecpkg.resource.Artifact} resources.
 *
 * @author Nicola Ferro
 * @version 1.00
 * @since 1.00
 */
public interface ArtifactDatastore extends Datastore<Artifact> {

	/**
	 * The possible actions for {@code ArtifactDatastore}.
	 *
	 * @author Nicola Ferro
	 * @version 1.00
	 * @since 1.00
	 */
	final class ACTIONS {

		/**
		 * The creation of an {@code Artifact}.
		 *
		 * See {@link ArtifactDatastore#create(Resource)}}.
		 */
		public static final String CREATE_ARTIFACT = "CREATE_ARTIFACT";

		/**
		 * The creation of an {@code Artifact}.
		 *
		 * See {@link ArtifactDatastore#create(Resource)}}.
		 */
		public static final String READ_ARTIFACT_PACKAGE = "READ_ARTIFACT_PACKAGE";


	}

	/**
	 * Reads the package of the given artifact.
	 *
	 * @param a the artifact whose package has to be read.
	 *
	 * @return the package of the artifact.
	 *
	 * @throws ResourceNotFoundException       if the resource does not exist.
	 * @throws AuthenticationRequiredException if authentication is required and no user is contained in the {@code
	 *                                         ApplicationContext}.
	 * @throws AuthorizationRequiredException  if the user currently contained in the {@code ApplicationContext} has
	 *                                         insufficient rights to perform the requested {@code Action}.
	 * @throws ComponentException              if there is any problem accessing the system.
	 * @throws NullPointerException            if the resource {@code a} is {@code null}.
	 */
	BinaryObject readPackage(Artifact a) throws ComponentException;

	/**
	 * Creates a link to an artifact for the given user.
	 *
	 * @param a the artifact whose link has to be created.
	 * @param u the user for whom the link has to be created
	 *
	 * @return the link to the artifact.
	 *
	 * @throws NullPointerException if the resource {@code a} or {@code u} is {@code null}.
	 * @throws ComponentException if something goes wrong while creating the link.
	 */
	String getArtifactDownloadLink(final Artifact a, final User u) throws ComponentException;

}
