/*
 * Copyright (c) 2021-2022 University of Padua, Italy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.unipd.dei.aecpkg.datastore;


import it.unipd.dei.aecpkg.component.AbstractComponent;
import it.unipd.dei.aecpkg.component.ComponentException;
import it.unipd.dei.aecpkg.component.ComponentManager;
import it.unipd.dei.aecpkg.component.AccessController;
import it.unipd.dei.aecpkg.resource.Resource;
import org.apache.logging.log4j.message.Message;
import org.apache.logging.log4j.message.StringFormattedMessage;




/**
 * Implements the {@link Datastore} interface by providing a starting implementation suitable for all the concrete
 * subclasses.
 *
 * For each method declared in {@code Datastore}, this class checks:
 * <ol>
 * <li>the control access rules for that method; and</li>
 * <li>the input parameters.</li>
 * </ol>
 *
 * @param <R> the type of {@code Resource} whose persistence is managed by this {@code Datastore}.
 *
 * @author Nicola Ferro
 * @version 1.00
 * @since 1.00
 */
public abstract class AbstractDatastore<R extends Resource<?>> extends AbstractComponent implements Datastore<R> {

    /**
     * The access controller
     */
    protected final AccessController ac;

    /**
     * Creates a new {@code Datastore}.
     *
     * @param configFileName the name of the file containing the configuration for this component.
     *
     * @throws ComponentException if something goes wrong while creating the component.
     */
    protected AbstractDatastore(final String configFileName) throws ComponentException {
        super(configFileName);

        ac = ComponentManager.getComponent(AccessController.class).orElseThrow(() -> {
            final Message msg = new StringFormattedMessage(
                    "No AccessController available.");
            LOGGER.error(msg);
            return new ComponentException(msg.getFormattedMessage());
        });
    }

    /*
     * -------------------------------------------------------------------------
     * Methods defined in the interface Datastore follow.
     * -------------------------------------------------------------------------
     */

    @Override
    public R create(final R r) throws ComponentException {

        // Checks control access rules
        ac.controlAccess();

        // Checks input parameters
        if (r == null) {
            final Message msg = new StringFormattedMessage("Resource cannot be null.");
            LOGGER.error(msg);
            throw new NullPointerException(msg.getFormattedMessage());
        }

        return null;
    }

    @Override
    public R read(final R r) throws ComponentException {

        // Checks control access rules
        ac.controlAccess();

        // Checks input parameters
        if (r == null) {
            final Message msg = new StringFormattedMessage("Resource cannot be null.");
            LOGGER.error(msg);
            throw new NullPointerException(msg.getFormattedMessage());
        }

        return null;
    }

    @Override
    public R update(final R r) throws ComponentException {

        // Checks control access rules
        ac.controlAccess();

        // Checks input parameters
        if (r == null) {
            final Message msg = new StringFormattedMessage("Resource cannot be null.");
            LOGGER.error(msg);
            throw new NullPointerException(msg.getFormattedMessage());
        }

        return null;

    }

    @Override
    public R delete(final R r) throws ComponentException {

        // Checks control access rules
        ac.controlAccess();

        // Checks input parameters
        if (r == null) {
            final Message msg = new StringFormattedMessage("Resource cannot be null.");
            LOGGER.error(msg);
            throw new NullPointerException(msg.getFormattedMessage());
        }

        return null;
    }

    @Override
    public Iterable<R> list() throws ComponentException {

        // Checks control access rules
        ac.controlAccess();

        return null;
    }

    /*
     * -------------------------------------------------------------------------
     * Methods defined in the superclass AbstractComponent follow.
     * -------------------------------------------------------------------------
     */

    @Override
    protected void doStart() throws Exception {
        ; // nothing to do
    }

    @Override
    protected void doStop() throws Exception {
        ; // nothing to do
    }


}
