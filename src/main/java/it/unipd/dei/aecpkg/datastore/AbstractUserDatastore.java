/*
 * Copyright (c) 2021-2022 University of Padua, Italy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.unipd.dei.aecpkg.datastore;

import it.unipd.dei.aecpkg.component.ComponentException;
import it.unipd.dei.aecpkg.resource.User;
import org.apache.logging.log4j.message.Message;
import org.apache.logging.log4j.message.StringFormattedMessage;

/**
 * Implements the {@link UserDatastore} interface by providing a starting implementation suitable for all the concrete
 * subclasses.
 *
 * @author Nicola Ferro
 * @version 1.00
 * @since 1.00
 */
public abstract class AbstractUserDatastore extends AbstractDatastore<User> implements UserDatastore {

	/**
	 * Creates a new {@code UserDatastore}.
	 *
	 * @param configFileName the name of the file containing the configuration for this component.
	 *
	 * @throws ComponentException if something goes wrong while creating the component.
	 */
	protected AbstractUserDatastore(final String configFileName) throws ComponentException {
		super(configFileName);
	}

	/*
	 * -------------------------------------------------------------------------
	 * Methods defined in the interface UserDatastore follow.
	 * -------------------------------------------------------------------------
	 */

	@Override
	public User authenticate(final User u) throws ComponentException {

		// Checks control access rules
		ac.controlAccess();

		// Checks input parameters
		if (u == null) {
			final Message msg = new StringFormattedMessage("User cannot be null.");
			LOGGER.error(msg);
			throw new NullPointerException(msg.getFormattedMessage());
		}

		return null;
	}


}
