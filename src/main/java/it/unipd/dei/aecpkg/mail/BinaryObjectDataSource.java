/*
 * Copyright (c) 2021-2022 University of Padua, Italy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.unipd.dei.aecpkg.mail;


import it.unipd.dei.aecpkg.resource.BinaryObject;
import jakarta.activation.DataSource;

import java.io.InputStream;
import java.io.OutputStream;

/**
 * Wraps a {@link BinaryObject} making it a suitable {@code DataSource} for e-mail attachments.
 *
 * @author Nicola Ferro
 * @version 1.00
 * @since 1.00
 */
final class BinaryObjectDataSource implements DataSource {

	/**
	 * The binary object wrapped by this data source
	 */
	private final BinaryObject bo;

	/**
	 * Creates a new {@code DataSource} from the given binary object {@code bo}.
	 *
	 * @param bo the binary object to wrap.
	 */
	BinaryObjectDataSource(final BinaryObject bo) {
		this.bo = bo;
	}

	@Override
	public InputStream getInputStream() {
		return bo.getContentAsBinaryStream();
	}

	@Override
	public OutputStream getOutputStream() {
		return bo.setContentAsBinaryStream(0);
	}

	@Override
	public String getContentType() {
		return bo.getMediaType().getEncodedName();
	}

	public String getName() {
		return Integer.toHexString(bo.hashCode());
	}
}
