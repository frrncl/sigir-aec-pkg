/*
 * Copyright (c) 2021-2022 University of Padua, Italy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.unipd.dei.aecpkg.mail;


import it.unipd.dei.aecpkg.component.AbstractComponent;
import it.unipd.dei.aecpkg.component.ComponentException;
import it.unipd.dei.aecpkg.component.OperationException;
import it.unipd.dei.aecpkg.resource.*;
import jakarta.activation.DataHandler;
import jakarta.mail.*;
import jakarta.mail.Message;
import jakarta.mail.internet.*;
import org.apache.logging.log4j.message.StringFormattedMessage;

import java.text.MessageFormat;
import java.util.Locale;
import java.util.Properties;
import java.util.ResourceBundle;


/**
 * Provides methods for sending e-mails.
 *
 * @author Nicola Ferro
 * @version 1.00
 * @since 1.00
 */

public final class MailManager extends AbstractComponent {

	/**
	 * Name of the default configuration file.
	 */
	private static final String DEFAULT_CONFIG_FILE = "mailManager.properties";

	/**
	 * The value of the property containing email of the sender.
	 */
	private final String from;

	/**
	 * The value of the property containing the name of the base {@code ResourceBundle} to be used for
	 * internazionalization.
	 */
	private final String baseResourceBundleName;

	/**
	 * The mail session.
	 */
	private final Session session;

	/**
	 * Constructs a new component with the given configuration.
	 *
	 * @param configFileName the name of the file containing the configuration for this component.
	 *
	 * @throws ComponentException if something goes wrong while creating the component.
	 */
	public MailManager(final String configFileName) throws ComponentException {

		super(configFileName);

		String tmp = null;

		tmp = config.get(MailManager.class.getName() + ".baseResourceBundleName");
		if (tmp == null || tmp.isBlank()) {
			final org.apache.logging.log4j.message.Message msg = new StringFormattedMessage(
					"Property %s missing or empty.", MailManager.class.getName() + ".baseResourceBundleName");
			LOGGER.error(msg);
			throw new ComponentException(msg.getFormattedMessage());
		}
		baseResourceBundleName = tmp;


		// setup the configuration for JavaMail
		final Properties p = new Properties();

		tmp = config.get(MailManager.class.getName() + ".from");
		if (tmp == null || tmp.isBlank()) {
			final org.apache.logging.log4j.message.Message msg = new StringFormattedMessage(
					"Property %s missing or empty.", MailManager.class.getName() + ".from");
			LOGGER.error(msg);
			throw new ComponentException(msg.getFormattedMessage());
		}
		from = tmp;
		p.put("mail.from", from);


		tmp = config.get(MailManager.class.getName() + ".smtp.host");
		if (tmp == null || tmp.isBlank()) {
			final org.apache.logging.log4j.message.Message msg = new StringFormattedMessage(
					"Property %s missing or empty.", MailManager.class.getName() + ".smtp.host");
			LOGGER.error(msg);
			throw new ComponentException(msg.getFormattedMessage());
		}
		p.put("mail.smtp.host", tmp);

		tmp = config.get(MailManager.class.getName() + ".smtp.port");
		if (tmp != null && !tmp.isBlank()) {
			p.put("mail.smtp.port", tmp);
		}

		tmp = config.get(MailManager.class.getName() + ".smtp.userName");
		if (tmp == null || tmp.isBlank()) { // ensure that null and blank are the same
			tmp = null;
		}
		final String username = tmp;

		tmp = config.get(MailManager.class.getName() + ".stmp.password");
		if (tmp == null || tmp.isBlank()) { // ensure that null and blank are the same
			tmp = null;
		}
		final String password = tmp;

		p.put("mail.transport.protocol", "smtp");
		p.put("mail.smtp.starttls.enable", "true");
		p.put("mail.debug", "false");

		if (username != null && password != null) {
			p.put("mail.smtp.auth", "true");
			session = Session.getInstance(p, new Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(username, password);
				}
			});
		} else {
			session = Session.getInstance(p);
		}

		Locale.setDefault(Locale.ENGLISH);

	}

	/**
	 * Constructs a component with default configuration.
	 *
	 * @throws ComponentException if something goes wrong while creating the component.
	 */
	public MailManager() throws ComponentException {
		this(DEFAULT_CONFIG_FILE);
	}


	/**
	 * Sends the e-mail about an artifact being ready for upload to the ACM DL.
	 *
	 * @param u   the recipient of the e-mail.
	 * @param a   the artifact.
	 * @param url the URL at which the artifact is available
	 *
	 * @throws OperationException if there is any problem in sending the e-mail
	 */
	public void sendArtifactReadyForUploadEmail(final User u, final Artifact a, final String url) throws
			OperationException {

		if (u == null) {
			final org.apache.logging.log4j.message.Message msg = new StringFormattedMessage(
					"Recipient of the email missing.");
			LOGGER.error(msg);
			throw new OperationException(msg.getFormattedMessage());
		}

		if (Role.ACM != u.getRole()) {
			final org.apache.logging.log4j.message.Message msg = new StringFormattedMessage(
					"Recipient of the email %s has role %s instead of %s.", u.getIdentifier(), u.getRole(), Role.ACM);
			LOGGER.error(msg);
			throw new OperationException(msg.getFormattedMessage());
		}

		if (a == null) {
			final org.apache.logging.log4j.message.Message msg = new StringFormattedMessage("Artifact missing.");
			LOGGER.error(msg);
			throw new OperationException(msg.getFormattedMessage());
		}

		final Locale currentLocale = u.getLocale();

		final ResourceBundle bundle = ResourceBundle.getBundle(baseResourceBundleName, currentLocale);


		final MessageFormat formatter = new MessageFormat("");
		formatter.setLocale(currentLocale);

		// subject of the e-mail
		final StringBuffer subject = new StringBuffer();
		formatter.applyPattern(bundle.getString("emails.createArtifact.subject"));
		formatter.format(new String[]{a.getIdentifier()}, subject, null);


		// body of the e-mail
		final StringBuffer body = new StringBuffer();

		// salutation
		formatter.applyPattern(bundle.getString("emails.createArtifact.body.salutation"));
		formatter.format(new String[]{u.getFirstName()}, body, null);

		// line 1
		formatter.applyPattern(bundle.getString("emails.createArtifact.body.line1"));
		formatter.format(new String[]{a.getIdentifier()}, body, null);

		// line 2
		formatter.applyPattern(bundle.getString("emails.createArtifact.body.line2"));
		formatter.format(new String[]{url}, body, null);

		// line 3
		formatter.applyPattern(bundle.getString("emails.createArtifact.body.line3"));
		formatter.format(new String[]{a.getRelatedPaperDOI()}, body, null);

		for(Badge b : a.getBadges()) {
			// line 4
			formatter.applyPattern(bundle.getString("emails.createArtifact.body.line4"));
			formatter.format(new String[]{b.getOfficialName()}, body, null);
		}

		// line  5
		body.append(bundle.getString("emails.createArtifact.body.line5"));

		// regards
		body.append(bundle.getString("emails.createArtifact.body.regards"));

		// Sends the e-mail
		sendMail(u.getIdentifier(), subject.toString(), body.toString(), MediaType.TEXT_HTML_UTF8);

		LOGGER.info("Email on artifact %s (OpenReview %d) ready for upload to ACM DL successfully sent to user %s.",
				a.getIdentifier(), a.getOpenReviewID(), u.getIdentifier());

	}

	/**
	 * Sends the e-mail about successful download of an artifact.
	 *
	 * @param u   the recipient of the e-mail.
	 * @param a   the artifact.
	 * @param url the URL at which the artifact is available
	 *
	 * @throws OperationException if there is any problem in sending the e-mail
	 */
	public void sendArtifactDownloadedEmail(final User u, final Artifact a, final String url) throws
			OperationException {

		if (u == null) {
			final org.apache.logging.log4j.message.Message msg = new StringFormattedMessage(
					"Recipient of the email missing.");
			LOGGER.error(msg);
			throw new OperationException(msg.getFormattedMessage());
		}

		if (Role.ACM != u.getRole()) {
			final org.apache.logging.log4j.message.Message msg = new StringFormattedMessage(
					"Recipient of the email %s has role %s instead of %s.", u.getIdentifier(), u.getRole(), Role.ACM);
			LOGGER.error(msg);
			throw new OperationException(msg.getFormattedMessage());
		}

		if (a == null) {
			final org.apache.logging.log4j.message.Message msg = new StringFormattedMessage("Artifact missing.");
			LOGGER.error(msg);
			throw new OperationException(msg.getFormattedMessage());
		}

		final Locale currentLocale = u.getLocale();

		final ResourceBundle bundle = ResourceBundle.getBundle(baseResourceBundleName, currentLocale);


		final MessageFormat formatter = new MessageFormat("");
		formatter.setLocale(currentLocale);

		// subject of the e-mail
		final StringBuffer subject = new StringBuffer();
		formatter.applyPattern(bundle.getString("emails.downloadArtifactPackage.subject"));
		formatter.format(new String[]{a.getIdentifier()}, subject, null);


		// body of the e-mail
		final StringBuffer body = new StringBuffer();

		// salutation
		formatter.applyPattern(bundle.getString("emails.downloadArtifactPackage.body.salutation"));
		formatter.format(new String[]{u.getFirstName()}, body, null);

		// line 1
		formatter.applyPattern(bundle.getString("emails.downloadArtifactPackage.body.line1"));
		formatter.format(new String[]{a.getIdentifier()}, body, null);

		// line 2
		formatter.applyPattern(bundle.getString("emails.downloadArtifactPackage.body.line2"));
		formatter.format(new String[]{url}, body, null);

		// line  3
		body.append(bundle.getString("emails.downloadArtifactPackage.body.line3"));

		// line 4
		formatter.applyPattern(bundle.getString("emails.downloadArtifactPackage.body.line4"));
		formatter.format(new String[]{a.getIdentifier(), Integer.toString(a.getOpenReviewID()), u.getIdentifier(),
				u.getFullName()}, body, null);

		// regards
		body.append(bundle.getString("emails.downloadArtifactPackage.body.regards"));

		// Sends the e-mail
		sendMail(u.getIdentifier(), subject.toString(), body.toString(), MediaType.TEXT_HTML_UTF8);

		LOGGER.info("Email on artifact %s (OpenReview %d) downloaded successfully sent to user %s.",
				a.getIdentifier(), a.getOpenReviewID(), u.getIdentifier());

	}


	/**
	 * Sends an e-mail without attachments.
	 *
	 * @param to      the receiver(s) of the e-mail.
	 * @param subject the subject of the e-mail.
	 * @param message the body of the e-mail.
	 * @param mime    the MIME media type of the message.
	 *
	 * @throws OperationException if there is any problem in sending the e-mail.
	 */
	private void sendMail(final String to, final String subject, final String message, final MediaType mime) throws
			OperationException {

		if (to == null || to.isBlank()) {
			final org.apache.logging.log4j.message.Message msg = new StringFormattedMessage(
					"Recipient of the email missing or empty.");
			LOGGER.error(msg);
			throw new OperationException(msg.getFormattedMessage());
		}

		if (subject == null || subject.isBlank()) {
			final org.apache.logging.log4j.message.Message msg = new StringFormattedMessage(
					"Subject of the email missing or empty.");
			LOGGER.error(msg);
			throw new OperationException(msg.getFormattedMessage());
		}

		if (message == null || message.isBlank()) {
			final org.apache.logging.log4j.message.Message msg = new StringFormattedMessage(
					"Body of the email missing or empty.");
			LOGGER.error(msg);
			throw new OperationException(msg.getFormattedMessage());
		}

		if (mime == null) {
			final org.apache.logging.log4j.message.Message msg = new StringFormattedMessage(
					"MIME media type of the email missing.");
			LOGGER.error(msg);
			throw new OperationException(msg.getFormattedMessage());
		}

		final MimeMessage mm = new MimeMessage(session); // the message
		InternetAddress ia = null; // to and cc addresses

		try {

			mm.setFrom();

			ia = new InternetAddress(to);

			mm.addRecipient(Message.RecipientType.TO, ia);

			ia = new InternetAddress(from);
			mm.addRecipient(Message.RecipientType.BCC, ia);

			if (mime.hasParameter("charset")) {
				mm.setSubject(subject, mime.getParameterValue("charset"));
			} else {
				mm.setSubject(subject);
			}

			// create the message part
			mm.setContent(message, mime.getEncodedName());

			// Send the message
			Transport.send(mm);

		} catch (AddressException e) {
			final org.apache.logging.log4j.message.Message msg = new StringFormattedMessage(
					"Invalid e-mail address %s for e-mail with subject %s.", to, subject);
			LOGGER.error(msg, e);
			throw new OperationException(msg.getFormattedMessage(), e);
		} catch (final MessagingException e) {
			final org.apache.logging.log4j.message.Message msg = new StringFormattedMessage(
					"Error while sending e-mail with subject %s to %s.", subject, to);
			LOGGER.error(msg, e);
			throw new OperationException(msg.getFormattedMessage(), e);
		}

		LOGGER.debug("E-mail with subject %s successfully sent to %s.", subject, to);
	}

	/**
	 * Sends an e-mail with an attachment.
	 *
	 * @param to                 the receiver(s) of the e-mail.
	 * @param subject            the subject of the e-mail.
	 * @param message            the textual part of the e-mail.
	 * @param mime               the MIME media type of the message.
	 * @param attachment         the attachment of the e-mail.
	 * @param attachmentFileName the file name to assign to the attachment.
	 *
	 * @throws OperationException if there is any problem in sending the e-mail.
	 */
	private void sendAttachmentMail(final String to, final String subject, final String message, final MediaType mime, final BinaryObject attachment, final String attachmentFileName) throws
			OperationException {

		if (to == null || to.isBlank()) {
			final org.apache.logging.log4j.message.Message msg = new StringFormattedMessage(
					"Recipient of the email missing or empty.");
			LOGGER.error(msg);
			throw new OperationException(msg.getFormattedMessage());
		}

		if (subject == null || subject.isBlank()) {
			final org.apache.logging.log4j.message.Message msg = new StringFormattedMessage(
					"Subject of the email missing or empty.");
			LOGGER.error(msg);
			throw new OperationException(msg.getFormattedMessage());
		}

		if (message == null || message.isBlank()) {
			final org.apache.logging.log4j.message.Message msg = new StringFormattedMessage(
					"Body of the email missing or empty.");
			LOGGER.error(msg);
			throw new OperationException(msg.getFormattedMessage());
		}

		if (mime == null) {
			final org.apache.logging.log4j.message.Message msg = new StringFormattedMessage(
					"MIME media type of the email missing.");
			LOGGER.error(msg);
			throw new OperationException(msg.getFormattedMessage());
		}

		if (attachment == null) {
			final org.apache.logging.log4j.message.Message msg = new StringFormattedMessage(
					"Attachment to the email missing.");
			LOGGER.error(msg);
			throw new OperationException(msg.getFormattedMessage());
		}

		if (attachmentFileName == null || attachmentFileName.isBlank()) {
			final org.apache.logging.log4j.message.Message msg = new StringFormattedMessage(
					"File name of the attachment missing or empty.");
			LOGGER.error(msg);
			throw new OperationException(msg.getFormattedMessage());
		}


		final MimeMessage mm = new MimeMessage(session); // the message
		final Multipart multipart = new MimeMultipart(); // the body of the message
		MimeBodyPart messageBodyPart = null; // part of the body
		InternetAddress ia = null; // to and cc addresses

		try {

			mm.setFrom();

			ia = new InternetAddress(to);

			mm.addRecipient(Message.RecipientType.TO, ia);

			ia = new InternetAddress(config.get(from));
			mm.addRecipient(Message.RecipientType.BCC, ia);

			if (mime.hasParameter("charset")) {
				mm.setSubject(subject, mime.getParameterValue("charset"));
			} else {
				mm.setSubject(subject);
			}

			// create the message part
			messageBodyPart = new MimeBodyPart();
			messageBodyPart.setContent(message, mime.getEncodedName());
			multipart.addBodyPart(messageBodyPart);

			// create the attachment part
			messageBodyPart = new MimeBodyPart();
			messageBodyPart.setDataHandler(new DataHandler(new BinaryObjectDataSource(attachment)));
			messageBodyPart.setFileName(attachmentFileName);
			multipart.addBodyPart(messageBodyPart);

			// Put parts in message
			mm.setContent(multipart);

			// Send the message
			Transport.send(mm);

		} catch (final AddressException e) {
			final org.apache.logging.log4j.message.Message msg = new StringFormattedMessage(
					"Invalid e-mail address %s for e-mail with subject %s and attachment %s.", to, subject,
					attachmentFileName);
			LOGGER.error(msg, e);
			throw new OperationException(msg.getFormattedMessage(), e);
		} catch (final MessagingException e) {
			final org.apache.logging.log4j.message.Message msg = new StringFormattedMessage(
					"Error while sending e-mail with subject %s and attachment %s to %s.", subject, attachmentFileName,
					to);
			LOGGER.error(msg, e);
			throw new OperationException(msg.getFormattedMessage(), e);
		}

		LOGGER.debug("E-mail with subject %s and attachment %s successfully sent to %s.", subject, attachmentFileName,
				to);
	}


	/*-------------------------------------------------------------------------
	 * Methods defined in the superclass AbstractComponent follow.
	 *-------------------------------------------------------------------------
	 */

	/**
	 * Starts up the {@code MailManager}.
	 */
	@Override
	protected final void doStart() {
		; // nothing to do
	}

	/**
	 * Shuts down the {@code MailManager}.
	 */
	@Override
	protected final void doStop() {
		; // nothing to do
	}

}
