/*
 * Copyright (c) 2021-2022 University of Padua, Italy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.unipd.dei.aecpkg.web;

/**
 * Lists the names of the {@link jakarta.servlet.ServletContext} parameters used for the configuration of the web
 * application.
 *
 * @author Nicola Ferro
 * @version 1.00
 * @since 1.00
 */
public interface ContextParamNames {

	/**
	 * Name of the context parameter defining the name of the base {@code ResourceBundle} for the application
	 */
	static final String BASE_RESOURCE_BUNDLE_NAME = "it.unipd.dei.aecpkg.web.baseResourceBundleName";

	/**
	 * Name of the context parameter defining the configuration to be loaded for the application
	 */
	static final String APPLICATION_CONFIG_FILE = "it.unipd.dei.aecpkg.web.application.configuration";

	/**
	 * Name of the context parameter defining the reCAPTCHA V3 secret for the application
	 */
	static final String RECAPTCHA_SECRET = "it.unipd.dei.aecpkg.web.recaptchaSecret";

	/**
	 * Name of the context parameter defining the reCAPTCHA V3 key for the client
	 */
	static final String RECAPTCHA_CLIENT = "it.unipd.dei.aecpkg.web.recaptchaClient";


}
