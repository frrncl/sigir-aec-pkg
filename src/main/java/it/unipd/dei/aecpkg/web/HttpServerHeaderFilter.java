/*
 * Copyright (c) 2021-2022 University of Padua, Italy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.unipd.dei.aecpkg.web;


import it.unipd.dei.aecpkg.component.ApplicationContext;
import jakarta.servlet.*;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.message.Message;
import org.apache.logging.log4j.message.StringFormattedMessage;
import org.apache.logging.log4j.message.StringFormatterMessageFactory;

import java.io.IOException;

/**
 * Adds the HTTP Server header for the BEE service.
 *
 * @author Nicola Ferro
 * @version 1.00
 * @since 1.00
 */
public class HttpServerHeaderFilter implements Filter {

    /**
     * The logger.
     */
    private static final Logger LOGGER = LogManager.getLogger(HttpServerHeaderFilter.class,
            StringFormatterMessageFactory.INSTANCE);

    /**
     * The HTTP Server header
     */
    private static final String HTTP_SERVER_HEADER = "Server";

    /**
     * Key to retrieve the value for the HTTP Server header from {@link FilterConfig}.
     */
    private static final String PARAM_SERVER_HEADER = "aecpkg.server.header";

    /**
     * The configuration for the filter
     */
    private FilterConfig config = null;

    /**
     * The value of the HTTP Server header
     */
    private String serverHeader = null;

    @Override
    public void init(final FilterConfig config) throws ServletException {

        if (config == null) {
            final Message msg = new StringFormattedMessage("Filter configuration cannot be null.");
            LOGGER.error(msg);
            throw new ServletException(msg.getFormattedMessage());
        }
        this.config = config;

        serverHeader = config.getInitParameter(PARAM_SERVER_HEADER);

        if (serverHeader == null || serverHeader.isBlank()) {
            final Message msg = new StringFormattedMessage("The value for the HTTP Server header is not defined.");
            LOGGER.error(msg);
            throw new ServletException(msg.getFormattedMessage());
        }
    }

    @Override
    public void doFilter(final ServletRequest servletRequest, final ServletResponse servletResponse,
                         final FilterChain chain) throws IOException, ServletException {

        ApplicationContext.setIPAddress(servletRequest.getRemoteAddr());

        try {
            if (!(servletRequest instanceof HttpServletRequest) || !(servletResponse instanceof HttpServletResponse)) {
                final Message msg = new StringFormattedMessage("Only HTTP requests/responses are allowed.");
                LOGGER.error(msg);
                throw new ServletException(msg.getFormattedMessage());
            }

            // Safe to downcast at this point.
            final HttpServletResponse response = (HttpServletResponse) servletResponse;

            response.setHeader(HTTP_SERVER_HEADER, serverHeader);

            chain.doFilter(servletRequest, servletResponse);
        } finally {
            ApplicationContext.removeIPAddress();
        }
    }

    @Override
    public void destroy() {
        config = null;
    }


}
