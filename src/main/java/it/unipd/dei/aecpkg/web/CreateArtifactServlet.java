/*
 * Copyright (c) 2021-2022 University of Padua, Italy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.unipd.dei.aecpkg.web;


import it.unipd.dei.aecpkg.component.ApplicationContext;
import it.unipd.dei.aecpkg.component.ComponentException;
import it.unipd.dei.aecpkg.component.DuplicatedResourceException;
import it.unipd.dei.aecpkg.datastore.ArtifactDatastore;
import it.unipd.dei.aecpkg.resource.*;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.Part;
import org.apache.logging.log4j.message.Message;
import org.apache.logging.log4j.message.StringFormattedMessage;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.text.MessageFormat;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

/**
 * Creates an {@link Artifact}.
 *
 * See {@link it.unipd.dei.aecpkg.datastore.ArtifactDatastore#create(Resource)}.
 *
 * @author Nicola Ferro (ferro@dei.unipd.it)
 * @version 1.00
 * @since 1.00
 */
public class CreateArtifactServlet extends AbstractArtifactServlet {

	/**
	 * The URL to forward to in case of success
	 */
	private static final String SUCCESS_URL = "/protected/aec/create-artifact.jsp";

	/**
	 * The URL to forward to in case of failure
	 */
	private static final String FAILURE_URL = "/protected/aec/create-artifact.jsp";


	@Override
	public void subDoPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {

		ApplicationContext.setAction(ArtifactDatastore.ACTIONS.CREATE_ARTIFACT);

		final User u = ensureUserInSession(req, res);

		final MessageFormat formatter = new MessageFormat("", u.getLocale());

		final ResourceBundle bundle = ResourceBundle.getBundle(getBaseResourceBundleName(), u.getLocale());

		final MediaType contentType = MediaType.parseEncodedName(req.getHeader("Content-Type"));

		if (MediaType.MULTIPART_FORMDATA != contentType) {
			LOGGER.warn("Expected %s, received %s.", MediaType.MULTIPART_FORMDATA.getEncodedName(),
					contentType.getEncodedName());

			req.setAttribute(MESSAGE_ATTRIBUTE,
					it.unipd.dei.aecpkg.resource.Message.create(bundle.getString("messages.error.invalidContentType"),
							CommonErrorTypes.INVALID_REQUEST));

			req.getRequestDispatcher(FAILURE_URL).forward(req, res);

			return;
		}

		String token = null;
		int openReviewID = -1;
		String acmID = null;
		String relatedPaperDOI = null;
		String title = null;
		List<String> authorFirstName = new ArrayList<>();
		List<String> authorLastName = new ArrayList<>();
		List<String> authorDisplayName = new ArrayList<>();
		List<String> authorAffiliation = new ArrayList<>();
		List<Artifact.Author> authors = new ArrayList<>();
		Artifact.Type type = null;
		List<Badge> badges = new ArrayList<>();
		String aAbstract = null;
		URL openReviewURL = null;
		String repository = null;
		List<String> keywords = new ArrayList<>();
		BinaryObject artifactFile = null;

		String tmp = null;

		for (Part p : req.getParts()) {

			switch (p.getName()) {
				case RECAPTCHA_TOKEN:
					try (InputStream is = p.getInputStream()) {
						token = new String(is.readAllBytes(), StandardCharsets.UTF_8);
					}
					break;

				case "orid":
					try (InputStream is = p.getInputStream()) {
						tmp = new String(is.readAllBytes(), StandardCharsets.UTF_8);

						openReviewID = Integer.parseInt(tmp);

						if (openReviewID <= 0) {
							throw new NumberFormatException("Expected to be greater than 0.");
						}
					} catch (NumberFormatException e) {
						final Message msg = new StringFormattedMessage("OpenReviewID %s is not a valid integer: %s.",
								tmp, e.getMessage());
						LOGGER.warn(msg, e);

						req.setAttribute(MESSAGE_ATTRIBUTE, it.unipd.dei.aecpkg.resource.Message.create(
								bundle.getString("messages.createArtifact.wrongOpenReviewID"),
								CommonErrorTypes.INVALID_PARAMETER));

						req.getRequestDispatcher(FAILURE_URL).forward(req, res);

						return;
					}
					break;

				case "acmid":
					try (InputStream is = p.getInputStream()) {
						acmID = new String(is.readAllBytes(), StandardCharsets.UTF_8).trim();

						if (Integer.parseInt(acmID) <= 0) {
							throw new NumberFormatException("Expected to be greater than 0.");
						}
					} catch (NumberFormatException e) {
						final Message msg = new StringFormattedMessage("AcmID %s is not a valid integer: %s.", acmID,
								e.getMessage());
						LOGGER.warn(msg, e);

						req.setAttribute(MESSAGE_ATTRIBUTE, it.unipd.dei.aecpkg.resource.Message.create(
								bundle.getString("messages.createArtifact.wrongAcmID"),
								CommonErrorTypes.INVALID_PARAMETER));

						req.getRequestDispatcher(FAILURE_URL).forward(req, res);

						return;
					}
					break;

				case "relatedPaperDoi":
					try (InputStream is = p.getInputStream()) {
						relatedPaperDOI = new String(is.readAllBytes(), StandardCharsets.UTF_8).trim();
					}
					break;

				case "title":
					try (InputStream is = p.getInputStream()) {
						title = new String(is.readAllBytes(), StandardCharsets.UTF_8).trim();
					}
					break;

				case "firstName":
					try (InputStream is = p.getInputStream()) {
						tmp = new String(is.readAllBytes(), StandardCharsets.UTF_8);
					}

					if (!tmp.isBlank()) {
						authorFirstName.add(tmp);
					}
					break;

				case "lastName":
					try (InputStream is = p.getInputStream()) {
						tmp = new String(is.readAllBytes(), StandardCharsets.UTF_8).trim();
					}

					if (!tmp.isEmpty()) {
						authorLastName.add(tmp);
					}
					break;

				case "displayName":
					try (InputStream is = p.getInputStream()) {
						tmp = new String(is.readAllBytes(), StandardCharsets.UTF_8).trim();
					}

					if (!tmp.isEmpty()) {
						authorDisplayName.add(tmp);
					}
					break;

				case "affiliation":
					try (InputStream is = p.getInputStream()) {
						tmp = new String(is.readAllBytes(), StandardCharsets.UTF_8).trim();
					}

					if (!tmp.isEmpty()) {
						authorAffiliation.add(tmp);
					}
					break;

				case "type":
					try (InputStream is = p.getInputStream()) {
						type = Artifact.Type.parse(new String(is.readAllBytes(), StandardCharsets.UTF_8));
					}
					break;

				case "awarded":
					try (InputStream is = p.getInputStream()) {
						final Badge b = Badge.parse(new String(is.readAllBytes(), StandardCharsets.UTF_8));

						if (b != null) {
							badges.add(b);
						}
					}
					break;

				case "abstract":
					try (InputStream is = p.getInputStream()) {
						aAbstract = new String(is.readAllBytes(), StandardCharsets.UTF_8).trim();
					}
					break;

				case "orurl":
					try (InputStream is = p.getInputStream()) {
						tmp = new String(is.readAllBytes(), StandardCharsets.UTF_8);
						openReviewURL = new URL(tmp);
					} catch (MalformedURLException e) {
						final Message msg = new StringFormattedMessage("OpenReview URL %s is not a valid url: %s.", tmp,
								e.getMessage());
						LOGGER.warn(msg, e);

						req.setAttribute(MESSAGE_ATTRIBUTE, it.unipd.dei.aecpkg.resource.Message.create(
								bundle.getString("messages.createArtifact.wrongOpenReviewURL"),
								CommonErrorTypes.INVALID_PARAMETER));

						req.getRequestDispatcher(FAILURE_URL).forward(req, res);

						return;
					}
					break;

				case "repository":
					try (InputStream is = p.getInputStream()) {
						repository = new String(is.readAllBytes(), StandardCharsets.UTF_8).trim();
					}
					break;

				case "keyword":
					try (InputStream is = p.getInputStream()) {
						tmp = new String(is.readAllBytes(), StandardCharsets.UTF_8).trim();
					}
					if (!tmp.isEmpty()) {
						keywords.add(tmp);
					}
					break;

				case "artifactfile":
					tmp = p.getSubmittedFileName();
					final MediaType mime = MediaType.parseEncodedName(p.getContentType());

					if (MediaType.APPLICATION_ZIP != mime) {
						LOGGER.warn("Submitted file %s has type %s instead of %s.", tmp,
								mime != null ? mime.getEncodedName() : null,
								MediaType.APPLICATION_ZIP.getEncodedName());

						req.setAttribute(MESSAGE_ATTRIBUTE, it.unipd.dei.aecpkg.resource.Message.create(
								bundle.getString("messages.createArtifact.wrongArtifactFile"),
								CommonErrorTypes.INVALID_PARAMETER));

						req.getRequestDispatcher(FAILURE_URL).forward(req, res);

						return;
					}

					artifactFile = BinaryObject.create(tmp, mime);

					try (InputStream is = p.getInputStream()) {
						artifactFile.setContentFromBinaryStream(is);
					}

					break;

			}

		}

		if (token == null || token.isBlank()) {
			LOGGER.warn("reCAPTCHA token missing.");

			req.setAttribute(MESSAGE_ATTRIBUTE, it.unipd.dei.aecpkg.resource.Message.create(
					bundle.getString("messages.error.missingRecaptchaToken"), CommonErrorTypes.INVALID_PARAMETER));

			req.getRequestDispatcher(FAILURE_URL).forward(req, res);

			return;
		}

		if (openReviewID < 0) {
			LOGGER.warn("OpenReviewID missing.");

			req.setAttribute(MESSAGE_ATTRIBUTE, it.unipd.dei.aecpkg.resource.Message.create(
					bundle.getString("messages.createArtifact.wrongOpenReviewID"), CommonErrorTypes.INVALID_PARAMETER));

			req.getRequestDispatcher(FAILURE_URL).forward(req, res);

			return;
		}

		if (acmID == null || acmID.isEmpty()) {
			LOGGER.warn("AcmID is missing.");

			req.setAttribute(MESSAGE_ATTRIBUTE, it.unipd.dei.aecpkg.resource.Message.create(
					bundle.getString("messages.createArtifact.wrongOpenReviewID"), CommonErrorTypes.INVALID_PARAMETER));

			req.getRequestDispatcher(FAILURE_URL).forward(req, res);

			return;
		}

		if (relatedPaperDOI == null || relatedPaperDOI.isEmpty()) {
			LOGGER.warn("Related Paper DOI missing.");

			req.setAttribute(MESSAGE_ATTRIBUTE, it.unipd.dei.aecpkg.resource.Message.create(
					bundle.getString("messages.createArtifact.wrongRelatedDOI"), CommonErrorTypes.INVALID_PARAMETER));

			req.getRequestDispatcher(FAILURE_URL).forward(req, res);

			return;
		}

		if (title == null || title.isEmpty()) {
			LOGGER.warn("Title missing.");

			req.setAttribute(MESSAGE_ATTRIBUTE,
					it.unipd.dei.aecpkg.resource.Message.create(bundle.getString("messages.createArtifact.wrongTitle"),
							CommonErrorTypes.INVALID_PARAMETER));

			req.getRequestDispatcher(FAILURE_URL).forward(req, res);

			return;
		}

		if (authorFirstName.isEmpty() || authorLastName.isEmpty() || authorDisplayName.isEmpty() || authorAffiliation.isEmpty()) {
			LOGGER.warn("Authors are missing: first name = %d, last name = %d, display name = %d, affiliation = %d.",
					authorFirstName.size(), authorLastName.size(), authorDisplayName.size(), authorAffiliation.size());

			req.setAttribute(MESSAGE_ATTRIBUTE,
					it.unipd.dei.aecpkg.resource.Message.create(bundle.getString("messages.createArtifact.wrongAuthor"),
							CommonErrorTypes.INVALID_PARAMETER));

			req.getRequestDispatcher(FAILURE_URL).forward(req, res);

			return;
		}

		if (!(authorFirstName.size() == authorLastName.size() && authorLastName.size() == authorDisplayName.size() && authorDisplayName.size() == authorAffiliation.size())) {
			LOGGER.warn(
					"Mismatch in the number of authors: first name = %d, last name = %d, display name = %d, affiliation = %d.",
					authorFirstName.size(), authorLastName.size(), authorDisplayName.size(), authorAffiliation.size());

			req.setAttribute(MESSAGE_ATTRIBUTE,
					it.unipd.dei.aecpkg.resource.Message.create(bundle.getString("messages.createArtifact.wrongAuthor"),
							CommonErrorTypes.INVALID_PARAMETER));

			req.getRequestDispatcher(FAILURE_URL).forward(req, res);

			return;
		}

		// create the list of author objects
		for (int i = 0, n = authorFirstName.size(); i < n; i++) {
			authors.add(new Artifact.Author(authorFirstName.get(i), authorLastName.get(i), authorDisplayName.get(i),
					authorAffiliation.get(i)));
		}

		if (type == null) {
			LOGGER.warn("Type missing.");

			req.setAttribute(MESSAGE_ATTRIBUTE,
					it.unipd.dei.aecpkg.resource.Message.create(bundle.getString("messages.createArtifact.wrongType"),
							CommonErrorTypes.INVALID_PARAMETER));

			req.getRequestDispatcher(FAILURE_URL).forward(req, res);

			return;
		}

		if (badges.isEmpty()) {
			LOGGER.warn("Badges missing.");

			req.setAttribute(MESSAGE_ATTRIBUTE,
					it.unipd.dei.aecpkg.resource.Message.create(bundle.getString("messages.createArtifact.wrongBadge"),
							CommonErrorTypes.INVALID_PARAMETER));

			req.getRequestDispatcher(FAILURE_URL).forward(req, res);

			return;
		}

		if (aAbstract == null || aAbstract.isEmpty()) {
			LOGGER.warn("Abstract missing.");

			req.setAttribute(MESSAGE_ATTRIBUTE, it.unipd.dei.aecpkg.resource.Message.create(
					bundle.getString("messages.createArtifact.wrongAbstract"), CommonErrorTypes.INVALID_PARAMETER));

			req.getRequestDispatcher(FAILURE_URL).forward(req, res);

			return;
		}

		if (openReviewURL == null) {
			LOGGER.warn("OpenReview URL missing.");

			req.setAttribute(MESSAGE_ATTRIBUTE, it.unipd.dei.aecpkg.resource.Message.create(
					bundle.getString("messages.createArtifact.wrongOpenReviewURL"),
					CommonErrorTypes.INVALID_PARAMETER));

			req.getRequestDispatcher(FAILURE_URL).forward(req, res);

			return;
		}

		if (keywords.isEmpty()) {
			LOGGER.warn("Keywords missing.");

			req.setAttribute(MESSAGE_ATTRIBUTE, it.unipd.dei.aecpkg.resource.Message.create(
					bundle.getString("messages.createArtifact.wrongKeyword"), CommonErrorTypes.INVALID_PARAMETER));

			req.getRequestDispatcher(FAILURE_URL).forward(req, res);

			return;
		}

		final Artifact artifact = Artifact.create(acmID, OffsetDateTime.now(), null, openReviewID, relatedPaperDOI,
				title, authors, type, badges, aAbstract, openReviewURL, repository, keywords, artifactFile);

		ApplicationContext.setResource(artifact);


		LOGGER.info("Creating artifact %s; reCAPTCHA token %s.", artifact.getIdentifier(), token);

		if (!verifyRecaptcha(token, u.getIdentifier(), ArtifactDatastore.ACTIONS.CREATE_ARTIFACT)) {
			req.setAttribute(MESSAGE_ATTRIBUTE, it.unipd.dei.aecpkg.resource.Message.create(
					bundle.getString("messages.error.failedRecaptchaValidation"), CommonErrorTypes.INVALID_REQUEST));

			req.getRequestDispatcher(FAILURE_URL).forward(req, res);

			return;
		}


		try {
			getArtifactDatastore().create(artifact);
		} catch (DuplicatedResourceException e) {
			final Message msg = new StringFormattedMessage("Artifact %s (OpenReview %d) already exists.",
					artifact.getIdentifier(), artifact.getOpenReviewID());
			LOGGER.error(msg, e);

			formatter.applyPattern(bundle.getString("messages.createArtifact.alreadyExist"));

			req.setAttribute(MESSAGE_ATTRIBUTE, it.unipd.dei.aecpkg.resource.Message.create(formatter.format(
							new String[]{artifact.getIdentifier(), String.format("%d", artifact.getOpenReviewID())}),
					CommonErrorTypes.DUPLICATED_RESOURCE));

			req.getRequestDispatcher(FAILURE_URL).forward(req, res);

			return;
		} catch (ComponentException e) {
			final Message msg = new StringFormattedMessage(
					"Unexpected exception while creating artifact %s (OpenReview %d).", artifact.getIdentifier(),
					artifact.getOpenReviewID());
			LOGGER.error(msg, e);

			formatter.applyPattern(bundle.getString("messages.createArtifact.unexpectedError"));

			req.setAttribute(MESSAGE_ATTRIBUTE, it.unipd.dei.aecpkg.resource.Message.create(formatter.format(
							new String[]{artifact.getIdentifier(), String.format("%d", artifact.getOpenReviewID())}),
					CommonErrorTypes.INTERNAL_ERROR));

			req.getRequestDispatcher(FAILURE_URL).forward(req, res);

			return;
		}

		LOGGER.info("Artifact %s (OpenReview %d) successfully created.", artifact.getIdentifier(),
				artifact.getOpenReviewID());

		formatter.applyPattern(bundle.getString("messages.createArtifact.success"));

		req.setAttribute(MESSAGE_ATTRIBUTE, it.unipd.dei.aecpkg.resource.Message.create(formatter.format(
				new String[]{artifact.getIdentifier(), String.format("%d", artifact.getOpenReviewID())})));

		req.getRequestDispatcher(SUCCESS_URL).forward(req, res);

	}


}
