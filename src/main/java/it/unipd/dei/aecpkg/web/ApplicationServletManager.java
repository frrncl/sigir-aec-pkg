/*
 * Copyright (c) 2021-2022 University of Padua, Italy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package it.unipd.dei.aecpkg.web;


import it.unipd.dei.aecpkg.component.Application;
import it.unipd.dei.aecpkg.component.ComponentManager;
import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletContextEvent;
import jakarta.servlet.ServletContextListener;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.message.Message;
import org.apache.logging.log4j.message.StringFormattedMessage;
import org.apache.logging.log4j.message.StringFormatterMessageFactory;

/**
 * Performs the start-up of the {@link Application} and any needed resource initialization for running it in a servlet
 * Web container.
 *
 * @author Nicola Ferro
 * @version 1.00
 * @since 1.00
 */
public class ApplicationServletManager implements ServletContextListener {

	/**
	 * The logger.
	 */
	private static final Logger LOGGER = LogManager.getLogger(ApplicationServletManager.class,
			StringFormatterMessageFactory.INSTANCE);

	@Override
	public void contextInitialized(final ServletContextEvent sce) {

		final ServletContext ctx = sce.getServletContext();

		final String configFileName = ctx.getInitParameter(ContextParamNames.APPLICATION_CONFIG_FILE);

		try {
			if (configFileName == null || configFileName.isBlank()) {
				ComponentManager.createComponent(Application.class);
			} else {
				ComponentManager.createComponent(Application.class, configFileName);
			}
		} catch (final Exception e) {
			final Message msg = new StringFormattedMessage("Unable to start up the application: %s.", e.getMessage());
			LOGGER.error(msg, e);
			throw new IllegalStateException(msg.getFormattedMessage(), e);
		}

		LOGGER.debug("Application successfully started up.");
	}

	@Override
	public void contextDestroyed(final ServletContextEvent sce) {
		try {
			ComponentManager.removeComponent(Application.class);
		} catch (final Exception e) {
			final Message msg = new StringFormattedMessage("Unable to shut down the application: %s.", e.getMessage());
			LOGGER.error(msg, e);
			throw new IllegalStateException(msg.getFormattedMessage(), e);
		}

		LOGGER.debug("Application successfully shut down.");

	}

}
