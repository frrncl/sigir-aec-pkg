/*
 * Copyright (c) 2021-2022 University of Padua, Italy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.unipd.dei.aecpkg.web;


import it.unipd.dei.aecpkg.component.ApplicationContext;
import it.unipd.dei.aecpkg.component.ComponentException;
import it.unipd.dei.aecpkg.component.ComponentManager;
import it.unipd.dei.aecpkg.jwt.JwtManager;
import it.unipd.dei.aecpkg.resource.CommonErrorTypes;
import it.unipd.dei.aecpkg.resource.Language;
import it.unipd.dei.aecpkg.resource.Role;
import it.unipd.dei.aecpkg.resource.User;
import jakarta.servlet.*;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.message.Message;
import org.apache.logging.log4j.message.StringFormattedMessage;
import org.apache.logging.log4j.message.StringFormatterMessageFactory;
import org.jose4j.jwt.JwtClaims;

import java.io.IOException;
import java.util.ResourceBundle;

/**
 * Checks for successful authentication to allow for accessing protected resources.
 *
 * @author Nicola Ferro
 * @version 1.00
 * @since 1.00
 */
public class ProtectedResourceFilter implements Filter {

	/**
	 * The logger.
	 */
	private static final Logger LOGGER = LogManager.getLogger(ProtectedResourceFilter.class,
			StringFormatterMessageFactory.INSTANCE);

	/**
	 * The URL to forward to in case of failure
	 */
	private static final String FAILURE_URL = "/login";

	/**
	 * Key to retrieve the value for the {@link Role#AEC} path from {@link FilterConfig}.
	 */
	private static final String PARAM_AEC_PATH = "aecpkg.path.aec";

	/**
	 * Key to retrieve the value for the {{@link Role#ACM} path from {@link FilterConfig}.
	 */
	private static final String PARAM_ACM_PATH = "aecpkg.path.acm";

	/**
	 * Key to retrieve the value for the assets path from {@link FilterConfig}.
	 */
	private static final String PARAM_ASSETS_PATH = "aecpkg.path.assets";

	/**
	 * Key to retrieve the value for the artifacts path from {@link FilterConfig}.
	 */
	private static final String PARAM_ARTIFACTS_PATH = "aecpkg.path.artifacts";

	/**
	 * The configuration for the filter
	 */
	private FilterConfig config = null;

	/**
	 * The value of the AEC path
	 */
	private String aecPath = null;

	/**
	 * The value of the ACM path
	 */
	private String acmPath = null;

	/**
	 * The value of the assets path
	 */
	private String assetsPath = null;

	/**
	 * The value of the artifacts path
	 */
	private String artifactsPath = null;

	/**
	 * The name of the base {@code ResourceBundle}
	 */
	private String baseResourceBundleName = null;

	/**
	 * The JwtManager
	 */
	private JwtManager jwtManager;

	@Override
	public void init(final FilterConfig config) throws ServletException {

		if (config == null) {
			final Message msg = new StringFormattedMessage("Filter configuration cannot be null.");
			LOGGER.error(msg);
			throw new ServletException(msg.getFormattedMessage());
		}
		this.config = config;

		baseResourceBundleName = config.getServletContext()
				.getInitParameter(ContextParamNames.BASE_RESOURCE_BUNDLE_NAME);

		if (baseResourceBundleName == null || baseResourceBundleName.isBlank()) {
			final Message msg = new StringFormattedMessage(
					"Name of base ResourceBundle cannot be null or empty: missing %s parameter. Cannot initialize filter %s.",
					ContextParamNames.BASE_RESOURCE_BUNDLE_NAME, this.getClass());
			LOGGER.error(msg);
			throw new ServletException(msg.getFormattedMessage());
		}

		aecPath = config.getInitParameter(PARAM_AEC_PATH);

		if (aecPath == null || aecPath.isBlank()) {
			final Message msg = new StringFormattedMessage("The value for the AEC path is not defined.");
			LOGGER.error(msg);
			throw new ServletException(msg.getFormattedMessage());
		}

		acmPath = config.getInitParameter(PARAM_ACM_PATH);

		if (acmPath == null || acmPath.isBlank()) {
			final Message msg = new StringFormattedMessage("The value for the ACM path is not defined.");
			LOGGER.error(msg);
			throw new ServletException(msg.getFormattedMessage());
		}

		assetsPath = config.getInitParameter(PARAM_ASSETS_PATH);

		if (assetsPath == null || assetsPath.isBlank()) {
			final Message msg = new StringFormattedMessage("The value for the assets path is not defined.");
			LOGGER.error(msg);
			throw new ServletException(msg.getFormattedMessage());
		}

		artifactsPath = config.getInitParameter(PARAM_ARTIFACTS_PATH);

		if (artifactsPath == null || artifactsPath.isBlank()) {
			final Message msg = new StringFormattedMessage("The value for the artifacts path is not defined.");
			LOGGER.error(msg);
			throw new ServletException(msg.getFormattedMessage());
		}

		jwtManager = ComponentManager.getComponent(JwtManager.class).orElseThrow(() -> {
			final Message msg = new StringFormattedMessage("No JwtManager available, cannot initialize servlet %s.",
					this.getClass());
			LOGGER.error(msg);
			return new ServletException(msg.getFormattedMessage());
		});
	}

	@Override
	public void doFilter(final ServletRequest servletRequest, final ServletResponse servletResponse, final FilterChain chain) throws
			IOException, ServletException {

		ApplicationContext.setIPAddress(servletRequest.getRemoteAddr());

		try {

			if (!(servletRequest instanceof HttpServletRequest) || !(servletResponse instanceof HttpServletResponse)) {
				final Message msg = new StringFormattedMessage("Only HTTP requests/responses are allowed.");
				LOGGER.error(msg);
				throw new ServletException(msg.getFormattedMessage());
			}

			// Safe to downcast at this point.
			final HttpServletRequest req = (HttpServletRequest) servletRequest;

			LOGGER.info("request URL =  %s", req.getRequestURL());

			final String path = req.getServletPath();

			if (path.startsWith(artifactsPath)) {

				checkArtifactAccess(servletRequest, servletResponse, chain);

				return;
			}


			final HttpSession session = req.getSession(false);

			if (session == null) {

				LOGGER.warn("Authentication required to access resource %s with method %s.", req.getRequestURI(),
						req.getMethod());

				req.setAttribute(AbstractServlet.MESSAGE_ATTRIBUTE, it.unipd.dei.aecpkg.resource.Message.create(
						ResourceBundle.getBundle(baseResourceBundleName, req.getLocale())
								.getString("messages.error.authenticationRequired"),
						CommonErrorTypes.AUTHENTICATION_REQUIRED));

				req.getRequestDispatcher(FAILURE_URL).forward(servletRequest, servletResponse);

				return;
			}

			final User u = (User) session.getAttribute(AbstractServlet.USER_ATTRIBUTE);

			if (u == null) {

				LOGGER.warn(
						"Authentication required to access resource %s with method %s. Session %s exists but no user found in session. Session invalidated.",
						req.getRequestURI(), req.getMethod(), session.getId());

				session.invalidate();

				req.setAttribute(AbstractServlet.MESSAGE_ATTRIBUTE, it.unipd.dei.aecpkg.resource.Message.create(
						ResourceBundle.getBundle(baseResourceBundleName, req.getLocale())
								.getString("messages.error.authenticationRequired"),
						CommonErrorTypes.AUTHENTICATION_REQUIRED));

				req.getRequestDispatcher(FAILURE_URL).forward(servletRequest, servletResponse);

				return;
			}

			ApplicationContext.setUser(u);

			if (path.startsWith(assetsPath)) {
				chain.doFilter(servletRequest, servletResponse);
				return;
			}

			switch (u.getRole()) {
				case ROOT:
					chain.doFilter(servletRequest, servletResponse);
					break;

				case AEC:

					if (!path.startsWith(aecPath)) {

						LOGGER.warn("Insufficient rights to access resource %s with method %s by user %s with role %s.",
								req.getRequestURI(), req.getMethod(), u.getIdentifier(), u.getRole());

						session.invalidate();

						req.setAttribute(AbstractServlet.MESSAGE_ATTRIBUTE, it.unipd.dei.aecpkg.resource.Message.create(
								ResourceBundle.getBundle(baseResourceBundleName, req.getLocale())
										.getString("messages.error.insufficientRights"),
								CommonErrorTypes.INSUFFICIENT_ACCESS_RIGHTS));

						req.getRequestDispatcher(FAILURE_URL).forward(servletRequest, servletResponse);

					}

					chain.doFilter(servletRequest, servletResponse);
					break;

				case ACM:

					if (!path.startsWith(acmPath)) {

						LOGGER.warn("Insufficient rights to access resource %s with method %s by user %s with role %s.",
								req.getRequestURI(), req.getMethod(), u.getIdentifier(), u.getRole());

						session.invalidate();

						req.setAttribute(AbstractServlet.MESSAGE_ATTRIBUTE, it.unipd.dei.aecpkg.resource.Message.create(
								ResourceBundle.getBundle(baseResourceBundleName, req.getLocale())
										.getString("messages.error.insufficientRights"),
								CommonErrorTypes.INSUFFICIENT_ACCESS_RIGHTS));

						req.getRequestDispatcher(FAILURE_URL).forward(servletRequest, servletResponse);

					}

					chain.doFilter(servletRequest, servletResponse);
					break;

				default:

					LOGGER.warn("Insufficient rights to access resource %s with method %s by user %s with role %s.",
							req.getRequestURI(), req.getMethod(), u.getIdentifier(), u.getRole());

					session.invalidate();

					req.setAttribute(AbstractServlet.MESSAGE_ATTRIBUTE, it.unipd.dei.aecpkg.resource.Message.create(
							ResourceBundle.getBundle(baseResourceBundleName, req.getLocale())
									.getString("messages.error.insufficientRights"),
							CommonErrorTypes.INSUFFICIENT_ACCESS_RIGHTS));

					req.getRequestDispatcher(FAILURE_URL).forward(servletRequest, servletResponse);

					break;
			}


		} finally {
			ApplicationContext.removeIPAddress();
			ApplicationContext.removeUser();
		}


	}

	@Override
	public void destroy() {
		config = null;
	}

	/**
	 * Checks the access to a protected artifact resource.
	 *
	 * @param servletRequest  the request to process.
	 * @param servletResponse the response associated with the request.
	 * @param chain           provides access to the next filter in the chain for this filter to pass the request and
	 *                        response to for further processing.
	 *
	 * @throws ServletException if the processing fails for any other reason
	 * @throws IOException      if an I/O error occurs during this filter's processing of the request
	 */
	private void checkArtifactAccess(final ServletRequest servletRequest, final ServletResponse servletResponse, final FilterChain chain) throws
			ServletException, IOException {

		// Safe to downcast at this point.
		final HttpServletRequest req = (HttpServletRequest) servletRequest;

		final String jwt = req.getParameter("jwt");

		if (jwt == null || jwt.isBlank()) {
			LOGGER.warn("JWE missing but authentication required to access resource %s with method %s.",
					req.getRequestURI(), req.getMethod());

			req.setAttribute(AbstractServlet.MESSAGE_ATTRIBUTE, it.unipd.dei.aecpkg.resource.Message.create(
					ResourceBundle.getBundle(baseResourceBundleName, req.getLocale())
							.getString("messages.error.authenticationRequired"),
					CommonErrorTypes.AUTHENTICATION_REQUIRED));

			req.getRequestDispatcher(FAILURE_URL).forward(servletRequest, servletResponse);

			return;
		}

		LOGGER.info("JWT to access resource %s with method %s: %s", req.getRequestURI(), req.getMethod(), jwt);

		final JwtClaims claims;

		try {
			claims = jwtManager.decodeJWE(jwt);
		} catch (ComponentException e) {
			LOGGER.warn("JWE not valid but authentication required to access resource %s with method %s.",
					req.getRequestURI(), req.getMethod());

			req.setAttribute(AbstractServlet.MESSAGE_ATTRIBUTE, it.unipd.dei.aecpkg.resource.Message.create(
					ResourceBundle.getBundle(baseResourceBundleName, req.getLocale())
							.getString("messages.error.authenticationRequired"),
					CommonErrorTypes.AUTHENTICATION_REQUIRED));

			req.getRequestDispatcher(FAILURE_URL).forward(servletRequest, servletResponse);

			return;
		}

		LOGGER.info("Claims to access resource %s with method %s: %s.", req.getRequestURI(), req.getMethod(),
				claims.toJson());

		final User u;
		try {
			final Role role = Role.parse(claims.getClaimValueAsString("role"));

			if (role == null) {
				throw new NullPointerException("Role missing");
			}

			final String firstName = claims.getClaimValueAsString("firstName");
			final String lastName = claims.getClaimValueAsString("lastName");
			final Language language = Language.parse(claims.getClaimValueAsString("language"));

			u = User.create(claims.getSubject(), null, null, null, role, firstName, null, lastName, null, null, language);

		} catch (Exception e) {
			final Message msg = new StringFormattedMessage("Invalid user: %s.", e.getMessage());
			LOGGER.warn(msg, e);

			req.setAttribute(AbstractServlet.MESSAGE_ATTRIBUTE, it.unipd.dei.aecpkg.resource.Message.create(
					ResourceBundle.getBundle(baseResourceBundleName, req.getLocale())
							.getString("messages.error.authenticationRequired"),
					CommonErrorTypes.AUTHENTICATION_REQUIRED));

			req.getRequestDispatcher(FAILURE_URL).forward(servletRequest, servletResponse);

			return;
		}

		HttpSession session = req.getSession(false);

		if (session == null) {
			session = req.getSession(true);


		} else {
			LOGGER.warn("Session already existed when asking to access to resource %s with method %s.",
					req.getRequestURI(), req.getMethod());

			User uu = (User) session.getAttribute(AbstractServlet.USER_ATTRIBUTE);
			if (uu != null) {
				LOGGER.warn(
						"Session already contained user %s instead of user %s, received from JWT, when asking to access to resource %s with method %s.",
						uu.getIdentifier(), u.getIdentifier(), req.getRequestURI(), req.getMethod());
			}

			session.invalidate();

			LOGGER.warn("Session invalidated.");

			session = req.getSession(true);
		}

		ApplicationContext.setUser(u);
		session.setAttribute(AbstractServlet.USER_ATTRIBUTE, u);

		LOGGER.info("New session created for user %s to access to resource %s with method %s.", u.getIdentifier(), req.getRequestURI(), req.getMethod());

		chain.doFilter(servletRequest, servletResponse);

	}


}
