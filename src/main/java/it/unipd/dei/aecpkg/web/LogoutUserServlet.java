/*
 * Copyright (c) 2021-2022 University of Padua, Italy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package it.unipd.dei.aecpkg.web;


import it.unipd.dei.aecpkg.component.ApplicationContext;
import it.unipd.dei.aecpkg.datastore.UserDatastore;
import it.unipd.dei.aecpkg.resource.CommonErrorTypes;
import it.unipd.dei.aecpkg.resource.User;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.ResourceBundle;

/**
 * Logs out a user.
 *
 * @author Nicola Ferro (ferro@dei.unipd.it)
 * @version 1.00
 * @since 1.00
 */
public class LogoutUserServlet extends AbstractUserServlet {

    /**
     * The URL to forward to in case of success
     */
    private static final String SUCCESS_URL = "/";

    /**
     * The URL to forward to in case of failure
     */
    private static final String FAILURE_URL = "/";

    @Override
    public void subDoGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {

        ApplicationContext.setAction(UserDatastore.ACTIONS.LOGOUT_USER);

        final MessageFormat formatter = new MessageFormat("");

        // get any existing session
        final HttpSession session = req.getSession(false);

        // nothing to do
        if (session == null) {
            LOGGER.info("Session already expired.");

            req.setAttribute(MESSAGE_ATTRIBUTE, it.unipd.dei.aecpkg.resource.Message
                    .create(ResourceBundle.getBundle(getBaseResourceBundleName(), req.getLocale())
                                          .getString("messages.logout.expired")));

            req.getRequestDispatcher(SUCCESS_URL).forward(req, res);

            return;
        }

        final User u = (User) session.getAttribute(USER_ATTRIBUTE);

        // a somehow unexpected case
        if (u == null) {
            LOGGER.error("Session %s terminated but no user found in session.", session.getId());

            req.setAttribute(MESSAGE_ATTRIBUTE, it.unipd.dei.aecpkg.resource.Message
                    .create(ResourceBundle.getBundle(getBaseResourceBundleName(), req.getLocale())
                                          .getString("messages.logout.terminated"), CommonErrorTypes.INVALID_REQUEST));

            req.getRequestDispatcher(FAILURE_URL).forward(req, res);

            session.invalidate();

            return;
        }


        ApplicationContext.setUser(u);
        ApplicationContext.setResource(u);

        formatter.setLocale(u.getLocale());
        formatter.applyPattern(
                ResourceBundle.getBundle(getBaseResourceBundleName(), u.getLocale()).getString("messages.logout.success"));

        req.setAttribute(MESSAGE_ATTRIBUTE,
                it.unipd.dei.aecpkg.resource.Message.create(formatter.format(new String[]{u.getIdentifier()})));

        req.getRequestDispatcher(SUCCESS_URL).forward(req, res);

        session.invalidate();

        LOGGER.info("User %s successfully logged out.", u.getIdentifier());

    }

}
