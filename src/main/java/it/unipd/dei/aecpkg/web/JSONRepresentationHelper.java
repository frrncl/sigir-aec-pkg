/*
 * Copyright (c) 2021-2022 University of Padua, Italy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.unipd.dei.aecpkg.web;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import it.unipd.dei.aecpkg.component.ComponentException;
import it.unipd.dei.aecpkg.component.InvalidResourceException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.message.Message;
import org.apache.logging.log4j.message.StringFormattedMessage;
import org.apache.logging.log4j.message.StringFormatterMessageFactory;

import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;

/**
 * Provides utility methods for representing resources to/from JSON.
 *
 * @author Nicola Ferro
 * @version 1.00
 * @since 1.00
 */
class JSONRepresentationHelper {

    /**
     * The LOGGER.
     */
    private static final Logger LOGGER = LogManager.getLogger(JSONRepresentationHelper.class,
            StringFormatterMessageFactory.INSTANCE);

    /**
     * The JSON factory to be used for creating JSON parsers and generators.
     */
    private static final JsonFactory JSON_FACTORY;

    static {

        // the JSON factory
        JSON_FACTORY = new JsonFactory();
        JSON_FACTORY.disable(JsonGenerator.Feature.AUTO_CLOSE_TARGET);
        JSON_FACTORY.disable(JsonParser.Feature.AUTO_CLOSE_SOURCE);

        LOGGER.debug("JSON factory factory successfully instantiated.");

    }

    /**
     * Parses an JSON document by using a cursor-based approach.
     *
     * @param in the input stream containing the JSON document.
     *
     * @return a JSON parser for parsing the document.
     *
     * @throws ComponentException if something goes wrong during the parsing.
     */
    public static JsonParser fromJSON(final InputStream in) throws ComponentException {
        try {
            return JSON_FACTORY.createParser(in);
        } catch (final IOException e) {
            final Message msg = new StringFormattedMessage("Unable to instantiate the JSON parser: %s.",
                    e.getMessage());
            LOGGER.error(msg, e);
            throw new ComponentException(msg.getFormattedMessage(), e);
        }
    }

    /**
     * Parses an JSON document by using a cursor-based approach.
     *
     * @param in the reader containing the JSON document.
     *
     * @return a JSON parser for parsing the document.
     *
     * @throws ComponentException if something goes wrong during the parsing.
     */
    public static JsonParser fromJSON(final Reader in) throws ComponentException {
        try {
            return JSON_FACTORY.createParser(in);
        } catch (final IOException e) {
            final Message msg = new StringFormattedMessage("Unable to instantiate the JSON parser: %s.",
                    e.getMessage());
            LOGGER.error(msg, e);
            throw new ComponentException(msg.getFormattedMessage(), e);
        }
    }

    /**
     * Aligns the given JSON parser at the start of the specified field.
     *
     * @param jp        the JSON parser to align.
     * @param fieldName the name of the field.
     *
     * @return {@code true} if the start of the field {@code fieldName} has been found; {@code false} otherwise.
     *
     * @throws InvalidResourceException if something goes wrong while parsing the JSON.
     */
    public static final boolean alignAtFieldStart(final JsonParser jp, final String fieldName)
            throws InvalidResourceException {

        if (fieldName == null || fieldName.isEmpty()) {
            return false;
        }

        try {
            // while we are not on the start of an element or the element is not
            // a token element, advance to the next element (if any)
            while (jp.getCurrentToken() != JsonToken.FIELD_NAME || !fieldName.equals(jp.getCurrentName())) {

                // there are no more events
                if (jp.nextToken() == null) {
                    return false;
                }
            }
        } catch (final IOException e) {
            final Message msg = new StringFormattedMessage(
                    "Unable to parse JSON and align stream to the start of field %s: %s.", fieldName, e.getMessage());
            LOGGER.error(msg, e);
            throw new InvalidResourceException(msg.getFormattedMessage(), e);
        }

        return true;
    }

}
