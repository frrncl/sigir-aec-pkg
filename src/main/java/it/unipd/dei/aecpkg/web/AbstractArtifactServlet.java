/*
 * Copyright (c) 2021-2022 University of Padua, Italy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.unipd.dei.aecpkg.web;

import it.unipd.dei.aecpkg.component.ComponentManager;
import it.unipd.dei.aecpkg.datastore.ArtifactDatastore;
import it.unipd.dei.aecpkg.datastore.UserDatastore;
import jakarta.servlet.ServletConfig;
import jakarta.servlet.ServletException;
import org.apache.logging.log4j.message.Message;
import org.apache.logging.log4j.message.StringFormattedMessage;

/**
 * Gets the {@link UserDatastore}.
 *
 * @author Nicola Ferro (ferro@dei.unipd.it)
 * @version 1.00
 * @since 1.00
 */
public abstract class AbstractArtifactServlet extends AbstractServlet {

    /**
     * The ArtifactDatastore
     */
    private ArtifactDatastore artifactDatastore = null;


    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);

        artifactDatastore = ComponentManager.getComponent(ArtifactDatastore.class).orElseThrow(() -> {
            final Message msg = new StringFormattedMessage(
                    "No ArtifactDatastore available, cannot initialize servlet %s.", this.getClass());
            LOGGER.error(msg);
            return new ServletException(msg.getFormattedMessage());
        });
    }

    /**
     * Returns the {@code ArtifactDatastore} to be used.
     *
     * @return the {@code ArtifactDatastore} to be used.
     */
    protected final ArtifactDatastore getArtifactDatastore () {
        return artifactDatastore;
    }

}
