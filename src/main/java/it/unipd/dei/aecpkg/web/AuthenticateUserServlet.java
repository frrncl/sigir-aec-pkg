/*
 * Copyright (c) 2021-2022 University of Padua, Italy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.unipd.dei.aecpkg.web;


import it.unipd.dei.aecpkg.component.ApplicationContext;
import it.unipd.dei.aecpkg.component.ComponentException;
import it.unipd.dei.aecpkg.component.ResourceNotFoundException;
import it.unipd.dei.aecpkg.datastore.UserDatastore;
import it.unipd.dei.aecpkg.resource.CommonErrorTypes;
import it.unipd.dei.aecpkg.resource.User;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import org.apache.logging.log4j.message.Message;
import org.apache.logging.log4j.message.StringFormattedMessage;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.ResourceBundle;

/**
 * Authenticates a {@link User}.
 *
 * See {@link UserDatastore#authenticate(User)}.
 *
 * @author Nicola Ferro (ferro@dei.unipd.it)
 * @version 1.00
 * @since 1.00
 */
public class AuthenticateUserServlet extends AbstractUserServlet {

    /**
     * The URL to forward to in case of success for the root user
     */
    private static final String ROOT_SUCCESS_URL = "/protected/aec/create-artifact.jsp";

    /**
     * The URL to forward to in case of success for the AEC user
     */
    private static final String AEC_SUCCESS_URL = "/protected/aec/create-artifact.jsp";

    /**
     * The URL to forward to in case of success for the ACM user
     */
    private static final String ACM_SUCCESS_URL = "/protected/acm/home.jsp";

    /**
     * The URL to forward to in case of failure
     */
    private static final String FAILURE_URL = "/login";

    @Override
    public void subDoPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {

        ApplicationContext.setAction(UserDatastore.ACTIONS.AUTHENTICATE_USER);

        final String token = req.getParameter(RECAPTCHA_TOKEN);
        final String username = req.getParameter("username");
        final String password = req.getParameter("password");

        final MessageFormat formatter = new MessageFormat("", req.getLocale());

        final ResourceBundle bundle = ResourceBundle.getBundle(getBaseResourceBundleName(), req.getLocale());

        if (token == null || token.isBlank()) {
            LOGGER.warn("reCAPTCHA token missing.");

            req.setAttribute(MESSAGE_ATTRIBUTE, it.unipd.dei.aecpkg.resource.Message
                    .create(bundle.getString("messages.error.missingRecaptchaToken"),
                            CommonErrorTypes.INVALID_PARAMETER));

            req.getRequestDispatcher(FAILURE_URL).forward(req, res);

            return;
        }

        if (username == null || username.isBlank()) {
            LOGGER.warn("Username missing.");

            req.setAttribute(MESSAGE_ATTRIBUTE, it.unipd.dei.aecpkg.resource.Message
                    .create(bundle.getString("messages.login.missingUsername"), CommonErrorTypes.INVALID_PARAMETER));

            req.getRequestDispatcher(FAILURE_URL).forward(req, res);

            return;
        }

        // provisional user before checking for authentication
        User u = User.create(username);
        ApplicationContext.setResource(u);

        if (password == null || password.isBlank()) {
            LOGGER.warn("Password missing.");

            req.setAttribute(MESSAGE_ATTRIBUTE, it.unipd.dei.aecpkg.resource.Message
                    .create(bundle.getString("messages.login.missingPassword"), CommonErrorTypes.INVALID_PARAMETER));

            req.getRequestDispatcher(FAILURE_URL).forward(req, res);

            return;
        }


        LOGGER.info("Authenticating user %s; reCAPTCHA token %s.", username, token);

        if (!verifyRecaptcha(token, username, UserDatastore.ACTIONS.AUTHENTICATE_USER)) {
            req.setAttribute(MESSAGE_ATTRIBUTE, it.unipd.dei.aecpkg.resource.Message
                    .create(bundle.getString("messages.error.failedRecaptchaValidation"),
                            CommonErrorTypes.INVALID_REQUEST));

            req.getRequestDispatcher(FAILURE_URL).forward(req, res);

            return;
        }


        try {
            u = getUserDatastore().authenticate(User.create(username, password));
        } catch (ResourceNotFoundException e) {
            LOGGER.warn("Wrong username and/or password for user %s.", username);

            formatter.applyPattern(bundle.getString("messages.login.wrongUsernamePassword"));

            req.setAttribute(MESSAGE_ATTRIBUTE, it.unipd.dei.aecpkg.resource.Message
                    .create(formatter.format(new String[]{username}), CommonErrorTypes.NOT_FOUND_RESOURCE));

            req.getRequestDispatcher(FAILURE_URL).forward(req, res);

            return;
        } catch (ComponentException e) {
            final Message msg = new StringFormattedMessage("Unexpected exception while authenticating user %s.",
                                                         username);
            LOGGER.error(msg, e);

            formatter.applyPattern(bundle.getString("messages.login.unexpectedError"));

            req.setAttribute(MESSAGE_ATTRIBUTE,
                             it.unipd.dei.aecpkg.resource.Message.create(formatter.format(new String[]{username}),
                                     CommonErrorTypes.INTERNAL_ERROR));

            req.getRequestDispatcher(FAILURE_URL).forward(req, res);

            return;
        }

        // invalidate any previous session
        HttpSession session = req.getSession(false);
        if (session != null) {
            session.invalidate();
            session = null;
        }

        // create a  new session
        session = req.getSession(true);

        session.setAttribute(USER_ATTRIBUTE, u);

        formatter.setLocale(u.getLocale());
        formatter.applyPattern(
                ResourceBundle.getBundle(getBaseResourceBundleName(), u.getLocale()).getString("messages.login.success"));

        req.setAttribute(MESSAGE_ATTRIBUTE,
                it.unipd.dei.aecpkg.resource.Message.create(formatter.format(new String[]{u.getIdentifier()})));


        LOGGER.info("User %s successfully authenticated.", username);

        switch (u.getRole()) {
            case ROOT -> req.getRequestDispatcher(ROOT_SUCCESS_URL).forward(req, res);
            case AEC -> req.getRequestDispatcher(AEC_SUCCESS_URL).forward(req, res);
            case ACM -> req.getRequestDispatcher(ACM_SUCCESS_URL).forward(req, res);
        }

    }
}
