/*
 * Copyright (c) 2021-2022 University of Padua, Italy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.unipd.dei.aecpkg.web;


import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import it.unipd.dei.aecpkg.component.ApplicationContext;
import it.unipd.dei.aecpkg.component.ComponentException;
import it.unipd.dei.aecpkg.component.ComponentManager;
import it.unipd.dei.aecpkg.mail.MailManager;
import it.unipd.dei.aecpkg.resource.BinaryObject;
import it.unipd.dei.aecpkg.resource.CommonErrorTypes;
import it.unipd.dei.aecpkg.resource.MediaType;
import it.unipd.dei.aecpkg.resource.User;
import jakarta.servlet.ServletConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.message.Message;
import org.apache.logging.log4j.message.StringFormattedMessage;
import org.apache.logging.log4j.message.StringFormatterMessageFactory;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

/**
 * The common superclass of all the servlets.
 *
 * @author Nicola Ferro (ferro@dei.unipd.it)
 * @version 1.00
 * @since 1.00
 */
public abstract class AbstractServlet extends HttpServlet {

	/**
	 * The format string for the body of the HTTP POST request
	 */
	private static final String FORMAT = "secret=%s&response=%s";

	/**
	 * A LOGGER available for all the subclasses.
	 */
	protected static final Logger LOGGER = LogManager.getLogger(AbstractServlet.class,
			StringFormatterMessageFactory.INSTANCE);

	/**
	 * The name of parameter containing the reCAPTCHA token sent by the client
	 */
	protected static final String RECAPTCHA_TOKEN = "g-recaptcha-response";

	/**
	 * The address of the default catch-all error page
	 */
	protected static final String ERROR_PAGE_URL = "/error";

	/**
	 * The name of the user attribute in requests
	 */
	protected static final String USER_ATTRIBUTE = "user";

	/**
	 * The name of the message attribute in requests
	 */
	protected static final String MESSAGE_ATTRIBUTE = "message";

	/**
	 * The HTTP client for reCAPTCHA verification
	 */
	private HttpClient httpClient = null;

	/**
	 * The base HTTP request for reCAPTCHA verification
	 */
	private HttpRequest.Builder requestBuilder = null;

	/**
	 * The MailManager
	 */
	private MailManager mailManager = null;

	/**
	 * The reCAPTCHA V3 secret
	 */
	private String recaptchaSecret = null;

	/**
	 * The name of the base {@code ResourceBundle}
	 */
	private String baseResourceBundleName = null;


	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		recaptchaSecret = config.getServletContext().getInitParameter(ContextParamNames.RECAPTCHA_SECRET);

		if (recaptchaSecret == null || recaptchaSecret.isBlank()) {
			final Message msg = new StringFormattedMessage(
					"Recaptcha secret cannot be null or empty: missing %s parameter. Cannot initialize servlet %s.",
					ContextParamNames.RECAPTCHA_SECRET, this.getClass());
			LOGGER.error(msg);
			throw new ServletException(msg.getFormattedMessage());
		}

		baseResourceBundleName = config.getServletContext()
				.getInitParameter(ContextParamNames.BASE_RESOURCE_BUNDLE_NAME);

		if (baseResourceBundleName == null || baseResourceBundleName.isBlank()) {
			final Message msg = new StringFormattedMessage(
					"Name of base ResourceBundle cannot be null or empty: missing %s parameter. Cannot initialize servlet %s.",
					ContextParamNames.BASE_RESOURCE_BUNDLE_NAME, this.getClass());
			LOGGER.error(msg);
			throw new ServletException(msg.getFormattedMessage());
		}

		mailManager = ComponentManager.getComponent(MailManager.class).orElseThrow(() -> {
			final Message msg = new StringFormattedMessage("No MailManager available, cannot initialize servlet %s.",
					this.getClass());
			LOGGER.error(msg);
			return new ServletException(msg.getFormattedMessage());
		});

		httpClient = HttpClient.newBuilder().build();

		requestBuilder = HttpRequest.newBuilder().uri(URI.create("https://www.google.com/recaptcha/api/siteverify"))
				.header("Content-Type", MediaType.APPLICATION_X_WWW_FORM_URLENCODED.getEncodedName())
				.header("Accept", MediaType.APPLICATION_JSON.getEncodedName());
	}

	@Override
	public final void doGet(final HttpServletRequest req, final HttpServletResponse res) throws ServletException,
			IOException {

		ApplicationContext.setIPAddress(req.getRemoteAddr());

		try {
			subDoGet(req, res);
		} catch (Throwable t) {
			unexpectedError(req, res, t);
		} finally {
			ApplicationContext.removeIPAddress();
			ApplicationContext.removeUser();
			ApplicationContext.removeAction();
			ApplicationContext.removeResource();
		}

	}

	/**
	 * Performs the actual processing of the GET request.
	 *
	 * The default implementation does nothing.
	 *
	 * @param req the HTTP request.
	 * @param res the HTTP response.
	 *
	 * @throws Exception if any error happens while processing the request.
	 */
	protected void subDoGet(final HttpServletRequest req, final HttpServletResponse res) throws Exception {
		unsupportedHTTPRequest(req, res);
	}


	@Override
	public final void doPost(final HttpServletRequest req, final HttpServletResponse res) throws ServletException,
			IOException {

		ApplicationContext.setIPAddress(req.getRemoteAddr());

		try {
			subDoPost(req, res);
		} catch (Throwable t) {
			unexpectedError(req, res, t);
		} finally {
			ApplicationContext.removeIPAddress();
			ApplicationContext.removeUser();
			ApplicationContext.removeAction();
			ApplicationContext.removeResource();
		}

	}

	/**
	 * Performs the actual processing of the POST request.
	 *
	 * The default implementation does nothing.
	 *
	 * @param req the HTTP request.
	 * @param res the HTTP response.
	 *
	 * @throws Exception if any error happens while processing the request.
	 */
	protected void subDoPost(final HttpServletRequest req, final HttpServletResponse res) throws Exception {
		unsupportedHTTPRequest(req, res);
	}


	@Override
	public final void doPut(final HttpServletRequest req, final HttpServletResponse res) throws ServletException,
			IOException {
		unsupportedHTTPRequest(req, res);
	}

	@Override
	public final void doOptions(final HttpServletRequest req, final HttpServletResponse res) throws ServletException,
			IOException {
		unsupportedHTTPRequest(req, res);
	}

	@Override
	public final void doHead(final HttpServletRequest req, final HttpServletResponse res) throws ServletException,
			IOException {
		unsupportedHTTPRequest(req, res);
	}

	/**
	 * Performs verification of the given reCAPTCHA V3 token.
	 *
	 * @param token    the reCAPTCHA V3 token to verify.
	 * @param username the username of the user for who the verification is performed.
	 * @param action   the action performed by the user.
	 *
	 * @return {@code true} if the verification is successful, {@code false} otherwise.
	 */
	protected final boolean verifyRecaptcha(final String token, final String username, final String action) {

		// create a new HTTP request for the provided reCAPTCHA V3 token
		final HttpRequest httpRequest = requestBuilder.copy()
				.POST(HttpRequest.BodyPublishers.ofString(String.format(FORMAT, recaptchaSecret, token))).build();


		InputStream response = null;
		JsonParser parser = null;

		// the result of the reCAPTCHA verification
		boolean success = false;

		// the score of the reCAPTCHA verification, if any
		float score = Float.NaN;

		// the action of the reCAPTCHA verification, if any
		String rAction = null;

		// the host of the reCAPTCHA verification, if any
		String host = null;

		// the errors of the reCAPTCHA verification, if any
		ArrayList<String> errors = new ArrayList<>();


		try {
			response = httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofInputStream()).body();

			parser = JSONRepresentationHelper.fromJSON(response);

			if (!JSONRepresentationHelper.alignAtFieldStart(parser, "success")) {
				LOGGER.error(
						"Unable to perform reCAPTCHA verification since the success field is missing; user %s; action %s; token %s.",
						username, action, token);

				return false;
			}

			parser.nextToken();
			success = parser.getBooleanValue();

			while (parser.nextToken() != JsonToken.END_OBJECT) {

				if (parser.getCurrentToken() == JsonToken.FIELD_NAME) {

					switch (parser.getCurrentName()) {

						case "score":
							// move to the value of the field
							parser.nextToken();
							score = parser.getFloatValue();
							break;

						case "action":
							// move to the value of the field
							parser.nextToken();
							rAction = parser.getText();
							break;

						case "hostname":
							// move to the value of the field
							parser.nextToken();
							host = parser.getText();
							break;

						case "error-codes":
							// move to the start array element
							parser.nextToken();

							// move to the first element of the array (or to the end of the array if empty)
							parser.nextToken();

							while (parser.currentToken() != JsonToken.END_ARRAY) {
								errors.add(parser.getText());
								parser.nextToken();
							}

							break;

					}

				}

			}

			if (success) {

				if (action.equalsIgnoreCase(rAction)) {

					LOGGER.info("reCAPTCHA verification succeeded: user %s; score %.4f; action %s; host %s; token %s.",
							username, score, action, host, token);

					return true;

				} else {

					LOGGER.warn(
							"reCAPTCHA verification succeeded but wrong requested action: user %s; score %.4f; action %s instead of %s; host %s; token %s.",
							username, score, rAction, action, host, token);

					return false;
				}

			} else {

				if (action.equalsIgnoreCase(rAction)) {

					LOGGER.warn(
							"reCAPTCHA verification failed: user %s; score %.4f; errors %s; action %s; host %s; token %s.",
							username, score, errors.stream().collect(Collectors.joining(",")), action, host, token);

					return false;

				} else {

					LOGGER.warn(
							"reCAPTCHA verification failed and wrong requested action: user %s; score %.4f; errors %s; action %s instead of %s; host %s; token %s.",
							username, score, errors.stream().collect(Collectors.joining(",")), rAction, action, host,
							token);

					return false;
				}


			}


		} catch (InterruptedException | IOException | ComponentException e) {
			final Message msg = new StringFormattedMessage(
					"Unable to perform reCAPTCHA verification due to an unexpected exception: user %s; action %s; token %s.",
					username, action, token);
			LOGGER.error(msg, e);

			return false;
		} finally {
			try {
				if (response != null) {
					response.close();
				}

				if (parser != null) {
					parser.close();
				}
			} catch (IOException e) {
				final Message msg = new StringFormattedMessage(
						"Unable to close the response during reCAPTCHA verification due to an unexpected exception: user %s; action %s; token %s.",
						username, action, token);
				LOGGER.error(msg, e);
			}
		}
	}

	/**
	 * Ensures that a {@code Session} exists and that there is a {@code User} in it.
	 *
	 * @param req the HTTP servlet request.
	 * @param res the HTTP servlet response.
	 *
	 * @return the {@code User} in the {@code Session}.
	 *
	 * @throws ServletException if something goes wrong while processing.
	 * @throws IOException      if something goes wrong while processing.
	 */
	protected final User ensureUserInSession(final HttpServletRequest req, final HttpServletResponse res) throws
			ServletException, IOException {

		final MessageFormat formatter = new MessageFormat("");

		// get any existing session
		final HttpSession session = req.getSession(false);

		// nothing to do
		if (session == null) {
			LOGGER.error("Expected session to exist.");

			req.setAttribute(MESSAGE_ATTRIBUTE, it.unipd.dei.aecpkg.resource.Message.create(
					ResourceBundle.getBundle(getBaseResourceBundleName(), req.getLocale())
							.getString("messages.error.authenticationRequired"), CommonErrorTypes.INVALID_REQUEST));

			req.getRequestDispatcher(ERROR_PAGE_URL).forward(req, res);

			return null;
		}

		final User u = (User) session.getAttribute(USER_ATTRIBUTE);

		// a somehow unexpected case
		if (u == null) {
			LOGGER.error("Session %s exists but no user found in session.", session.getId());

			req.setAttribute(MESSAGE_ATTRIBUTE, it.unipd.dei.aecpkg.resource.Message.create(
					ResourceBundle.getBundle(getBaseResourceBundleName(), req.getLocale())
							.getString("messages.error.authenticationRequired"), CommonErrorTypes.INVALID_REQUEST));

			req.getRequestDispatcher(ERROR_PAGE_URL).forward(req, res);

			session.invalidate();

			return null;
		}

		ApplicationContext.setUser(u);

		return u;

	}

	/**
	 * Downloads the given binary object.
	 *
	 * @param bo  the binary object to download.
	 * @param res the HTTP servlet response.
	 *
	 * @throws IOException if something goes wrong while processing.
	 */
	protected final void download(final BinaryObject bo, final HttpServletResponse res) throws
			IOException {

		res.setContentType(bo.getMediaType().getEncodedName());
		res.setContentLengthLong(bo.length());
		res.setHeader("Content-disposition", String.format("attachment; filename=%s", bo.getIdentifier()));


		try (OutputStream out = res.getOutputStream(); InputStream is = bo.getContentAsBinaryStream()) {
			is.transferTo(out);
			out.flush();
		}
	}


	/**
	 * Returns the {@code MailManager} to be used.
	 *
	 * @return the {@code MailManager} to be used.
	 */
	protected final MailManager getMailManager() {
		return mailManager;
	}

	/**
	 * Returns the name of the base {@code ResourceBundle}.
	 *
	 * @return the name of the base {@code ResourceBundle}.
	 */
	protected final String getBaseResourceBundleName() {
		return baseResourceBundleName;
	}

	/**
	 * Forwards to the error page sending an unsupported HTTP request message.
	 *
	 * @param req the HTTP request.
	 * @param res the HTTP response.
	 *
	 * @throws ServletException if anything goes wrong during processing.
	 * @throws IOException      if anything goes wrong during processing.
	 */
	private final void unsupportedHTTPRequest(final HttpServletRequest req, final HttpServletResponse res) throws
			ServletException, IOException {

		ApplicationContext.setAction("UNSUPPORTED_HTTP_REQUEST");

		final ResourceBundle bundle = ResourceBundle.getBundle(baseResourceBundleName, req.getLocale());

		final MessageFormat formatter = new MessageFormat(bundle.getString("messages.error.unsupportedHttpRequest"),
				req.getLocale());

		final Message msg = new StringFormattedMessage("Unsupported %s request for servlet %s.", req.getMethod(),
				this.getClass().getName());
		LOGGER.warn(msg);

		req.setAttribute(MESSAGE_ATTRIBUTE,
				it.unipd.dei.aecpkg.resource.Message.create(formatter.format(new String[]{req.getMethod()}),
						CommonErrorTypes.INVALID_REQUEST));

		req.getRequestDispatcher(ERROR_PAGE_URL).forward(req, res);

	}

	/**
	 * Forwards to the error page sending an internal error message.
	 *
	 * @param req the HTTP request.
	 * @param res the HTTP response.
	 * @param t   the occurred error.
	 *
	 * @throws ServletException if anything goes wrong during processing.
	 * @throws IOException      if anything goes wrong during processing.
	 */
	private final void unexpectedError(final HttpServletRequest req, final HttpServletResponse res, final Throwable t) throws
			ServletException, IOException {

		final ResourceBundle bundle = ResourceBundle.getBundle(baseResourceBundleName, req.getLocale());

		final MessageFormat formatter = new MessageFormat(bundle.getString("messages.error.internalError"),
				req.getLocale());

		final Message msg = new StringFormattedMessage(
				"Unexpected exception while processing %s request within servlet %s.", req.getMethod(),
				this.getClass().getName());
		LOGGER.error(msg, t);

		req.setAttribute(MESSAGE_ATTRIBUTE,
				it.unipd.dei.aecpkg.resource.Message.create(formatter.format(new String[]{req.getMethod()}),
						CommonErrorTypes.INTERNAL_ERROR));

		req.getRequestDispatcher(ERROR_PAGE_URL).forward(req, res);
	}

}

