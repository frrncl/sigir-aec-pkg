/*
 * Copyright (c) 2021-2022 University of Padua, Italy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.unipd.dei.aecpkg.web;


import it.unipd.dei.aecpkg.component.ApplicationContext;
import it.unipd.dei.aecpkg.component.ComponentException;
import it.unipd.dei.aecpkg.component.ComponentManager;
import it.unipd.dei.aecpkg.component.ResourceNotFoundException;
import it.unipd.dei.aecpkg.datastore.ArtifactDatastore;
import it.unipd.dei.aecpkg.jwt.JwtManager;
import it.unipd.dei.aecpkg.resource.Artifact;
import it.unipd.dei.aecpkg.resource.BinaryObject;
import it.unipd.dei.aecpkg.resource.CommonErrorTypes;
import it.unipd.dei.aecpkg.resource.User;
import jakarta.servlet.ServletConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.message.Message;
import org.apache.logging.log4j.message.StringFormattedMessage;
import org.jose4j.jwt.JwtClaims;

import java.io.IOException;
import java.text.MessageFormat;
import java.time.OffsetDateTime;
import java.time.format.DateTimeParseException;
import java.util.ResourceBundle;

/**
 * Downloads the package of an {@link Artifact}.
 *
 * See {@link ArtifactDatastore#readPackage(Artifact)}.
 *
 * @author Nicola Ferro (ferro@dei.unipd.it)
 * @version 1.00
 * @since 1.00
 */
public class DownloadArtifactPackageServlet extends AbstractArtifactServlet {

	/**
	 * The URL to forward to in case of success
	 */
	private static final String SUCCESS_URL = "/";

	/**
	 * The URL to forward to in case of failure
	 */
	private static final String FAILURE_URL = "/error";

	/**
	 * The JwtManager
	 */
	private JwtManager jwtManager;

	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		jwtManager = ComponentManager.getComponent(JwtManager.class).orElseThrow(() -> {
			final Message msg = new StringFormattedMessage("No JwtManager available, cannot initialize servlet %s.",
					this.getClass());
			LOGGER.error(msg);
			return new ServletException(msg.getFormattedMessage());
		});
	}


	@Override
	public void subDoPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {

		ApplicationContext.setAction(ArtifactDatastore.ACTIONS.READ_ARTIFACT_PACKAGE);

		final User u = ensureUserInSession(req, res);

		final MessageFormat formatter = new MessageFormat("", u.getLocale());
		final ResourceBundle bundle = ResourceBundle.getBundle(getBaseResourceBundleName(), u.getLocale());

		final String jwt = req.getParameter("jwt");

		if (jwt == null || jwt.isBlank()) {
			LOGGER.warn("JWE missing.");

			req.setAttribute(MESSAGE_ATTRIBUTE, it.unipd.dei.aecpkg.resource.Message.create(
					bundle.getString("messages.downloadArtifactPackage.missingJWT"),
					CommonErrorTypes.INVALID_PARAMETER));

			req.getRequestDispatcher(FAILURE_URL).forward(req, res);

			return;
		}

		final JwtClaims claims;

		try {
			claims = jwtManager.decodeJWE(jwt);
		} catch (ComponentException e) {
			LOGGER.warn("JWE not valid.", e);

			req.setAttribute(MESSAGE_ATTRIBUTE, it.unipd.dei.aecpkg.resource.Message.create(
					bundle.getString("messages.downloadArtifactPackage.invalidJWT"),
					CommonErrorTypes.INVALID_PARAMETER));

			req.getRequestDispatcher(FAILURE_URL).forward(req, res);

			return;
		}


		final String acmID;
		final int openReviewID;
		final OffsetDateTime created;

		String tmp = null;

		try {

			tmp = claims.getClaimValueAsString("acmID");

			if (Integer.parseInt(tmp) <= 0) {
				throw new NumberFormatException("Expected to be greater than 0.");
			}

			acmID = tmp;
		} catch (NumberFormatException e) {
			final Message msg = new StringFormattedMessage("AcmID %s is not a valid integer: %s.", tmp, e.getMessage());
			LOGGER.warn(msg, e);

			req.setAttribute(MESSAGE_ATTRIBUTE, it.unipd.dei.aecpkg.resource.Message.create(
					bundle.getString("messages.downloadArtifactPackage.wrongAcmID"),
					CommonErrorTypes.INVALID_PARAMETER));

			req.getRequestDispatcher(FAILURE_URL).forward(req, res);

			return;
		}

		try {
			tmp = claims.getClaimValueAsString("orID");

			openReviewID = Integer.parseInt(tmp);

			if (openReviewID <= 0) {
				throw new NumberFormatException("Expected to be greater than 0.");
			}
		} catch (NumberFormatException e) {
			final Message msg = new StringFormattedMessage("OpenReviewID %s is not a valid integer: %s.", tmp,
					e.getMessage());
			LOGGER.warn(msg, e);

			req.setAttribute(MESSAGE_ATTRIBUTE, it.unipd.dei.aecpkg.resource.Message.create(
					bundle.getString("messages.downloadArtifactPackage.wrongOpenReviewID"),
					CommonErrorTypes.INVALID_PARAMETER));

			req.getRequestDispatcher(FAILURE_URL).forward(req, res);

			return;
		}

		try {
			tmp = claims.getClaimValueAsString("created");

			created = OffsetDateTime.parse(tmp);
		} catch (DateTimeParseException e) {
			final Message msg = new StringFormattedMessage("Created %s is not a valid date: %s.", tmp, e.getMessage());
			LOGGER.warn(msg, e);

			req.setAttribute(MESSAGE_ATTRIBUTE, it.unipd.dei.aecpkg.resource.Message.create(
					bundle.getString("messages.downloadArtifactPackage.wrongCreated"),
					CommonErrorTypes.INVALID_PARAMETER));

			req.getRequestDispatcher(FAILURE_URL).forward(req, res);

			return;
		}


		final Artifact a = Artifact.create(acmID, created, openReviewID);

		ApplicationContext.setResource(a);

		final BinaryObject bo;

		try {
			bo = getArtifactDatastore().readPackage(a);
		} catch (ResourceNotFoundException e) {
			final Message msg = new StringFormattedMessage("Package of artifact %s (OpenReview %d) does not exist.",
					a.getIdentifier(), a.getOpenReviewID());
			LOGGER.error(msg, e);

			formatter.applyPattern(bundle.getString("messages.downloadArtifactPackage.notExist"));

			req.setAttribute(MESSAGE_ATTRIBUTE, it.unipd.dei.aecpkg.resource.Message.create(
					formatter.format(new String[]{a.getIdentifier(), String.format("%d", a.getOpenReviewID())}),
					CommonErrorTypes.DUPLICATED_RESOURCE));

			req.getRequestDispatcher(FAILURE_URL).forward(req, res);

			return;
		} catch (ComponentException e) {
			final Message msg = new StringFormattedMessage(
					"Unexpected exception while reading package of artifact %s (OpenReview %d).", a.getIdentifier(),
					a.getOpenReviewID());
			LOGGER.error(msg, e);

			formatter.applyPattern(bundle.getString("messages.downloadArtifactPackage.unexpectedError"));

			req.setAttribute(MESSAGE_ATTRIBUTE, it.unipd.dei.aecpkg.resource.Message.create(
					formatter.format(new String[]{a.getIdentifier(), String.format("%d", a.getOpenReviewID())}),
					CommonErrorTypes.INTERNAL_ERROR));

			req.getRequestDispatcher(FAILURE_URL).forward(req, res);

			return;
		}

		LOGGER.info("Package of artifact %s (OpenReview %d) successfully read.", a.getIdentifier(),
				a.getOpenReviewID());

		download(bo, res);

		try {
			getMailManager().sendArtifactDownloadedEmail(u, a, getArtifactDatastore().getArtifactDownloadLink(a, u));
		} catch (ComponentException e) {
			final Message msg = new StringFormattedMessage(
					"Unexpected exception while sending download email artifact %s (OpenReview %d).", a.getIdentifier(),
					a.getOpenReviewID());
			LOGGER.error(msg, e);

			formatter.applyPattern(bundle.getString("messages.downloadArtifactPackage.unexpectedError"));

			req.setAttribute(MESSAGE_ATTRIBUTE, it.unipd.dei.aecpkg.resource.Message.create(
					formatter.format(new String[]{a.getIdentifier(), String.format("%d", a.getOpenReviewID())}),
					CommonErrorTypes.INTERNAL_ERROR));

			req.getRequestDispatcher(FAILURE_URL).forward(req, res);

			return;
		}

		//req.getRequestDispatcher(SUCCESS_URL).forward(req, res);

	}


}
