/*
 * Copyright (c) 2021-2022 University of Padua, Italy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.unipd.dei.aecpkg.jwt;


import it.unipd.dei.aecpkg.component.AbstractComponent;
import it.unipd.dei.aecpkg.component.ComponentException;
import org.apache.logging.log4j.message.Message;
import org.apache.logging.log4j.message.StringFormattedMessage;
import org.jose4j.jwa.AlgorithmConstraints;
import org.jose4j.jwe.ContentEncryptionAlgorithmIdentifiers;
import org.jose4j.jwe.JsonWebEncryption;
import org.jose4j.jwe.KeyManagementAlgorithmIdentifiers;
import org.jose4j.jwt.JwtClaims;
import org.jose4j.jwt.consumer.InvalidJwtException;
import org.jose4j.jwt.consumer.JwtConsumer;
import org.jose4j.jwt.consumer.JwtConsumerBuilder;
import org.jose4j.keys.AesKey;
import org.jose4j.lang.JoseException;

import java.security.Key;
import java.util.Base64;


/**
 * Provides methods for sending e-mails.
 *
 * @author Nicola Ferro
 * @version 1.00
 * @since 1.00
 */

public final class JwtManager extends AbstractComponent {

	/**
	 * Name of the default configuration file.
	 */
	private static final String DEFAULT_CONFIG_FILE = "jwtManager.properties";

	/**
	 * The secret key for symmetric encryption of JWE
	 */
	private final Key secretKey;


	/**
	 * Constructs a new component with the given configuration.
	 *
	 * @param configFileName the name of the file containing the configuration for this component.
	 *
	 * @throws ComponentException if something goes wrong while creating the component.
	 */
	public JwtManager(final String configFileName) throws ComponentException {

		super(configFileName);

		String tmp = config.get(JwtManager.class.getName() + ".secretKey");
		if (tmp == null || tmp.isBlank()) {
			final Message msg = new StringFormattedMessage("Property %s missing or empty.",
					JwtManager.class.getName() + ".secretKey");
			LOGGER.error(msg);
			throw new ComponentException(msg.getFormattedMessage());
		}

		try {
			secretKey = new AesKey(Base64.getDecoder().decode(tmp));
		} catch (IllegalArgumentException e) {
			final Message msg = new StringFormattedMessage("Unable to decode secret key: %s.", e.getMessage());
			LOGGER.error(msg, e);
			throw new ComponentException(msg.getFormattedMessage(), e);
		}

	}

	/**
	 * Constructs a component with default configuration.
	 *
	 * @throws ComponentException if something goes wrong while creating the component.
	 */
	public JwtManager() throws ComponentException {
		this(DEFAULT_CONFIG_FILE);
	}


	/**
	 * Encodes the provided {@code claims} into a JWE, using symmetric encryption.
	 *
	 * @param claims the claims to encode.
	 *
	 * @return the compact serialization of the JWT.
	 *
	 * @throws ComponentException if something goes wrong during the encoding.
	 */
	public String encodeJWE(final JwtClaims claims) throws ComponentException {

		JsonWebEncryption jwe = new JsonWebEncryption();
		jwe.setAlgorithmHeaderValue(KeyManagementAlgorithmIdentifiers.A256KW);
		jwe.setEncryptionMethodHeaderParameter(ContentEncryptionAlgorithmIdentifiers.AES_256_CBC_HMAC_SHA_512);
		jwe.setKey(secretKey);

		jwe.setPayload(claims.toJson());

		final String serializedJwe;

		try {
			serializedJwe = jwe.getCompactSerialization();
		} catch (JoseException e) {
			final Message msg = new StringFormattedMessage("Unable encode JWE: %s.", e.getMessage());
			LOGGER.error(msg, e);
			throw new ComponentException(msg.getFormattedMessage(), e);
		}

		return serializedJwe;
	}

	/**
	 * Decodes the provided JWE, using symmetric encryption.
	 *
	 * @param jwt the compact serialization of the JWT.
	 *
	 * @return the contained claims.
	 *
	 * @throws ComponentException if something goes wrong during the decoding.
	 */
	public JwtClaims decodeJWE(final String jwt) throws ComponentException {

		final JwtConsumer jwtConsumer = new JwtConsumerBuilder().setJweAlgorithmConstraints(
						new AlgorithmConstraints(AlgorithmConstraints.ConstraintType.PERMIT,
								KeyManagementAlgorithmIdentifiers.A256KW)).setJweContentEncryptionAlgorithmConstraints(
						new AlgorithmConstraints(AlgorithmConstraints.ConstraintType.PERMIT,
								ContentEncryptionAlgorithmIdentifiers.AES_256_CBC_HMAC_SHA_512)).setDecryptionKey(secretKey)
				.setDisableRequireSignature().setRequireSubject().setEnableRequireEncryption()
				.setEnableRequireIntegrity().build();

		final JwtClaims claims;

		try {
			claims = jwtConsumer.processToClaims(jwt);
		} catch (InvalidJwtException e) {
			final Message msg = new StringFormattedMessage("Unable decode JWE: %s.", e.getMessage());
			LOGGER.error(msg, e);
			throw new ComponentException(msg.getFormattedMessage(), e);
		}

		return claims;
	}


	/*-------------------------------------------------------------------------
	 * Methods defined in the superclass AbstractComponent follow.
	 *-------------------------------------------------------------------------
	 */

	/**
	 * Starts up the {@code MailManager}.
	 */
	@Override
	protected final void doStart() {
		; // nothing to do
	}

	/**
	 * Shuts down the {@code MailManager}.
	 */
	@Override
	protected final void doStop() {
		; // nothing to do
	}

}
